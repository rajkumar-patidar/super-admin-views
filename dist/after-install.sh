#! /bin/sh

PROJECT_HOME='<%= prefix %>'

# Detecting web server user, might need to add  grep -v `whoami`
USER=$(cut -d: -f 1 /etc/passwd | egrep '(www-data|apache|apache2|nginx)' | head -n 1)

# Change file permissions
chown $USER -R $PROJECT_HOME
chmod 775 -R $PROJECT_HOME/storage/
chmod 775 -R $PROJECT_HOME/bootstrap/cache/

# Refresh / Generate cache files.
php $PROJECT_HOME/artisan route:cache

# Clear Application cache files.
php $PROJECT_HOME/artisan cache:clear

if [ "$(ls $PROJECT_HOME/storage/framework/views)" ]; then
	echo "Removing old views cache."
	rm -r $PROJECT_HOME/storage/framework/views/*
fi

if [ "$USER" == "apache" ]; then
	# is centOS having httpd
	if [ -z "$(apachectl -M | grep rewrite_module)" ]; then
		# rewrite module not enabled, enabling it
		echo "rewrite module not enabled, please enable it httpd.conf"
	fi

	if [ -z "$(apachectl -M | grep expires_module)" ]; then
		# expire module not enabled, enabling it
		echo "expire module not enabled, please enable it httpd.conf"
	fi

	if [ -z "$(apachectl -M | grep deflate_module)" ]; then
		# deflate module not enabled, enabling it
		echo "deflate module not enabled, please enable it httpd.conf"
	fi
fi

echo "<%= name %> <%= version %>-<%= iteration %> successfully installed at $PROJECT_HOME."
