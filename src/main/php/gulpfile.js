var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    mix

    /**
     *     Compile Less files
     */
    .less(['hs-main.less'], 'resources/assets/vendor/tmp/hs-main-compiled.css')
    /**
     *     Combine & Minify CSS files
     */
    .styles([
            '../vendor/bootstrap/dist/css/bootstrap.min.css',
            'AdminLTE.css',
            'skins/skin-green.css',
            '../vendor/datatables/media/css/dataTables.bootstrap.css',
            '../vendor/tmp/hs-main-compiled.css'], 'public/css/application.css')

        .styles(['daterangepicker-bs3.css'], 'public/css/datetime.css')

        .styles(['../vendor/select2/dist/css/select2.min.css'], 'public/css/select2.css')

    /**
     *     Copy fonts & images to public folder
     */

    .copy(
        'resources/assets/vendor/bootstrap/fonts',
        'public/build/fonts'
    )
    .copy(
        'resources/assets/fonts',
        'public/build/fonts'
    )
    .copy(
        'resources/assets/img',
        'public/img'
    )
    /**
     *     Combine & Minify js files
     */
    .scripts([
        '../vendor/jquery/dist/jquery.js',
        '../vendor/bootstrap/dist/js/bootstrap.js',
        '../vendor/datatables/media/js/jquery.dataTables.js',
        '../vendor/datatables/media/js/dataTables.bootstrap.js',
        '../vendor/datatables.net-buttons/js/dataTables.buttons.js',
        '../vendor/moment/min/moment.min.js',
        'buttons.server-side.js',
        '../vendor/js-cookie/src/js.cookie.js',
        'app.js',
        'hs-admin.js'
    ], 'public/js/application.js')
    .scripts([
        'lib/daterangepicker.js',
        'datetime.js'
    ], 'public/js/datetime.js')
    .scripts([
        '../vendor/select2/dist/js/select2.min.js'
    ], 'public/js/select2.js');

    /**
     * Do Versioning of assets
     */
    mix.version([
        'css/application.css',
        'css/datetime.css',
        'css/select2.css',
        'js/application.js',
        'js/datetime.js',
        'js/select2.js'
    ]);
});