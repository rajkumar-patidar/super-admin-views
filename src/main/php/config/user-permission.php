<?php
/**
 * User: spatel
 * Date: 19/4/16
 */

return array(


    /**
     * List of Allowed routes for specific user role.
     * It may contain * value, which will allow all route except route name in `not_allowed_role_routes` array
     *  all routes info can be found via `php artisan routes` command
     *
     */
    'allowed_role_routes'   =>  array( 
        //  admin is allowed for all routes
        'ROLE_SUPER_ADMIN' => array('*'), 

        //  allowed routes of ROLE_PROVIDER
        'ROLE_PROVIDER' => array(
            'dashboard',

            'report.coach-weekly-participation',

            'ajax.report.coach-weekly-participation',

	        'ajax.cache-remove',

            'fitbit-steps',

            'ajax.fitbit-steps',
            'ajax.send-messages-to-user'
        ),
        
        //  allowed routes of ROLE_ADMIN
        'ROLE_ADMIN' => array(
            'dashboard',
            
            'patient.import',
            
            'patient.enrol',

	        'ajax.cache-remove',
            ),
        
        //  allowed routes of ROLE_FACILITY_ADMIN
        'ROLE_FACILITY_ADMIN' => array(
            'dashboard',
            
            'patient.import',
            
            'patient.enrol',

	        'ajax.cache-remove',
            ),

    ),

    /**
     * List of NOT Allowed routes for specific user role.
     * It does not support * value
     */
    'not_allowed_role_routes'   =>  array(
        //  not allowed routes for SUPER_ADMIN user
        'ROLE_SUPER_ADMIN' => array(),

        //  not allowed routes of COACH
        //  But by adding them here we will pretend those routes 404 for ROLE_PROVIDER user.
        'ROLE_PROVIDER' => array(),
        
        //  not allowed routes for ROLE_ADMIN user
        'ROLE_ADMIN' => array(),
        
        //  not allowed routes for ROLE_FACILITY_ADMIN user
        'ROLE_FACILITY_ADMIN' => array(),

    )
);