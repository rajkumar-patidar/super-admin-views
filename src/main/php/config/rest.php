<?php
/**
 * User: spatel
 * Date: 4/1/16
 */
$api_base_url = env( 'HS_SERVICE_BASE_URL', 'https://localhost:8080/' );

$curriculum_api_base_url = env( 'HS_CURRICULUM_SERVICE_BASE_URL', 'https://localhost:3000/' );

$curriculum_v2_api_base_url = rtrim( env( 'HS_CURRICULUM_SERVICE_V2_BASE_URL', 'https://ckh1xro9qf.execute-api.us-east-1.amazonaws.com/' ), '/' ) . '/';


$URL_PREFIX = rtrim( $api_base_url, '/' ) . '/services/v2/';

$CURRICULUM_URL_PREFIX = rtrim( $curriculum_api_base_url, '/' ) . '/api/';


return [

	'guzzle_config'       => [
		'base_uri'        => $URL_PREFIX,
		'http_errors'     => false,
		'decode_content'  => true,
		'verify'          => true,
		'cookies'         => false,
		'connect_timeout' => 15,
		'headers'         => [
			'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) HealthSlate-Super-Admin/1.0',
			'Accept'     => 'application/json',
			//'auth' => ['username', 'password']
		]
	],
	/**
	 * =========================================================
	 * =========================================================
	 *
	 * Health Slate Service API URLs, Should not be modified
	 */

	/**
	 * Oauth Login URL
	 */
	'POST_get_auth_token' => 'auth/getAuthTokenAdmin',
	/**
	 * validate user token
	 */
	'GET_validate_token' => 'auth/validateToken',
	/**
	 * get refresh token
	 */
	'GET_get_refresh_token' => 'auth/getRefreshToken',
	/**
	 * get user info & user roles by access token
	 */
	'GET_user_info_by_token' => 'user/getUserRole',

	'POST_bulk_patient_sign_up'       =>  rtrim( $api_base_url, '/' ) . '/services/v3/signUp/bulk/patientSignUp',

	'GET_curriculum_users_report_by_id'       =>  $CURRICULUM_URL_PREFIX.'users/',

	'GET_dpp_curriculum_users_report_by_uuid'       =>  rtrim( $curriculum_api_base_url, '/' ) .'/curriculum/report/',

	'GET_dpp_curriculum_users_sections_by_uuid'       =>  rtrim( $curriculum_api_base_url, '/' ) .'/curriculum/user/',
	/**
	 * Batch request API for getting list of completed sections
	 */
	'POST_curriculum_users_sections_completed_by_uuids'       =>  rtrim( $curriculum_api_base_url, '/' ) .'/curriculum/report/sections/',
	/**
	 * curriculum v2 API URL
	 */
	'POST_curriculum_v2_request_url'       => $curriculum_v2_api_base_url . 'prod/curriculum',



	'solera_grant_type' => 'authorization_code',
	'solera_Initial_Access_Token_Request_URL' => 'https://anthony-soleranetwork.cs52.force.com/developers/services/oauth2/authorize',
	'solera_client_id' => '3MVG9jfQT7vUue.Efi1_7IEGi9TfdXnNzUB.253TlnXX1Q4OqrAIxluVo0aolnv1NF.W4j5vi_6By4EpDFM13',
	'solera_client_secret' => '7169304064514424541',
	'solera_redirect_uri' => 'https://diabetes.healthslate.com/solerapi/',

    'post_send_messages_to_patient'  =>  rtrim( $api_base_url, '/' ) . '/services/v3/message/avatar-messages',

];