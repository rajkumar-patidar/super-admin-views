<?php
/**
 * User: spatel
 * Date: 13/1/16
 */

return [


	/**
	 * =========================================================
	 * =========================================================
	 *
	 * Health Slate Custom Configurations
	 */

	'profile_image_base_url' => rtrim(env('PROFILE_IMAGE_BASE_URL', 'https://healthslate.com/'),'/'),

	'motivation_image_base_url' => rtrim(env('MOTIVATION_IMAGE_BASE_URL', 'https://healthslate.com/'),'/').'/motivationImages/',

	'message_image_base_url' => rtrim(env('MESSAGE_IMAGE_BASE_URL', 'https://healthslate.com/'),'/'),

	'device_support_db_name'    =>  env('HS_DEVICE_SUPPORT_DB_NAME', 'device_support'),

	'group_db_name'    =>  env('GROUP_DB_DATABASE', 'groups'),

	/**
	 * allowed tags in chat message
	 */
	'allowed_html_tags'     =>  '<p><i><br><img><a><b>',
	/**
	 * min. number of chat messages to show by default
	 * int
	 */
	'last_chat_msg_to_show'     =>  100,
	/**
	 * min. number of chat users to show by default
	 * int
	 */
	'last_chat_user_to_show'  => 50,

	'report_datatable_row_per_page'  => 50,
	/**
	 *  Minimum Table rows per page
	 *  For Laravel Paginator
	 */
	'table_row_per_page'     => 50,
	/**
	 * Default Date Time format
	 */
	'default_date_time_format'  =>  'm/d/Y h:i A',
	/**
	 * Default Date format
	 */
	'default_date_format'  =>  'm/d/Y',
	/**
	 * Default Date format for mysql queries
	 */
	'mysql_default_date_format'  =>  '%m/%d/%Y',

	'default_js_date_time_format'   => 'MM/DD/YYYY hh:mm A',

	'disable_report_cache'     => env('DISABLE_REPORT_CACHE', false),
	/**
	 * Healthslate CONSTANTS.
	 *          DO NOT MODIFY UNTIL NEEDED.
	 */
	'hs_constants' => [
		'GENDER_MALE'              => 'Male',
		'GENDER_FEMALE'            => 'Female',
		/**
		 * DIABETES_TYPE_IDs from diabetes_type table.
		 */
		'TYPE_2_DIABETES_TYPE_ID'  => 1,
		'DPP_FAT_DIABETES_TYPE_ID' => 2,
		'DPP_CAL_DIABETES_TYPE_ID' => 3,
	],
];