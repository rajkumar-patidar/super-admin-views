<?php

return [


	/**
	 * =========================================================
	 * =========================================================
	 *
	 * Health Slate Service API URLs, Should not be modified
	 */

	/**
	 * Oauth Login URL
	 */

	'solera_grant_type' => 'authorization_code',
	'solera_Initial_Access_Token_Request_URL' => 'https://anthony-soleranetwork.cs52.force.com/developers/services/oauth2/authorize',
	'solera_client_id' => '3MVG9jfQT7vUue.Efi1_7IEGi9TfdXnNzUB.253TlnXX1Q4OqrAIxluVo0aolnv1NF.W4j5vi_6By4EpDFM13',
	'solera_client_secret' => '7169304064514424541',
	'solera_redirect_uri' => 'https://diabetes.healthslate.com/solerapi/',

	'solera_url' => 'https://anthony-soleranetwork.cs52.force.com/developers/services/oauth2/token',
	'refresh_token' => '5Aep8618cVxjR0JxHs8ngHyLTMR4Clj4w0a5Deq6oNWWFaPHEsnSsRL9Rs5.4rCGnYV.EUzrelYUV2XWFOEMvX1',
	'grant_type_refresh' => 'refresh_token',
	'daily_Participant_Status' => 'https://anthony-soleranetwork.cs52.force.com/developers/services/apexrest/DailyParticipantStatus',
	'enrollmentDataToSolera' => 'https://anthony-soleranetwork.cs52.force.com/developers/services/apexrest/ParticipantEnrollmentStatus',
	'engagementDataToSolera' => 'https://anthony-soleranetwork.cs52.force.com/developers/services/apexrest/ParticipantEngagementActivity',

    //https://api.fitbit.com/1/user/33R2TH/activities/steps/date/today/7d.json
    'fitbit_url'            => 'https://api.fitbit.com/1/user/',
    'fitbit_days'           => '1m',
    'fitbit_days_7'         => '7d',
    'fitbit_client_secret'  => 'MjI5V0g2OmYzMTVmZjQ3MWRhOTc4ODE0YTZkOGY3NWVhZGRiOGNk',
    'fitbit_access_token'   => 'https://api.fitbit.com/oauth2/token?grant_type=refresh_token&refresh_token=',


];