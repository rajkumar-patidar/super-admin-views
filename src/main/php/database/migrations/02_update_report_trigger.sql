
--- Updating report_trigger_query to treat `is_processed` as bit and send only 1 week old results count

UPDATE report_trigger_query
SET query = 'SELECT count(*) as record_count FROM log WHERE log_time/1000 > (unix_timestamp() - (60*60*24*7)) and log_type = "meal" and log_time/1000 < (unix_timestamp() - 60*60*3) and (ORD(is_processed)=0 OR is_processed IS NULL)'
WHERE report_trigger_query_id = 1;