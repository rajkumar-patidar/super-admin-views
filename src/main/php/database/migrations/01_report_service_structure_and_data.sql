-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 08, 2016 at 02:19 PM
-- Server version: 5.5.47-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `report_service`
--
CREATE DATABASE IF NOT EXISTS `report_service` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `report_service`;

-- --------------------------------------------------------

--
-- Table structure for table `report_trigger_query`
--

CREATE TABLE IF NOT EXISTS `report_trigger_query` (
  `report_trigger_query_id` int(11) NOT NULL AUTO_INCREMENT,
  `query_description` varchar(255) NOT NULL,
  `query` text NOT NULL,
  `result_threshold` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`report_trigger_query_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `report_trigger_query`
--

INSERT INTO `report_trigger_query` (`report_trigger_query_id`, `query_description`, `query`, `result_threshold`, `timestamp`) VALUES
(1, 'Unprocessed Meals: Execute only when >15 records and are older than 3h ', 'SELECT count(*) as record_count FROM log WHERE log_type=''meal'' AND timestampdiff(hour,from_unixtime(last_modified/1000),now()) >3 AND is_processed=0', 15, '2016-04-08 06:52:08');

-- --------------------------------------------------------

--
-- Table structure for table `schedule`
--

CREATE TABLE IF NOT EXISTS `schedule` (
  `schedule_id` int(11) NOT NULL AUTO_INCREMENT,
  `report_name` varchar(255) NOT NULL,
  `report_trigger` int(11) DEFAULT NULL,
  `send_to` varchar(255) NOT NULL,
  `occurrence` varchar(255) NOT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `scheduled_by` varchar(255) DEFAULT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`schedule_id`),
  KEY `report_trigger_id` (`report_trigger`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `schedule`
--

INSERT INTO `schedule` (`schedule_id`, `report_name`, `report_trigger`, `send_to`, `occurrence`, `start_date`, `end_date`, `scheduled_by`, `last_updated`) VALUES
(1, 'unprocessed-meals', 1, 'sandeep.patel@sofmen.com,sandeep.patel+1@sofmen.com', '0 * * * *', '2016-04-06 00:00:00', NULL, 'sandeep.patel@sofmen.com', '2016-04-08 08:33:18');

-- --------------------------------------------------------

--
-- Table structure for table `schedule_run_log`
--

CREATE TABLE IF NOT EXISTS `schedule_run_log` (
  `schedule_run_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `schedule_id` int(11) NOT NULL,
  `sent_to` varchar(255) DEFAULT NULL,
  `success_status` tinyint(1) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`schedule_run_log_id`),
  KEY `schedule_id_index` (`schedule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `schedule`
--
ALTER TABLE `schedule`
  ADD CONSTRAINT `fk_report_trigger_query_id` FOREIGN KEY (`report_trigger`) REFERENCES `report_trigger_query` (`report_trigger_query_id`);

--
-- Constraints for table `schedule_run_log`
--
ALTER TABLE `schedule_run_log`
  ADD CONSTRAINT `fk_schedule_id` FOREIGN KEY (`schedule_id`) REFERENCES `schedule` (`schedule_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
