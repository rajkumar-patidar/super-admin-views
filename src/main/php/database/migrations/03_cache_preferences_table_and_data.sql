
CREATE TABLE `cache_preference` (
  `cache_preference_id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `report_identifier` varchar(255) NOT NULL,
  `cache_duration` int NOT NULL COMMENT 'cache expire duration in seconds'
) ENGINE='InnoDB'  DEFAULT CHARSET=utf8;

ALTER TABLE `cache_preference`  ADD INDEX `report_identifier` (`report_identifier`);

-- INSERT INTO `cache_preference` (`report_identifier`, `cache_duration`) VALUES ('ajax.report.engagement', '43200');