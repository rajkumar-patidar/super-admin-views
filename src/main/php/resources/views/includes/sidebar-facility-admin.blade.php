@section('sidebar-facility-admin')
     <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li class="header">HEADER</li>
                <!-- Optionally, you can add icons to the links -->
                <li @if(in_array(Request::route()->getName(), array('dashboard') )) class="active" @endif><a href="{{ route('dashboard') }}"><i class="glyphicon glyphicon-home"></i> <span>Dashboard</span></a></li>
                <li @if(in_array(Request::route()->getName(), ['patient.import'] )) class="active" @endif>
                    <a href="#"><i class="glyphicon glyphicon-user"></i>
                        <span>Patient</span>
                        <i class="glyphicon glyphicon-menu-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('patient.import') }}"> <i class="glyphicon glyphicon-import"></i> Import Patient</a></li>
                    </ul>
                </li>
            </ul><!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>
@stop