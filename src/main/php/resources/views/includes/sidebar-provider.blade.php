@section('sidebar-provider')
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li class="header">HEADER</li>
                <!-- Optionally, you can add icons to the links -->
                <li @if(in_array(Request::route()->getName(), array('dashboard') )) class="active" @endif><a href="{{ route('dashboard') }}"><i class="glyphicon glyphicon-home"></i> <span>Dashboard</span></a></li>
                <li class="treeview @if(request()->is('report/*')) active @endif">
                    <a href="#"><i class="glyphicon glyphicon-dashboard"></i>
                        <span>Reports</span>
                        <i class="glyphicon glyphicon-menu-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('report.coach-weekly-participation') }}">Weekly Participation Report</a></li>
                    </ul>
                </li>
                <li @if(in_array(Request::route()->getName(), array('fitbit-steps') )) class="active" @endif><a href="{{ route('fitbit-steps') }}"><i class="glyphicon glyphicon-info-sign"></i> <span>Fitbit Steps</span></a></li>
            </ul><!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>
@stop