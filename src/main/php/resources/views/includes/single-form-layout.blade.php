@include('includes.header-styles')
@include('includes.footer-scripts')

<!DOCTYPE html>
    <html>
    <head>
        <title>{{{ $page_title or 'Home Page' }}} | Heath Slate Admin </title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="Content-Language" content="en">
        @yield('header-styles')
    </head>
    <body class="hold-transition @yield('body-class')">
        @yield('content')

        @yield('footer-scripts')
    </body>
    </html>
