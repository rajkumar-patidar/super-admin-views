@section('footer-scripts')
    @parent
    <script src="{{ asset(elixir('js/application.js')) }}" ></script>
    @if (isset($js))
        @foreach ($js as $n)
            <script src="{{ asset(elixir('js/'.$n.'.js')) }}" ></script>
        @endforeach
    @endif
@stop