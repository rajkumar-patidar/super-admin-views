@section('header-styles')
    @parent
    <link href="{{ asset(elixir('css/application.css')) }}"  rel="stylesheet" type="text/css">
    @if (isset($css))
        @foreach ($css as $c)
            <link href="{{ asset(elixir('css/'.$c.'.css')) }}"  rel="stylesheet" type="text/css">
        @endforeach
    @endif
@stop
