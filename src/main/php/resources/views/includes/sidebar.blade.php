@section('sidebar')
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- search form (Optional) -->
            <form action="#" method="get" class="sidebar-form hide">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
                </div>
            </form>
            <!-- /.search form -->

            <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li class="header">HEADER</li>
                <!-- Optionally, you can add icons to the links -->
                <li @if(in_array(Request::route()->getName(), array('dashboard') )) class="active" @endif><a href="{{ route('dashboard') }}"><i class="glyphicon glyphicon-home"></i> <span>Dashboard</span></a></li>
                <li @if(in_array(Request::route()->getName(), array('facilities.list') )) class="active" @endif><a href="{{ route('facilities.list') }}"><i class="glyphicon glyphicon-list-alt"></i> <span>Facilities</span></a></li>

                <li @if(in_array(Request::route()->getName(), ['search.patient','patient.import'] )) class="active" @endif>
                    <a href="#"><i class="glyphicon glyphicon-user"></i>
                        <span>Patient</span>
                        <i class="glyphicon glyphicon-menu-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('search.patient') }}"> <i class="glyphicon glyphicon-search"></i> Search Patient</a></li>
                        <li><a href="{{ route('patient.import') }}"> <i class="glyphicon glyphicon-import"></i> Import Patient</a></li>
                    </ul>
                </li>

                <li class="treeview @if(request()->is('report/*')) active @endif">
                    <a href="#"><i class="glyphicon glyphicon-dashboard"></i>
                        <span>Reports</span>
                        <i class="glyphicon glyphicon-menu-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('report.facility-reports') }}">Facility Reports</a></li>
                        <li @if(in_array(Request::route()->getName(), array('report.week-cohort-all-facility') )) class="active" @endif><a href="{{ route('report.week-cohort-all-facility') }}">Week Cohort Report</a></li>
                        <li @if(in_array(Request::route()->getName(), array('report.import.patient') )) class="active" @endif><a href="{{ route('report.import.patient') }}">Patient Bulk Import Report</a></li>
                        <li><a href="{{ route('report.member-gone-inactive') }}">Member Gone Inactive</a></li>
                        <li><a href="{{ route('report.feedbackMessages') }}">Feedback Messages</a></li>
                        <li><a href="{{ route('report.noLeadCoachOneonOne') }}">Patient w/o Lead Coach<br/><small> One on One Session</small></a></li>
                        <li><a href="{{ route('report.unRepliedMessages') }}">Unreplied Messages</a></li>
                        <li><a href="{{ route('report.enrollment') }}">Enrollment Report</a></li>
                        <li><a href="{{ route('report.missing-fitbit-log.activity', ['days' => 1]) }}">Missing Fitbit Activity Logs</a></li>
                        <li><a href="{{ route('report.missing-fitbit-log.weight', ['days' => 1]) }}">Missing Fitbit Weight Logs</a></li>
                        <li><a href="{{ route('report.members-missing-facility-mapping') }}">Member Missing Facility Mapping</a></li>
                        <li @if(in_array(Request::route()->getName(), array('report.fitbit-steps-mismatch') )) class="active" @endif><a href="{{ route('report.fitbit-steps-mismatch' , ['days' => 30]) }}">Fitbit Steps Mismatch</a></li>
                        <li @if(in_array(Request::route()->getName(), array('cache-email') )) class="active" @endif><a href="{{ route('cache-email') }}">Cached Report Email Status</a></li>
                    </ul>
                </li>

                <li @if(in_array(Request::route()->getName(), array('manage-schedule') )) class="active" @endif><a href="{{ route('manage-schedule') }}"><i class="glyphicon glyphicon-time"></i> <span>Schedule Report</span></a></li>

                <li @if(in_array(Request::route()->getName(), ['support.tickets'] )) class="active" @endif>
                    <a href="#"><i class="glyphicon glyphicon-dashboard"></i>
                        <span>Support Service</span>
                        <i class="glyphicon glyphicon-menu-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('support.tickets') }}">Support Tickets</a></li>
                    </ul>
                </li>

                <li @if(in_array(Request::route()->getName(), array('fitbit-steps') )) class="active" @endif><a href="{{ route('fitbit-steps') }}"><i class="glyphicon glyphicon-info-sign"></i> <span>Fitbit Steps</span></a></li>

            </ul><!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>
@stop