@include('includes.header-styles')
@include('includes.header')
@include('includes.footer-scripts')

<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <title>@yield('page-title','Home') | Heath Slate Admin </title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Content-Language" content="en">
    @yield('header-styles')
</head>
<body class="hold-transition skin-green sidebar-mini @yield('body-class')">
<!--[if lt IE 7]>
<p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or
    <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
<![endif]-->
<noscript>Javascript is required to view this page. Your browser does not support JavaScript or you have disabled JavaScript.</noscript>
<fieldset class="hidden parameters">
    <input type="hidden" name="alt_image_url" value="{{ asset('img/default-user-100x100.png') }}">
    <input type="hidden" name="report_datatable_row_per_page" value="{{ config('healthslate.report_datatable_row_per_page', 10) }}">
    <input type="hidden" name="table_row_per_page" value="{{ config( 'healthslate.table_row_per_page', 10 ) }}">
    <input type="hidden" name="default_js_date_time_format" value="{{ config( 'healthslate.default_js_date_time_format', 10 ) }}">
    <input type="hidden" name="internal_server_error" value="{{ trans( 'common.internal_server_error') }}">
</fieldset>
<div class="wrapper">

    @yield('header')

    @if(session('user')->userRole == 'ROLE_SUPER_ADMIN')
        @include('includes.sidebar')
        @yield('sidebar')
    @elseif(session('user')->userRole == 'ROLE_PROVIDER')
        @include('includes.sidebar-provider')
        @yield('sidebar-provider')
    @elseif(session('user')->userRole == 'ROLE_ADMIN')
        @include('includes.sidebar-admin')
        @yield('sidebar-admin')
    @elseif(session('user')->userRole == 'ROLE_FACILITY_ADMIN')
        @include('includes.sidebar-facility-admin')
        @yield('sidebar-facility-admin')
    @else
        @include('includes.sidebar-default')
        @yield('sidebar-default')
    @endif
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                @yield('page-title')
                <br/>
                <small id="page_description">{{ $page_description or null }}</small>

                <div class="pull-right action-btns" id="content-cache-btn">
                    <span class="label label-default small" title="Content is not current">Cached on <span class="time"></span></span>
                    <a href="#" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#conf-clear-cache"><i class="glyphicon glyphicon-trash"></i> Clear</a>
                    <a href="#" class="btn btn-primary btn-sm bg-purple hide"><i class="glyphicon glyphicon-floppy-saved"></i> Enable Cache</a>
                </div>
            </h1>

            @yield('breadcrumb')

        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Your Page Content Here -->
            @yield('content')

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <div class="modal" id="conf-clear-cache">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                {!! Form::open( ['method' => 'POST', 'id' => 'clear_cache', 'onsubmit' => 'return false;','role' => 'form', 'url' => route('ajax.cache-remove') ] ) !!}
                {!! Form::hidden('cache_key', null ) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Confirm Action</h4>
                </div>
                <div class="modal-body">
                    <p>{{ trans('common.confirm_record_action',['action' => 'clear', 'element' => 'cached data']) }}</p>
                    <div class="message-bag"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Yes</button>
                </div>
                {!! Form::close() !!}
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
            Heath Slate Admin
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; {{ date('Y') }}.</strong> All rights reserved.
    </footer>

    @yield('right-sidebar')
</div>
<!-- ./wrapper -->

<div class="loader" id="hs-loader">
    <div class="overlay">
        <i class="glyphicon glyphicon-refresh glyphicon-spin"></i>
    </div>
</div>
<div id="alerts-container"></div>
@yield('footer-scripts')

</body>
</html>