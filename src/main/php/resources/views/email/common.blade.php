<!doctype html>
<html>
<head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>{{ $subject or '' }}</title>
    <style>
        * {
            font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
        }
    </style>
</head>
<body style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;height: 100%;-webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important;">
<p>Hello,</p>

<div>
    <p>{{ $content or '' }}</p>
    @if(isset($table) && is_array($table) && !empty($table))
        <?php $headers = array_keys( (array) $table[0]);?>
        <table class="hs-table" style="text-align: center;line-height: 15px;border-collapse:collapse;">
            <thead>
                <tr>
                @foreach($headers as $th)
                <th style="border: 1px solid #C0C0C0;padding: 4px;">{{ $th }}</th>
                @endforeach
                </tr>
            </thead>
            <tbody>
                 @foreach($table as $row)
                    <tr>
                        @foreach($headers as $th)
                            @if(isset($row->$th))
                                    <td style="border: 1px solid #C0C0C0;padding: 4px;">{{ $row->$th }}</td>
                            @endif
                        @endforeach
                    </tr>
                 @endforeach
            </tbody>
        </table>
    @endif
    <p>
        <small>{{ $footer_message or '' }}</small>
    </p>
</div>
<p>
    Sincerely,<br/>
    The HealthSlate Team
</p>
</body>
</html>
