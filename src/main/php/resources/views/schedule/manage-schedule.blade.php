@extends('includes.admin-layout')

@section('page-title')
    Manage Scheduled Alerts
@stop

@section('body-class')
    manage-schedule
@stop

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><span class="glyphicon glyphicon-home"></span> Dashboard</a></li>
        <li class="active">Manage Scheduled Alerts </li>
    </ol>
@stop

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#list-tasks" data-toggle="tab" onclick="return false;">Schedule List</a></li>
                    <li><a href="#add-task" data-toggle="tab" onclick="return false;">Schedule New Alert</a></li>
                </ul>
                <div class="tab-content">

                    <!-- add Schedule -->
                    <div class="tab-pane" id="add-task">
                        {!! Form::open( ['method' => 'POST', 'class' => 'schedule-form', 'onsubmit' => 'return false;','role' => 'form', 'url' => route('ajax.schedule-alert') ] ) !!}
                        {!! Form::hidden('schedule_id', null, ['id' => 'schedule_id'] ) !!}
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Report Name<small class="v-align-t">*</small></label>
                                    {!! Form::select('report_name', $available_reports, null, ['class' => 'form-control','autocomplete' => 'off' ] ) !!}
                                </div><!-- /.form-group -->
                            </div><!-- /.col -->
                        </div><!-- /.row -->

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Send To<small class="v-align-t">*</small></label>
                                    <div class="col-md-12" style="padding: 0;">
                                        {!! Form::select('send_to[]', $send_to_list, null, ['class' => 'form-control', 'autocomplete' => 'off', 'multiple' => 'multiple', 'data-width' => '100%' ] ) !!}
                                    </div>
                                    <div class="clearfix"></div>
                                </div><!-- /.form-group -->
                            </div><!-- /.col -->
                        </div><!-- /.row -->

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Occurrence<small class="v-align-t">*</small></label>
                                    {!! Form::select('occurrence', $available_occurrence, null, ['class' => 'form-control','autocomplete' => 'off' ] ) !!}
                                </div><!-- /.form-group -->
                            </div><!-- /.col -->
                        </div><!-- /.row -->

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Additional Trigger</label>
                                    {!! Form::select('trigger', $available_report_triggers, null, ['class' => 'form-control','autocomplete' => 'off' ] ) !!}
                                </div><!-- /.form-group -->
                            </div><!-- /.col -->
                        </div><!-- /.row -->

                        <div class="form-message" style="line-height:1;"></div>
                        {!! Form::submit('Schedule', ['class' => 'btn btn-primary'] ) !!}

                        {!! Form::close() !!}
                    </div>

                    <!-- list Schedule -->
                    <div class="tab-pane active" id="list-tasks">
                        <table class="table table-bordered table-hover hs-table">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Report Name</th>
                                <th>Send To</th>
                                <th>Occurrence</th>
                                <th>Additional Trigger</th>
                                <th>Scheduled By</th>
                                <th>Active</th>
                                <th>Duration</th>
                                <th style="min-width: 70px;">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse ($scheduled_alerts as $row)
                                <tr class="{{ $row->is_active ? '' : 'text-muted' }}">
                                    <td>{{ $row->schedule_id }}</td>
                                    <td>{{ isset($available_reports[$row->report_name]) ? $available_reports[$row->report_name] : $row->report_name }}</td>
                                    <td>{{ $row->send_to }}</td>
                                    <td>{{ isset($available_occurrence[$row->occurrence]) ? $available_occurrence[$row->occurrence] : $row->occurrence }}</td>
                                    <td>{{ (!empty($row->reportTrigger) ? $row->reportTrigger->query_description : '') }}</td>
                                    <td>{{ $row->scheduled_by }}</td>
                                    <td class="text-center">
                                        <span class="label {{ $row->is_active ? 'label-success' : 'label-danger' }}"> {{ $row->is_active ? 'Y' : 'N' }} </span>
                                    </td>
                                    <td>{{ (!empty($row->start_date) ? $row->start_date->format(config('healthslate.default_date_format')) : '' )
                                    .' - '.
                                    ( !empty($row->end_date) ? $row->end_date->format(config('healthslate.default_date_format')) : '')  }}</td>
                                    <td>

                                        <div class="dropdown">
                                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                                                Action <span class="caret"></span>
                                            </a>
                                            <ul class="dropdown-menu  pull-right">
                                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#" class="schedule-edit" data-schedule-id="{{ $row->schedule_id }}" data-report-name="{{ $row->report_name }}" data-send-to="{{ $row->send_to }}" data-occurrence="{{ $row->occurrence }}" data-trigger="{{ (!empty($row->reportTrigger) ? $row->reportTrigger->report_trigger_query_id : '') }}">Edit</a></li>
                                                @if($row->is_active)
                                                <li role="presentation" class="divider"></li>
                                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#" data-toggle="modal" data-target="#schedule-delete" data-schedule-id="{{ $row->schedule_id }}"><b class="text-danger">Deactivate</b></a></li>
                                                @endif
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="8" class="text-center">{{ trans('common.table_no_records') }} </td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="schedule-delete">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                {!! Form::open( ['method' => 'POST', 'class' => 'schedule-delete', 'onsubmit' => 'return false;','role' => 'form', 'url' => route('ajax.deactivate-alert') ] ) !!}
                {!! Form::hidden('schedule_id', null, ['id' => 'schedule_id'] ) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Confirm Action</h4>
                </div>
                <div class="modal-body">
                    <p>{{ trans('common.confirm_record_action',['action' => 'Deactivate', 'element' => 'schedule']) }}</p>
                    <div class="message-bag"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Yes</button>
                </div>
                {!! Form::close() !!}
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@stop
