@extends('includes.single-form-layout')

@section('body-class')
    skin-blue
@stop

@section('content')
    <div class="wrapper">
    <section class="content">
    <div class="error-page">
        <h2 class="headline text-yellow"> {{ $code or '' }}</h2>
        <div class="error-content">
            <h3><i class="glyphicon glyphicon-warning-sign text-yellow"></i>
                @if(isset($code) && $code == 404)
                    Oops! Page not found.
                @else
                    Whoops, looks like something went wrong.
                @endif
            </h3>
            <p>
                We could not find the page you were looking for.
                Meanwhile, you may <a href="{{ route('dashboard') }}">return to dashboard</a> or try using the search form.
            </p>
        </div><!-- /.error-content -->
    </div>
    </section>
    </div>
@stop
