@extends('includes.admin-layout')

@section('page-title')
    Cached Report Email Send Status
    <br/>
    <small>If checked than we will not send cached report email to logged in user.</small>
@stop

@section('body-class')
    manage-schedule
@stop

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><span class="glyphicon glyphicon-home"></span> Dashboard</a></li>
        <li class="active">Cache Email Send Status</li>
    </ol>
@stop

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                </ul>
                <div class="tab-content">

                    <!-- add Schedule -->
                    <div class="tab-pane active" id="add-task">
                        {!! Form::open( ['method' => 'POST', 'class' => 'send-message-form', 'role' => 'form', 'url' => route('cache-email') ] ) !!}
                        <div class="row">

                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Cache Email</label>
                                    {!! Form::checkbox('email_send', 1, $email_send, ['class' => 'checkbox']) !!}
                                </div><!-- /.form-group -->
                            </div><!-- /.col -->

                            <div class="col-md-1">
                                <div class="form-group">
                                    <label><small class="v-align-t"></small><br/></label>
                                    {!! Form::submit('Save', ['class' => 'btn btn-primary report-links form-control'] ) !!}
                                </div><!-- /.form-group -->
                            </div><!-- /.col -->


                        </div><!-- /.row -->


                        <div class="row">
                            <div class="col-md-8">
                                <table id="table_div" class="table table-striped">
                                    <tbody></tbody>
                                </table>
                            </div><!-- /.col -->
                        </div>

                        <div class="form-message" style="line-height:1;"></div>

                        {!! Form::close() !!}
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop
