@extends('includes.admin-layout')

@section('page-title')
    Fitbit Steps
@stop

@section('body-class')
    manage-schedule
@stop

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><span class="glyphicon glyphicon-home"></span> Dashboard</a></li>
        <li class="active">Fitbit Steps</li>
    </ol>
@stop

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                </ul>
                <div class="tab-content">

                    <!-- add Schedule -->
                    <div class="tab-pane active" id="add-task">
                        {!! Form::open( ['method' => 'POST', 'class' => 'send-message-form', 'onsubmit' => 'return false;','role' => 'form', 'url' => route('ajax.send-messages-to-user') ] ) !!}
                        <div class="row">

                            {{--<div class="col-md-4">
                                <div class="form-group">
                                    <label>Select Facility<small class="v-align-t">*</small></label>
                                    {!! Form::select('send_message_facility_id', $facilities, $facility_id, array('class' => 'form-control')) !!}
                                </div><!-- /.form-group -->
                            </div><!-- /.col -->--}}

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Enter Patient ID<small class="v-align-t">*</small></label>
                                    {!! Form::number('patient_id', $patient_id, ['id' => 'patient_id', 'class' => 'form-control', 'placeholder'=>'Enter Patient ID'] ) !!}
                                </div><!-- /.form-group -->
                            </div><!-- /.col -->

                            <div class="col-md-2">
                                <div class="form-group">
                                    <label><small class="v-align-t"></small><br/></label>
                                    {!! Form::submit('Get Steps', ['class' => 'btn btn-primary report-links form-control'] ) !!}
                                </div><!-- /.form-group -->
                            </div><!-- /.col -->


                            {{--<div class="col-md-4">
                                <div class="form-group hide send_message_users">
                                    <label>Select Any Patient<small class="v-align-t">*</small></label>
                                    <div class="col-md-12" style="padding: 0;">
                                        <select id="send_message_to_patient" name="send_message_to_patient" autocomplete="off" class="form-control">
                                            <option value="">Filter By User</option>
                                        </select>
                                    </div>
                                    <div class="clearfix"></div>
                                </div><!-- /.form-group -->
                            </div><!-- /.col -->--}}



                        </div><!-- /.row -->


                        <div class="row">
                            <div class="col-md-8">
                                <table id="table_div" class="table table-striped">
                                    <tbody></tbody>
                                </table>
                            </div><!-- /.col -->
                        </div>


                        {{--<div class="row">
                            <div class="col-md-8">
                                <div class="form-group hide send_message_notes">
                                    <label>Message<small class="v-align-t">*</small></label>
                                    {!! Form::textarea('notes', null, ['class' => 'form-control', 'maxlength' => 70, 'placeholder' => 'Please Enter Message. Max 70 Characters Allowed']) !!}
                                </div><!-- /.form-group -->
                            </div><!-- /.col -->
                        </div><!-- /.row -->--}}

                        <div class="form-message" style="line-height:1;"></div>
                        {{--{!! Form::submit('Get Steps', ['class' => 'btn btn-primary report-links'] ) !!}--}}

                        {!! Form::close() !!}
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop
