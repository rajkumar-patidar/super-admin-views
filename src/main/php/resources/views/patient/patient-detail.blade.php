@extends('includes.admin-layout')

@section('page-title')
    Patient Detail
@stop

@section('body-class')
    sidebar-collapse patient-detail
@stop

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><span class="glyphicon glyphicon-home"></span> Dashboard</a></li>
        <li><a href="{{route('facilities.list')}}"><span class="glyphicon glyphicon-list-alt"></span> Facilities</a></li>
        @if($facility_id)
            <li><a href="{{ route('facility.patients', ['facility_id' => $facility_id])}}"><span class="glyphicon glyphicon-list-alt"></span> Patients</a>
            </li>
        @endif
        <li class="active">Patient Detail</li>
    </ol>
@stop

@section('content')


    <div class="row">

        <div class="col-md-3">

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"></h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="glyphicon glyphicon-minus"></i></button>
                    </div>
                </div>

                <div class="box-body box-profile table-responsive">
                    <div class="text-center">
                        <img class="profile-user-img img-responsive img-circle" src="{{ !empty($patient->image_path) ? config('healthslate.profile_image_base_url').$patient->image_path : '' }}" alt="User profile picture" onerror="this.src=document.querySelector('.parameters [name=alt_image_url]').value;this.onerror='';"/>
                    </div>
                    <h3 class="profile-username text-center" data-patient-id="{{ $patient->patient_id }}" data-uuid="{{ $patient->uuid }}" data-user-id="{{ $patient->user->user_id }}">{{ $patient->user->fullName }}
                        @if(bit_to_int($patient->user->is_registration_completed))
                            <i class="glyphicon glyphicon-ok font-small text-green v-align-t" data-toggle="tooltip" title="Registration completed"></i>
                        @endif
                    </h3>
                    <p class="text-muted">
                        <span class="pull-left"> <b>Patient Id</b>: {{ $patient->patient_id }}</span>
                        <span class="pull-right">@if(!empty($patient->facilities) && !empty($patient->facilities->first())) <b data-toggle="tooltip" title="Facility Name" data-facility-id="{{ $patient->facilities->first()->facility_id }}">{{ $patient->facilities->first()->name }}</b> @endif {{ $patient->diabetes_type_id ? '('.$patient->diabetesType->name.')' : '' }} </span>
                        &nbsp;
                    </p>

                    <table class="table table-hover user-info-table">
                        <tbody>
                        <tr>
                            <td><b>Address</b></td>
                            <td>
                                <a class="pull-right text-light-blue">{{ $patient->user->address_info }}</a>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td><b>Lead Coach</b></td>
                            <td><a class="pull-right text-light-blue">
                                @if(!is_null($patient->leadCoach))
                                    <span data-lead-coach-id="{{ $patient->leadCoach->provider_id }}">{{ $patient->leadCoach->user->full_name }}</span>
                                @else
                                    &nbsp;
                                @endif
                            </a></td>
                        </tr>
                        <tr>
                            <td><b>Primary Coach</b></td>
                            <td><a class="pull-right text-light-blue">
                                @if(!is_null($patient->primaryFoodCoach))
                                    <span data-primary-food-coach-id="{{ $patient->primaryFoodCoach->provider_id }}">{{ $patient->primaryFoodCoach->user->full_name }}</span>
                                @else
                                    &nbsp;
                                @endif
                            </a></td>
                        </tr>
                        <tr>
                            <td><b>Birth Date</b></td>
                            <td><a class="pull-right text-light-blue">{{ $patient->readable_dob }}</a>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Email</b></td>
                            <td><a class="pull-right text-light-blue word-break">{{ $patient->user->email }}</a>
                            </td>
                        </tr>
                        <tr>
                            <td><b>MRN</b></td>
                            <td><a class="pull-right text-light-blue">{{ $patient->mrn }}</a>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td><b>UUID</b></td>
                            <td><a class="pull-right text-light-blue">{{ $patient->uuid }}</a>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td><b>Device</b></td>
                            <td data-toggle="tooltip" title="{{ empty($patient->device)? "" : ($patient->device->device_type).' / ' }}{{ empty($patient->serviceFilterLogs)? "" : 'App Version: '.(@$patient->serviceFilterLogs->first()->version_no) }}">
                                <a class="pull-right text-light-blue">{{ empty($patient->device)? "&nbsp;" : ($patient->device->device_type) }} {{ empty($patient->serviceFilterLogs)? "" : 'App Version: '.(@$patient->serviceFilterLogs->first()->version_no) }}</a>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Timezone</b></td>
                            <td><a class="pull-right text-light-blue">{{ $patient->user->offset_key or '-' }}</a>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="badge-row"><b>Motivation Images</b>
                                <a class="pull-right pointer"><span class="badge bg-blue">{{ $patient->patientMotivationImages->count() }}</span></a>
                            </td>
                        </tr>
                        @if($patient->patientMotivationImages->count())
                        <tr>
                            <td colspan="2" class="text-center">
                                @foreach($patient->patientMotivationImages as $image)
                                    <img src="{{config('healthslate.motivation_image_base_url').($image->image_name)}}" class="col-md-12 col-sm-12 col-xs-12"  onerror="this.src=document.querySelector('.parameters [name=alt_image_url]').value;this.onerror='';"/>
                                    <span class="text-light-blue">{{ $image->description }}</span>
                                @endforeach
                            </td>
                        </tr>
                        @endif
                        <!-- <tr>
                            <td colspan="2" class="text-center">
                                <a class="pointer" href="{{ route('patient.debug-info',['patient_id' => $patient->patient_id ]) }}">Export Debug Info</a>
                            </td>
                        </tr> -->
                        </tbody>
                    </table>

                </div>
                <!-- /.box-body -->
            </div>

            <div class="box box-widget direct-chat direct-chat-primary direct-chat-contacts-open contacts-only">
                <div class="box-header">
                    <h3 class="box-title">
                        Patient Team
                    </h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="glyphicon glyphicon-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="direct-chat-contacts">
                        <ul class="contacts-list">
                            @if(isset($patient_team) && $patient_team->count())
                                @foreach($patient_team as $team_member)
                                <li data-provider-id="{{ $team_member->provider_id }}">
                                    <div>
                                        <img class="contacts-list-img" src="{{ !empty($team_member->image_path) ? config('healthslate.profile_image_base_url').'/providerImages/'.$team_member->image_path : asset('img/default-user-100x100.png') }}" onerror="this.src=document.querySelector('.parameters [name=alt_image_url]').value;this.onerror='';">
                                        <div class="contacts-list-info">
                                            <span class="contacts-list-name">
                                              {{  $team_member->user ? $team_member->user->full_name : '' }} &nbsp;
                                            </span>
                                            <span class="contacts-list-msg">
                                                <span class="html-text">{{ $team_member->type }}</span>
                                                @if(bit_to_int($team_member->is_deleted))
                                                    <small class="pull-right">
                                                        <i class="label label-danger font-small">Deleted</i>
                                                    </small>
                                                @endif
                                            </span>
                                            </div>
                                            <!-- /.contacts-list-info -->
                                        </div>
                                    </li><!-- End Contact Item -->
                                @endforeach
                            @else
                                <li><p class="text-center">Patient Team is empty</p></li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-9">

            <div class="box direct-chat direct-chat-primary direct-chat-contacts-open contacts-only">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Messages
                        <span data-toggle="tooltip" title="{{ $patient->messages->count() }} Message" class="badge bg-light-blue">{{ $patient->messages->count() }}</span>
                    </h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="glyphicon glyphicon-minus"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                    <!-- Contacts are loaded here -->
                    <div class="direct-chat-contacts">
                        <ul class="contacts-list">
                            @forelse($patient->messages->take(config('healthslate.last_chat_user_to_show')) as $message)
                                @include('blocks.chat-user-li', ['message' => $message,'patient' => $patient ])
                            @empty
                                <li><p class="text-center">No Messages Found</p></li>
                            @endforelse
                        </ul><!-- /.contatcts-list -->
                        @if($patient->messages->count() > config('healthslate.last_chat_user_to_show'))<li><a class="btn btn-link pull-right disabled"> {{ ($patient->messages->count() - config('healthslate.last_chat_user_to_show'))  }} More</a></li>@endif
                    </div><!-- /.direct-chat-pane -->

                </div>
                <!-- /.box-body -->
            </div>

            <div class="box box-widget">
                <div class="box-header">
                    <h3 class="box-title">
                        Recent Week Logs
                        <span data-toggle="tooltip" title="{{ $patient->logs->count() }} Log" class="badge bg-light-blue">{{ $patient->logs->count() }}</span>
                    </h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="glyphicon glyphicon-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-hover logs-table">
                        <thead>
                        <tr>
                            <th>Type</th>
                            <th class="text-center">Time</th>
                            <th class="text-center">Post Id</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($patient->logs as $log)
                            <tr>
                                <td>{{ $log->log_type }}</td>
                                <td class="text-center"><span class="time"> {{ $log->readable_log_time }} <i class="glyphicon glyphicon-time"></i></span></td>
                                <td class="text-center">{{ $log->post_id }}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3" class="text-center">No Recent Logs Found</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>


                </div>
                <div class="box-footer">
                    <a href="{{ route('patient.activity.log',['patient_id' => $patient->patient_id ]) }}" class="btn btn-link pull-right"><b>View All Logs ></b></a>
                </div>
            </div>

        </div>
    </div>
@stop