@extends('includes.admin-layout')

@section('page-title')
    Search Patient
@stop

@section('body-class')
    search-patient
@stop

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><span class="glyphicon glyphicon-home"></span> Dashboard</a></li>
        <li class="active">Patient</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header hide">
                    <h3 class="box-title">&nbsp;</h3>

                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="hs-dt-filter well margin-0">

                        <label>Search Patient By patient_id</label>
                        <select id="search-patient-input" class="form-control" style="width: 400px" data-src="{{ route('ajax.search.patient') }}"></select>
                        {!! Form::hidden('patient_id', null,['id' => 'patient_id']) !!}
                        <button class="btn btn-primary btn-sm" disabled> View Patient</button>
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">

                </div>
            </div>

            <div class="clearfix"></div>

        </div><!-- /.col -->
    </div>

@stop