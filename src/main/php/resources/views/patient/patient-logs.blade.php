@extends('includes.admin-layout')

@section('page-title')
    {{ $patient->user->full_name }}'s Activity Logs
@stop

@section('body-class')
    patient-logs
@stop

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><span class="glyphicon glyphicon-home"></span> Dashboard</a></li>
        <li>
            <a href="{{route('patient.detail',['patient_id' => $patient->patient_id, 'facility_id' => ''])}}"><span class="glyphicon glyphicon-user"></span> Patient Detail</a>
        </li>
        <li class="active">Patient Activities</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            {!! Form::open( ['url' => route('ajax.patient.activity.log', ['patient_id' => $patient->patient_id ]), 'method' => 'post', 'id' => 'log-filter-form', 'role' => 'form','onsubmit' => 'return false;'] ) !!}
                {!! Form::hidden('start_date', Carbon\Carbon::now()->subDay(30)->format('m/d/Y'), ['id' => 'startRange']) !!}
                {!! Form::hidden('end_date', Carbon\Carbon::now()->format('m/d/Y'), ['id' => 'endRange']) !!}
            {!! Form::close() !!}
            <div class="form-group pull-right">
                <div class="input-group date-range-picker" data-toggle="tooltip" title="Date range picker">
                    <button class="btn btn-default pull-right" id="daterange-btn">
                        <i class="glyphicon glyphicon-calendar"></i>
                        <span>Date range picker</span>
                        <i class="glyphicon glyphicon-chevron-down"></i>
                    </button>
                </div>
            </div><!-- /.form group -->
            <div class="clearfix"></div>

            <div id="timeline-wrapper"> </div>
        </div><!-- /.col -->
    </div>

@stop