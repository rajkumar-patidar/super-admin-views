@extends('includes.admin-layout')

@section('page-title')
    {{ $patient->user->full_name }}'s Messages
@stop

@section('body-class')
    patient-msgs
@stop

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><span class="glyphicon glyphicon-home"></span> Dashboard</a></li>
        <li><a href="{{route('patient.detail',['patient_id' => $patient->patient_id, 'facility_id' => ''])}}"><span class="glyphicon glyphicon-user"></span> Patient Detail</a></li>
        <li class="active">Patient Messages</li>
    </ol>
@stop

@section('content')

    <div class="row">
        <div class="col-md-6">
            <div class="box direct-chat direct-chat-success1 direct-chat-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Coach: {{ $coach->full_name or 'N/A' }}</h3>

                    <div class="box-tools pull-right">

                        <button class="btn btn-box-tool text-inherit" data-toggle="tooltip" title="Switch Coach" data-widget="chat-pane-toggle">
                            <i class="glyphicon glyphicon-comment"></i> Switch Coach
                        </button>
                    </div>
                </div>

                <div class="box-body">
                    <!-- Conversations are loaded here -->
                    <div class="direct-chat-messages">
                        @forelse($messages->take(config('healthslate.last_chat_msg_to_show')) as $message)
                            @include('blocks.chat-message-div', ['message' => $message,'patient' => $patient ])
                        @empty
                            <p class="text-center">No Messages Found</p>
                        @endforelse
                        @if($messages->count() > config('healthslate.last_chat_msg_to_show'))<a class="btn btn-link pull-right disabled"> {{ ($messages->count() - config('healthslate.last_chat_msg_to_show'))  }} More</a>@endif
                    </div>

                    <!-- Contacts are loaded here -->
                    <div class="direct-chat-contacts">
                        <ul class="contacts-list">
                            @forelse($all_messages as $messages)
                                @include('blocks.chat-user-li', array('message' => ($messages[0]),'patient' => $patient ))
                            @empty
                                <li><p class="text-center">No Messages Found</p></li>
                            @endforelse
                        </ul>
                        <!-- /.contatcts-list -->
                    </div>
                    <!-- /.direct-chat-pane -->

                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <div class="input-group">
                        <input type="text" name="message" placeholder="Type Message ..." class="form-control disabled" disabled>
                      <span class="input-group-btn">
                        <button type="submit" class="btn btn-primary btn-flat disabled">Send</button>
                      </span>
                    </div>
                </div>
                <!-- /.box-footer-->
            </div>
        </div>
    </div>

@stop