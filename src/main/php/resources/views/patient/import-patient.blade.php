@extends('includes.admin-layout')

@section('page-title')
   Import Patient
@stop

@section('body-class')
    import-patient
@stop

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><span class="glyphicon glyphicon-home"></span> Dashboard</a></li>
        <li class="active">Import Patient</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="@if(!$on_list_page) active @endif"><a href="#upload-csv" data-toggle="tab" onclick="return false;">Upload Patient Data</a></li>
                    <li @if($on_list_page) class="active" @endif><a href="#list-failed" data-toggle="tab" onclick="return false;">Failed Patients</a></li>
                </ul>
                <div class="tab-content">

                    <div class="tab-pane @if(!$on_list_page) active @endif" id="upload-csv">
                        <div class="box box-widget no-shadow no-margin">
                            {!! Form::open( [ 'class' => 'upload-form', 'role' => 'form', 'route' => 'patient.enrol', 'files'=> true ] ) !!}
                            <div class="box-body">
                                <div class="col-md-8 no-padding">
                                    <table class="table hs-table borderless">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <label for="csvFile">Choose Data File</label> (<small>.csv,.txt</small>)
                                            </td>
                                            <td class="form-inline">
                                                <label class="btn btn-default btn-file margin-r-5">
                                                    Browse <input id="csvFile" name="data_file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel, text/plain" class="hide" type="file">
                                                </label>
                                                <small id="file_name_label" class="word-break"></small>
                                                <span class="margin-l-10">
                                                    <b>Delimiter</b>:
                                                    {!! Form::select('delimiter', ['|' => '|', ',' => ','], old('delimiter'), [
                                                    'class' => 'form-control',
                                                    'id' => 'delimiter-list'
                                                    ]) !!}
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>Facility Name </label>
                                            </td>
                                            <td>
                                                {!! Form::select('facility', $facilities, old('facility'), [
                                                'class' => 'form-control',
                                                'id' => 'facility-list'
                                                ]) !!}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>Type </label>
                                            </td>
                                            <td>
                                                {!! Form::select('type', $diabetes_type, old('type', $default_diabetes_type), [
                                                'class' => 'form-control',
                                                'id' => 'diabetes-type-list'
                                                ]) !!}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>Lifestyle Coach </label>
                                            </td>
                                            <td>
                                                {!! Form::select('lead_coach', $lead_coach, old('lead_coach'), [
                                                'class' => 'form-control',
                                                'id' => 'lead-coach-list'
                                                ]) !!}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>Food Coach</label>
                                            </td>
                                            <td>
                                                {!! Form::select('food_coach', $food_coach, old('food_coach'),[
                                                'class' => 'form-control',
                                                'id' => 'food-coach-list'
                                                ]) !!}
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="clearfix"></div>
                                @if(Session::has('errorMsg'))
                                    <div class="text-warning">
                                        {!! Session::get('errorMsg') !!}
                                    </div>
                                @endif
                                @if(Session::has('success'))
                                    <div class="alert alert-success alert-dismissable margin-0">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <p> {!! Session::get('success') !!} </p>
                                    </div>
                                @endif
                                @if(Session::has('infoMsg'))
                                    <div class="alert alert-warning alert-dismissable margin-0">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <p> {!! Session::get('infoMsg') !!} </p>
                                    </div>
                                @endif
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button class="btn btn-primary btn-sm" type="submit"> Upload </button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

                    <div class="tab-pane manage-schedule @if($on_list_page) active @endif" id="list-failed">
                        <table class="table table-hover hs-table">
                            <thead>
                            <tr>
                                <th>Patient Name</th>
                                <th>Email</th>
                                <th>MRN</th>
                                <th>Facility</th>
                                <th>Type</th>
                                <th>Failure Cause</th>
                            </tr>
                            </thead>
                            <tbody>
                                @forelse ($pending_patient_sign_up as $row)
                                    <tr>
                                        <td>{{ $row->first_name or '' }} {{ $row->last_name or '' }}</td>
                                        <td>{{ $row->email or '' }}</td>
                                        <td>{{ $row->solera_id or '' }}</td>
                                        <td>{{ !empty($row->facility) ? $row->facility->name : '' }}</td>
                                        <td>{{ !empty($row->diabetesType) ? $row->diabetesType->name : '' }}</td>
                                        <td>{{ $row->failure_cause or '' }}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="6" class="text-center">{{ trans('common.table_no_records') }} </td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                        {!! $pending_patient_sign_up->render() !!}
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

        </div><!-- /.col -->
    </div>

@stop