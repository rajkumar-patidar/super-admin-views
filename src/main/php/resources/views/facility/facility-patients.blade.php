@extends('includes.admin-layout')

@section('page-title')
    {{ $facility->name }} - Patients
@stop

@section('body-class')
    falicity-patients
@stop

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><span class="glyphicon glyphicon-home"></span> Dashboard</a></li>
        <li><a href="{{route('facilities.list')}}"><span class="glyphicon glyphicon-list-alt"></span> Facilities</a></li>
        <li class="active">Patients</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header hide">
                    <h3 class="box-title">&nbsp;</h3>

                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="hs-dt-filter well margin-0">
                        <h4>Filters</h4>
                        {!! Form::open( array('method' => 'get', 'class' => 'filter-form', 'role' => 'form', 'url' => route('facility.patients',['facility_id' => $facility->facility_id]) ) ) !!}
                        <div class="form-group">
                            Status Filter
                            {!! Form::select('status', $status_filter, request('status',0), [
                            'class' => 'form-control trigger-dt-change',
                            'data-toggle' => 'tooltip', 'title' => 'Filter by Patient Status'
                            ]) !!}
                        </div>

                        <div class="form-group">
                            Invitation Sent Days
                            {!! Form::select('days', $days_list, request('days',null), [
                            'class' => 'form-control trigger-dt-change',
                            'data-toggle' => 'tooltip', 'title' => 'Filter by days since invitation sent'
                            ]) !!}
                        </div>
                        {!! Form::close() !!}
                        <div class="clearfix"></div>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-bordered table-hover hs-table" data-src="{{ route('ajax.facility.patients',['facility_id' => $facility->facility_id]) }}">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>App Version</th>
                                <th>MRN</th>
                                <th>Lead Coach Name</th>
                                <th class="text-center">Detail</th>
                                <th class="text-center">Deleted</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="7" class="text-center"> {{ trans('common.table_loading_records') }} </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <blockquote>
                        <p class="text-muted text-sm">{!! trans('common.note_deleted_patient_excluded') !!}</p>
                    </blockquote>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>

@stop
