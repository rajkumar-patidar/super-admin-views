<?php
/**
 * Created by PhpStorm.
 * User: spatel
 * Date: 13/2/17
 * Time: 5:21 PM
 */
?>
@extends('includes.admin-layout')

@section('page-title')
	Manage Setting - {{ $facility->name or '' }}
@stop

@section('body-class')
	facility-setting
@stop

@section('breadcrumb')
	<ol class="breadcrumb">
		<li><a href="{{route('dashboard')}}"><span class="glyphicon glyphicon-home"></span> Dashboard</a></li>
		<li><a href="{{route('facilities.list')}}"><span class="glyphicon glyphicon-list-alt"></span> Facilities</a></li>
		<li class="active">Manage Setting </li>
	</ol>
@stop

@section('content')
<div class="row">
	{!! Form::open( ['url' => request()->getUri(), 'method' => 'post', 'class' => 'form-manage-facility', 'role' => 'form', 'onsubmit' => 'return false;'] ) !!}
	<div class="col-md-6">
		<!-- general form elements -->
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Manage On Board App</h3>
			</div>
			<!-- /.box-header -->
			<!-- form start -->

			<div class="box-body">
				<div class="col-md-12">
					<div class="form-group">
						<label>Identifier</label>
						<input type="text" class="form-control" name="identifier" value="{{ $co_brand_data->identifier or '' }}" autocomplete="off" placeholder="On Board App URL identifier">
						<p class="help-block">This will be visible in URL.</p>
					</div>
					<div class="form-group">
						<label>Brand Name</label>
						<input type="text" class="form-control" name="brand_name" value="{{ $co_brand_data->brand_name or '' }}" autocomplete="off" placeholder="Brand Name">
					</div>
					<div class="form-group">
						<label>Page Title</label>
						<input type="text" class="form-control" name="page_title" value="{{ $co_brand_data->page_title or '' }}" autocomplete="off" placeholder="Brand page title">

						<p class="help-block">On Board App page title Here.</p>
					</div>
					<div class="blogo-wrapper form-group">
						<label>Logo file</label> (<small>.png</small>)
						<label class="btn btn-default btn-file margin-r-5">
							Browse <input accept=".png" class="hide image-chooser" type="file">
						</label>
						<small class="file_name_label word-break"></small>
						<div class="pull-right">
							<input type="hidden" class="image-data" name="logo" value="{{$co_brand_data->logo or ''}}"/>
							<i aria-hidden="true" class="remove-img glyphicon glyphicon-remove-circle" title="Remove image" @if(!empty($co_brand_data->logo)) style="display: block;"@endif></i>
							<img src="{{$co_brand_data->logo or ''}}" title="Logo Image" />
						</div>
					</div>
					<div class="blogo-wrapper form-group">
						<label>Name Logo file</label> (<small>optional, .png</small>)
						<label class="btn btn-default btn-file margin-r-5">
							Browse <input accept=".png" class="hide image-chooser" type="file">
						</label>
						<small class="file_name_label word-break"></small>
						<div class="pull-right">
							<input type="hidden" class="image-data" name="brand_name_logo" value="{{$co_brand_data->brand_name_logo or ''}}"/>
							<i aria-hidden="true" class="remove-img glyphicon glyphicon-remove-circle" title="Remove image" @if(!empty($co_brand_data->logo)) style="display: block;"@endif></i>
							<img src="{{$co_brand_data->brand_name_logo or ''}}" title="Name Text Image" />
						</div>
					</div>
					<div class="form-group">
						<label>Brand footer copyright message</label>
						<input type="text" class="form-control" name="footer_copyright_message" value="{{ $co_brand_data->footer_copyright_message or '' }}" autocomplete="off" placeholder="Brand footer copyright message">
					</div>
					<div class="form-group">
						<label>Footer links</label>
						<table class="table table-bordered table-hover" id="link_table">
							<thead>
							<tr>
								<th>Link Title</th>
								<th>Link URL</th>
								<th style="width:10px">
									<span class="glyphicon glyphicon-plus addBtn pointer" id="add_btn_link"></span>
								</th>
							</tr>
							</thead>
							<tbody>
							<?php $row_num = 0;?>
							@if(isset($co_brand_data->footer_links_list) && !empty($co_brand_data->footer_links_list))
								@foreach($co_brand_data->footer_links_list as $title => $link)
									<tr data-id="{{++$row_num}}">
										<td><input type="text" class="form-control" name="footer_links[{{$row_num}}][title]" value="{{ $title }}" /></td>
										<td><input type="text" class="form-control" name="footer_links[{{$row_num}}][href]" value="{{ $link }}" /></td>
										<td><span class="glyphicon glyphicon-minus remove-row pointer"></span></td>
									</tr>
								@endforeach
							@endif
							</tbody>
						</table>
					</div>
					<div class="form-group">
						<label>Disabled Fields</label> <small class="disabled-counter"></small>
						{!! Form::select('disabled_fields[]', $form_fields, (array) @$co_brand_data->disabled_fields, ['class' => 'form-control', 'multiple']) !!}
						<p class="help-block">Optional, Choose fields to disable in Form.</p>
					</div>

					<div class="form-group">
						<label>Hide Form Content</label> <small class="disabled-counter"></small>
						{!! Form::select('hide_form_fields[]', $hide_form_fields, (array) @$co_brand_data->hide_form_fields, ['class' => 'form-control', 'multiple']) !!}
						<p class="help-block">Optional, Choose fields to hide in Form.</p>
					</div>

					<div class="form-group">
						<label>Cobrand Charge</label>
						<input type="text" class="form-control" name="cobrand_charge" value="{{ $co_brand_data->cobrand_charge or '' }}" autocomplete="off" placeholder="Cobrand Charge">
						<p class="help-block">On Consumer Web Payment Charge.</p>
					</div>

					<div class="form-group">
						<label>Logo Link</label>
						<input type="text" class="form-control" name="logo_link" value="{{ $co_brand_data->logo_link or '' }}" autocomplete="off" placeholder="Cobrand Logo Link">
					</div>

					<div class="form-group">
						<label>Custom page style</label>
						<textarea class="form-control" rows="3" name="custom_style" placeholder="Brand specific page CSS style">{{ $co_brand_data->custom_style or '' }}</textarea>
					</div>

					<div class="form-group">
						<label class="checkbox-inline"><input type="checkbox" name="landing_page"  @if (@$co_brand_data->landing_page == 1) checked  @endif  value="1">Show Landing Page</label>
					</div>

				</div>
				<div class="clearfix"></div>
				<div class="col-md-12 form-msg"></div>
			</div>
			<!-- /.box-body -->

			<div class="box-footer">
				<button type="submit" class="btn btn-primary">Save</button>
				<a href="{{ route('facilities.list') }}" class="btn btn-default pull-right">Cancel</a>
			</div>
		</div>
		<!-- /.box -->

	</div>
	{!! Form::close() !!}
</div>
@stop