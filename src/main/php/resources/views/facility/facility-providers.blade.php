@extends('includes.admin-layout')

@section('page-title')
    {{ $facility->name }} - Providers
@stop

@section('body-class')
    sidebar-collapse falicity-providers
@stop

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><span class="glyphicon glyphicon-home"></span> Dashboard</a></li>
        <li><a href="{{route('facilities.list')}}"><span class="glyphicon glyphicon-list-alt"></span> Facilities</a></li>
        <li class="active">Providers</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">&nbsp;</h3>

                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table class="table table-bordered table-hover hs-table" data-src="{{ route('ajax.facility.providers',['facility_id' => $facility->facility_id]) }}">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>User Authority</th>
                            <th>Provider Type</th>
                            <th class="text-center">Patients</th>
                            <th class="text-center">Access All</th>
                            <th class="text-center">Deleted</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="7" class="text-center"> {{ trans('common.table_loading_records') }} </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <blockquote>
                        <p class="text-muted text-sm">{!! trans('common.note_deleted_patient_excluded') !!}</p>
                    </blockquote>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>

@stop
