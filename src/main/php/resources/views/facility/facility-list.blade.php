@extends('includes.admin-layout')

@section('page-title')
    Facilities
@stop

@section('body-class')
    falicity-list
@stop

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><span class="glyphicon glyphicon-home"></span> Dashboard</a></li>
        <li class="active">Facilities </li>
    </ol>
@stop

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">&nbsp;</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table class="table table-bordered table-hover hs-table" data-src="{{ route('ajax.facilities.list') }}">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Contact Person</th>
                            <th>Address</th>
                            <th class="text-center">Providers</th>
                            <th class="text-center">Patients</th>
                            <th class="text-center">Notification Enabled</th>
                            <th class="text-center">Manage</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="7" class="text-center">{{ trans('common.table_loading_records') }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->

            </div>
            <!-- /.box -->
        </div>
    </div>

@stop
