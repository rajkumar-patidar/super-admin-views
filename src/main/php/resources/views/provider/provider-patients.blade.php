@extends('includes.admin-layout')

@section('page-title')
    {{ (! empty( $provider->user ) ? $provider->user->full_name.' - Patients' : '') }}
@stop

@section('body-class')
    provider-patients-report
@stop

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><span class="glyphicon glyphicon-home"></span> Dashboard</a></li>
        <li><a href="{{route('report.facility-reports',['facility_id' => $provider->facility->facility_id])}}"><span class="glyphicon glyphicon-list-alt"></span> Facility Reports</a></li>
        <li><a href="{{route('report.coach.patients',['facility_id' => $provider->facility->facility_id ])}}"><span class="glyphicon glyphicon-list-alt"></span> {{ $provider->facility->name }} - Coach List</a></li>
        <li class="active">{{ (! empty( $provider->user ) ? $provider->user->full_name.' - Patients' : '') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header hide">
                    <h3 class="box-title">&nbsp;</h3>

                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="hs-dt-filter well margin-b-20">
                        <h4>Filters</h4>
                        {!! Form::open( array('method' => 'get', 'class' => 'filter-form', 'role' => 'form') ) !!}

                        <div class="form-group">
                            Recent Weeks
                            {!! Form::select('week', ['0' => 'Any', '1' => '1', '2' => '2', '3' => '3','5' => '5'], request('week',0), [
                            'class' => 'form-control trigger-dt-change',
                            'data-toggle' => 'tooltip', 'title' => 'Filter by weeks'
                            ]) !!}
                        </div>
                        {!! Form::close() !!}
                        <div class="clearfix"></div>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-bordered table-hover hs-table" data-src="{{ route('ajax.report.coach.patient-interaction',['provider_id' => $provider->provider_id ]) }}">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>MRN</th>
                                <th class="text-center">
                                    Last Active
                                    <span data-container="body" data-toggle="tooltip" title="Days since last activity of patient" class="text-blue glyphicon glyphicon-info-sign"></span>
                                </th>
                                <th class="text-center">
                                    # of Messages Received
                                    <span data-container="body" data-toggle="tooltip" title="No. of Messages Received from {{ (! empty( $provider->user ) ? $provider->user->full_name : 'provider') }}" class="text-blue glyphicon glyphicon-info-sign"></span>
                                </th>
                                <th class="text-center">
                                    # of Messages Sent
                                    <span data-container="body" data-toggle="tooltip" title="No. of Messages Sent to {{ (! empty( $provider->user ) ? $provider->user->full_name : 'provider') }}" class="text-blue glyphicon glyphicon-info-sign"></span>
                                </th>
                                <th class="text-center">
                                    # of 1:1 calls
                                    <span data-container="body" data-toggle="tooltip" title="One 2 One Sessions" class="text-blue glyphicon glyphicon-info-sign"></span>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="7" class="text-center"> {{ trans('common.table_loading_records') }} </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->


                <div class="box-footer">
                    <blockquote>
                        <p class="text-muted text-sm">{!! trans('common.note_deleted_patient_excluded') !!}</p>
                    </blockquote>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>

@stop
