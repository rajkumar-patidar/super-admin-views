<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Messages
                @if(isset($messages))
                    <small>Count: {{ count($messages) }}</small>
                @endif
            </h4>
        </div>
        <div class="modal-body">
            @if(isset($messages))
                <div class="direct-chat direct-chat-success">
                    <div class="direct-chat-messages">
                        @foreach($messages as $message)
                            <div class="direct-chat-msg @if(!$message->is_sent) right @endif" data-message-id="{{$message->message_id}}">
                                <div class="direct-chat-info clearfix">
                                    <span class="direct-chat-name @if($message->is_sent) pull-left @else pull-right @endif">@if($message->is_sent) {{ $message->patient->user->full_name }}  @else {{ @$message->providerUser->full_name }}@endif</span>
                                    <span class="direct-chat-timestamp  @if($message->is_sent) pull-right @else pull-left @endif"><span class="timestamp" data-timestamp="{{ ($message->timestamp/1000) }}"></span></span>
                                </div>
                                <!-- /.direct-chat-info -->
                                <img class="direct-chat-img" src="@if($message->is_sent){{ !empty($message->patient->image_path) ? config('healthslate.profile_image_base_url').$message->patient->image_path : '' }} @else {{ '' }}@endif" alt="Message User Image" onerror="this.src=document.querySelector('.parameters [name=alt_image_url]').value;this.onerror='';"/><!-- /.direct-chat-img -->
                                <div class="direct-chat-text">
                                    <div class="html-text">{!! strip_tags($message->message, config('healthslate.allowed_html_tags')) !!}</div>
                                    @if(!empty($message->image_path))
                                        <img src="{{ config('healthslate.message_image_base_url').$message->image_path }}" class="sm-image" onerror="this.src=document.querySelector('.parameters [name=alt_image_url]').value;this.onerror='';"/>
                                    @endif
                                    @if($message->readed)
                                        <small class="pull-right" data-toggle="tooltip" title="Read Status" data-container="body">
                                            <i class="glyphicon glyphicon-ok font-small"></i>
                                            <i class="glyphicon glyphicon-ok font-small"></i>
                                        </small>@endif
                                </div>
                                <!-- /.direct-chat-text -->
                            </div>
                            <!--/.direct-chat-messages-->
                        @endforeach
                    </div>
                </div>
            @else
                <p class="text-center">{{ trans('common.table_no_records') }}</p>
            @endif
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary hide">update</button>
        </div>
    </div>
</div>