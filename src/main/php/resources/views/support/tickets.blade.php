@extends('includes.admin-layout')

@section('page-title')
    Support Tickets
@stop

@section('body-class')
    support-tickets
@stop

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><span class="glyphicon glyphicon-home"></span> Dashboard</a></li>
        <li class="active">Tickets</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header hide">
                    <h3 class="box-title">&nbsp;</h3>

                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="hs-dt-filter well margin-b-20 hide">
                        <h4>Filters</h4>
                        {!! Form::open( array('method' => 'get', 'class' => 'filter-form', 'role' => 'form') ) !!}

                        <div class="form-group">
                            Status
                            {!! Form::select('week', ['open' => 'Open', 'close' => 'Closed', 'any' => 'Any'], request('status','open'), [
                            'class' => 'form-control trigger-dt-change',
                            'data-toggle' => 'tooltip', 'title' => 'Filter by Status'
                            ]) !!}
                        </div>
                        {!! Form::close() !!}
                        <div class="clearfix"></div>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-bordered table-hover hs-table" data-src="{{ route('ajax.support.tickets') }}">
                            <thead>
                            <tr>
                                <th>Ticket Id</th>
                                <th>Description</th>
                                <th>Patient</th>
                                <th>Opened on</th>
                                <th>Status</th>
                                <th style="min-width: 70px;">Messages</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td colspan="7" class="text-center"> {{ trans('common.table_loading_records') }} </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->


                <div class="box-footer">

                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="messages-modal" tabindex="-1" role="dialog"></div>

@stop
