@extends('includes.admin-layout')

@section('page-title')
    {{ $report_meta->title or '' }}
@stop

@section('body-class')
{{ $report_meta->body_class or '' }}
@stop

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><span class="glyphicon glyphicon-home"></span> Dashboard</a></li>
        @if(isset($facility_id))<li><a href="{{route('report.facility-reports',['facility_id' => $facility_id])}}"><span class="glyphicon glyphicon-list-alt"></span> Facility Reports</a></li>@endif
        <li class="active">{{ $report_meta->title or '' }}</li>
    </ol>
@stop

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">&nbsp;</h3>
                        <div class="box-tools-left">
                            <a class="btn btn-default build-csv @if(config('healthslate.disable_report_cache' )) caching-disabled @endif" data-href="{{ rtrim($report_src ,'?').'?action=csv' }}">
                                <span><i class="glyphicon glyphicon-save-file"></i> CSV</span>
                            </a>
                        </div>
                    <div class="box-tools duration-filter">
                        @if(isset($show_duration_filter) && $show_duration_filter)
                            {!! Form::open( ['url' => $report_src,'method' => 'GET', 'class' => 'filter-form','role' => 'form' ,'onsubmit' => 'return false;'] ) !!}
                            {!! Form::hidden('start_date', Carbon\Carbon::now()->subDay((isset($duration_info) ? $duration_info->subtract_days : 6))->format('m/d/Y'), ['id' => 'startRange']) !!}
                            {!! Form::hidden('end_date', Carbon\Carbon::now()->format('m/d/Y'), ['id' => 'endRange']) !!}
                            {!! Form::hidden('duration_label', (isset($duration_info) ? $duration_info->label : 'Last Week'), ['id' => 'durationLabel']) !!}
                            {!! Form::hidden('page', 1) !!}
                            {!! Form::hidden('days', @$_GET['days']) !!}
                            {!! Form::hidden('patient_id', @$_GET['patient_id']) !!}
                            {!! Form::hidden('report_title', @$report_meta->title) !!}

                            @if(isset($show_days_filter))
                                <div class="form-group pull-right">
                                    <label>Days </label>
                                    {!! Form::select('days', $days_list, $days, [
                                    'class' => 'form-control text-center',
                                    'data-url' => route(request()->route()->getName(),['facility_id' => (isset($facility_id) ? $facility_id : null)]),
                                    'data-toggle' => 'tooltip', 'title' => 'Filter by days'
                                    ]) !!}
                                </div>
                            @endif
                            @if(isset($timezone_list))
                                <div class="margin-r-5 inline">
                                    <select name="user_timezone" autocomplete="off" class="form-control">
                                        <option value="">Default Timezone is UTC, Click here to change it</option>
                                    </select>
                                </div>
                            @endif

                            @if(isset($facilities))
                                <div class="margin-r-5 inline">
                                    <select name="facility_id" autocomplete="off" class="form-control">
                                        <option value="0">Filter By Facility</option>
                                    </select>
                                </div>
                            @endif

                            @if(isset($coach_list))
                            <div class="margin-r-5 inline">
                                <select name="coach" autocomplete="off" class="form-control">
                                    @if(isset($self_user_filter))
                                        <option value="">{{$self_user_filter}}</option>
                                    @endif
                                    @if(!isset($self_user_filter))
                                            <option value="">Filter By Coach</option>
                                    @endif
                                </select>
                            </div>
                            @endif

                            @if(!isset($hide_duration_filter_dropdown))
                            <div class="form-group pull-right">
                                <div class="input-group date-range-picker" data-toggle="tooltip" title="Date range picker">
                                    <button class="btn btn-default pull-right" id="duration-btn" data-start="#startRange" data-end="#endRange" data-label="#durationLabel" data-subtract-days="{{ (isset($duration_info) ? $duration_info->subtract_days : 6) }}">
                                        <i class="glyphicon glyphicon-calendar"></i>
                                        <span>{{ (isset($duration_info) ? $duration_info->label : 'Last Week') }}</span>
                                        <i class="glyphicon glyphicon-chevron-down"></i>
                                    </button>
                                </div>
                            </div><!-- /.form group -->
                            @endif
                            {!! Form::close() !!}
                        @endif
                    </div>

                    <div class="clearfix"></div>
                </div>
                <!-- /.box-header -->
                <div id="ajax-content">
                    <p class="text-center" style="line-height: 80px;">{{ trans('common.table_loading_records') }}</p>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
    @if(isset($coach_list))
    <script type="application/javascript">
        var COACH_LIST = [];
        try{
            COACH_LIST = JSON.parse('{!! json_encode($coach_list) !!}');
        }catch(e){}
    </script>
    @endif
    @if(isset($facilities))
        <script type="application/javascript">
            var FACILITY_LIST = [];
            try{
                FACILITY_LIST = JSON.parse('{!! json_encode($facilities) !!}');
            }catch(e){}
        </script>
    @endif
    @if(isset($timezone_list))
        <script type="application/javascript">
            var TIMEZONE_LIST = [];
            try{
                TIMEZONE_LIST = JSON.parse('{!! json_encode($timezone_list) !!}');
            }catch(e){}
        </script>
    @endif
@stop
