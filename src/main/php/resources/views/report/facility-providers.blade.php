@extends('includes.admin-layout')

@section('page-title')
    {{ $facility->name }} - Coach List
@stop

@section('body-class')
    falicity-providers-report
@stop

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><span class="glyphicon glyphicon-home"></span> Dashboard</a></li>
        <li><a href="{{route('report.facility-reports',['facility_id' => $facility->facility_id])}}"><span class="glyphicon glyphicon-list-alt"></span> Facility Reports</a></li>
        <li class="active">{{ $facility->name }} - Coach List</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header hide">
                    <h3 class="box-title">&nbsp;</h3>

                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table class="table table-bordered table-hover hs-table" data-src="{{ route('ajax.report.coach.patients',['facility_id' => $facility->facility_id]) }}">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Provider Type</th>
                            <th class="text-center">Patients</th>
                            <th class="text-center">Access All</th>
                            <th class="text-center">Deleted</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="6" class="text-center"> {{ trans('common.table_loading_records') }} </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <blockquote>
                        <p class="text-muted text-sm">{!! trans('common.note_deleted_patient_excluded') !!}</p>
                    </blockquote>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>

@stop
