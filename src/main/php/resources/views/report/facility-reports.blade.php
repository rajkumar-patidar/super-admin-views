@extends('includes.admin-layout')

@section('page-title')
    Facility Reports
@stop

@section('body-class')
    facility-reports
@stop

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><span class="glyphicon glyphicon-home"></span> Dashboard</a></li>
        <li class="active">Facility Reports</li>
    </ol>
@stop

@section('content')

    <div class="row">
        <div class="col-lg-8 col-md-9 col-sm-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title" data-default="Please select a facility"></h3>

                    <div class="box-tools">
                        <div class="form-group">
                            <label>Facility </label>
                            {!! Form::select('facility_id', $facilities, $facility_id, array('class' => 'form-control')) !!}
                        </div>
                    </div>

                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">

                    <table class="table table-bordered table-hover hs-table">
                        <thead>
                        <tr>
                            <th>Report Name</th>
                            <th class="text-center">View</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Active Users</td>
                            <td class="text-center">
                                <a data-href="{{ route('report.facility.users.active',['facility_id' => '']) }}" class="report-links btn btn-primary btn-sm"> View </a>
                            </td>
                        </tr>
                        <tr>
                            <td>User Log Entries </td>
                            <td class="text-center">
                                <a data-href="{{ route('report.facility.users.entries',['facility_id' => '']) }}" class="report-links btn btn-primary btn-sm"> View </a>
                            </td>
                        </tr>
                        <tr>
                            <td>User Registrations </td>
                            <td class="text-center">
                                <a data-href="{{ route('report.user.registration',['facility_id' => '']) }}" class="report-links btn btn-primary btn-sm"> View </a>
                            </td>
                        </tr>
                        <tr>
                            <td>User misfit Activity </td>
                            <td class="text-center">
                                <a data-href="{{ route('report.facility.misfit.steps',['facility_id' => '']) }}" class="report-links btn btn-primary btn-sm"> View </a>
                            </td>
                        </tr>
                        <tr>
                            <td>User Never Active via misfit </td>
                            <td class="text-center">
                                <a data-href="{{ route('report.facility.misfit.never.active',['facility_id' => '']) }}" class="report-links btn btn-primary btn-sm"> View </a>
                            </td>
                        </tr>
                        <tr>
                            <td>Messages Activity </td>
                            <td class="text-center">
                                <a data-href="{{ route('report.facility.messages',['facility_id' => '']) }}" class="report-links btn btn-primary btn-sm"> View </a>
                            </td>
                        </tr>
                        <tr>
                            <td>Video Calls Activity </td>
                            <td class="text-center">
                                <a data-href="{{ route('report.facility.video.calls',['facility_id' => '']) }}" class="report-links btn btn-primary btn-sm"> View </a>
                            </td>
                        </tr>
                        <tr>
                            <td>Patient - Lead Coach </td>
                            <td class="text-center">
                                <a data-href="{{ route('report.facility.users.leadCoach',['facility_id' => '']) }}" class="report-links btn btn-primary btn-sm"> View </a>
                            </td>
                        </tr>
                        <tr>
                            <td>Patient Preferences</td>
                            <td class="text-center">
                                <a data-href="{{ route('report.facility.patient-preferences',['facility_id' => '']) }}" class="report-links btn btn-primary btn-sm"> View </a>
                            </td>
                        </tr>
                        <tr>
                            <td>Patient Curriculum Activity </td>
                            <td class="text-center">
                                <a data-href="{{ route('report.facility.curriculum.activity',['facility_id' => '']) }}" class="report-links btn btn-primary btn-sm"> View </a>
                            </td>
                        </tr>
                        <tr>
                            <td>Coach Interaction Report </td>
                            <td class="text-center">
                                <a data-href="{{ route('report.coach.patients',['facility_id' => '']) }}" class="report-links btn btn-primary btn-sm"> View </a>
                            </td>
                        </tr>
                        <tr>
                            <td>Weekly Participation Report </td>
                            <td class="text-center">
                                <a data-href="{{ route('report.weekly.participation',['facility_id' => '']) }}" class="report-links btn btn-primary btn-sm"> View </a>
                            </td>
                        </tr>
                        <tr>
                            <td>Engagement Report </td>
                            <td class="text-center">
                                <a data-href="{{ route('report.engagement',['facility_id' => '']) }}" class="report-links btn btn-primary btn-sm"> View </a>
                            </td>
                        </tr>
                        <tr>
                            <td>Coach Report </td>
                            <td class="text-center">
                                <a data-href="{{ route('report.coach.engagement',['facility_id' => '']) }}" class="report-links btn btn-primary btn-sm"> View </a>
                            </td>
                        </tr>
                        <tr>
                            <td>Week Cohort Report </td>
                            <td class="text-center">
                                <a data-href="{{ route('report.week-cohort',['facility_id' => '']) }}" class="report-links btn btn-primary btn-sm"> View </a>
                            </td>
                        </tr>
                        <tr>
                            <td>Patient Fitbit Activity Report </td>
                            <td class="text-center">
                                <a data-href="{{ route('report.patient.fitbit.activity',['facility_id' => '']) }}" class="report-links btn btn-primary btn-sm"> View </a>
                            </td>
                        </tr>
                        <tr>
                            <td>Patient Fitbit(Aria) Weight Report </td>
                            <td class="text-center">
                                <a data-href="{{ route('report.patient.fitbit.weight',['facility_id' => '']) }}" class="report-links btn btn-primary btn-sm"> View </a>
                            </td>
                        </tr>
                        <tr>
                            <td>Missing Patient Provider Mappings </td>
                            <td class="text-center">
                                <a data-href="{{ route('report.missing.patient-provider',['facility_id' => '']) }}" class="report-links btn btn-primary btn-sm"> View </a>
                            </td>
                        </tr>
                        <tr>
                            <td>Shipping Addresses for New Scales</td>
                            <td class="text-center">
                                <a data-href="{{ route('report.facility.shipping-addresses',['facility_id' => '']) }}" class="report-links btn btn-primary btn-sm"> View </a>
                            </td>
                        </tr>
                        <tr>
                            <td> Patient's Coach List </td>
                            <td class="text-center">
                                <a data-href="{{ route('report.facility.member-coach-list',['facility_id' => '']) }}" class="report-links btn btn-primary btn-sm"> View </a>
                            </td>
                        </tr>
                        <tr>
                            <td> Weight Loss Report </td>
                            <td class="text-center">
                                <a data-href="{{ route('report.weight.loss',['facility_id' => '']) }}" class="report-links btn btn-primary btn-sm"> View </a>
                            </td>
                        </tr>
                        <tr>
                            <td> Member Group Report </td>
                            <td class="text-center">
                                <a data-href="{{ route('report.member-group',['facility_id' => '']) }}" class="report-links btn btn-primary btn-sm"> View </a>
                            </td>
                        </tr>
                        <tr>
                            <td> Sessions Completed Report </td>
                            <td class="text-center">
                                <a data-href="{{ route('report.sessions.completed',['facility_id' => '']) }}" class="report-links btn btn-primary btn-sm"> View </a>
                            </td>
                        </tr>
                        <tr>
                            <td> Self Register Report </td>
                            <td class="text-center">
                                <a data-href="{{ route('report.self.register',['facility_id' => '']) }}" class="report-links btn btn-primary btn-sm"> View </a>
                            </td>
                        </tr>
                        <tr>
                            <td>Fitbit Invalid Token Report</td>
                            <td class="text-center">
                                <a data-href="{{ route('report.fitbitInvalidToken') }}" class="report-links btn btn-primary btn-sm"> View </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->

            </div>
            <!-- /.box -->
        </div>
    </div>

@stop
