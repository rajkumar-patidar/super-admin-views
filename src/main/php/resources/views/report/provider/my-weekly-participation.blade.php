@extends('includes.admin-layout')

@section('page-title')
    {{ $report_meta->title or '' }}
@stop

@section('body-class')
    {{ $report_meta->body_class or '' }}
@stop

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><span class="glyphicon glyphicon-home"></span> Dashboard</a></li>
        <li class="active">{{ $report_meta->title or '' }}</li>
    </ol>
@stop

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">&nbsp;</h3>
                    <div class="box-tools-left">
                        <a class="btn btn-default build-csv caching-disabled" data-href="{{ rtrim($report_src ,'?').'?action=csv' }}">
                            <span><i class="glyphicon glyphicon-save-file"></i> CSV</span>
                        </a>
                    </div>
                    <div class="box-tools duration-filter">
                        @if(isset($show_duration_filter) && $show_duration_filter)
                            {!! Form::open( ['url' => $report_src,'method' => 'GET', 'class' => 'filter-form','role' => 'form' ,'onsubmit' => 'return false;'] ) !!}
                            {!! Form::hidden('start_date', Carbon\Carbon::now()->subDay(6)->format('m/d/Y'), ['id' => 'startRange']) !!}
                            {!! Form::hidden('end_date', Carbon\Carbon::now()->format('m/d/Y'), ['id' => 'endRange']) !!}
                            {!! Form::hidden('duration_label', 'Last Week', ['id' => 'durationLabel']) !!}
                            {!! Form::hidden('page', 1) !!}
                            {!! Form::hidden('coach', '') !!}

                            <div class="form-group pull-right">
                                <div class="input-group date-range-picker" data-toggle="tooltip" title="Date range picker">
                                    <button class="btn btn-default pull-right" id="duration-btn" data-start="#startRange" data-end="#endRange" data-label="#durationLabel">
                                        <i class="glyphicon glyphicon-calendar"></i>
                                        <span>Last Week</span>
                                        <i class="glyphicon glyphicon-chevron-down"></i>
                                    </button>
                                </div>
                            </div><!-- /.form group -->
                            {!! Form::close() !!}
                        @endif
                    </div>

                    <div class="clearfix"></div>
                </div>
                <!-- /.box-header -->
                <div id="ajax-content">
                    <p class="text-center" style="line-height: 80px;">{{ trans('common.table_loading_records') }}</p>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
@stop
