@extends('includes.admin-layout')

@section('page-title')
    {{ $report_meta->title or '' }}
@stop

@section('body-class')
    common-facility-report {{ $report_meta->body_class or '' }}
@stop

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><span class="glyphicon glyphicon-home"></span> Dashboard</a></li>
        @if(isset($facility_id))<li><a href="{{route('report.facility-reports',['facility_id' => $facility_id])}}"><span class="glyphicon glyphicon-list-alt"></span> Facility Reports</a></li>@endif
        <li class="active">{{ $report_meta->title or '' }}</li>
    </ol>
@stop

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">&nbsp;</h3>
                    @if($paginated_result->count())
                    <div class="box-tools-left">
                        <a class="btn btn-default" href="{{ rtrim(route(request()->route()->getName(),['facility_id' => (isset($facility_id) ? $facility_id : null), 'days' => (!empty(request()->days) ? request()->days : null), 'week' => (!empty(request()->week) ? request()->week : null) ]),'?').'?action=csv'.(isset($current_page) ? '&page='.$current_page : '') }}">
                            <span><i class="glyphicon glyphicon-save-file"></i> CSV</span>
                        </a>
                    </div>
                    @endif
                    <div class="box-tools days-filter">
                        @if($show_days_filter)
                        <div class="form-group pull-right">
                            <label>Days </label>
                            {!! Form::select('days', $days_list, $days, [
                            'class' => 'form-control text-center',
                            'data-url' => route(request()->route()->getName(),['facility_id' => (isset($facility_id) ? $facility_id : null)]),
                            'data-toggle' => 'tooltip', 'title' => 'Filter by days'
                            ]) !!}
                        </div>
                        @elseif(isset($show_week_filter) && $show_week_filter)
                            <div class="form-group pull-right">
                                <label>Week Duration </label>
                                {!! Form::select('days', ['1' => 1, '2' => 2, '3' => 3, '4' => 4, '5' => 5, '10' => 10], request()->week, [
                                'class' => 'form-control text-center',
                                'data-url' => route(request()->route()->getName(),['facility_id' => $facility_id]),
                                'data-toggle' => 'tooltip', 'title' => 'Filter by # of Week to look up for'
                                ]) !!}
                            </div>
                        @endif
                    </div>

                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    @if(!empty($report_meta->result_headers))
                        @if(isset($total_records))
                            <div aria-live="polite" role="status" class="table-info pull-right">Total {{ $total_records }} entries</div>
                        @endif
                    <table class="table table-bordered table-hover hs-table">
                        <thead>
                        <tr>
                            @foreach($report_meta->result_headers as $th => $th_detail)
                                <th class="{{ $th_detail['class'] or ''}}" @if(isset($th_detail['tooltip']))data-container="body" data-toggle="tooltip" data-html="true" title="{!! $th_detail['tooltip'] !!}"@endif>
                                    {{ $th_detail['name'] or '' }}
                                </th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>
                            @forelse ($paginated_result as $row)
                            <tr>
                                @foreach($report_meta->result_headers as $th => $th_detail)
                                    <td class="{{ $th_detail['class'] or ''}}">
                                        @if(isset($th_detail['format_date']))
                                            {{ !empty($row->$th) ? Carbon\Carbon::createFromFormat( 'Y-m-d H:i:s', $row->$th )->format( $th_detail['format_date'] ) : '' }}
                                        @elseif($th == 'patient_id' && isset($row->$th))
                                            <a href="{{ route('patient.detail', ['patient_id' => $row->$th]) }}">{{ $row->$th }}</a>
                                        @else
                                            {{ $row->$th or '' }}
                                        @endif
                                    </td>
                                @endforeach
                            </tr>
                            @empty
                                <tr>
                                    <td colspan="{{ count($report_meta->result_headers) }}" class="text-center">{{ trans('common.table_no_records') }} </td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                    @endif
                </div>
                <!-- /.box-body -->


                <div class="box-footer clearfix">
                    <div class="pagination-sm no-margin pull-right">
                        {!! $paginated_result->render() !!}
                    </div>
                    @if(!isset($hide_footer_note) || !$hide_footer_note)
                    <blockquote class="pull-left">
                        <p class="text-muted text-sm">{!! trans('common.note_deleted_patient_excluded') !!}</p>
                        @if(isset($test_patient_excluded_note) && $test_patient_excluded_note )
                            <p class="text-muted text-sm">{!! trans('common.note_test_patient_excluded') !!}</p>
                        @endif
                    </blockquote>
                    @endif
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>

@stop
