@extends('includes.admin-layout')

@section('page-title')
    Enrollment Report
@stop

@section('body-class')

@stop

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><span class="glyphicon glyphicon-home"></span> Dashboard</a></li>
        <li class="active">Enrollment Report</li>
    </ol>
@stop

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"> &nbsp;</h3>
                    @if($paginated_report->count())
                        <div class="box-tools-left">
                            <a class="btn btn-default" href="{{ rtrim(route(request()->route()->getName(),['days' => (!empty(request()->days) ? request()->days : null) ]),'?').'?action=csv' }}">
                                <span><i class="glyphicon glyphicon-save-file"></i> CSV</span>
                            </a>
                        </div>
                    @endif
                    <div class="box-tools days-filter">
                        <div class="form-group pull-right">
                            <label>Invitation Sent Days  </label>
                            {!! Form::select('days', $days_list, $days, [
                            'class' => 'form-control text-center',
                            'data-url' => route(request()->route()->getName()),
                            'data-toggle' => 'tooltip', 'title' => 'Filter by days since invitation sent'
                            ]) !!}
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">

                    <table class="table table-bordered table-hover hs-table">
                        <thead>
                        <tr>
                            <th>Facility Name</th>
                            <th class="text-center">
                                Total Invitations
                                <span data-container="body" data-toggle="tooltip" title="Total Invitations Sent" class="text-blue glyphicon glyphicon-info-sign"></span>
                            </th>
                            <th class="text-center">
                                Invitation Acceptance Pending
                                <span data-container="body" data-toggle="tooltip" title="Invitations Sent, Not Yet Accepted invitation" class="text-blue glyphicon glyphicon-info-sign"></span>
                            </th>
                            <th class="text-center">
                                Accepted Invitation
                                <span data-container="body" data-toggle="tooltip" title="Accepted invitation, Not Yet Registered" class="text-blue glyphicon glyphicon-info-sign"></span>
                            </th>
                            <th class="text-center">
                                Enrolled
                                <span data-container="body" data-toggle="tooltip" title="Accepted invitation & registered" class="text-blue glyphicon glyphicon-info-sign"></span>
                            </th>
                            <th class="text-center">
                                Registered, Not Activated
                                <span data-container="body" data-toggle="tooltip" title="Registered but Not Activated" class="text-blue glyphicon glyphicon-info-sign"></span>
                            </th>
                            <th class="text-center">
                                Perform activity
                                <span data-container="body" data-toggle="tooltip" title="Performed activity after registration" class="text-blue glyphicon glyphicon-info-sign"></span>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse ($paginated_report as $row)
                            <tr>
                                <td data-facility_id="{{ $row->facility_id }}">{{ $row->facility_name }}</td>
                                <td class="text-center"><a href="{{ route('facility.patients',['facility_id' => $row->facility_id,'status' => 'invited_patient', 'days' => $days]) }}">{{ $row->invited_patient }}</a></td>
                                <td class="text-center"><a href="{{ route('facility.patients',['facility_id' => $row->facility_id,'status' => 'invitation_acceptance_pending', 'days' => $days]) }}">{{ $row->invitation_acceptance_pending }}</a></td>
                                <td class="text-center"><a href="{{ route('facility.patients',['facility_id' => $row->facility_id,'status' => 'accepted_invitation_not_registered', 'days' => $days]) }}">{{ $row->accepted_invitation_not_registered }}</a></td>
                                <td class="text-center"><a href="{{ route('facility.patients',['facility_id' => $row->facility_id,'status' => 'registered', 'days' => $days]) }}">{{ $row->registered }}</a></td>
                                <td class="text-center"><a href="{{ route('facility.patients',['facility_id' => $row->facility_id,'status' => 'registered_no_activity', 'days' => $days]) }}">{{ $row->registered_no_activity }}</a></td>
                                <td class="text-center"><a href="{{ route('facility.patients',['facility_id' => $row->facility_id,'status' => 'performed_activity', 'days' => $days]) }}">{{ $row->performed_activity }}</a></td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6" class="text-center">{{ trans('common.table_no_records') }} </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->

                <div class="box-footer clearfix">
                    <div class="pagination-sm no-margin pull-right">
                        {!! $paginated_report->render() !!}
                    </div>
                    <blockquote class="pull-left">
                        <p class="text-muted text-sm">{!! trans('common.note_deleted_patient_excluded') !!}</p>
                    </blockquote>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>

@stop
