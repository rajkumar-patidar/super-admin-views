@extends('includes.admin-layout')

@section('page-title')
    {{ $report->visibleName }}
@stop

@section('body-class')
    sidebar-collapse common-report-view
@stop

@section('breadcrumb')
 <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><span class="glyphicon glyphicon-home"></span> Dashboard</a></li>
     @if(isset($facility_id))<li><a href="{{route('report.facility-reports',['facility_id' => $facility_id])}}"><span class="glyphicon glyphicon-list-alt"></span> Facility Reports</a></li>@endif
     @if(isset($breadcrumb))
         @foreach($breadcrumb as $link)
            <li><a href="{{ $link->href or '' }}"><span class="glyphicon {{ $link->icon or 'glyphicon-list-alt' }}"></span> {{ $link->text or '' }}</a></li>
         @endforeach
     @endif
        <li class="active">{{ $report->visibleName or '' }}</li>
    </ol>
@stop

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header hide">
                    <h3 class="box-title"></h3>
                </div>
                <!-- /.box-header -->

                @if(isset($report->table_src))
                <div class="box-body table-responsive">
                    <table class="table table-bordered table-hover hs-table {{ $report->table_class or '' }}" data-src="{{ $report->table_src }}">
                        <thead>
                        <input type="hidden" name="report_title" value="{{ $report->visibleName }}" />
                        <tr>
                            @foreach($report->result_headers as $th => $th_text)
                                <th>{{ $th_text }}</th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="{{ count($report->result_headers) }}" class="text-center">{{ trans('common.table_loading_records') }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                @endif


                <div class="box-footer clearfix">
                    @if(isset($deleted_patient_excluded_note) || isset($test_patient_excluded_note) || isset($note_dates_are_in_timezone))
                        <blockquote class="pull-left">
                            @if(isset($deleted_patient_excluded_note))
                                <p class="text-muted text-sm">{!! trans('common.note_deleted_patient_excluded') !!}</p>
                            @endif
                            @if(isset($test_patient_excluded_note))
                                <p class="text-muted text-sm">{!! trans('common.note_test_patient_excluded') !!}</p>
                            @endif
                            @if(isset($note_dates_are_in_timezone))
                                <p class="text-muted text-sm">{!! trans('common.note_dates_are_in_timezone', ['utc' => 'UTC']) !!}</p>
                            @endif
                        </blockquote>
                    @endif
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>

@stop
