
<div class="box-body table-responsive">
    @if(!empty($result_headers))
        @if(isset($total_records))
            <div aria-live="polite" role="status" class="table-info pull-right">Total {{ $total_records }} entries</div>
        @endif
        <table class="table table-bordered table-hover hs-table" @if(isset($init_datatable) && count($paginated_result)) data-init="datatable" @endif>
            <thead>
            <tr>
                @foreach($result_headers as $th => $th_detail)
                    <th class="{{ $th_detail['class'] or ''}}" @if(isset($th_detail['tooltip']))data-container="body" data-toggle="tooltip" data-html="true" title="{!! $th_detail['tooltip'] !!}"@endif>
                        {{ $th_detail['name'] or '' }}
                    </th>
                @endforeach
            </tr>
            </thead>
            <tbody>
<!-- Removed indentation to reduce response json size -->
<!-- DO NOT ReFormat Below Code -->
@forelse ($paginated_result as $row)
<tr>
@foreach($result_headers as $th => $th_detail)
<td @if(isset($th_detail['class']))class="{{ $th_detail['class'] }}"@endif> @if(isset($th_detail['format_date'])) {{ !empty($row->$th) ? Carbon\Carbon::createFromFormat( 'Y-m-d H:i:s', $row->$th )->format( $th_detail['format_date'] ) : '' }}
@elseif($th == 'patient_id' && isset($row->$th) && is_null(request('_patient_hyperlink'))) <a href="{{ route('patient.detail', ['patient_id' => $row->$th]) }}">{{ $row->$th }}</a>
@elseif($th == 'link' && isset($row->$th) ) <a href="{{ $row->$th }}">Detail</a>
@elseif($th == 'link_view' && isset($row->$th) ) <a href="{{ $row->$th }}">Detail</a>
@else {{ $row->$th or '' }} @endif </td>
@endforeach
</tr>
@empty
<tr><td colspan="{{ count( (array) $result_headers) }}" class="text-center">{{ trans('common.table_no_records') }} </td></tr>
@endforelse
            </tbody>
        </table>
    @endif
</div>
<!-- /.box-body -->


<div class="box-footer clearfix">
    @if(@$paginate_counts != '')
        @if(@$paginate_counts instanceof \Illuminate\Pagination\LengthAwarePaginator)
        <div class="pagination-sm no-margin pull-right">
            {!! $paginate_counts->render() !!}
        </div>
        @endif
    @else
        @if($paginated_result instanceof \Illuminate\Pagination\LengthAwarePaginator)
            <div class="pagination-sm no-margin pull-right">
                {!! $paginated_result->render() !!}
            </div>
        @endif
    @endif
    @if(!isset($hide_footer_note) || !$hide_footer_note)
        <blockquote class="pull-left">
            @if(!isset($note_deleted_patient_excluded))
            <p class="text-muted text-sm">{!! trans('common.note_deleted_patient_excluded') !!}</p>
            @endif
            @if(isset($test_patient_excluded_note) && $test_patient_excluded_note )
                <p class="text-muted text-sm">{!! trans('common.note_test_patient_excluded') !!}</p>
            @endif
            @if(isset($disabled_patient_excluded_note) && $disabled_patient_excluded_note )
                <p class="text-muted text-sm">{!! trans('common.note_disabled_patient_excluded') !!}</p>
            @endif
            @if(isset($deleted_coach_excluded_note) && $deleted_coach_excluded_note )
                <p class="text-muted text-sm">{!! trans('common.note_deleted_coaches_excluded') !!}</p>
            @endif
        </blockquote>
    @else
        <div class="margin pad margin-bottom">&nbsp;</div>
    @endif
</div>
