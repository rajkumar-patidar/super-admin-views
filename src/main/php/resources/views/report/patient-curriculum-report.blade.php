@extends('includes.admin-layout')

@section('page-title')
    Patient Curriculum Activity
@stop

@section('body-class')
    curriculum-activity-report
@stop

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><span class="glyphicon glyphicon-home"></span> Dashboard</a></li>
        <li><a href="{{route('report.facility-reports',['facility_id' => $facility_id])}}"><span class="glyphicon glyphicon-list-alt"></span> Facility Reports</a></li>
        <li class="active">Patient Curriculum Activity </li>
    </ol>
@stop

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">&nbsp;</h3>
                    @if($paginated_report->count())
                        <div class="box-tools-left">
                            <a class="btn btn-default" href="{{ route(request()->route()->getName(),['facility_id' => $facility_id]).'?action=csv' }}">
                                <span><i class="glyphicon glyphicon-save-file"></i> CSV</span>
                            </a>
                        </div>
                        @if(isset($total_records))
                            <div aria-live="polite" role="status" class="table-info pull-right">Total {{ $total_records }} entries</div>
                        @endif
                    @endif
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive curriculum-report">
                    <table class="table table-bordered table-hover hs-table">
                        <thead>
                        <tr>
                            <th>Patient Id</th>
                            <th>Name</th>
                            <th>uuid</th>
                            <th>Diabetes Type</th>
                            <th>Email</th>
                            <th>Weeks</th>
                        </tr>
                        </thead>
                        <tbody>
                            @forelse ($paginated_report as $row)
                            <tr>
                                <td><a href="{{ route('patient.detail', ['patient_id' => $row->patient_id]) }}">{{ $row->patient_id }}</a></td>
                                <td>{{ $row->name }}</td>
                                <td>{{ $row->uuid }}</td>
                                <td>{{ $row->diabetes_type_name }}</td>
                                <td>{{ $row->email }}</td>
                                <td>
                                    @if(!empty($row->weeks))
                                        <pre class="json">{{ json_encode($row->weeks,JSON_PRETTY_PRINT) }}</pre>
                                    @endif
                                </td>
                            </tr>
                            @empty
                                <tr>
                                    <td colspan="2" class="text-center">{{ trans('common.table_no_records') }} </td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->


                <div class="box-footer clearfix">
                    <div class="pagination-sm no-margin pull-right">
                        {!! $paginated_report->render() !!}
                    </div>
                    <blockquote class="pull-left">
                        <p class="text-muted text-sm">{!! trans('common.note_deleted_patient_excluded') !!}</p>
                    </blockquote>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>

@stop
