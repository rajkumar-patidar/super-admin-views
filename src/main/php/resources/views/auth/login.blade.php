@extends('includes.single-form-layout')

@section('body-class')
    login-page
@stop

@section('content')

    <div class="login-box">
        <!--
        <div class="login-logo">
            <a href="../../index.html"><b>Admin</b>LTE</a>
        </div>
        -->
        <div class="logo-wrapper">
            <img src="{{ asset('img/logowhite.png') }}">
            <a href="#"><b>Admin</b></a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">Sign in to start your session</p>
            {!! Form::open( array('url' => URL::route('login-handler'), 'method' => 'post', 'class' => 'form-signin', 'role' => 'form') ) !!}
            <div class="form-group has-feedback">
                {!! Form::text('username', old('username'), array('placeholder' => 'Username', 'class' => 'form-control', 'required', 'autofocus')) !!}
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                {!! Form::password('password', array('placeholder' => 'Password', 'class' => 'form-control', 'required')) !!}
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            @if(Session::has('errorMsg'))
                <div class="row">
                    <div class="col-xs-12 text-center text-warning">
                        {!! Session::get('errorMsg') !!}
                    </div>
                </div>
            @endif
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck hide">
                        <label>
                            <input type="checkbox"> Remember Me
                        </label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
                <!-- /.col -->
            </div>

            {!! Form::close() !!}

        </div>
        <!-- /.login-box-body -->
    </div><!-- /.login-box -->
@stop