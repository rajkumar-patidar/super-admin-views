<?php
/**
 * User: spatel
 * Date: 14/1/16
 */
?>
<li>
    <div class="{{ !empty($message->providerUser) ? 'redirect-to pointer' : '' }}" data-href="{{route('patient.user.messages',['patient_id' => $patient->patient_id ,'user_id' => @$message->providerUser->user_id])}}">
        <img class="contacts-list-img" src="{{ asset('img/default-user-100x100.png') }}">

        <div class="contacts-list-info">
            <span class="contacts-list-name">
              {{ !empty($message->providerUser) ? $message->providerUser->full_name : '' }}&nbsp;
                <small class="contacts-list-date pull-right">{{ $message->sent_on}}</small>
            </span>
            <span class="contacts-list-msg">
                <span class="html-text">{!! str_limit(strip_tags($message->message, config('healthslate.allowed_html_tags')), 30) !!}</span>
                @if($message->readed)
                    <small class="pull-right" data-toggle="tooltip" title="Read Status" data-container="body">
                        <i class="glyphicon glyphicon-ok font-small text-green"></i>
                        <i class="glyphicon glyphicon-ok font-small text-green"></i>
                    </small>
                @endif
            </span>
        </div>
        <!-- /.contacts-list-info -->
    </div>
</li><!-- End Contact Item -->
