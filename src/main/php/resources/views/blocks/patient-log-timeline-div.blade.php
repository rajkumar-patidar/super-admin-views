<?php
/**
 * User: spatel
 * Date: 18/1/16
 */
?>
<!-- The time line -->
<ul class="timeline">
    @forelse($log_group as $day => $logs)
        <!-- timeline time label -->
        <li class="time-label">
            <span class="bg-light-blue">
            {{ $day }}
            </span>
        </li>
        <!-- /.timeline-label -->

        <!-- timeline item -->
        <li>
            <i class="glyphicon glyphicon-info-sign bg-green"></i>
            <div class="timeline-item">
                <div class="timeline-body">
                    @foreach($logs->reverse() as $log)
                        <div class="log-block">
                            <a class="btn btn-app {{ strtolower($log->log_type) }} @if(strtolower($log->log_type) == 'meal'){{ $log->is_processed ? "bg-light-green" : "bg-light-gray" }}@elseif(strtolower($log->log_type) == 'weight') bg-light-gray @else bg-light-green @endif">
                                <p>
                                    @if(strtolower($log->log_type) == 'meal')
                                        @if($log->is_processed)
                                        @if($patient->diabetes_type_id == $HS_CONSTANTS->TYPE_2_DIABETES_TYPE_ID)
                                            {{ (!empty($log->foodLogSummary) ? format_weight($log->foodLogSummary->carbs, 1).'g' : "" ) }}
                                            <br/>
                                            Carbs
                                        @elseif($patient->diabetes_type_id == $HS_CONSTANTS->DPP_FAT_DIABETES_TYPE_ID)
                                            {{ (!empty($log->foodLogSummary) ? format_weight($log->foodLogSummary->fats, 1).'g' : "" ) }}
                                            <br/>
                                            Fat
                                        @elseif($patient->diabetes_type_id == $HS_CONSTANTS->DPP_CAL_DIABETES_TYPE_ID)
                                            {{ (!empty($log->foodLogSummary) ? format_weight($log->foodLogSummary->calories, 1).'g' : "" ) }}
                                            <br/>
                                            Calories
                                        @endif
                                        @else
                                            <br/><small>In Progress</small>
                                        @endif
                                        <i class="glyphicon glyphicon-apple text-green log-icon"></i>
                                    @elseif(strtolower($log->log_type) == 'medication')
                                        Took <br/>Med
                                    @elseif(strtolower($log->log_type) == 'glucose')
                                        {{ !empty($log->glucoseLog) ? $log->glucoseLog->glucose_level : "&nbsp;" }} <br/>mg/dl
                                    @elseif(strtolower($log->log_type) == 'activityminute')
                                        {{ !empty($log->activityLog) ? $log->activityLog->minutes_performed : "&nbsp;" }} <br/> mins
                                    @elseif(strtolower($log->log_type) == 'weight')
                                        {{ !empty($log->miscLog) ? format_weight($log->miscLog->weight) : "&nbsp;" }} <br/> lbs
                                    @else
                                        {{ ($log->log_type) }}
                                    @endif
                                </p>
                                <p class="hide">{{ $log->log_type }}</p>
                                <span>{{ $log->log_time_only }}</span>
                            </a>
                                <span class="log-text">
                                    @if(strtolower($log->log_type) == 'meal')
                                        {{ !empty($log->foodLogSummary) ? $log->foodLogSummary->type : "&nbsp;" }}
                                    @elseif(strtolower($log->log_type) == 'activityminute')
                                        Activity
                                    @else
                                        {{ $log->log_type }}
                                    @endif
                                </span>
                        </div>
                    @endforeach
                </div>
            </div>
        </li>
        <!-- END timeline item -->

    @empty
        <li>
            <div class="timeline-item">
                <div class="timeline-body">
                    No Recent Logs Found
                </div>
            </div>
        </li>
    @endforelse

</ul>