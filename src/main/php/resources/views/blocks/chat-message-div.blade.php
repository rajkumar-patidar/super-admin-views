<?php
/**
 * User: spatel
 * Date: 14/1/16
 */
?>
<div class="direct-chat-msg @if(!$message->is_sent) right @endif" data-message-id="{{$message->message_id}}">
    <div class="direct-chat-info clearfix">
        <span class="direct-chat-name @if($message->is_sent) pull-left @else pull-right @endif">@if($message->is_sent) {{ $patient->user->full_name }}  @else {{ @$message->providerUser->full_name }}@endif</span>
        <span class="direct-chat-timestamp  @if($message->is_sent) pull-right @else pull-left @endif">{{ $message->sent_on}}</span>
    </div>
    <!-- /.direct-chat-info -->
    <img class="direct-chat-img" src="@if($message->is_sent){{ !empty($patient->image_path) ? config('healthslate.profile_image_base_url').$patient->image_path : '' }} @else {{ '' }}@endif" alt="Message User Image" onerror="this.src=document.querySelector('.parameters [name=alt_image_url]').value;this.onerror='';"/><!-- /.direct-chat-img -->
    <div class="direct-chat-text">
        <div class="html-text">{!! strip_tags($message->message, config('healthslate.allowed_html_tags')) !!}</div>
        @if(!empty($message->image_path))
            <img src="{{ config('healthslate.message_image_base_url').$message->image_path }}" class="sm-image" onerror="this.src=document.querySelector('.parameters [name=alt_image_url]').value;this.onerror='';"/>
        @endif
        @if($message->readed)
            <small class="pull-right" data-toggle="tooltip" title="Read Status" data-container="body">
                <i class="glyphicon glyphicon-ok font-small"></i>
                <i class="glyphicon glyphicon-ok font-small"></i>
            </small>@endif
    </div>
    <!-- /.direct-chat-text -->
</div>
<!--/.direct-chat-messages-->