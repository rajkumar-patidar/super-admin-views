<?php
/**
 * User: spatel
 * Date: 6/1/16
 */

return [

	/*
	|--------------------------------------------------------------------------
	| Common Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/

	'internal_server_error' => 'An unexpected error occurred. Please refresh the page or try again after some time.',
	'invalid_request_data'  => 'Please check the data you have submitted and try again.',

	'table_loading_records'  => 'Loading Records',
	'table_no_records'  => 'No Records found.',

	'note_deleted_patient_excluded'  => 'Note: Deleted Patients are excluded here.',
	'note_test_patient_excluded'  => 'Note: Test Patients are excluded here.',
	'note_disabled_patient_excluded'  => 'Note: Disabled Patients are excluded here.',
	'note_dates_are_in_timezone'  => 'Note: Dates are in :utc timezone.',

	'note_deleted_coaches_excluded'  => 'Note: Deleted Coaches are excluded here.',

	'email_subject_unprocessed_meal_alert' => 'Alert: :record Unprocessed Meal',
	'email_message_unprocessed_meal_alert' => 'Unprocessed Meal Alert. There are :record unprocessed meals.',

	'record_stored_updated_success' => 'Record :action successfully|Records :action successfully',
	'confirm_record_action' => 'Are you sure to :action this :element?',

	'redirecting_please_wait' => 'Redirecting....please wait or click <a href=":location">here</a>',

	'processing_cache'  =>  'Preparing :report, please wait or come back in :n minute.|Preparing :report, please wait or come back in :n minutes.',

	'email_already_registered' => 'The email address :email is already registered in our system',
	'patient_import_unsuccessful' => 'Import Unsuccessful.',
	'select_a_facility_name' => 'Select a Facility Name',
	'select_a_type' => 'Select a Type',
	'select_a_lead_coach' => 'Select a Lifestyle Coach',
	'select_a_food_coach' => 'Select a Food Coach',


];
