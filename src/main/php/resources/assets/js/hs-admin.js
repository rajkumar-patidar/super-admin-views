/**
 * Created by spatel on 4/1/16.
 */

//To set cookies
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
/*! jstz - v1.0.4 - 2012-12-18 */
(function(e){var t=function(){"use strict";var e="s",n=function(e){var t=-e.getTimezoneOffset();return t!==null?t:0},r=function(e,t,n){var r=new Date;return e!==undefined&&r.setFullYear(e),r.setDate(n),r.setMonth(t),r},i=function(e){return n(r(e,0,2))},s=function(e){return n(r(e,5,2))},o=function(e){var t=e.getMonth()>7?s(e.getFullYear()):i(e.getFullYear()),r=n(e);return t-r!==0},u=function(){var t=i(),n=s(),r=i()-s();return r<0?t+",1":r>0?n+",1,"+e:t+",0"},a=function(){var e=u();return new t.TimeZone(t.olson.timezones[e])},f=function(e){var t=new Date(2010,6,15,1,0,0,0),n={"America/Denver":new Date(2011,2,13,3,0,0,0),"America/Mazatlan":new Date(2011,3,3,3,0,0,0),"America/Chicago":new Date(2011,2,13,3,0,0,0),"America/Mexico_City":new Date(2011,3,3,3,0,0,0),"America/Asuncion":new Date(2012,9,7,3,0,0,0),"America/Santiago":new Date(2012,9,3,3,0,0,0),"America/Campo_Grande":new Date(2012,9,21,5,0,0,0),"America/Montevideo":new Date(2011,9,2,3,0,0,0),"America/Sao_Paulo":new Date(2011,9,16,5,0,0,0),"America/Los_Angeles":new Date(2011,2,13,8,0,0,0),"America/Santa_Isabel":new Date(2011,3,5,8,0,0,0),"America/Havana":new Date(2012,2,10,2,0,0,0),"America/New_York":new Date(2012,2,10,7,0,0,0),"Asia/Beirut":new Date(2011,2,27,1,0,0,0),"Europe/Helsinki":new Date(2011,2,27,4,0,0,0),"Europe/Istanbul":new Date(2011,2,28,5,0,0,0),"Asia/Damascus":new Date(2011,3,1,2,0,0,0),"Asia/Jerusalem":new Date(2011,3,1,6,0,0,0),"Asia/Gaza":new Date(2009,2,28,0,30,0,0),"Africa/Cairo":new Date(2009,3,25,0,30,0,0),"Pacific/Auckland":new Date(2011,8,26,7,0,0,0),"Pacific/Fiji":new Date(2010,11,29,23,0,0,0),"America/Halifax":new Date(2011,2,13,6,0,0,0),"America/Goose_Bay":new Date(2011,2,13,2,1,0,0),"America/Miquelon":new Date(2011,2,13,5,0,0,0),"America/Godthab":new Date(2011,2,27,1,0,0,0),"Europe/Moscow":t,"Asia/Yekaterinburg":t,"Asia/Omsk":t,"Asia/Krasnoyarsk":t,"Asia/Irkutsk":t,"Asia/Yakutsk":t,"Asia/Vladivostok":t,"Asia/Kamchatka":t,"Europe/Minsk":t,"Australia/Perth":new Date(2008,10,1,1,0,0,0)};return n[e]};return{determine:a,date_is_dst:o,dst_start_for:f}}();t.TimeZone=function(e){"use strict";var n={"America/Denver":["America/Denver","America/Mazatlan"],"America/Chicago":["America/Chicago","America/Mexico_City"],"America/Santiago":["America/Santiago","America/Asuncion","America/Campo_Grande"],"America/Montevideo":["America/Montevideo","America/Sao_Paulo"],"Asia/Beirut":["Asia/Beirut","Europe/Helsinki","Europe/Istanbul","Asia/Damascus","Asia/Jerusalem","Asia/Gaza"],"Pacific/Auckland":["Pacific/Auckland","Pacific/Fiji"],"America/Los_Angeles":["America/Los_Angeles","America/Santa_Isabel"],"America/New_York":["America/Havana","America/New_York"],"America/Halifax":["America/Goose_Bay","America/Halifax"],"America/Godthab":["America/Miquelon","America/Godthab"],"Asia/Dubai":["Europe/Moscow"],"Asia/Dhaka":["Asia/Yekaterinburg"],"Asia/Jakarta":["Asia/Omsk"],"Asia/Shanghai":["Asia/Krasnoyarsk","Australia/Perth"],"Asia/Tokyo":["Asia/Irkutsk"],"Australia/Brisbane":["Asia/Yakutsk"],"Pacific/Noumea":["Asia/Vladivostok"],"Pacific/Tarawa":["Asia/Kamchatka"],"Africa/Johannesburg":["Asia/Gaza","Africa/Cairo"],"Asia/Baghdad":["Europe/Minsk"]},r=e,i=function(){var e=n[r],i=e.length,s=0,o=e[0];for(;s<i;s+=1){o=e[s];if(t.date_is_dst(t.dst_start_for(o))){r=o;return}}},s=function(){return typeof n[r]!="undefined"};return s()&&i(),{name:function(){return r}}},t.olson={},t.olson.timezones={"-720,0":"Etc/GMT+12","-660,0":"Pacific/Pago_Pago","-600,1":"America/Adak","-600,0":"Pacific/Honolulu","-570,0":"Pacific/Marquesas","-540,0":"Pacific/Gambier","-540,1":"America/Anchorage","-480,1":"America/Los_Angeles","-480,0":"Pacific/Pitcairn","-420,0":"America/Phoenix","-420,1":"America/Denver","-360,0":"America/Guatemala","-360,1":"America/Chicago","-360,1,s":"Pacific/Easter","-300,0":"America/Bogota","-300,1":"America/New_York","-270,0":"America/Caracas","-240,1":"America/Halifax","-240,0":"America/Santo_Domingo","-240,1,s":"America/Santiago","-210,1":"America/St_Johns","-180,1":"America/Godthab","-180,0":"America/Argentina/Buenos_Aires","-180,1,s":"America/Montevideo","-120,0":"Etc/GMT+2","-120,1":"Etc/GMT+2","-60,1":"Atlantic/Azores","-60,0":"Atlantic/Cape_Verde","0,0":"Etc/UTC","0,1":"Europe/London","60,1":"Europe/Berlin","60,0":"Africa/Lagos","60,1,s":"Africa/Windhoek","120,1":"Asia/Beirut","120,0":"Africa/Johannesburg","180,0":"Asia/Baghdad","180,1":"Europe/Moscow","210,1":"Asia/Tehran","240,0":"Asia/Dubai","240,1":"Asia/Baku","270,0":"Asia/Kabul","300,1":"Asia/Yekaterinburg","300,0":"Asia/Karachi","330,0":"Asia/Kolkata","345,0":"Asia/Kathmandu","360,0":"Asia/Dhaka","360,1":"Asia/Omsk","390,0":"Asia/Rangoon","420,1":"Asia/Krasnoyarsk","420,0":"Asia/Jakarta","480,0":"Asia/Shanghai","480,1":"Asia/Irkutsk","525,0":"Australia/Eucla","525,1,s":"Australia/Eucla","540,1":"Asia/Yakutsk","540,0":"Asia/Tokyo","570,0":"Australia/Darwin","570,1,s":"Australia/Adelaide","600,0":"Australia/Brisbane","600,1":"Asia/Vladivostok","600,1,s":"Australia/Sydney","630,1,s":"Australia/Lord_Howe","660,1":"Asia/Kamchatka","660,0":"Pacific/Noumea","690,0":"Pacific/Norfolk","720,1,s":"Pacific/Auckland","720,0":"Pacific/Tarawa","765,1,s":"Pacific/Chatham","780,0":"Pacific/Tongatapu","780,1,s":"Pacific/Apia","840,0":"Pacific/Kiritimati"},typeof exports!="undefined"?exports.jstz=t:e.jstz=t})(this);
//# sourceMappingURL=public-application.js.map


//Make sure jQuery has been loaded before hs-admin.js
if (typeof jQuery === "undefined") {
    throw new Error("HSAdmin requires jQuery");
}

$.HSAdmin = {};

(function ($) {

    'use strict';

    var tz = jstz.determine(); // Determines the time zone of the browser client
    var timezone = tz.name();
    setCookie('user_timezone', timezone,1);
    //console.log(timezone);



    if ($.fn.DataTable) {

        $(document).on( 'xhr.dt', function ( e, settings, json ) {
            // show building cache message, for datatable
            // if response has `cache_in_progress` key
            if (json && json.cache_in_progress != undefined && json.wait != undefined) {
                // we do not need to auto hide this, until data is ready
                $.HSAdmin.build_notification('#alerts-container', json.message ? json.message : '', 'info creating_cache_alert', 99);
            }
        } );

        /**
         * Log an error message instead of showing alert, fallback to exception.
         *
         * @param {object} settings dataTables settings object
         * @param {int} level log error messages, or display them to the user
         * @param {string} msg error message
         * @param {int} tn Technical note id to get more information about the error.
         * @private
         */
        $.fn.dataTableExt.sErrMode = function (settings, level, msg, tn) {
            msg = 'DataTables warning: ' +
            (settings !== null ? 'table id=' + settings.sTableId + ' - ' : '') + msg;

            if (tn) {
                msg += '. For more information about this error, please see ' +
                'http://datatables.net/tn/' + tn;
            }
            if (window.console && console.log) {
                console.log(msg);
            }
            else {
                throw new Error(msg);
            }
        };

    }

    /**
     *  global ajax Complete event for datatables
     */
    $(document).ajaxComplete(function (event, xhr, settings) {
        if (xhr.responseJSON) {
            ajax_response_validator(xhr.responseJSON, null, null, xhr);
        }
    });

    /**
     * global ajax Error event for datatables
     */
    $(document).ajaxError(function (event, xhr, settings, thrownError) {
        $.HSAdmin.onAjaxError(xhr, xhr.statusText, thrownError);
    });

    $('.redirect-to').click(function () {
        var link = $.trim($(this).data('href'));
        if (link.length) {
            window.location = link;
        }
    });

    if ($('body.falicity-list').length) {
        var src = $.trim($('.hs-table').data('src'));
        var pageLength = $('.parameters [name="table_row_per_page"]').val();
        if (src.length) {
            var table = $('.hs-table').DataTable({
                processing: true,
                lengthChange: false,
                serverSide: true,
                pageLength: pageLength,
                ajax: src,
                "columns": [
                    null,
                    null,
                    null,
                    null,
                    {className: "text-center", searchable: false, "orderData": 7 },
                    {className: "text-center", searchable: false, "orderData": 8 },
                    {className: "text-center", searchable: false},
                    {className: "text-center", searchable: false},
                    {visible: false, searchable: false, type: 'num'},
                    {visible: false, searchable: false, type: 'num'}
                ]
            });
        }
    }

    else if ($('body.falicity-providers').length) {
        var src = $.trim($('.hs-table').data('src'));
        var pageLength = $('.parameters [name="table_row_per_page"]').val();
        if (src.length) {
            var table = $('.hs-table').DataTable({
                processing: true,
                lengthChange: false,
                bAutoWidth: false,
                serverSide: true,
                pageLength: pageLength,
                ajax: src,
                "columns": [
                    null,
                    null,
                    null,
                    null,
                    {className: "text-center", searchable: false, "orderData": 7 },
                    {className: "text-center", searchable: false},
                    {className: "text-center", searchable: false},
                    {visible: false, searchable: false, type: 'num'}
                ],
                createdRow: function ( row, data, index ) {
                    if($(row).find('.is_deleted').data('is-deleted')){
                        $(row).addClass( 'text-muted' );
                    }
                }
            });
        }
    }

    else if ($('body.falicity-providers-report').length) {
        var src = $.trim($('.hs-table').data('src'));
        var pageLength = $('.parameters [name="table_row_per_page"]').val();
        if (src.length) {
            var table = $('.hs-table').DataTable({
                processing: true,
                lengthChange: false,
                bAutoWidth: false,
                serverSide: true,
                pageLength: pageLength,
                ajax: src,
                dom: 'Bfrtip',
                buttons: ['csv'],
                "columns": [
                    null,
                    null,
                    null,
                    {className: "text-center", searchable: false, "orderData": 6 },
                    {className: "text-center", searchable: false},
                    {className: "text-center", searchable: false},
                    {visible: false, searchable: false, type: 'num'}
                ],
                createdRow: function ( row, data, index ) {
                    if($(row).find('.is_deleted').data('is-deleted')){
                        $(row).addClass( 'text-muted' );
                    }
                }
            });
        }
    }

    else if ($('body.provider-patients-report').length) {
        var src = $.trim($('.hs-table').data('src'));
        var pageLength = $('.parameters [name="table_row_per_page"]').val();
        if (src.length) {
            var table = $('.hs-table').DataTable({
                processing: true,
                lengthChange: false,
                bAutoWidth: false,
                serverSide: true,
                pageLength: pageLength,
                ajax: src,
                dom: 'Bfrtip',
                buttons: ['csv'],
                fnServerParams: function (aoData) {
                    if ($('.filter-form select[name="week"]').val()) {
                        aoData['week'] = $('.filter-form select[name="week"]').val();
                    }
                },
                "columns": [
                    null,
                    null,
                    null,
                    {className: "text-center", searchable: false},
                    {className: "text-center", searchable: false, orderData: 8},
                    {className: "text-center", searchable: false, orderData: 9},
                    {className: "text-center", searchable: false},
                    {visible: false, searchable: false},
                    {visible: false, searchable: false},
                    {visible: false, searchable: false},
                    {visible: false, searchable: true}
                ],
                createdRow: function (row, data, index) {
                    if ($(row).find('td:last').text() == 'Deleted') {
                        $(row).addClass('text-muted');
                    }
                    if(data[10]){
                        $(row).addClass(data[10]);
                    }
                },
                preDrawCallback: function (oSettings) {
                }
            });

            $('.filter-form .trigger-dt-change').change(function () {
                return $(this).parents('form:first').submit();
            });
        }
    }

    else if ($('body.falicity-patients').length) {
        var src = $.trim($('.hs-table').data('src'));
        var pageLength = $('.parameters [name="table_row_per_page"]').val();
        if (src.length) {
            var table = $('.hs-table').DataTable({
                processing: true,
                lengthChange: false,
                bAutoWidth: false,
                serverSide: true,
                pageLength: pageLength,
                ajax: src,
                fnServerParams: function (aoData) {
                    if ($('.filter-form select[name="status"]').val()) {
                        aoData['status'] = $('.filter-form select[name="status"]').val();
                    }
                    if ($('.filter-form select[name="days"]').val()) {
                        aoData['days'] = $('.filter-form select[name="days"]').val();
                    }
                },
                "columns": [
                    null,
                    null,
                    null,
                    null,
                    null,
                    {className: "text-center", searchable: false},
                    {className: "text-center", searchable: false}
                ],
                createdRow: function (row, data, index) {
                    if ($(row).find('.is_deleted').data('is-deleted')) {
                        $(row).addClass('text-muted');
                    }
                },
                preDrawCallback: function (oSettings) {
                    $('.hs-dt-filter').removeClass('margin-0');
                }
            });

        }
    }

    else if($('body.patient-msgs').length){
        $(".direct-chat-messages").animate({ scrollTop: $('.direct-chat-messages').prop("scrollHeight")}, 1500);
    }
    else if ($('body.common-report-view').length) {
        var src = $.trim($('.hs-table').data('src'));
        var pageLength = $('.parameters [name="report_datatable_row_per_page"]').val();
        if (src.length) {
            $('.hs-table').DataTable({
                processing: true,
                lengthChange: false,
                bAutoWidth: false,
                serverSide: true,
                pageLength: pageLength,
                ajax: {
                    "url": src,
                    "data": function ( d ) {
                        d.report_title = $('.hs-table [name="report_title"]').val();
                 }},
                'dom': 'Bfrtip',
                'buttons': ['csv'],
                aaSorting: []
            });
        }
    }
    else if($('body.facility-reports').length){

        // pre-fill select if cookie available.
        var hs_report_f_id = Cookies.get('hs_report_f_id');
        if(hs_report_f_id && hs_report_f_id > 0){
            $('select[name="facility_id"]').val(hs_report_f_id);
        }
        // toggle all view link on change of facility filter
        $('select[name="facility_id"]').on('change', function () {
            var val = $(this).val();
            var label = $(this).find('option[value="' + val + '"]').text();

            // reset
            $('.box-title').html($('.box-title').data('default'));
            $('.report-links').removeAttr('href').addClass('disabled');

            if (val > 0) {
                Cookies.set('hs_report_f_id', val);
                $('.box-title').html('Reports for <b>' + label + '</b>');
                $('.report-links').each(function () {
                    var _this = $(this);
                    var name_report = _this.data('href').split('/').pop();
                    if('video-calls' == name_report || 'entries' == name_report || 'active' == name_report
                    || 'steps' == name_report)
                    {
                        _this.attr('href', _this.data('href') + '/' + val + '/10').removeClass('disabled');
                    }
                    else if('fitbit-invalid-token' == name_report)
                    {
                        _this.attr('href', _this.data('href')).removeClass('disabled');
                    }
                    else
                    {
                        _this.attr('href', _this.data('href') + '/' + val).removeClass('disabled');
                    }
                });
            }
        }).trigger('change');
    }

    // common & enrollment report view
    // filter by days
    $('.box-tools.days-filter select[name="days"]').on('change', function(){
        var days = $(this).val();
        if($(this).data('url')){
            jQuery('#hs-loader').show();
            window.location = $(this).data('url') +(days > 0 ? '/'+days : '');
        }
    });

    $('.filter-form .trigger-dt-change').change(function () {
        return $(this).parents('form:first').submit();
    });

    /**
     * read more btn event
     */
    $('body').delegate('.more-desc', 'click', function (e) {
        e.preventDefault();
        $(this).parents('text:first').find('.hide').removeClass('hide');
        $(this).parents('text:first').find('.more-ellipse, .more-desc').addClass('hide');
        return false;
    });

    /**
     * Build Self destructing Alert notification
     * @param append_to
     * @param message
     * @param type
     * @param destroy_after
     * @returns {boolean}
     */
    $.HSAdmin.build_notification = function (append_to, message, type, destroy_after) {
        var _target = $(append_to);
        if (_target.length != 1) {
            return false;
        }
        if (type == undefined) {
            type = 'success';
        }
        if (destroy_after == undefined) {
            destroy_after = 5;
        }
        var _html = $('<div role="alert" class="alert alert-' + type + ' alert-dismissible fade in">\
                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button> \
                    <p> ' + message + ' </p>  </div>');
        setTimeout(function () {
            _html.slideUp(function(){ $(this).remove();});
        }, (parseInt(destroy_after) * 1000));
        _target.append(_html);
        return true;
    };

    /**
     *
     * @param jqXHR
     * @param textStatus
     * @param errorThrown
     */
    $.HSAdmin.onAjaxError = function(jqXHR, textStatus, errorThrown){
        if (console && console.log) {
            console.log('ajax failed. ' + (jqXHR ? jqXHR.status : '') + ' ' +textStatus);
        }
        if(textStatus != 'abort'){
            $.HSAdmin.build_notification('#alerts-container', $('.parameters [name="internal_server_error"]').val(), 'error');
        }
    };

    $.refresh_timeout = null;

    /**
     * ajax_response_validator
     *  custom ajax request response validator
     *
     * @param response
     * @param callback
     * @param parseJSON
     * @param xhr
     */
    var ajax_response_validator = function (response, callback, parseJSON, xhr) {
        var ajaxResponse = response;
        try {
            ajaxResponse = JSON.parse(ajaxResponse);
        } catch (e) {
            if (parseJSON === true) {
                ajaxResponse = '';
            }
        }
        finally {
            /**
             * redirecting If Needed
             *  It should be called after every ajax/post/get call to server
             */
            if (ajaxResponse != "" && ajaxResponse.redirect_to != undefined) {
                if (ajaxResponse.wait != undefined) {

                    setTimeout(function () {
                        window.location = ajaxResponse.redirect_to;
                    }, (parseFloat(ajaxResponse.wait) * 1000));

                } else {
                    window.location = ajaxResponse.redirect_to;
                }
                // commenting as it was preventing additional callback
                //return false;
            }
            // Check if response is having `Cached-On` Header
            // File export does not have cached-on header
            var cached_on = parseInt(xhr.getResponseHeader('Cached-On'));
            if( !isNaN(cached_on) ){
                var key = xhr.getResponseHeader('Cache-Key');
                cached_on = moment(cached_on).format($('.parameters [name="default_js_date_time_format"]').val());
                $('#content-cache-btn .time').text(cached_on);
                $('#clear_cache [name="cache_key"]').val(key);
                $('#content-cache-btn').show();
	            $('.build-csv').removeClass('disabled');
                $('.creating_cache_alert').hide();
            }
            else if(ajaxResponse.cache_in_progress != undefined && ajaxResponse.wait != undefined){
                // No need to hide Content cache message for file export.
                if(ajaxResponse.type != undefined && ajaxResponse.type != 'file_export'){
	                $('#content-cache-btn').hide();
	                $('.build-csv').addClass('disabled');
                }
                clearTimeout($.refresh_timeout);
                $.refresh_timeout = setTimeout(function () {
                    // We will check if this cache is prepared, after wait duration
                    $.HSAdmin.check_if_cache_exists(ajaxResponse.cache_in_progress);
                }, (parseFloat(ajaxResponse.wait) * 60 * 1000) );
            }
        }
        if (typeof callback === "function") {
            if (parseJSON === true) {
                callback.apply(this, [ajaxResponse, xhr]);
            } else {
                callback.apply(this, [response, xhr]);
            }
        }
    };

    /**
     * call_jquery_ajax
     *        Common function for calling ajax post/get request
     *
     * @param url
     * @param datastring
     * @param parseJSON : only be true when Response::json is not used
     * @param beforeCall
     * @param callback
     * @param method
     * @param showLoading
     * @param loadingElement
     * @param async
     * @returns {*}
     */
    $.HSAdmin.call_jquery_ajax = function (url, datastring, parseJSON, beforeCall, callback, method, showLoading, loadingElement, async) {
        if (typeof async == "undefined") {
            async = true;
        }
        if (typeof showLoading == "undefined") {
            showLoading = false;
        }
        if (typeof method == "undefined") {
            method = 'POST';
        }
        if (method != 'POST' && method != 'GET') {
            return false;
        }

        if (showLoading && loadingElement) {
            jQuery(loadingElement).show();
        }

        var jqxhr = jQuery.ajax({
            url: url,
            data: datastring,
            type: method,
            async: async,
            global: false,
            cache: false,
            beforeSend: beforeCall
        })
            .done(function (response, status, xhr) {
                ajax_response_validator(response, callback, parseJSON, xhr);
            })
            .fail(function () {
                $.HSAdmin.onAjaxError();
            })
            .always(function () {
                if (loadingElement) {
                    jQuery(loadingElement).hide();
                }
            });
        return jqxhr;
    };

    /**
     *
     */
    $.HSAdmin.refresh_dates = function(){
        $('.timestamp:not(.done)').each(function () {
            var _this = $(this),
                timestamp = moment.unix($.trim(_this.data('timestamp'))),
                format = $('.parameters [name="default_js_date_time_format"]').val();
            _this.addClass('done');
            if (timestamp.isValid()) {
                _this.text(timestamp.format(format));
            }
        });
    };

    /**
     *  Call ajax to URL, and see if cache is prepared.
     * @param link
     */
    $.HSAdmin.check_if_cache_exists = function (link) {
        var _checking_cache_msg = $('.checking_cache_msg').html('Checking Now..');
        $.HSAdmin.call_jquery_ajax(link, {}, false, false, function (response) {
            if (response.success != undefined) {
                // cache exists
                if(response.type && response.type == 'file_export'){
	                return $('.build-csv:first').trigger('click');
                }
                jQuery(window).trigger("refresh");
            }
            else if (response.cache_in_progress != undefined) {
                // cache does not exists yet, still in progress.
                // do nothing,
                _checking_cache_msg.html('Still in progress.');
            }

        }, 'GET', true, '#hs-loader');
    };
})(jQuery);


jQuery(document).ready(function($){

    /**
     * Refresh Page view
     */
    $(window).on("refresh", function () {
        var _duration_filter = $('.duration-filter [name="end_date"]');
        var _filter_form = $('form.filter-form');
        var _datatable = $('.dataTable');
        if (_duration_filter.length) {
            _duration_filter.trigger('change');
        }
        else if (_filter_form.length) {
            _filter_form.submit();
        }
        else if(_datatable.length == 1){
            // page has datatable refresh it
            var tables = $.fn.dataTable.tables(true);
            $(tables).DataTable().ajax.reload(null,false);
        }
        else{
            window.location.reload();
        }
    });

    if($('body.search-patient').length){

        $('select#search-patient-input')
            .select2({
                placeholder: {
                    id: '0', // the value of the option
                    text: 'Search a patient'
                },
                minimumInputLength: 1,
                ajax: {
                    url: $('select#search-patient-input').data('src'),
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q:params.term
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: data.items ? data.items : []
                        };
                    },
                    cache: false
                },
                escapeMarkup: function (markup) { return markup;},

                templateResult: function (patient) {
                    if (patient.loading) return patient.text;

                    var markup =
                        "<div class='select2-result-repository clearfix'>" +
                        "<div class='select2-result-repository__title'>" + patient.text + "</div>";
                    markup += "<div class='select2-result-repository__statistics'>" +
                    "<div class='select2-result-meta_info'> <b>Facility</b>: " + (patient.facility_id ? patient.facility_id : '') + "</div>" +
                    "<div class='select2-result-meta_info'> <b>User Id</b>: " + (patient.user_id ? patient.user_id : '') + "</div>" +
                    "</div>" +
                    "</div>";

                    return markup;
                },

                templateSelection: function (patient) {
                    if(patient.url) {
                        $('.hs-dt-filter .btn-primary').removeAttr('disabled');
                        $('.hs-dt-filter .btn-primary').data('redirect-to', patient.url);
                    }else{
                        $('.hs-dt-filter .btn-primary').attr('disabled','disabled').data('redirect-to', null);
                    }
                    return patient.text;
                }
            })
        ;
        $('.hs-dt-filter .btn-primary').click(function(){
            var url =  $(this).data('redirect-to');
            if(url && $.trim(url) != ""){
                window.location = url;
            }
        });
    }

    /**
     * common init for duration filter used in report
     */
    if ($('#duration-btn').length) {
        var subtract_days = $('#duration-btn').data('subtract-days');
        subtract_days = isNaN(parseInt(subtract_days)) ? 6 : parseInt(subtract_days);
        $('#duration-btn').daterangepicker({
                ranges: {
                    'Today'     : [moment().subtract(0, 'days'), moment()],
                    'Last Week': [moment().subtract(6, 'days'), moment()],
                    'Last 2 Week': [moment().subtract(13, 'days'), moment()],
                    'Last 3 Week': [moment().subtract(20, 'days'), moment()],
                    'Last 4 Week': [moment().subtract(27, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()]
                },
                startDate: moment().subtract(subtract_days, 'days'),
                maxDate: moment()
            },
            function (start, end, label) {
                if($(this.element) && $(this.element).find('span').length){
                    $(this.element).find('span').text(label);
                    label = label + ' (' + start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY') + ')';
                    $($(this.element).data('label')).val(label);
                    $('.duration-filter [name="page"]').val(1);
                    $($(this.element).data('start')).val(start.format('MM/DD/YYYY'));
                    $($(this.element).data('end')).val(end.format('MM/DD/YYYY')).trigger('change');
                }
            }
        );
    }

    if($('body.weekly-participation').length){


        // common & enrollment report view
        // filter by days
        $('.box-tools select[name="days"]').on('change', function(){
            var days = $(this).val();
            if($(this).data('url')){
                jQuery('#hs-loader').show();
                window.location = $(this).data('url') +(days > 0 ? 'days='+days : '');
            }
        });

        /**
         * Coach Filter init
         */
        if($('select[name="coach"]').length) {
            var coach_list = (typeof COACH_LIST != "undefined") ? COACH_LIST : [];
            $('select[name="coach"]').on('change', function () {
                $(this).parents('form').find('[name="end_date"]').trigger('change');
            }).select2({
                    data: coach_list,
                    templateResult: function (state) {
                        if (!state.id) { return state.text; }
                        var $state = $('<div/>').text(state.text);
                        if(state.type) { $state.append('<br/>').append($('<span/>').text(state.type).prepend($('<b/>').text('Type: '))); }
                        return $state;
                    }
                });
        }

        if($('select[name="user_timezone"]').length) {
            var timezone_list = (typeof TIMEZONE_LIST != "undefined") ? TIMEZONE_LIST : [];
            $('select[name="user_timezone"]').on('change', function () {
                $(this).parents('form').find('[name="end_date"]').trigger('change');
            }).select2({
                data: timezone_list,
                templateResult: function (state) {
                    if (!state.id) { return state.text; }
                    var $state = $('<div/>').text(state.text);
                    return $state;
                }
            });
        }

        if($('select[name="facility_id"]').length) {
            var facility_list = (typeof FACILITY_LIST != "undefined") ? FACILITY_LIST : [];
            $('select[name="facility_id"]').on('change', function () {
                $(this).parents('form').find('[name="end_date"]').trigger('change');
            }).select2({
                data: facility_list,
                templateResult: function (state) {
                    if (!state.id) { return state.text; }
                    var $state = $('<div/>').text(state.text);
                    return $state;
                }
            });
        }

        /**
         * weekly-participation report duration filter action
         */
        $('.duration-filter [name="end_date"]').on('change', function (e) {
            e.preventDefault();
            var _form = $(this).parents('form:first');

            var start = moment(_form.find('[name="start_date"]').val(), "MM/DD/YYYY");
            var end = moment(_form.find('[name="end_date"]').val(), "MM/DD/YYYY");
            if (!start.isValid() || !end.isValid()) {
                return false;
            }
            $.HSAdmin.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {
                if (response.success != undefined) {
                    $('#ajax-content').html(response.html);
                    $('#page_description').html(response.page_description);
                    $('.build-csv').addClass('disabled');
                    if(response.total_records == undefined || response.total_records > 0){
                        $('.build-csv').removeClass('disabled');
                    }
                    $('.hs-table[data-init="datatable"]').dataTable({
                        processing: true,
                        lengthChange: false,
                        bAutoWidth: false,
                        searching: false,
                        pageLength: parseInt($('.parameters [name="report_datatable_row_per_page"]').val()),
                        dom: 'r<"#table-info" i>t<"#pagination-sm" p>'
                    });
                }
                else if(response.cache_in_progress != undefined){
                    $('#ajax-content').html('<div class="box-body text-center">' + response.message + '</div>');
                }
                else{
                    $('#ajax-content .text-center').text($('.parameters [name="internal_server_error"]').val());
                }
            }, _form.attr('method'), true, '#hs-loader');
        }).trigger('change');

        /**
         * Preventing default paginator
         */
        $('body').delegate('#ajax-content .pagination a', 'click', function (e) {
            if ($('.filter-form [name="page"]').length) {
                e.preventDefault();
                var url = $(this).attr('href'),
                    page = url.split('page=')[1];
                $('.filter-form [name="page"]').val(page).parent('form').find('[name="end_date"]').trigger('change');
            }
        });

        /**
         * Build CSV Export URL, and redirect
         */
        $('.build-csv').on('click', function (e) {
            var url = $(this).data('href');
            var data_string = $('.filter-form').serialize();
            url = url + '&' + data_string;
            // For disabled and case when view does not have cached content
            if($(this).hasClass('caching-disabled') || ($('#content-cache-btn:visible').length == 0)){
                window.location = url + '&_=' + ((new Date()).getTime());
                return true;
            }
	        $.HSAdmin.call_jquery_ajax(url, {}, false, false, function (response, xhr) {
		        var disp = xhr.getResponseHeader('Content-Disposition');
		        if (disp && disp.search('attachment') != -1) {
			        window.location = url + '&_=' + ((new Date()).getTime());
		        }else if(response.message){
			        $.HSAdmin.build_notification('#alerts-container', response.message, 'success', 30);
                }
		        else{
			        $.HSAdmin.build_notification('#alerts-container', $('.parameters [name="internal_server_error"]').val(), 'error');
                }
	        }, 'GET', true, '#hs-loader');
        });
    }

    var email_regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    /**
     *
     * manage-schedule page events
     *
     */
    if($('body.manage-schedule').length){
        // initializing send_to select.
        $('select[name="send_to[]"]')
            .select2({
                placeholder: 'Add or select Email Addresses',
                tags: true,
                tokenSeparators: [',', ' '],
                maximumSelectionLength: 10
            })
            .on('select2:selecting', function (event) {
                var text = event.params.args.data.text;
                return email_regex.test(text);
            });

        /**
         * schedule-form submit event Handler
         */
        $('form.schedule-form').on('submit', function (e) {
            var _this = $(this);
            $('.form-message').html('');
            if(_this.find('[name="report_name"]').val() == 0 || _this.find('[name="occurrence"]').val() == 0 || _this.find('[name="send_to[]"]').val() == ""){
                $('.form-message').html($('<p>').addClass('text-danger').text('Please specify all required fields.'));
                return false;
            }
            if(_this.hasClass('submitted')){
                return false;
            }
            _this.addClass('submitted');
            $.HSAdmin.call_jquery_ajax(_this.attr('action'), _this.serialize(), false, false, function (response) {
                _this.removeClass('submitted');
                if(response.success != undefined){
                    _this.trigger('reset');
                    _this.find('[name="send_to[]"]').val([0]).trigger('change');
                    $('.form-message').html($('<p>').addClass('text-success').html(response.success));
                    if(response.send_to != undefined && response.redirect_msg != undefined) {
                        _this.find(':submit').addClass('disabled');
                        _this.append($('<p>').html(response.redirect_msg))
                        setTimeout(function () {
                            window.location = response.send_to;
                        }, 3000);
                    }

                }else if(response.error != undefined){
                    $('.form-message').html(response.error);
                    setTimeout(function(){
                        $('.form-message').html('');
                    },8000);
                }
            }, _this.attr('method'), true, '#hs-loader');
        });

        /**
         * edit schedule button action
         */
        $('.schedule-edit').on('click', function (e) {
            e.preventDefault();
            var _this = $(this);
            var _form = $('form.schedule-form');
            var schedule_id = $.trim(_this.data('schedule-id'));
            var trigger = $.trim(_this.data('trigger'));
            if (schedule_id != "") {
                _form.find('[name="schedule_id"]').val(schedule_id);
                _form.find('[name="report_name"]').val(_this.data('report-name'));
                _form.find('[name="send_to[]"]').val(_this.data('send-to').split(', ')).trigger('change');
                _form.find('[name="occurrence"]').val(_this.data('occurrence'));
                _form.find('[name="trigger"]').val(trigger);
                $('a[data-toggle="tab"][href="#add-task"]').click();
            }
            return false;
        });

        /**
         * delete schedule modal show event
         */
        $('.modal#schedule-delete').on('show.bs.modal', function (e) {
            var _modal = $(this);
            _modal.find('[name="schedule_id"]').val('');
            _modal.find('.modal-body .message-bag').html('');
            var _element = e.relatedTarget ? $(e.relatedTarget) : undefined;
            if(_element){
                _modal.find('[name="schedule_id"]').val($.trim(_element.data('schedule-id')));
            }else{
                return false;
            }
        });

        /**
         * delete cofirmation handler
         */
        $('form.schedule-delete').on('submit', function (e) {
            var _this = $(this);

            if(_this.find('[name="schedule_id"]').val() == ""){
                return false;
            }
            if(_this.hasClass('submitted')){
                return false;
            }
            _this.addClass('submitted');
            _this.find('.modal-body .message-bag').html('');
            $.HSAdmin.call_jquery_ajax(_this.attr('action'), _this.serialize(), false, false, function (response) {
                _this.removeClass('submitted');
                if(response.error != undefined){
                    _this.find('.modal-body .message-bag').html(response.error);
                }else{
                    $('.modal#schedule-delete').modal('hide');
                }
            }, _this.attr('method'), true, '#hs-loader');
        });
    }


    /**
     * events and init for support tickets pages
     */
    if($('body.support-tickets').length){
        var src = $.trim($('.hs-table').data('src'));
        if (src.length) {
            var table = $('.hs-table').DataTable({
                processing: true,
                lengthChange: false,
                bAutoWidth: false,
                serverSide: true,
                pageLength: $('.parameters [name="table_row_per_page"]').val(),
                ajax: src,
                'dom': 'Bfrt<"col-md-4" i>p',
                'buttons': ['csv'],
                "order": [[ 3, "asc" ]],
                "columns": [
                    {data: 'support_ticket_id'},
                    {data: 'description'},
                    {data: 'patient', className: "text-center"},
                    {data: 'date_opened', className: "text-center", sortable: false},
                    {
                        data: function (row, type, val, meta) {
                            var _label = $('<small/>');
                            if (row.is_open == false) {
                                _label.addClass('label bg-red').text('closed');
                            } else {
                                _label.addClass('label bg-green').text('open');
                            }
                            return $('<p/>').append(_label).html();
                        },
                        className: "text-center",
                        searchable: false
                    },
                    {
                        data: function (row, type, val, meta) {
                            var _button = $('<button/>')
                                .text('Messages')
                                .addClass('btn btn-primary btn-sm view-msg')
                                .attr('data-support-ticket-id', row.support_ticket_id)
                                .attr('data-src', row.messages_link);
                            return $('<p/>').append(_button).html();
                        },
                        className: "text-center",
                        searchable: false,
                        sortable: false,
                        "width": "100px",
                        defaultContent: ''
                    },
                    {data: 'date_closed', visible: false, searchable: false}
                ],
                drawCallback: function( settings ) {
                    $.HSAdmin.refresh_dates();
                }
            });

            /**
             * Load messages on click
             */
            $('.hs-table').on('click', '.view-msg', function(){
                var _this = $(this),
                link = $.trim(_this.data('src'));
                if(link.length){
                    $.HSAdmin.call_jquery_ajax(link, {}, false, false, function (response) {
                        if (response.success != undefined && response.html != undefined) {
                            $('#messages-modal').html(response.html).modal('show');
                            $.HSAdmin.refresh_dates();
                        }
                    }, 'GET', true, '#hs-loader');
                }
            });

            /**
             * scroll to button when open
             */
            $('#messages-modal').on('shown.bs.modal', function (e) {
                $("#messages-modal .direct-chat-messages").animate({ scrollTop: $('#messages-modal .direct-chat-messages').prop("scrollHeight")}, 1500);
            });

        }
    }

    $('#clear_cache').on('submit', function(){
        var _form = $(this);
        if(_form.hasClass('submitted')){
            return false;
        }
        _form.parents('.modal:first').modal('hide');
        _form.addClass('submitted');
        $.HSAdmin.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {
            _form.removeClass('submitted');
            if (response.success != undefined && response.reload != undefined) {
                $('#content-cache-btn').hide();
                jQuery(window).trigger("refresh");
            }
            else if(response.error){
                $.HSAdmin.build_notification('#alerts-container', response.error, 'error');
            }
            else{
                $.HSAdmin.build_notification('#alerts-container', $('.parameters [name="internal_server_error"]').val(), 'error');
            }
        }, _form.attr('method'), true, '#hs-loader');
    });

    if ($('body.import-patient').length) {
        /**
         *  Update other dropdown when Facility Dropdown changes.
         */
        $('#facility-list').on('change', function () {
            var _this = $(this);
            var _dd = $('#lead-coach-list, #food-coach-list');
            var facility_name = $.trim(_this.find('option:selected').text());
            _dd.find('optgroup')
                .removeAttr('disabled')
                .show()
                .not('[label="' + facility_name + '"]')
                .attr('disabled', 'disabled')
                .hide();
            _dd.trigger('change');
        }).trigger('change');

        /**
         *  Show file name when changed
         */
        $('#csvFile').on('change', function () {
            var name = ($(this)[0].files.length) ? ($(this)[0].files[0].name) : $(this).val();
            if (name.length > 30) {
                name = name.substring(0, 30) + '...';
            }
            $('#file_name_label').text(name);
        });
    }

	if ($('body.facility-setting').length) {

		$('#link_table').on('click', '.remove-row', function () {
			$(this).closest('tr').remove();
		});

        $('#add_btn_link').on('click', function () {
            var next_row_num = $('#link_table tbody tr').length + 1;

            var _tr = $('<tr data-id="' + next_row_num + '">\
                <td><input type="text" class="form-control" name="footer_links[' + next_row_num + '][title]" /></td>\
                <td><input type="text" class="form-control" name="footer_links[' + next_row_num + '][href]" /></td>\
                <td><span class="glyphicon glyphicon-minus remove-row pointer"></span></td>\
            </tr>');
            $('#link_table tbody').append(_tr);
        });

		$('.remove-img').on('click', function () {
            var _this = $(this);
            _this.siblings('img').attr('src', '');
            _this.siblings('input:hidden').val('');
            _this.parents('.blogo-wrapper').find('.file_name_label').html('');
            _this.hide();
		});


		var validImageFileExtensions = ['png','jpg','jpeg'];

		$('.image-chooser').on("change", function () {
            var _wrapper = $(this).parents('.blogo-wrapper'),
			    _img = _wrapper.find('img'),
				_target = _wrapper.find('.image-data'),
				_icon = _wrapper.find('.remove-img'),
                _file_name_label = _wrapper.find('.file_name_label'),
				file = $(this)[0].files[0],
				fileName = file.name;
			var extractFileExtension = fileName.split('.').pop().toLowerCase();

            if (fileName.length > 30) {
                fileName = fileName.substring(0, 30) + '...';
            }
			if ($(this).val().length > 0 && (validImageFileExtensions.indexOf(extractFileExtension) !== -1)) {
				if (this.files[0]) {
					var reader = new FileReader();
					reader.onload = function (e) {
						var dataURL = e.target.result;
						_img.attr('src', dataURL);
						_target.val(dataURL);
                        _file_name_label.html(fileName);
                        _icon.show();
					};
					reader.readAsDataURL(this.files[0]);
                    return true;
				}
			}
            _img.attr('src', '');
            _target.val('');
            _file_name_label.html('<i class="text-danger">Please choose a image file</i>');
            _icon.hide();

		});

		$('.form-manage-facility').on('submit', function () {
			var _form = $(this);
            if(_form.hasClass('submitted')){
                return false;
            }
            _form.addClass('submitted');
			var _alert_container = _form.find('.form-msg').html('');
			$.HSAdmin.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {
				_form.removeClass('submitted');
				if (response.success != undefined) {
					_alert_container.append(response.success);
				}
				else {
					_alert_container.append((response.error ? response.error : $('.parameters [name="internal_server_error"]').val() ));
				}
			}, _form.attr('method'), true, '#hs-loader');
		});
	}


    // toggle all view link on change of facility filter
    $('select[name="send_message_facility_id"]').on('change', function () {
        var val = $(this).val();
        var _this = $(this);
        $('.form-message').html('');
        if (val > 0) {
            $('#send_message_to_patient').empty().trigger('change');
            $.HSAdmin.call_jquery_ajax('ajax/fitbit-steps', _this.serialize(), false, false, function (response) {
                _this.removeClass('submitted');
                if(response.error != undefined){

                }else{
                    $('.send_message_users').removeClass('hide');
                    $('.send_message_notes').removeClass('hide');
                    $('.report-links').removeClass('hide');
                    var coach_list = (typeof response.coach_list != "undefined") ? response.coach_list : [];
                    $('#send_message_to_patient').on('change', function () {
                    }).select2({
                        data: coach_list,
                        templateResult: function (state) {
                            if (!state.id) { return state.text; }
                            var $state = $('<div/>').text(state.text);
                            return $state;
                        }
                    });
                }
            }, 'GET', true, '#hs-loader');
        }
        else
        {
            $('.send_message_users').addClass('hide');
            $('.send_message_notes').addClass('hide');
            $('.report-links').addClass('hide');
        }
    }).trigger('change');





    /**
     * schedule-form submit event Handler
     */
    $('form.send-message-form').on('submit', function (e) {
        var _this = $(this);
        $('.form-message').html('');
        if(_this.find('#patient_id').val() == null || _this.find('[name="patient_id"]').val() == ''){
            $('.form-message').html($('<p>').addClass('text-danger').text('Please specify all required fields.'));
            return false;
        }
        if(_this.hasClass('submitted')){
            return false;
        }
        _this.addClass('submitted');
        $.HSAdmin.call_jquery_ajax(_this.attr('action'), _this.serialize(), false, false, function (response) {
            _this.removeClass('submitted');
            if(response.success != undefined){
                //_this.trigger('reset');

                //$('#send_message_to_patient').empty().trigger('change');

                //$('.form-message').html($('<p>').addClass('text-success').text(response.success));
                $("#table_div > tbody").html("");
                $('#table_div > tbody:last').append('<tr><th>Date</th><th>Fitbit Steps</th><th>Synced Steps</th></tr>');
                $.each(response.success, function (i) {
                    $.each(response.success[i].reverse(), function (key, val) {
                        //console.log(val.dateTime);
                        if(val.value != 0 || val.steps_from_db != 0) {
                            $('#table_div > tbody:last').append('<tr><td>'+ val.dateTime +'</td><td>'+ val.value +'</td><td>'+ val.steps_from_db +'</td></tr>');
                        }
                    });
                });

            }else if(response.error != undefined){
                //_this.trigger('reset');
                //$('#send_message_to_patient').empty().trigger('change');
                $("#table_div > tbody").html("");
                $('.form-message').html($('<p>').addClass('text-danger').html(response.error));
            }
        }, _this.attr('method'), true, '#hs-loader');
    });




    $('.fitbit-hs-table[data-init="datatable"]').dataTable({
        processing: true,
        lengthChange: false,
        bAutoWidth: false,
        searching: false,
        pageLength: 50,
        dom: 'r<"#table-info" i>t<"#pagination-sm" p>'
    });

    window.onload = function(){
        if($('#patient_id').val() == null || $('[name="patient_id"]').val() == ''){
            return false;
        }
        else
            $('.send-message-form').submit();
    }



});