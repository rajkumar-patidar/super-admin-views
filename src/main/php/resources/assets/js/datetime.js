/**
 * Created by spatel on 18/1/16.
 */
(function ($) {

    'use strict';

    $('#log-filter-form').on('submit', function (e) {
        e.preventDefault();
        var _this = $(this);

        var start = moment($('#startRange').val(),"MM/DD/YYYY");
        var end = moment($('#endRange').val(),"MM/DD/YYYY");
        if(!start.isValid() || !end.isValid()){
            return false;
        }
        $.HSAdmin.call_jquery_ajax(_this.attr('action'), _this.serialize(), false, false, function (response) {
            if(response.success != undefined){
                $('#daterange-btn span').html(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
                $('#timeline-wrapper').html(response.html);
            }
        }, _this.attr('method'), true, '#hs-loader');
    }).trigger('submit');

    if ($('#daterange-btn').length) {
        $('#daterange-btn').daterangepicker({
                ranges: {
                    //'Today': [moment(), moment()],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    'Last 2 Months': [moment().subtract(2, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                startDate: moment().subtract(29, 'days'),
                maxDate: moment()
            },
            function (start, end) {
                $('#startRange').val(start.format('MM/DD/YYYY'));
                $('#endRange').val(end.format('MM/DD/YYYY'));
                $('form#log-filter-form').trigger('submit');
            }
        );
    }
})(jQuery);
