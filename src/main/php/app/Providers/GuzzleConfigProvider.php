<?php

namespace HealthSlateAdmin\Providers;

use Illuminate\Support\ServiceProvider;
use GuzzleHttp\Client;

class GuzzleConfigProvider extends ServiceProvider {
	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot() {
		//
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register() {

		$guzzle_config = isset( $this->app->config['rest'] ) ? $this->app->config['rest']['guzzle_config'] : [ ];
		$this->app->bind( 'GuzzleHttp\Client', function () use ( $guzzle_config ) {
			return new  Client( $guzzle_config );
		} );
	}
}
