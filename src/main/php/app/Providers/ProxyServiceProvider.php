<?php namespace HealthSlateAdmin\Providers;

use Illuminate\Support\ServiceProvider;

use URL;
class ProxyServiceProvider extends ServiceProvider {

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $request = $this->app['request'];
        $proxies = $this->app['config']->get('proxy.proxies');
        if ($proxies == "*") {

            // Trust all proxies, means current Ip
            $proxies = array($request->getClientIp());
        }
        if (is_array($proxies)) {
            $request->setTrustedProxies($proxies);
        }
        $url = $this->app['url'];
        $url->forceSchema($request->secure() ? "https" : "http");
    }
}