<?php

namespace HealthSlateAdmin\Http\Middleware;

use Closure;
use Log;
use Request;
use Response;

class Authenticate {
	/**
	 * Create a new filter instance.
	 *
	 */
	public function __construct() {

	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 *
	 * @return mixed
	 */
	public function handle( $request, Closure $next ) {
		if ( ! $request->cookie('access_token',false) ) {
			Log::info( 'Unauthorized access detected.' );
			return $this->commonErrorResponseHandler( 'Unauthorized access', '/' );
		}

		if(config('app.log_db_query')) {
			// for dev only
			\DB::enableQueryLog();
			\DB::connection('report_service')->enableQueryLog();
			\DB::connection('groups')->enableQueryLog();
		}

		return $next( $request );
	}

	/**
	 * commonErrorResponseHandler
	 *  a common function for error response generation and redirect from Middleware
	 *
	 * @param $message
	 * @param string $redirect_to
	 *
	 * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	function commonErrorResponseHandler( $message, $redirect_to = "/" ) {
		if ( Request::ajax() ) {
			if ( Request::server( 'HTTP_REFERER', false ) ) {
				session( [ 'url.intended' => Request::server( 'HTTP_REFERER' ) ] );
			}
			if ( request( 'draw' ) ) {
				//  is datatable request
				return Response::json( [ 'draw'            => intval( request( 'draw' ) ),
				                         'recordsTotal'    => 0,
				                         'recordsFiltered' => 0,
				                         'data'            => [ ],
				                         'error'           => $message,
				                         'redirect_to'     => url( $redirect_to )
				] );
			}

			//  is normal ajax request
			return Response::json( [ 'error' => $message, 'redirect_to' => url( $redirect_to ) ] );
		} else {
			session( [ 'url.intended' => request()->fullUrl() ] );
			//  is browser request
			return redirect( $redirect_to );
		}
	}
}
