<?php

namespace HealthSlateAdmin\Http\Middleware;

use Closure;
use Log;

class AfterMiddleware {
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 *
	 * @return mixed
	 */
	public function handle( $request, Closure $next ) {
		$response = $next($request);

		$response->headers->set('Cache-Control', 'no-cache, no-store, max-age=0, must-revalidate');
		$response->headers->set('Expires', 'Fri, 01 Jan 1990 00:00:00 GMT');

		if(config('app.log_db_query')) {
			// for dev only
			$this->logDBQuery();
			$this->logDBQuery('report_service');
			$this->logDBQuery('groups');
		}

		return $response;
	}

	/**
	 * @param null $connection_name
	 */
	private function logDBQuery( $connection_name = null ) {
		$min_time = floatval( config( 'app.slow_db_query_time', 0 ) );
		$logs = ( \DB::connection( $connection_name )->getQueryLog() );
		if ( $count = count( $logs ) ) {
			foreach ( $logs as $query_log ) {
				if ( $query_log['time'] >= $min_time ) {
					info( 'Slow_Query : ' . var_export( $query_log, true ) );
				}
			}
			info( 'Total Queries ' . $connection_name . ' : ' . $count );
		}
	}

	/**
	 * perform action after HTTP response has already been sent to browser.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Illuminate\Http\Response $response
	 */
	public function terminate($request, $response) {
		if(defined('LARAVEL_START')){
			Log::info($request->getMethod() . "[" . $request->path() . "] Execution time : ".round((microtime(true)-LARAVEL_START)*1000,4)." ms");
		}
	}
}
