<?php

namespace HealthSlateAdmin\Http\Middleware;

use Closure;

class UserPermission {
	/**
	 * @param $user_role
	 *
	 * @return \Illuminate\Http\JsonResponse
	 * @throw \Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
	 */
	public function unauthorizedUserHandler( $user_role ) {
		logger( 'Unauthorized user access detected for UserPermission of ' . $user_role );
		if ( request()->ajax() ) {
			//  is normal ajax request
			return response()->json( array( 'error' => trans( 'auth.invalid_user_role' ), 'level' => 'warning' ), 200 );
		}
		throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 *
	 * @return mixed
	 */
	public function handle( $request, Closure $next ) {
		$route_name = $request->route()->getName();
		if ( empty( $route_name ) ) {
			$route_name = $request->path();
		}
		$user               = session( 'user', false );
		$role               = strtoupper( str_replace( ' ', '_', $user->userRole ) );
		$allowed_routes     = config( 'user-permission.allowed_role_routes.' . $role, [ ] );
		$not_allowed_routes = config( 'user-permission.not_allowed_role_routes.' . $role, [ ] );

		if ( ( in_array( $route_name, $not_allowed_routes ) ) || ( ! in_array( '*', $allowed_routes ) && ! in_array( $route_name, $allowed_routes ) ) ) {
			//  user not allowed for this route
			return $this->unauthorizedUserHandler( $role );
		}

		return $next( $request );
	}
}
