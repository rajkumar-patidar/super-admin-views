<?php

namespace HealthSlateAdmin\Http\Middleware;

use Closure;
use HealthSlateAdmin\Models\AuthServiceProvider;
use Request;
use Response;

class AuthenticateToken {

	/**
	 * @var AuthServiceProvider
	 */
	protected $authServiceProvider;

	/**
	 * @param AuthServiceProvider $authServiceProvider
	 */
	public function __construct( AuthServiceProvider $authServiceProvider ) {
		$this->authServiceProvider        = $authServiceProvider;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 *
	 * @return mixed
	 */
	public function handle($request, Closure $next) {
        $user = session('user', false);
		if ( empty( $user ) ) {
			// User is from Portal
			$user = $this->authServiceProvider->getAdminUserByToken( $request->cookie( 'access_token' ) );
			if ( empty( $user ) || isset( $user->errorDetail ) ) {
				// Not able to fetch user by access token
				return $this->commonErrorResponseHandler( 'Unauthorized access, Invalid Token', '/logout' );
			}
			session( [ 'user' => $user ] );
		} else {
			// Check if user token is invalid ?
			//(time() > $user->now + $user->expiresIn) /* Commented for SSO */
			if ( $this->authServiceProvider->isInvalidToken( $user ) ) {
				// not valid, refresh token
				if ( ! $this->authServiceProvider->refreshToken( $user ) ) {
					// refresh token failed
					return $this->commonErrorResponseHandler( 'Unauthorized access, Invalid Token', '/logout' );
				}
			}
		}

        return $next($request);
    }

    /**
	 * commonErrorResponseHandler
	 *  a common function for error response generation and redirect from Middleware
	 *
	 * @param $message
	 * @param string $redirect_to
	 *
	 * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	function commonErrorResponseHandler( $message, $redirect_to = "/" ) {
		if ( Request::ajax() ) {
			if ( Request::server( 'HTTP_REFERER', false ) ) {
				session( [ 'url.intended' => Request::server( 'HTTP_REFERER' ) ] );
			}
			if ( request( 'draw' ) ) {
				//  is datatable request
				return Response::json( [ 'draw'            => intval( request( 'draw' ) ),
				                         'recordsTotal'    => 0,
				                         'recordsFiltered' => 0,
				                         'data'            => [ ],
				                         'error'           => $message,
				                         'redirect_to'     => url( $redirect_to )
				] );
			}

			//  is normal ajax request
			return Response::json( [ 'error' => $message, 'redirect_to' => url( $redirect_to ) ] );
		} else {
			//  is browser request
			return redirect( $redirect_to );
		}
	}
}
