<?php

namespace HealthSlateAdmin\Http\Middleware;

use Closure;
use HealthSlateAdmin\Http\Controllers\CacheServiceController;

class CacheServiceManager
{
	/**
	 * @var CacheServiceController
	 */
	private $cacheServiceController;

	/**
	 * CacheServiceManager constructor.
	 *
	 * @param CacheServiceController $cacheServiceController
	 */
	public function __construct( CacheServiceController $cacheServiceController )
	{

		$this->cacheServiceController = $cacheServiceController;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure                 $next
	 *
	 * @return mixed
	 */
	public function handle( $request, Closure $next )
	{

		if ($this->cacheServiceController->isCacheableRequest()) {
			// caching not disabled globally
			$cached_preference = $this->cacheServiceController->getCachePreference();
			// if caching is enabled for this request?
			if ( ! empty( $cached_preference )) {

				return $this->cacheServiceController->createOrSendCacheResponse( $request, $next, $cached_preference );
			}
		}

		return $next( $request );
	}


}
