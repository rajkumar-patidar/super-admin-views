<?php

namespace HealthSlateAdmin\Http\Middleware;

use Closure;
use Log;
use Redirect;

class RedirectIfAuthenticated {


	/**
	 * Create a new filter instance.
	 *
	 */
	public function __construct() {
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 *
	 * @return mixed
	 */
	public function handle( $request, Closure $next ) {
		if ($request->cookie('access_token',false)) {
			Log::info('User already logged in.');
			return Redirect::route('dashboard')->with('flash_notice', 'You are already logged in!');
		}

		return $next( $request );
	}
}
