<?php

namespace HealthSlateAdmin\Http\Middleware;

use Closure;

class RestCallMiddleware {
	/**
	 * Create a new filter instance.
	 *
	 */
	public function __construct() {

		// Disabling cookie and session for rest call
		config()->set('session.driver', 'array');
		config()->set('cookie.driver', 'array');

		if ( config( 'app.log_db_query' ) ) {
			// for dev only
			\DB::enableQueryLog();
			\DB::connection( 'report_service' )->enableQueryLog();
			\DB::connection( 'groups' )->enableQueryLog();
		}
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 *
	 * @return mixed
	 */
	public function handle( $request, Closure $next ) {
		$rest_token = $request->get( 'rest_token', null );
		//info( 'Validating Rest Call, rest_token: ' . $rest_token );

		return $next( $request );
	}
}
