<?php

namespace HealthSlateAdmin\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \HealthSlateAdmin\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \HealthSlateAdmin\Http\Middleware\VerifyCsrfToken::class,
        \HealthSlateAdmin\Http\Middleware\AfterMiddleware::class,
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \HealthSlateAdmin\Http\Middleware\Authenticate::class,
        'auth.token' => \HealthSlateAdmin\Http\Middleware\AuthenticateToken::class,
        'ajax-filter' => \HealthSlateAdmin\Http\Middleware\AjaxFilterMiddleware::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest' => \HealthSlateAdmin\Http\Middleware\RedirectIfAuthenticated::class,
        'user-permission' => \HealthSlateAdmin\Http\Middleware\UserPermission::class,
        'rest-call' => \HealthSlateAdmin\Http\Middleware\RestCallMiddleware::class,
	    'cache-service' => \HealthSlateAdmin\Http\Middleware\CacheServiceManager::class,
    ];
}
