<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::pattern( 'facility_id', '[0-9]+' );
Route::pattern( 'patient_id', '[0-9]+' );
Route::pattern( 'tech_support_message_id', '[0-9]+' );
Route::pattern( 'days', '[0-9]{1,2}' );
Route::pattern( 'week', '[0-9]{1,2}' );
Route::pattern( 'log_week', '[0-9]{1,2}' );

Route::group( array( 'middleware' => [ 'guest' ] ), function () {

	Route::get( '/', array( 'as' => 'login', 'uses' => 'AuthController@index' ) );

	Route::get( 'login/byToken', array( 'as' => 'login-by-token', 'uses' => 'AuthController@loginByTokenHandler' ) );

	Route::post( '/login', array( 'as' => 'login-handler', 'uses' => 'AuthController@loginHandler' ) );

    Route::get( 'fitbit-steps-mismatch-get', array(
        'as'   => 'fitbit-steps-mismatch-get',
        'uses' => 'v2\\FitbitActivitySteps@fitbitStepsMismatchGet'
    ) );

} );

Route::get( 'logout', array( 'as' => 'logout', 'uses' => 'AuthController@logout' ) );

Route::group( array( 'middleware' => [ 'auth', 'auth.token', 'user-permission' ] ), function () {

	Route::get( '/dashboard', array( 'as' => 'dashboard', 'uses' => 'HomeController@index' ) );

    Route::get('errorlog', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

    Route::get( 'cacheremove', 'CacheServiceController@clearAllCacheKey');

	Route::get( 'facilities', array( 'as' => 'facilities.list', 'uses' => 'HomeController@facilityList' ) );

	Route::get( 'facility/{facility_id}/providers', array( 'as' => 'facility.providers', 'uses' => 'HomeController@facilityProviders' ) );

	Route::get( 'facility/{facility_id}/patients', array( 'as' => 'facility.patients', 'uses' => 'HomeController@facilityPatients' ) );

	Route::any( 'facility/{facility_id}/manage', [ 'as' => 'facility.setting', 'uses' => 'HomeController@facilitySetting' ] );

	Route::get( 'patient/{patient_id}/detail/{facility_id?}', array( 'as' => 'patient.detail', 'uses' => 'HomeController@patientDetail' ) );

	Route::get( 'patient/{patient_id}/messages/{user_id?}', array(
		'as'   => 'patient.user.messages',
		'uses' => 'HomeController@patientUserMessages'
	) );

	Route::get( 'patient/{patient_id}/activities', array(
		'as'   => 'patient.activity.log',
		'uses' => 'HomeController@patientLogs'
	) );
	Route::get( 'patient/{patient_id}/debug-info', array(
		'as'   => 'patient.debug-info',
		'uses' => 'HomeController@patientDebugInfo'
	) );

	Route::get( 'search-patient', array(
		'as'   => 'search.patient',
		'uses' => 'HomeController@searchPatient'
	) );

	Route::get( 'patient/import', array(
		'as'   => 'patient.import',
		'uses' => 'HomeController@patientImport'
	) );
	Route::post( 'patient/enrol', array(
		'as'   => 'patient.enrol',
		'uses' => 'HomeController@patientEnrollment'
	) );

	Route::get( 'manage-report-schedule', array(
		'as'   => 'manage-schedule',
		'uses' => 'ReportSchedule@manageReportSchedule'
	) );

    Route::get( 'fitbit-steps', array(
        'as'   => 'fitbit-steps',
        'uses' => 'ReportController@sendMessages'
    ) );

	Route::get( 'support/tickets', array(
		'as'   => 'support.tickets',
		'uses' => 'SupportController@viewTickets'
	) );

    Route::any( 'cache-email', array(
        'as'   => 'cache-email',
        'uses' => 'CommonController@cacheEmail'
    ) );

	Route::group( array( 'as' => 'report.', 'prefix' => 'report', 'middleware' => [ 'cache-service' ] ), function () {
		Route::get( 'facility-reports/{facility_id?}', [
			'as'   => 'facility-reports',
			'uses' => 'ReportController@facilityReports'
		] );
		Route::get( 'users/active/{facility_id}/{days?}', [
			'as'   => 'facility.users.active',
			'uses' => 'ReportController@activeUsers'
		] );
		Route::get( 'users/entries/{facility_id}/{days?}', [
			'as'   => 'facility.users.entries',
			'uses' => 'ReportController@userEntries'
		] );
		Route::get( 'users/registration/{facility_id}/{days?}', [
			'as'   => 'facility.users.registration',
			'uses' => 'ReportController@userRegistrations'
		] );
		Route::get( 'misfit/steps/{facility_id}/{days?}', [
			'as'   => 'facility.misfit.steps',
			'uses' => 'ReportController@userMisfitSteps'
		] );
		Route::get( 'misfit/never-active/{facility_id}', [
			'as'   => 'facility.misfit.never.active',
			'uses' => 'ReportController@userMisfitNeverActive'
		] );
		Route::get( 'facility/messages/{facility_id}', [
			'as'   => 'facility.messages',
			'uses' => 'ReportController@facilityMessages'
		] );
		Route::get( 'video-calls/{facility_id}/{days?}', [
			'as'   => 'facility.video.calls',
			'uses' => 'ReportController@facilityVideoCalls'
		] );
		Route::get( 'users/lead-coach/{facility_id}', [
			'as'   => 'facility.users.leadCoach',
			'uses' => 'ReportController@facilityUsersLeadCoach'
		] );
		Route::get( 'facility/patient-preferences/{facility_id}', [
			'as'   => 'facility.patient-preferences',
			'uses' => 'ReportController@facilityPatientPreferences'
		] );
		Route::get( 'curriculum/activity/{facility_id}', [
			'as'   => 'facility.curriculum.activity',
			'uses' => 'ReportController@facilityCurriculumActivity'
		] );
		Route::get( 'facility/coach-patients/{facility_id}', [
			'as'   => 'coach.patients',
			'uses' => 'ReportController@facilityCoaches'
		] );
		Route::get( 'facility/weekly-participation/{facility_id}', [
			'as'   => 'weekly.participation',
			'uses' => 'ReportController@weeklyParticipation'
		] );
		Route::get( 'facility/sessions-completed/{facility_id}', [
			'as'   => 'sessions.completed',
			'uses' => 'ReportController@sessionsCompletedReport'
		] );
        Route::get( 'facility/self-register/{facility_id}', [
            'as'   => 'self.register',
            'uses' => 'ReportController@selfRegisterReport'
        ] );
        Route::get( 'facility/registration/{facility_id}', [
            'as'   => 'user.registration',
            'uses' => 'ReportController@usersRegistration'
        ] );
        Route::get( 'facility/import-patient', [
            'as'   => 'import.patient',
            'uses' => 'ReportController@importPatient'
        ] );
		Route::get( 'facility/engagement/{facility_id}', [
			'as'   => 'engagement',
			'uses' => 'ReportController@engagementReport'
		] );
		Route::get( 'facility/coach-engagement/{facility_id}', [
			'as'   => 'coach.engagement',
			'uses' => 'ReportController@coachEngagementReport'
		] );
		Route::get( 'facility/week-cohort/{facility_id}', [
			'as'   => 'week-cohort',
			'uses' => 'ReportController@weekCohortReport'
		] );

        Route::get( 'facility/fitbit-steps-mismatch', array(
            'as'   => 'fitbit-steps-mismatch',
            'uses' => 'v2\\FitbitActivitySteps@fitbitStepsMismatchReport'
        ) );

        Route::get( 'facility/fitbit-steps-mismatch-detail', array(
            'as'   => 'fitbit-steps-mismatch-detail',
            'uses' => 'v2\\FitbitActivitySteps@fitbitStepsMismatchReportDetail'
        ) );

        Route::get( 'facility/fitbit-steps-mismatch-details', array(
            'as'   => 'fitbit-steps-mismatch-details',
            'uses' => 'v2\\FitbitActivitySteps@fitbitStepsMismatchReportDetails'
        ) );

        Route::get( 'facility/week-cohort', [
            'as'   => 'week-cohort-all-facility',
            'uses' => 'CommonController@weekCohortReport'
        ] );

		Route::get( 'facility/week-cohort/{facility_id}/group/{week?}/{coach?}', [
			'as'   => 'week-cohort-patients',
			'uses' => 'ReportController@weekCohortPatients'
		] );
		Route::get( 'facility/week-cohort/{facility_id}/group/{week}/detail/{log_week}', [
			'as'   => 'week-cohort-patients-log',
			'uses' => 'ReportController@weekCohortPatientsLog'
		] );
		Route::get( 'facility/patient-fitbit-activity/{facility_id}', [
			'as'   => 'patient.fitbit.activity',
			'uses' => 'ReportController@patientFitbitActivity'
		] );
		Route::get( 'facility/patient-fitbit-weight/{facility_id}/{days?}', [
			'as'   => 'patient.fitbit.weight',
			'uses' => 'ReportController@patientFitbitWeightReport'
		] );
		Route::get( 'facility/missing-patient-provider/{facility_id}', [
			'as'   => 'missing.patient-provider',
			'uses' => 'ReportController@missingPatientProviderMapping'
		] );

		Route::get( 'facility/member-coach-list/{facility_id}', [
			'as'   => 'facility.member-coach-list',
			'uses' => 'ReportController@memberCoachesListReport'
		] );

		Route::get( 'facility/shipping-addresses/{facility_id}', [
			'as'   => 'facility.shipping-addresses',
			'uses' => 'ReportController@facilityShippingAddresses'
		] );

		Route::get( 'provider/{provider_id}/patient-interaction/{week?}', [
			'as'   => 'coach.patient-interaction',
			'uses' => 'ReportController@coachPatientInteraction'
		] );


		Route::get( 'enrollment/{days?}', [
			'as'   => 'enrollment',
			'uses' => 'ReportController@enrollmentReport'
		] );
		Route::get( 'missing-fitbit-logs/activity/{days?}', [
			'as'   => 'missing-fitbit-log.activity',
			'uses' => 'ReportController@missingFitbitActivityLog'
		] );
		Route::get( 'missing-fitbit-logs/weight/{days?}', [
			'as'   => 'missing-fitbit-log.weight',
			'uses' => 'ReportController@missingFitbitWeightLog'
		] );

		Route::get( 'coach-weekly-participation', array(
			'as'   => 'coach-weekly-participation',
			'uses' => 'ProviderController@providerWeeklyParticipationReport'
		) );

		Route::get( 'feedback-messages', [
			'as'   => 'feedbackMessages',
			'uses' => 'ReportController@feedbackMessages'
		] );
		Route::get( 'patient-without-lead-coach', [
			'as'   => 'noLeadCoachOneonOne',
			'uses' => 'ReportController@patientWithoutLeadCoachOneOnOne'
		] );

		Route::get( 'messages/unreplied', [
			'as'   => 'unRepliedMessages',
			'uses' => 'ReportController@unRepliedMessages'
		] );

        Route::get( 'facility/weight-loss/{facility_id}', [
			'as'   => 'weight.loss',
			'uses' => 'ReportController@weightLossReport'
		] );

		// v2 reports
		Route::get( 'facility/member-group/{facility_id}', array(
			'as'   => 'member-group',
			'uses' => 'v2\\ReportController@memberGroupReport'
		) );

		Route::get( 'members-missing-facility', [
			'as'   => 'members-missing-facility-mapping',
			'uses' => 'v2\\ReportController@memberMissingFacilityMapping'
		] );

		Route::get( 'member-gone-inactive', [
			'as'   => 'member-gone-inactive',
			'uses' => 'v2\\ReportController@memberGoneInactive'
		] );

        Route::get( 'fitbit-invalid-token', [
            'as'   => 'fitbitInvalidToken',
            'uses' => 'ReportController@fitbitInvalidToken'
        ] );

	} );

	Route::group( array( 'as' => 'ajax.', 'prefix' => 'ajax', 'middleware' => [ 'ajax-filter' ] ), function () {

		Route::get( 'facilities', array(
			'as'   => 'facilities.list',
			'uses' => 'AjaxServiceController@facilities'
		) );

        Route::get( 'fitbit-steps', array(
            'as'   => 'fitbit-steps',
            'uses' => 'AjaxServiceController@sendMessages'
        ) );

        Route::post( 'send-messages-to-user', array(
            'as'   => 'send-messages-to-user',
            'uses' => 'v2\\FitbitActivitySteps@sendMessagesToUser'
        ) );

		Route::get( 'facility/{facility_id}/providers', array(
			'as'   => 'facility.providers',
			'uses' => 'AjaxServiceController@facilityProviders'
		) );

		Route::get( 'facility/{facility_id}/patients', array(
			'as'   => 'facility.patients',
			'uses' => 'AjaxServiceController@facilityPatients'
		) );

		Route::post( 'patient/{patient_id}/activities', array(
			'as'   => 'patient.activity.log',
			'uses' => 'AjaxServiceController@patientLogs'
		) );

		Route::get( 'search-patient', array(
			'as'   => 'search.patient',
			'uses' => 'AjaxServiceController@searchPatient'
		) );

		Route::post( 'schedule-alert', array(
			'as'   => 'schedule-alert',
			'uses' => 'ReportSchedule@scheduleAlert'
		) );

		Route::post( 'deactivate-schedule', array(
			'as'   => 'deactivate-alert',
			'uses' => 'ReportSchedule@deleteSchedule'
		) );

		Route::get( 'support/tickets', array(
			'as'   => 'support.tickets',
			'uses' => 'SupportController@ajaxViewTickets'
		) );

		Route::get( 'support/ticket-messages/{tech_support_message_id}', array(
			'as'   => 'support.ticket-messages',
			'uses' => 'SupportController@ajaxTicketMessages'
		) );

		// remove cache key
		Route::post( 'cache/remove', array( 'as' => 'cache-remove', 'uses' => 'CacheServiceController@clearCacheKey' ) );

		// check cache key exists ?
		Route::get( 'cache/key-exists/{cache_key}/{type?}', array( 'as' => 'cache-key-exists', 'uses' => 'CacheServiceController@checkCacheKeyExists' ) );

		Route::group( [ 'as' => 'report.', 'prefix' => 'report', 'middleware' => [ 'cache-service' ] ], function () {
			Route::get( 'feedback-messages', [
				'as'   => 'feedbackMessages',
				'uses' => 'ReportController@ajaxFeedbackMessages'
			] );
			Route::get( 'patient-without-lead-coach', [
				'as'   => 'patientWithoutLeadCoachOneOnOne',
				'uses' => 'ReportController@ajaxPatientWithoutLeadCoachOneOnOne'
			] );

			Route::get( 'messages/unreplied', [
				'as'   => 'unRepliedMessages',
				'uses' => 'ReportController@ajaxUnRepliedMessages'
			] );

			Route::get( 'facility/coach-patients/{facility_id}', [
				'as'   => 'coach.patients',
				'uses' => 'ReportController@ajaxFacilityCoaches'
			] );

			Route::get( 'facility/patient-preferences/{facility_id}', [
				'as'   => 'facility.patient-preferences',
				'uses' => 'ReportingServiceAjaxController@facilityPatientPreferences'
			] );

			Route::any( 'facility/weekly-participation/{facility_id}', [
				'as'   => 'weekly.participation',
				'uses' => 'ReportingServiceAjaxController@weeklyParticipationReport'
			] );

			Route::any( 'facility/sessions-completed/{facility_id}', [
				'as'   => 'sessions.completed',
				'uses' => 'ReportingServiceAjaxController@sessionsCompletedReport'
			] );

            Route::any( 'facility/self-register/{facility_id}', [
                'as'   => 'self.register',
                'uses' => 'ReportingServiceAjaxController@selfRegisterReport'
            ] );

            Route::get( 'facility/registration/{facility_id}', [
                'as'   => 'user.registration',
                'uses' => 'ReportingServiceAjaxController@usersRegistration'
            ] );

            Route::get( 'facility/import-patient', [
                'as'   => 'import.patient',
                'uses' => 'ReportingServiceAjaxController@importPatient'
            ] );

			Route::get( 'coach-weekly-participation', array(
				'as'   => 'coach-weekly-participation',
				'uses' => 'ProviderController@ajaxProviderWeeklyParticipationReport'
			) );

			Route::get( 'facility/engagement/{facility_id}', [
				'as'   => 'engagement',
				'uses' => 'ReportingServiceAjaxController@engagementReport'
			] );

			Route::get( 'facility/coach-engagement/{facility_id}', [
				'as'   => 'coach.engagement',
				'uses' => 'ReportingServiceAjaxController@coachEngagementReport'
			] );
			Route::get( 'facility/week-cohort/{facility_id}', [
				'as'   => 'week-cohort',
				'uses' => 'ReportingServiceAjaxController@weekCohortReport'
			] );
			Route::get( 'facility/week-cohort/{facility_id}/group/{week?}/{coach?}', [
				'as'   => 'week-cohort-patients',
				'uses' => 'ReportingServiceAjaxController@weekCohortPatients'
			] );
			Route::get( 'facility/week-cohort/{facility_id}/group/{week}/detail/{log_week}', [
				'as'   => 'week-cohort-patients-log',
				'uses' => 'ReportingServiceAjaxController@weekCohortPatientsLog'
			] );

            Route::get( 'facility/week-cohort', [
                'as'   => 'week-cohort-all-facility',
                'uses' => 'CommonController@ajaxWeekCohortReport'
            ] );


            Route::get( 'facility/fitbit-steps-mismatch', array(
                'as'   => 'fitbit-steps-mismatch',
                'uses' => 'v2\\FitbitActivitySteps@fitbitStepsMismatch'
            ) );

            Route::get( 'facility/fitbit-steps-mismatch-detail', array(
                'as'   => 'fitbit-steps-mismatch-detail',
                'uses' => 'v2\\FitbitActivitySteps@fitbitStepsMismatchDetail'
            ) );


            Route::get( 'facility/fitbit-steps-mismatch-details', array(
                'as'   => 'fitbit-steps-mismatch-details',
                'uses' => 'v2\\FitbitActivitySteps@fitbitStepsMismatchDetails'
            ) );


			Route::get( 'provider/{provider_id}/patients', array(
				'as'   => 'coach.patient-interaction',
				'uses' => 'ReportController@ajaxCoachPatientInteraction'
			) );

			Route::get( 'facility/shipping-addresses/{facility_id}', [
				'as'   => 'facility.shipping-addresses',
				'uses' => 'ReportingServiceAjaxController@facilityShippingAddresses'
			] );

			Route::get( 'facility/messages/{facility_id}', [
				'as'   => 'facility.messages',
				'uses' => 'ReportingServiceAjaxController@facilityMessagesAjax'
			] );

			Route::get( 'facility/weight-loss/{facility_id}', [
				'as'   => 'weight.loss',
				'uses' => 'ReportingServiceAjaxController@weightLossReportAjax'
			] );

			Route::get( 'facility/patient-fitbit-activity/{facility_id}', [
				'as'   => 'patient.fitbit.activity',
				'uses' => 'ReportingServiceAjaxController@patientFitbitActivity'
			] );
		} );

	} );
} );

Route::group( [ 'prefix' => 'internal', 'middleware' => [ 'rest-call' ] ], function () {
	Route::get( 'report/weekly-participation', [
		'as'   => 'internal.weekly.participation',
		'uses' => 'ReportingServiceAjaxController@apiGroupParticipationReport'
	] );
} );


Route::get( 'solerapi/callback', [
	'as'   => 'solerapi',
	'uses' => 'v2\\SoleraApiIntegration@index'
] );

// Application config dumper route, Note: for debug purpose only
// Route::get( 'dumpConfig/{key}', array( 'as' => 'dumpConfig', 'uses' => 'HomeController@dumpConfig' ));