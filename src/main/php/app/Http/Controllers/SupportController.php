<?php

namespace HealthSlateAdmin\Http\Controllers;

use Carbon\Carbon;
use HealthSlateAdmin\Helpers\DataTableServiceProvider;
use HealthSlateAdmin\Models\SupportTicket;
use HealthSlateAdmin\Models\TechSupportMessage;
use Illuminate\Http\Request;

use HealthSlateAdmin\Http\Requests;
use HealthSlateAdmin\Http\Controllers\Controller;

class SupportController extends Controller {

	/**
	 *  common variable for passing data to view
	 * @var array
	 */
	protected $data = array();

	/**
	 * @var DataTableServiceProvider
	 */
	protected $dataTableService;

	/**
	 * @var SupportTicket
	 */
	protected $supportTicket;

	/**
	 * @var TechSupportMessage
	 */
	protected $techSupportMessage;

	/**
	 * @param DataTableServiceProvider $dataTableService
	 * @param SupportTicket $supportTicket
	 * @param TechSupportMessage $techSupportMessage
	 */
	function __construct( DataTableServiceProvider $dataTableService, SupportTicket $supportTicket, TechSupportMessage $techSupportMessage ) {
		$this->dataTableService   = $dataTableService;
		$this->supportTicket      = $supportTicket;
		$this->techSupportMessage = $techSupportMessage;
	}

	/**
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function viewTickets() {
		return view( 'support.tickets', $this->data );
	}

	/**
	 * @return \Illuminate\Http\JsonResponse|mixed
	 */
	public function ajaxViewTickets() {
		$data                = collect();
		$is_download_request = $this->isTableDownloadRequest();
		$result              = $this->supportTicket
								->with( [
									'techSupportMessage',
									'techSupportMessage.patient',
									'techSupportMessage.patient.user'
								] )
								->status( true )
								->orderBy( 'date_opened' )->get();
		foreach ( $result as $row ) {
			if ( ! empty( $row->techSupportMessage ) ) {
				$tr = [
					"support_ticket_id"             => $row->support_ticket_id,
					"description"                   => $row->description,
					"patient_id"                    => $row->techSupportMessage->patient_id,
					"patient"                       => empty( $row->techSupportMessage->patient ) ? '' : @$row->techSupportMessage->patient->user->full_name,
					"date_opened"                   => ( $row->date_opened instanceof Carbon ) ? '<span class="timestamp" data-timestamp="' . $row->date_opened->timestamp . '"></span>' : '',
					'is_open'                       =>  @$row->is_active,
					"messages_link"                 =>  route('ajax.support.ticket-messages', ['tech_support_message_id' => $row->tech_support_message_id]),
					"date_opened_date_time"         => ( $row->date_opened instanceof Carbon ) ? $row->date_opened->format( config( 'healthslate.default_date_time_format' ) ) : null,
					"date_closed"                   => ( $row->date_closed instanceof Carbon ) ? $row->date_closed->format( config( 'healthslate.default_date_time_format' ) ) : null,
					"tech_support_message_id"       => $row->tech_support_message_id,
					"close_tech_support_message_id" => $row->close_tech_support_message_id,
				];
				if ( $is_download_request ) {
					unset( $tr['date_opened'] );
					unset( $tr['messages_link'] );
				}
				$data->push( ( $is_download_request ? $tr : ( $tr ) ) );
			}
		}

		return $this->dataTableService->dataTableResponse( $data, $is_download_request );
	}

	/**
	 * @param $tech_support_message_id
	 *
	 * @return \Illuminate\Http\JsonResponse
	 * @throws \Exception
	 * @throws \Throwable
	 */
	public function ajaxTicketMessages( $tech_support_message_id ) {

		$support_message = $this->techSupportMessage->find( $tech_support_message_id );

		// is $tech_support_message_id is valid?
		if ( ! empty( $support_message ) ) {
			$messages = $this->techSupportMessage
				->with( [
					'providerUser',
					'patient',
					'patient.user',
				] )
				->where( 'patient_id', '=', @$support_message->patient_id )
				->where( 'message_id', '>=', @$support_message->message_id )
				->orderBy( 'timestamp', 'asc' )
				->get();

			$this->data['messages'] = $messages;
		}

		return response()->json( [
			'success' => true,
			'html'    => view( 'support.ticket-messages', $this->data )->render()
		] );

	}
}
