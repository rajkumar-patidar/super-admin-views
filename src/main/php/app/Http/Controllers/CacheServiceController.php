<?php
/**
 * Created by PhpStorm.
 * User: spatel
 * Date: 11/1/17
 * Time: 6:23 PM
 */

namespace HealthSlateAdmin\Http\Controllers;

use Cache;
use Closure;
use HealthSlateAdmin\Helpers\DataTableServiceProvider;
use HealthSlateAdmin\Models\ReportService\CachePreference;
use Illuminate\Http\JsonResponse;
use HealthSlateAdmin\Helpers\MailServiceHelper;
use Request;
use Session;
use Cookie;

class CacheServiceController extends Controller
{
	/**
	 * @var CachePreference
	 */
	private $cachePreference;

	/**
	 * @var float|string
	 */
	private $cacheWaitTime = 'few';

	/**
	 * Minutes of auto check for cache status
	 * @var float
	 */
	private $cacheAutoRefreshTime = 1;

	/**
	 * @var DataTableServiceProvider
	 */
	private $dataTableServiceProvider;

	/**
	 * @param CachePreference          $cachePreference
	 * @param DataTableServiceProvider $dataTableServiceProvider
	 */

    /**
     * @var MailServiceHelper
     */
    protected $mailServiceHelper;

	function __construct( CachePreference $cachePreference, DataTableServiceProvider $dataTableServiceProvider, MailServiceHelper $mailServiceHelper )
	{
		$this->cachePreference          = $cachePreference;
		$this->dataTableServiceProvider = $dataTableServiceProvider;
		$this->isAjax                   = request()->ajax();
		$this->currentRouteName         = request()->route()->getName();
		$this->isExportRequest          = $this->dataTableServiceProvider->isExportRequest();
        $this->mailServiceHelper        = $mailServiceHelper;
	}

	/**
	 * @return bool
	 */
	public function isCacheableRequest()
	{
		// Check if global configuration is allowing caching
		// Now we want caching of file export also. So isAjax will be false. So removing it
		if (! ( config( 'healthslate.disable_report_cache' ) )) {
			return true;
		}
		return false;
	}

	/**
	 * @return mixed
	 */
	public function getCachePreference()
	{
		$cached_preference = $this->cachePreference->where( 'report_identifier', '=', $this->currentRouteName )->first();
		info( 'Processing Route "' . $this->currentRouteName . '" Cache Preference Found? ' . ( ! empty( $cached_preference ) ? $cached_preference->cache_duration . ' sec' : '-' ) );

		return $cached_preference;
	}

	/**
	 * @return array
	 */
	public function clearCacheKey()
	{
		$key = request( 'cache_key' );
		if ( ! empty( $key )) {
			Cache::forget( $key );
			info( 'Removed Cache Key: ' . $key );
		}

		return [ 'success' => true, 'reload' => true ];
	}

    /**
     * @return array
     */
    public function clearAllCacheKey()
    {
        Cache::flush();
        dd('Done');
    }

	/**
	 * @param        $key
	 * @param string $type
	 *
	 * @return array
	 */
	public function checkCacheKeyExists( $key, $type = '' )
	{
		$exists = Cache::has( $key );
		info( 'Cache "' . $type . '" Key ' . $key . ' Exists? ' . ( $exists ? 'Y' : 'N' ) );

		if ( $exists ) {

            $this->sendEmailToUser($key);

			return [ 'success' => true, 'type' => $type ];
		} else {
			return [
				'cache_in_progress' => route( 'ajax.cache-key-exists', [ 'cache_key' => $key, 'type' => $type ] ),
				'wait'              => $this->cacheAutoRefreshTime,
				'type'              => $type
			];
		}
	}


    /**
     * @param  $key
     * @param string $type
     * @return array
     */
	public function sendEmailToUser($key = '')
    {
        if ( Cookie::get('cache_email_send', 0) == 1 ) {
            $title = Session::get($key, null);
            if(Session::has($key) && $title != null)
            {
                $to = session('user')->userName;
                info( 'Email Sending Of Caching  to : ' . $to );
                $subject = 'Cached '. $title;
                $message = "<body><p>Hello,</p> <br/><div> $title Cached. Please <a href=".Request::header('referer').">Click Here</a> to view </div><br/><p> Sincerely,<br/>The HealthSlate Team</p></body>";
                $body    = [
                    'content' => $message,
                ];
                //$to = 'rajkumar.patidar62@gmail.com';
                $status = $this->mailServiceHelper->sendEmail( $subject, $body, trim( $to ) );
                info( 'Email Status : ' . $status );
            }
        }
    }


	/**
	 * @param          $request
	 * @param callable $next
	 * @param          $cached_preference
	 *
	 * @return JsonResponse|void
	 */
	public function createOrSendCacheResponse( $request, Closure $next, $cached_preference )
	{
		$cache = $this->getCurrentRouteCache();

		if ( ! empty( $cache->content )) {
			// cache response found
			return $this->sendCachedResponse( $cache );
		} else {
			// not found, echo processing response and create cache.
			info( 'Sending Processing Response' );
			$this->sendCacheBuildingResponse( $cache );

			info( 'Building data for cache, ' . $cache->name );
			// Forcing DataTableServiceProvider to create file instead of export
			request()->merge( [ '_store' => 'yes' ] );
			$response = $next( $request );
			if($this->isExportRequest && $response instanceof JsonResponse){
				// Response is for export request and is Json object
				$fileInfo = $response->getData();
				$fileInfo->cache_type = 'file_export';
				$response->setData($fileInfo);
			}
			info( 'Cache data prepared, storing ' . $cache->name );
			$response->headers->set( 'Cached-On', time() * 1000 );
			$response->headers->set( 'Cache-Key', $cache->name );
            Session::put( $cache->name, request( 'report_title', null ) );
			Cache::put( $cache->name, $response, $cached_preference->cache_duration / 60 );

			return null;
		}

	}

	/**
	 * Echo Processing Response json & headers
	 *
	 * @param $cache
	 *
	 * @return bool
	 */
	private function sendCacheBuildingResponse( $cache )
	{
		// Buffer all upcoming output...
		ob_start();

		// This will help in js, to toggle Cached Content message
		$type = ( $this->isExportRequest ? 'file_export' : '' );

		$html_message = trans_choice( 'common.processing_cache', $this->cacheWaitTime,
				[ 'report' => 'report', 'n' => $this->cacheWaitTime ] ) . ' <span class="checking_cache_msg"></span>';

		$response = [
			'message'           => $html_message,
			'cache_in_progress' => route( 'ajax.cache-key-exists', [ 'cache_key' => $cache->name, 'type' => $type ] ),
			'wait'              => $this->cacheAutoRefreshTime,
			'type'              => $type
		];
		if (request( 'draw' )) {
			//  is datatable request
			$response = $response + [
					'draw'            => intval( request( 'draw' ) ),
					'recordsTotal'    => 0,
					'recordsFiltered' => 0,
					'data'            => [ ],
				];
		}
		// Send your response.
		echo json_encode( $response );
		// Get the size of the output.
		$size = ob_get_length();

		// Disable compression (in case content length is compressed).
		header( "Content-Encoding: none" );
		header( "Content-Type: application/json" );

		// Set the content length of the response.
		header( "Content-Length: {$size}" );

		// Close the connection.
		header( "Connection: close" );

		// Flush all output.
		ob_end_flush();
		ob_flush();
		flush();

		// Close current session (if it exists).
		if (session_id()) {
			session_write_close();
		}
		return true;
	}

	/**
	 *
	 * @return object
	 */
	private function getCurrentRouteCache()
	{
		$user    = session( 'user' );
		$request = request()->except( [ '_', 'draw' ] );

        $name = $this->currentRouteName .'.'. request()->route()->getParameter('facility_id') . '_' . md5( json_encode( $request ) ) . '_' . $user->userName;

        $safe_name = preg_replace( '/[^a-zA-Z0-9\-_]/', '_', $name );

		$tag = $this->currentRouteName . '_' . $user->userName;
		$tag = preg_replace( '/[^a-zA-Z0-9\-_]/', '_', $tag );

		return (object) [ 'name' => $safe_name, 'tags' => [ $tag ], 'content' => Cache::get( $safe_name ) ];
	}

	/**
	 * @param $cache
	 *
	 * @return JsonResponse
	 */
	private function sendCachedResponse( $cache )
	{
		info( 'Cache data found ' . $cache->name );
		if ($cache->content instanceof JsonResponse) {
			$data = ( $cache->content->getData() );
			if (isset( $data->draw )) {
				// is DataTable Response
				info( 'JsonResponse Found, has draw: ' . $data->draw . ' But need draw: ' . request( 'draw' ) );
				$data->draw = request( 'draw', $data->draw );
				$cache->content->setData( $data );
			}
			else if(isset( $data->cache_type ) && $data->cache_type == 'file_export'){
				// is CSV Export Response
				info('Export file meta: ', (array) $data );
				info('Export file exists?', [@file_exists(@$data->full)]);
				return response()->download(@$data->full, null, ['Content-Type' => 'text/csv']);
			}

		}
		return $cache->content;
	}
}