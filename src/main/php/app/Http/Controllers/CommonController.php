<?php

namespace HealthSlateAdmin\Http\Controllers;

use HealthSlateAdmin\Http\Controllers\Controller;
use HealthSlateAdmin\Models\CurriculumServiceProvider;
use HealthSlateAdmin\Models\Facility;
use HealthSlateAdmin\Models\Healthslate;
use Carbon\Carbon;
use HealthSlateAdmin\Models\Provider;
use Illuminate\Support\Str;
use HealthSlateAdmin\Helpers\DataTableServiceProvider;
use Log;
use Validator;
use Request;
use Cookie;

class CommonController extends Controller {

	/**
	 *  common variable for passing data to view
	 * @var array
	 */
	protected $data = [];

	/**
	 * @var Healthslate
	 */
	protected $healthslate_model;

	/**
	 * @var DataTableServiceProvider
	 */
	protected $dataTableService;

	/**
	 * @var Facility
	 */
	protected $facility;

	/**
	 * @var Provider
	 */
	protected $provider;

	/**
	 * @var CurriculumServiceProvider
	 */
	protected $curriculumServiceProvider;

	/**
	 * @param Healthslate $healthslate
	 * @param DataTableServiceProvider $dataTableService
	 * @param Facility $facility
	 * @param Provider $provider
	 * @param CurriculumServiceProvider $curriculumServiceProvider
	 */
	function __construct( Healthslate $healthslate, DataTableServiceProvider $dataTableService,
		Facility $facility, Provider $provider, CurriculumServiceProvider $curriculumServiceProvider
	) {
		$this->healthslate_model         = $healthslate;
		$this->dataTableService          = $dataTableService;
		$this->facility                  = $facility;
		$this->curriculumServiceProvider = $curriculumServiceProvider;
		$this->provider                  = $provider;
	}


    /**
     * @param $facility_id
     *
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function weekCohortReport() {

        $this->data['css'] = ['datetime', 'select2'];
        $this->data['js'] = ['datetime', 'select2'];

        $this->data['facility_id'] = 40;
        $this->data['show_duration_filter'] = true;
        $this->data['hide_duration_filter_dropdown'] = true;

        $facilities = $this->facility->orderBy( 'name' )->get();
        $facilities = $facilities->pluck('name','facility_id');

        $facility_list = [];
        foreach($facilities as $key => $value){
            $facility_list[] = (object) [
                'id'        => $key,
                'text'      => $value,
            ];
        }
        $this->data['facilities'] = $facility_list;

        $this->data['report_src'] = route('ajax.report.week-cohort-all-facility' );

        $this->data['report_meta'] = (object) [
            'title'      => 'Week Cohort Report',
            'body_class' => 'weekly-participation'
        ];
        $coaches = collect($this->healthslate_model->get_facility_coach(false,true))->sortBy('full_name');

        $coach_list = [];
        foreach($coaches as $coach){
            $coach_list[] = (object) [
                'id'    => $coach->provider_id,
                'text'    => $coach->full_name,
                'first_name'    => $coach->first_name,
                'last_name'    => $coach->last_name,
                'type'    => $coach->type
            ];
        }
        $this->data['coach_list'] = $coach_list;
        return view( 'report.common-duration-report', $this->data );


    }


    /**
     * Ajax Handler for Weekly Participation Report
     *
     * @param Validator $validator
     * @param $facility_id
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Throwable
     */
    public function ajaxWeekCohortReport(Validator $validator) {
        ini_set('memory_limit','-1');
        $start          = request( 'start_date', null );
        $end            = request( 'end_date', null );
        $coach          = request( 'coach', 0);
        $facility_id    = request( 'facility_id', 0 );
        $duration_label = request( 'duration_label' );

        $validator = $validator::make(
            request()->input(), array(
            'start_date' => 'required|date_format:m/d/Y',
            'end_date'   => 'required|date_format:m/d/Y',
            'coach'   => 'numeric'
        ) );
        $response = [];
        if ( $validator->passes() ) {
            $start = Carbon::createFromFormat( 'm/d/Y', $start )->setTime( 0, 0 );
            $end   = Carbon::createFromFormat( 'm/d/Y', $end )->setTime( 0, 0 )->addDay( 1 );

            Log::info( 'Loading Weekly Participation report for: ' . $start . ' - ' . $end );



            $is_download_request = $this->isTableDownloadRequest();
            $result              = collect( $this->healthslate_model->get_week_cohorts( $facility_id , $coach) );
            $result              = $result->groupBy( 'week' );
            $data                = collect();
            // we would have done this using mysql query
            // but it would not possible to calculate and debug Group weight loss percentage.
            foreach ( $result as $week => $group ) {
                info('Week Group: '.$week);
                $row = (object) [
                    'week_group'             => $week,
                    'patients'               => $group->count(),
                    'active_patients'        => $group->filter(function ($item){
                        return $this->isActivePatient($item);
                    })->count(),
                    'num_log_entries'        => $group->sum('num_log_entries'),
                    'num_weight_entries'     => $group->sum('num_weight_entries'),
                    'weight_loss_percentage' => '',
                ];
                if ( ! empty( $group ) ) {
                    $weight_loss = $this->getWeightLossByGroup($group);
                    $row->weight_loss_percentage = $weight_loss->goal_completion_percentage;
                }
                if(!$is_download_request){
                    $row->link = route('report.week-cohort-patients', ['facility_id' =>  $facility_id, 'week' => $week, 'coach' => $coach]);
                }
                info('End of Week Group');
                //$row = (array) $row;
                $data->push( ( $is_download_request ? $row : $row ) );
            }

            // is_download_request ?
            if ( $is_download_request) {
                return $this->dataTableService->dataTableResponse($data, $is_download_request);
            }

            //dd($data);

            $this->data['paginated_result'] = $data;
            $this->data['init_datatable'] = true;
            $this->data['test_patient_excluded_note'] = true;
            $this->data['result_headers'] = (object) [
                'week_group'                => [ 'name' => "Week Group" ],
                'patients'                  => [ 'name' => "Users" ],
                'active_patients'           => [ 'name' => "Active Users(<= 3d)" ],
                'num_log_entries'           => [ 'name' => "# of Log Entries" ],
                'num_weight_entries'        => [ 'name' => "# of Weight Entries" ],
                'weight_loss_percentage'    => [ 'name' => "Weight Loss %" ],
                'link'                      => [ 'name' => "View" ],

            ];
            $response = [
                'start_date'  => $start,
                'end_date'  => $end,
                'html' => view( 'report.blocks.common-duration-report', $this->data )->render(),
                'page_description' => $duration_label ? 'Filtered for '.$duration_label : '',
                'success' => true,
                'total_records' => count($data)
            ];
        }
        else{
            $response = [
                'error' => $validator->errors()
            ];
        }
        return response()->json( $response );
    }





    private function getWeightLossByGroup($patients, $no_data_message = 'Group target not set'){
        /*$patients = [
            (object) [ 'starting_weight' => 120, 'current_weight' => 110, 'target_weight' => 90],
            (object) [ 'starting_weight' => 130, 'current_weight' => 140, 'target_weight' => 100],
            (object) [ 'starting_weight' => 130, 'current_weight' => 130, 'target_weight' => 100],
            (object) [ 'starting_weight' => 150, 'current_weight' => 100, 'target_weight' => 120],
            (object) [ 'starting_weight' => 200, 'current_weight' => 150, 'target_weight' => 150],
            (object) [ 'starting_weight' => 150, 'current_weight' => 141, 'target_weight' => 100],
        ];*/

        $group_weight_loss          = (object) [ ];
        $weight_loss                = 0;
        $target_weight_loss         = 0;
        $goal_completion_percentage = 0;
        foreach ( $patients as $key => $patient ) {

            $starting_weight = floatval( $patient->starting_weight );
            $current_weight  = floatval( $patient->current_weight );
            $target_weight   = floatval( $patient->target_weight );

            if ( ! empty( $current_weight ) && $current_weight > 0 ) {
                // only +ve loss is counted towards group goal
                // ony till 100% is counted if done more than 100% loss.
                if($starting_weight >= $current_weight) {
                    if ( $current_weight >= $target_weight ) {
                        $weight_loss_temp = $starting_weight - $current_weight;

                    } else {
                        $weight_loss_temp = $starting_weight - $target_weight;

                    }
                    $weight_loss        = $weight_loss + $weight_loss_temp;
                    $target_weight_loss_temp = $starting_weight - $target_weight;
                    $target_weight_loss = $target_weight_loss + $target_weight_loss_temp;
                    info( 'Processing : ' . $key );
                    info( 'patient_id: ' . @$patient->patient_id . ' weight_loss: ' . $weight_loss_temp . ', target_weight_loss: ' . $target_weight_loss_temp . '  weight loss %: ' . ( $target_weight_loss_temp ? ( ( $weight_loss_temp / $target_weight_loss_temp ) * 100 ) : 0 ) );
                }
            }
        }
        info('Total weight_loss: '.$weight_loss. ' Total target_weight_loss: '.$target_weight_loss);
        if ( $weight_loss && $target_weight_loss ) {
            $goal_completion_percentage = ( $weight_loss / $target_weight_loss ) * 100;
        }
        else if($weight_loss == 0 && $target_weight_loss > 0 ){
            $goal_completion_percentage = 0;
        }
        else {
            $goal_completion_percentage = $no_data_message;
        }

        if ( is_numeric($goal_completion_percentage) && $goal_completion_percentage > 100 ) {
            $goal_completion_percentage = 100;
        }
        $group_weight_loss->total_weight_loss          = format_weight($weight_loss);
        $group_weight_loss->goal_completion_percentage = is_numeric($goal_completion_percentage) ? format_weight($goal_completion_percentage) : $goal_completion_percentage;

        info(json_encode($group_weight_loss));
        return $group_weight_loss;
    }

    /**
     *
     * Determine if patient is Active user on basis of last log date
     * @param $item
     * @param null|int $log_week
     *
     * @return bool
     */
    private function isActivePatient($item, $log_week = null){
        if ( ! empty( $item->last_entry ) && isset( $item->num_days_last_entry ) ){
            info( 'patient_id: ' . @$item->patient_id . ' Last Log Entry : ' . $item->last_entry.' num_days_last_entry: ' . intval( $item->num_days_last_entry ) );
            return (intval( $item->num_days_last_entry ) <= 3 ) ;
        }
        else if ($log_week && ! empty( $item->last_entry ) && ! empty( $item->password_created_date ) ) {
            // calculation of log week end date
            $password_created_date = Carbon::createFromFormat( 'Y-m-d H:i:s', $item->password_created_date )->setTime( 0, 0 );
            $last_entry            = Carbon::createFromFormat( 'Y-m-d H:i:s', $item->last_entry );
            $end_date              = $password_created_date->copy()->addWeeks( intval( $log_week ) );
            //$start_date          = $end_date->copy()->subWeeks(1);
            $num_days_last_entry = $last_entry->diffInDays( $end_date );
            info( 'patient_id: ' . @$item->patient_id . ' log week end date: ' . $end_date . ' Last Log Entry : ' . $item->last_entry . ' num_days_last_entry: ' . intval( $num_days_last_entry ) );

            return ( $num_days_last_entry <= 3 );
        }
        return false;
    }



    // To set cookies for send cache email send or not
    public function cacheEmail()
    {
        $this->data['email_send'] = 0;
        if(Request::isMethod('post'))
        {
            $email_send = request( 'email_send', 0 );
            $this->data['email_send'] = $email_send;

            Cookie::queue(Cookie::make('cache_email_send', $email_send));
            Cookie::queue(Cookie::make('expires_in', time() + (10 * 365 * 24 * 60 * 60)));
            return redirect()->route('cache-email');
        }
        else
        {
            $this->data['email_send'] = Cookie::get('cache_email_send', 0);
        }

        return view( 'message.cache_email_send', $this->data );
    }



}
