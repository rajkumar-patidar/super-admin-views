<?php
/**
 * User: spatel
 * Date: 29/3/16
 */

namespace HealthSlateAdmin\Http\Controllers;

use Carbon\Carbon;
use HealthSlateAdmin\Http\Controllers\Controller;
use HealthSlateAdmin\Models\CurriculumServiceProvider;
use HealthSlateAdmin\Helpers\DataTableServiceProvider;
use HealthSlateAdmin\Models\Healthslate;
use HealthSlateAdmin\Models\Common;
use HealthSlateAdmin\Models\Groups\HealthslateGroups;
use Log;
use Validator;

class ReportingServiceAjaxController extends Controller {

	/**
	 *  common variable for passing data to view
	 * @var array
	 */
	protected $data = array();
	/**
	 * @var Healthslate
	 */
	protected $healthslate_model;
	/**
	 * @var DataTableServiceProvider
	 */
	protected $dataTableService;
	/**
	 * @var CurriculumServiceProvider
	 */
	protected $curriculumServiceProvider;
	/**
	 * @var HealthslateGroups
	 */
	protected $healthSlate_group;

    protected $common_model;
	/**
	 * ReportingServiceAjaxController constructor.
	 *
	 * @param Healthslate $healthslate
	 * @param DataTableServiceProvider $dataTableService
	 * @param CurriculumServiceProvider $curriculumServiceProvider
	 * @param HealthslateGroups $healthSlateGroup
	 */
	function __construct( Common $common_model,
		Healthslate $healthslate, DataTableServiceProvider $dataTableService,
		CurriculumServiceProvider $curriculumServiceProvider, 
                HealthslateGroups $healthSlateGroup
	) {
		$this->healthslate_model         = $healthslate;
		$this->dataTableService          = $dataTableService;
		$this->curriculumServiceProvider = $curriculumServiceProvider;
        $this->healthSlate_group         = $healthSlateGroup;
        $this->common_model              = $common_model;
	}

	/**
	 * Ajax Handler for Weekly Participation Report
	 *
	 * @param Validator $validator
	 * @param $facility_id
	 *
	 * @return \Illuminate\Http\JsonResponse
	 * @throws \Exception
	 * @throws \Throwable
	 */
	public function weeklyParticipationReport(Validator $validator, $facility_id ) {
        ini_set('memory_limit','-1');
        ini_set('max_execution_time', 0);
		$start = request( 'start_date', null );
		$end   = request( 'end_date', null );
		$coach   = request( 'coach', null );
		$duration_label   = request( 'duration_label' );

		$validator = $validator::make(
			request()->input(), array(
			'start_date' => 'required|date_format:m/d/Y',
			'end_date'   => 'required|date_format:m/d/Y',
			'coach'   => 'numeric'
		) );
		$response = [];
		if ( $validator->passes() ) {
			$start = Carbon::createFromFormat( 'm/d/Y', $start )->setTime( 0, 0 );
			$end   = Carbon::createFromFormat( 'm/d/Y', $end )->setTime( 0, 0 )->addDay( 1 );

			Log::info( 'Loading Weekly Participation report for: ' . $start . ' - ' . $end );
			$is_download_request = $this->isTableDownloadRequest();

            $weekly_participation_report = collect( $this->common_model->get_weekly_participation_report_for_patient(  $facility_id, $start, $end, $coach ) );
            $patient_details = collect( $this->common_model->get_weekly_participation_report_for_patient_by_patientids( $facility_id, $start, $end , $weekly_participation_report->pluck( 'patient_id' )->toArray()) );

            foreach ($weekly_participation_report as $r_key => $row) {
                foreach ($patient_details as $section_data) {
                    if($row->patient_id == $section_data->patient_id)
                    {
                        $row->no_of_weeks_logged_weight = $section_data->no_of_weeks_logged_weight;
                        $row->total_logs = $section_data->total_logs;
                        $row->manual_activity_minutes = $section_data->manual_activity_minutes;
                        $row->no_of_manual_activity_days = $section_data->no_of_manual_activity_days;
                        $row->no_of_manual_activity_log = $section_data->no_of_manual_activity_log;
                        $row->no_of_fitbit_activity_logs = $section_data->no_of_fitbit_activity_logs;
                        $row->total_steps_synced = $section_data->total_steps_synced;
                        $row->no_of_fitbit_activity_days = $section_data->no_of_fitbit_activity_days;
                        $row->avg_steps_per_day = $section_data->avg_steps_per_day;
                        $row->current_weight = $section_data->current_weight;
                        $row->no_of_days_weight_logged = $section_data->no_of_days_weight_logged;
                        $row->sent_msg = $section_data->sent_msg;
                        $row->no_of_meal_log = $section_data->no_of_meal_log;
                        $row->no_of_weight_log = $section_data->no_of_weight_log;
                        $row->no_of_goal_logged = $section_data->no_of_goal_logged;
                        $row->glasses_of_water_consumed = $section_data->glasses_of_water_consumed;
                        $row->one_to_one_sessions = $section_data->one_to_one_sessions;
                    }
                }
            }

			//$weekly_participation_report = $this->common_model->get_weekly_participation_report_for_duration( $facility_id, $start, $end, $coach );

            Log::info( 'Total records in Weekly Participation report are: ' . count( $weekly_participation_report ) );
			$weekly_participation_report = $this->curriculumServiceProvider->getEducationModulesCompleted( $weekly_participation_report, $start, $end, false );

			$patient_ids = collect($weekly_participation_report)->pluck( 'patient_id' )->toArray();
			$patient_weight_logs_per_week = [];
			$users_post_count = [];
			if(!empty($patient_ids)){
				$patient_weight_logs_per_week = collect($this->healthslate_model->get_patient_weeks_weight_logged($patient_ids, $end));
				$patient_weight_logs_per_week = $patient_weight_logs_per_week->groupBy('patient_id');

				$patients_min_weight = collect( $this->healthslate_model->get_patient_minimum_weight( $patient_ids));
                $patients_min_weight = $patients_min_weight->groupBy("patient_id");

                $user_ids  = collect($weekly_participation_report)->pluck( 'user_id' )->toArray();
                $users_post_count = collect($this->healthSlate_group->get_patient_group_wall_post_count($user_ids, $start, $end));
                $users_post_count = $users_post_count->groupBy('user_id');
			}
                        
                        
                        
			// is_download_request ?
			if ( $is_download_request && ! empty( $weekly_participation_report ) ) {
				// send all records
				info( 'Building data source for export' );
				$time = microtime( true );
				foreach ( $weekly_participation_report as $row ) {
                    $user_id = $row->user_id;
					// removing last name for coach report export only
					if(!is_null(request('_patient_hyperlink'))){
						unset( $row->last_name );
					}
                    unset( $row->user_id );
                    unset( $row->uuid );
                    unset( $row->diabetes_type_id );
                    unset( $row->no_of_manual_activity_log );
                    unset( $row->no_of_fitbit_activity_logs );

                    $row->completed_units = isset( $row->completed_units ) ? count( $row->completed_units ) : '';
                    $row->viewed_units    = isset( $row->viewed_units ) ? ( $row->viewed_units ) : '';

					$row  = $this->getWeightLossData( $row );

					$patient_min_weight_log = isset($patients_min_weight[$row->patient_id]) ? $patients_min_weight[$row->patient_id][0] : null;
	                if(!empty($patient_min_weight_log) && isset($patient_min_weight_log->min_weight)){
	                        $row->min_weight = $patient_min_weight_log->min_weight;
	                }else{
	                    $row->min_weight = 0;
	                }
	                //user Group wall count
	                $patient_group_wall_count = isset($users_post_count[$user_id]) ? $users_post_count[$user_id][0] : null;
	                if(!empty($patient_group_wall_count) && isset($patient_group_wall_count->post_count)){
	                        $row->post_count = $patient_group_wall_count->post_count;
	                }else{
	                        $row->post_count = 0;
	                }
				}

				info('Created data set of weeklyParticipationReport '.count($weekly_participation_report)." time: " . round( ( microtime( true ) - $time ) * 1000, 2 ) . " ms");
				return $this->dataTableService->dataTableResponse( collect( $weekly_participation_report ), true );
			}

			info( 'Building data source for table' );
			$time = microtime( true );
			foreach ( $weekly_participation_report as $row ) {
				$row->full_name       = ( $row->first_name . ' ' . $row->last_name );
				$row                  = $this->getWeightLossData( $row );
				$row->completed_units = isset( $row->completed_units ) ? count( $row->completed_units ) : '';
				$row->viewed_units    = isset( $row->viewed_units ) ? ( $row->viewed_units ) : '';
				$row->no_of_weeks_logged_weight = isset($patient_weight_logs_per_week[$row->patient_id]) ? $patient_weight_logs_per_week[$row->patient_id]->count() : 0;

	            $patient_min_weight_log = isset($patients_min_weight[$row->patient_id]) ? $patients_min_weight[$row->patient_id][0] : null;
	            if(!empty($patient_min_weight_log) && isset($patient_min_weight_log->min_weight)){
	                    $row->min_weight = $patient_min_weight_log->min_weight;
	            }else{
	                    $row->min_weight = 0;
	            }
	            //user Group wall count
	            $patient_group_wall_count = isset($users_post_count[$row->user_id]) ? $users_post_count[$row->user_id][0] : null;
	            if(!empty($patient_group_wall_count) && isset($patient_group_wall_count->post_count)){
	                    $row->post_count = $patient_group_wall_count->post_count;
	            }else{
	                    $row->post_count = 0;
	            }
                                
			}

			$this->data['paginated_result'] = $weekly_participation_report;
			$this->data['init_datatable'] = true;
			$this->data['test_patient_excluded_note'] = true;
            $this->data['note_deleted_patient_excluded'] = true;
			$this->data['result_headers'] = (object) [
				'patient_id'     => [ 'name' => "Patient Id" ],
				'full_name'      => [ 'name' => "Name" ],
				'mrn'      => [ 'name' => "MRN" ],
				'invite_date'      => [ 'name' => "Invite Date", 'tooltip' => 'Invitation Sent Date' ],
				'commit_date'      => [ 'name' => "Commit Date", 'tooltip' => 'Patient Approval Date' ],
				'enrollment_date'      => [ 'name' => "Enrolled", 'tooltip' => 'Patient Password Created Date' ],
				'no_of_weeks_in_program'      => [ 'name' => "# of Weeks in Program", 'class' => 'text-center', 'tooltip' => 'Number of Weeks in the Program From Password Created Date' ],
				'no_of_weeks_logged_weight'      => [ 'name' => "# of Weeks Logged Weight", 'class' => 'text-center', 'tooltip' => 'Number of Weeks in which at Once weight is Logged Digitally' ],
				'lead_coach_name'      => [ 'name' => "Lead Health Coach", 'tooltip' => 'Patient Lead Coach' ],

				'manual_activity_minutes'      => [ 'name' => "Activity Minutes", 'class' => 'text-center', 'tooltip' => 'Total Activity Minutes Logged Manually' ],
				'no_of_manual_activity_days'      => [ 'name' => "# of Activity Days", 'class' => 'text-center', 'tooltip' => '# of Days When Manually Logged Activity is Performed' ],

				'total_steps_synced'      => [ 'name' => "Activity Steps", 'class' => 'text-center', 'tooltip' => 'Total Activity Steps synced' ],
				'no_of_fitbit_activity_days'      => [ 'name' => "# of Fitbit Activity Days", 'class' => 'text-center', 'tooltip' => '# of Days When Fitbit Activity is Performed' ],
				'avg_steps_per_day'      => [ 'name' => "Avg. Steps/Day", 'class' => 'text-center', 'tooltip' => 'Average Steps per Day' ],

				'starting_weight'      => [ 'name' => "Starting Weight", 'class' => 'text-center', 'tooltip' => 'Starting Weight (lbs)' ],
				'current_weight'      => [ 'name' => "Current Weight", 'class' => 'text-center', 'tooltip' => 'Current Weight (lbs)<br/>i.e. Last day Logged Weight' ],
                'weight_loss'      => [ 'name' => "Weight Loss", 'class' => 'text-center', 'tooltip' => 'Weight Loss (lbs)' ],
				'weight_loss_percent'      => [ 'name' => "Weight Loss Percent", 'class' => 'text-center', 'tooltip' => 'Weight Loss Percent <br/>[(weight_loss/start_weight) * 100]' ],
                'no_of_days_weight_logged'      => [ 'name' => "Days Weight Logged on", 'class' => 'text-center', 'tooltip' => '# of days Weight Logged on' ],
				'sent_msg'      => [ 'name' => "Sent Msg", 'class' => 'text-center', 'tooltip' => '# of Sent Messages' ],

				'no_of_meal_log'      => [ 'name' => "# of Meal Logs", 'class' => 'text-center', 'tooltip' => '# of Meal Logged' ],

				'no_of_goal_logged'      => [ 'name' => "# of Goal Logged", 'class' => 'text-center', 'tooltip' => '# of Goals Logged' ],
                'glasses_of_water_consumed'      => [ 'name' => "Glasses of Water Consumed", 'class' => 'text-center', 'tooltip' => '# of Water Logged' ],
				'one_to_one_sessions'      => [ 'name' => "# of 1:1 Sessions", 'class' => 'text-center', 'tooltip' => '# of 1:1 Session' ],

				'completed_units'      => [ 'name' => "Completed Units", 'class' => 'text-center', 'tooltip' => '# of Completed Units ' ],
				'viewed_units'      => [ 'name' => "Viewed Units", 'class' => 'text-center', 'tooltip' => '# of Unique Units Viewed' ],
                'min_weight'         => [ 'name' => "Min Weight Archived", 'class' => 'text-center', 'tooltip' => 'Minimum weight archived' ],
                'post_count'         => [ 'name' => "Group Post Count", 'class' => 'text-center', 'tooltip' => 'User group wall post count' ],
			];
			$response = [
				'start_date'  => $start,
				'end_date'  => $end,
				'html' => view( 'report.blocks.common-duration-report', $this->data )->render(),
				'page_description' => $duration_label ? 'Filtered for '.$duration_label : '',
				'success' => true,
				'total_records' => count($weekly_participation_report)
			];
		}
		else{
			$response = [
				'error' => $validator->errors()
			];
		}
		return response()->json( $response );
	}

	/**
	 * @Desc: multidimensional arrays check key exist
	 * @param
	 *
	 * @return array
	 */
	function in_multiarray($elem,$patient_weight,$field)
	{
		foreach ($patient_weight as $p_key => $p_row) {
			if($p_row->patient_id == $elem)
				return $p_row->$field;
		}
		return null;
	}

	/**
	 * @param Validator $validator
	 *
	 * @return array
	 */
	public function apiGroupParticipationReport(Validator $validator){
		$group_start = request( 'startDate' );
		$patient_ids  = request( 'patientIds' );

		if ( ! empty( $group_start ) ) {
			$group_start = intval( $group_start ) / 1000;
		}
		$validate = $validator::make(
			[
				'patientIds' => $patient_ids
			],
			[
				'patientIds' => 'required|regex:/^(\d+(?:,\d+)*)$/'
			] );
		if ( $validate->fails() ) {
			return [
				'error' => trans( 'common.invalid_request_data' ),
				'errorDetail' => $validate->errors()
			];
		}
		$patient_ids = explode(',', $patient_ids);
		info( 'Patient Ids are:', $patient_ids );
		$now = Carbon::now();
		info( 'DateTime Today is: ' . $now );
		if($now->isSunday()){
			$last_sunday = $now->copy()->startOfDay();
		}else{
			$last_sunday = new Carbon('last sunday');
		}

		$start = $last_sunday;
		$end   = $now;
		info( 'Processing Group report for: ' . $start . ' - ' . $end );

		$last_week_start = $start->copy()->subWeeks(1);
		$last_week_end = $start;

		info( 'Prior week will be: ' . $last_week_start . ' - ' . $last_week_end );
		$result = [];

		$patients = $this->healthslate_model->get_group_report_for_duration( $patient_ids, $start, $end, $last_week_start, $last_week_end );

		$patient_weight = $this->healthslate_model->get_group_report_for_duration_weight_details( $patient_ids, $start, $end, $last_week_start, $last_week_end );


		if(!empty($patients)) {
			foreach ($patients as $p_key => $p_row) {
				$p_current_weight    		= $this->in_multiarray($p_row->patient_id, $patient_weight,"current_weight");
				$p_prior_week_weight 		= $this->in_multiarray($p_row->patient_id, $patient_weight,"prior_week_weight");
				$p_row->current_weight 		=  isset($p_current_weight) ? $p_current_weight : null;
				$p_row->prior_week_weight 	=  isset($p_prior_week_weight) ? $p_prior_week_weight : null;
			}
		}

		$this_week_activity_minutes  = collect( $this->healthslate_model->get_patients_synced_minutes( $patient_ids, $start, $end ) );

		$prior_week_activity_minutes = collect( $this->healthslate_model->get_patients_synced_minutes( $patient_ids, $last_week_start, $last_week_end ) );

		// week number has starting index 0 on curriculum server
		$patients = $this->curriculumServiceProvider->getUnitsCompletedInSection($patients);

		$patients = $this->curriculumServiceProvider->getSectionListByPatientType($patients);

		foreach ( $patients as $record ) {

			$is_week_one = ( $record->member_completed_week == 1 ) ? true : false;

			$goal = $this->getWeightLossByGroup([$record], null);

			$minutes = floatval( $record->manual_activity_minutes );
			$prior_week_minutes = intval( $record->prior_week_manual_activity_minutes );

			$this_week = $this_week_activity_minutes->where( 'patient_id', $record->patient_id )->first();
			if ( ! empty( $this_week ) ) {
				$minutes = $minutes + floatval( $this_week->total_activity_minutes_synced );
			}
			$last_week = $prior_week_activity_minutes->where( 'patient_id', $record->patient_id )->first();
			if ( ! empty( $last_week ) ) {
				$prior_week_minutes = $prior_week_minutes + floatval( $last_week->total_activity_minutes_synced );
			}
			// for week 1, starting_weight will be prior weight
			$prior_weight = $is_week_one ? $record->starting_weight : ( is_null( $record->prior_week_weight ) ? $record->prior_week_weight : floatval( $record->prior_week_weight ) );

			$goal_achieved = null;
			$change_since_start_weight = null;
			$change_since_start_weight_percent = null;
			// consider This week weight
			// if not present then fall back to prior weight
			if(!is_null($record->current_weight) ){
				$goal_achieved = round($goal->goal_completion_percentage);
				$change_since_start_weight = floatval($record->current_weight) - floatval($record->starting_weight);
				$change_since_start_weight_percent = ( ! empty( $record->current_weight ) ) ? ( ( $change_since_start_weight / $record->current_weight ) * 100 ) : $change_since_start_weight_percent;
			}else if(!empty($prior_weight)){
				$data_set = (object) [ 'starting_weight' => $record->starting_weight, 'current_weight' => $prior_weight, 'target_weight' => $record->target_weight];
				$prior_goal = $this->getWeightLossByGroup([$data_set], null);
				$goal_achieved = round($prior_goal->goal_completion_percentage);
				// for week 1, change since starting should be null, if current weight is not present.
				if(!$is_week_one){
					$change_since_start_weight         = ( floatval( $prior_weight ) - floatval( $record->starting_weight ) );
					$change_since_start_weight_percent = ( ! empty( $prior_weight ) ) ? ( ( $change_since_start_weight / floatval( $prior_weight ) ) * 100 ) : $change_since_start_weight_percent;
				}
			}

			$change_since_prior_weight   = null;
			$change_since_prior_activity = null;
			if ( ! empty( $record->current_weight ) && ! empty( $prior_weight ) ) {
				$change_since_prior_weight = $record->current_weight - $prior_weight;
			}
			if ( ! empty( $minutes ) && ! empty( $prior_week_minutes ) ) {
				$change_since_prior_activity = $minutes - $prior_week_minutes;
			}
			$result[] = [
				'userTypeId'                    => $record->patient_id,
				'thisWeekWeight'                => is_null( $record->current_weight ) ? $record->current_weight : floatval( $record->current_weight ),
				'priorWeight'                   => $prior_weight,
				'startingWeight'                => $record->starting_weight,
				'targetWeight'                  => $record->target_weight,
				'goalAcheived'                  => $goal_achieved,
				'thisWeekActivityMinutes'       => $minutes,
				'priorWeekActivityMinutes'      => $is_week_one ? null : $prior_week_minutes,
				'changeSinceStartWeight'        => $change_since_start_weight,
				'changeSinceStartWeightPercent' => $change_since_start_weight_percent,
				'changeSincePriorWeight'        => $change_since_prior_weight,
				'changeSincePriorActivity'      => $change_since_prior_activity,
				'logDaysCount'                  => $record->days_logged,
				'logsCount'                     => $record->total_logs,
				'viewedContentCount'            => isset( $record->viewed_units_in_week ) ? ( $record->viewed_units_in_week ) : 0,
				'totalContentCount'             => isset( $record->units_in_week ) ? ( $record->units_in_week ) : 0,
				'programWeek'             		=> isset( $record->member_completed_week ) ? ( $record->member_completed_week + 1) : 1,
				'topicName'						=> isset( $record->sectionTitle ) ? ( $record->sectionTitle ) : null,
			];
		}
		return $result;
	}

	/**
	 * add weight_loss & weight_loss_percent after processing starting_weight & current_weight
	 * @param $row
	 *
	 * @return mixed
	 */
	private function getWeightLossData( $row ) {
		$row->weight_loss         = '';
		$row->weight_loss_percent = '';
		if ( ! empty( $row->starting_weight ) && ! is_null( $row->current_weight )  ) {
			$row->weight_loss         = format_weight(( $row->starting_weight - $row->current_weight ));
			$row->weight_loss_percent = format_weight( ( $row->weight_loss ? ( $row->weight_loss / $row->starting_weight ) * 100 : '' ) );
		}
		if ( ! empty( $row->starting_weight ) ){
			$row->starting_weight = format_weight($row->starting_weight);
		}
		if ( ! empty( $row->current_weight ) ){
			$row->current_weight = format_weight($row->current_weight);
		}
		return $row;
	}
        
        /**
	 * @param Validator $validator
	 * @param $facility_id
	 *
	 * @return \Illuminate\Http\JsonResponse|mixed
	 * @throws \Exception
	 * @throws \Throwable
	 */
	public function engagementReport(Validator $validator, $facility_id ){
		$start = request( 'start_date', null );
		$end   = request( 'end_date', null );
		$duration_label   = request( 'duration_label' );
        ini_set("memory_limit", -1);
        ini_set('max_execution_time', 0);
		$validator = $validator::make(
			request()->input(), array(
			'start_date' => 'required|date_format:m/d/Y',
			'end_date'   => 'required|date_format:m/d/Y',
		) );
		$response = [];
		if ( $validator->passes() ) {
			$start = Carbon::createFromFormat('m/d/Y', $start)->setTime(0, 0);
			$end = Carbon::createFromFormat('m/d/Y', $end)->setTime(0, 0)->addDay(1);

			Log::info('Loading Engagement report for: ' . $start . ' - ' . $end);
			$is_download_request = $this->isTableDownloadRequest();

			if ($is_download_request) {
				$results = $this->healthslate_model->get_weekly_participation_report_for_duration( $facility_id, $start, $end );
			}
			else
			{
				$offset = config('healthslate.table_row_per_page') * (request('page', 0) - 1);
				$total_patients = $this->healthslate_model->get_weekly_participation_report_total_patients($facility_id);
				$patient_ids = 0;
				if(count($total_patients) > 0)
				{
					$patient_ids = collect(array_slice($total_patients,$offset,config('healthslate.table_row_per_page')))->implode('patient_id', ',');
					$patient_ids = explode(',', $patient_ids);
				}
				$results = $this->healthslate_model->get_weekly_participation_report_for_duration_engagement($facility_id, $start, $end, '', $patient_ids);
			}


			Log::info('Total records in Engagement report are: ' . count($results));

			$result_headers = [
				'Partner ID' => ['name' => 'Partner ID'],
				'Solera ID' => ['name' => 'Solera ID', 'class' => 'text-center', 'tooltip' => 'Patient\'s MRN'],
				'First_Name' => ['name' => 'First_Name'],
				'Last_Name' => ['name' => 'Last_Name'],
				'Week' => ['name' => 'Week', 'class' => 'text-center', 'tooltip' => 'Number of Weeks in the Program From Password Created Date'],
				'Engaged' => ['name' => 'Engaged', 'class' => 'text-center', 'tooltip' => 'Fulfilled at least 3 of activity criteria'],
				'Engaged Flag' => ['name' => 'Engaged Flag'],
				'Physical Activity Minutes' => ['name' => 'Physical Activity Minutes', 'class' => 'text-center', 'tooltip' => 'Manually Logged Minutes + (# of FitbitSteps/105) '],
				'Weight lbs' => ['name' => 'Weight lbs', 'class' => 'text-center', 'tooltip' => 'Patient Last Synced Weight'],
				'Engagement 1' => ['name' => 'Engagement 1'],
				'Engagement 2' => ['name' => 'Engagement 2'],
				'Engagement 3' => ['name' => 'Engagement 3'],
				'Engagement 4' => ['name' => 'Engagement 4'],
				'Engagement 5' => ['name' => 'Engagement 5'],
				'Engagement 6' => ['name' => 'Engagement 6'],
				'Engagement 7' => ['name' => 'Engagement 7'],
				'Engagement 8' => ['name' => 'Engagement 8'],
				'Engagement 9' => ['name' => 'Engagement 9'],
				'Engagement 10' => ['name' => 'Engagement 10'],
				'Starting weight lbs' => ['name' => 'Starting weight lbs', 'class' => 'text-center'],
				'Weight change % change' => ['name' => 'Weight change % change', 'class' => 'text-center'],
				'Weight change lb' => ['name' => 'Weight change lb', 'class' => 'text-center'],
				'Enrolled date' => ['name' => 'Enrolled date'],
				'Coach Name' => ['name' => 'Coach Name'],
			];

			// is_download_request ?
			if ($is_download_request && !empty($results)) {
				// send all records
				$results = $this->curriculumServiceProvider->getEducationModulesCompleted($results, $start, $end, false);
				$results = $this->buildEngagementReport($results);
				return $this->dataTableService->dataTableResponse(collect($results), true);
			}

			$empty_array = array();
			$total_patient = count($total_patients);
			if ($total_patient > 0) {
				for ($i = 0; $i < $total_patient; $i++) {
					$empty_array[$i] = $i;
				}
			}

			$paginate_counts = $this->makeLengthAwarePaginator( collect( $empty_array ), config( 'healthslate.table_row_per_page' ) );
			$paginated_result = $this->curriculumServiceProvider->getEducationModulesCompleted( collect( $results ), $start, $end, false );
			$paginated_result = $this->buildEngagementReport($paginated_result);

			$this->data['paginated_result'] = $paginated_result;
			$this->data['paginate_counts'] 	= $paginate_counts;
			$this->data['total_records'] = $total_patient;
			$this->data['test_patient_excluded_note'] = true;
            $this->data['note_deleted_patient_excluded'] = true;
			$this->data['result_headers'] = (object) $result_headers;

			$response = [
				'start_date'  => $start,
				'end_date'  => $end,
				'html' => view( 'report.blocks.common-duration-report', $this->data )->render(),
				'success' => true,
				'page_description' => $duration_label ? 'Filtered for '.$duration_label : '',
				'total_records' => $total_patient
			];
		}
		else{
			$response = [
				'error' => $validator->errors()
			];
		}
		return response()->json( $response );
	}

	/**
	 * @param $results
	 * @param string $default
	 *
	 * @return mixed
	 */
	private function buildEngagementReport( &$results, $default = '' ) {
		// get collection for patient id pluck
		$result_collection = ( $results instanceof \Illuminate\Pagination\LengthAwarePaginator ) ? $results->getCollection() : collect( $results );
		$patient_ids       = $result_collection->pluck( 'patient_id' )->toArray();
                $user_ids          = $result_collection->pluck( 'user_id' )->toArray();
		info( 'All Patients are: ', $patient_ids );

		$start_date = Carbon::createFromFormat( 'm/d/Y', request('start_date') )->setTime( 0, 0 );
                $end_date   = Carbon::createFromFormat( 'm/d/Y', request( 'end_date') )->setTime( 0, 0 )->addDay( 1 );
		// get previous weight logs for all patients
		$patients_weight_log = collect( $this->healthslate_model->get_patient_recent_weight_log( $patient_ids, $start_date ) );
                $patients_weight_log = $patients_weight_log->groupBy("patient_id");
                
                $users_post_count = collect($this->healthSlate_group->get_patient_group_wall_post_count($user_ids, $start_date, $end_date));
                $users_post_count = $users_post_count->groupBy('user_id');
                
		info('Collected all information, Building Engagement Report');
		for($i = 0; $i < count($results); $i++){
			$source = $results[$i];

			$physical_activity_minutes = 0;
			if(isset($source->manual_activity_minutes)){
				$physical_activity_minutes += intval($source->manual_activity_minutes);
			}
			// @todo spatel
			//  If Fitbit Minutes are not yet tracked, use (# of FitbitSteps/105) as an approximation
			if(isset($source->total_steps_synced)){
				$physical_activity_minutes += (intval($source->total_steps_synced)/105);
			}

			// Weight Info
			$recent_weight = null;
			$weight_loss = null;
			$weight_loss_percent = null;
	        //user post count
	        $post_count = null;
	        $post_array = null;

	        $post_array = isset($users_post_count[$source->user_id]) ? $users_post_count[$source->user_id][0] : null;
	        if (!empty($post_array) && isset($post_array->post_count)) {
	            $source->post_count = $post_array->post_count;
	        }else{
	            $source->post_count = 0;
	        }

	        if(!empty($source->current_weight)){
				$recent_weight = $source->current_weight;
			}else{
				$weight_log = isset($patients_weight_log[$source->patient_id]) ? $patients_weight_log[$source->patient_id][0] : null;
				if(!empty($weight_log) && isset($weight_log->weight)){
					$recent_weight = $weight_log->weight;
				}
			}

			if ( ! empty( $source->starting_weight ) && ! is_null( $recent_weight )  ) {
				$weight_loss         = format_weight(( $source->starting_weight - $recent_weight ));
				$weight_loss_percent = format_weight(( $weight_loss ? ( $weight_loss / $source->starting_weight ) * 100 : '' ));

			}
			if ( ! empty( $source->starting_weight ) ){
				$source->starting_weight = format_weight($source->starting_weight);
			}
			if ( ! empty( $recent_weight ) ){
				$recent_weight = format_weight($recent_weight);
			}
			$row = (object) [
				'Partner ID'                => $source->patient_id,
				'Solera ID'                 => $source->mrn,
				'First_Name'                => $source->first_name,
				'Last_Name'                 => $source->last_name,
				'Week'                      => isset($source->no_of_weeks_in_program) ? sprintf("%02d", intval($source->no_of_weeks_in_program)).' ' : '',
				'Engaged'                   => $this->is_Engaged($source),
				'Engaged Flag'              => '',
				'Physical Activity Minutes' => floor($physical_activity_minutes),
				'Weight lbs'                => $recent_weight,
				'Engagement 1'              => '',
				'Engagement 2'              => '',
				'Engagement 3'              => '',
				'Engagement 4'              => '',
				'Engagement 5'              => '',
				'Engagement 6'              => '',
				'Engagement 7'              => '',
				'Engagement 8'              => '',
				'Engagement 9'              => '',
				'Engagement 10'             => '',
				'Starting weight lbs'       => $source->starting_weight,
				'Weight change % change'    => $weight_loss_percent,
				'Weight change lb'          => $weight_loss,
				'Enrolled date'             => isset($source->enrollment_date) ? $source->enrollment_date : $default,
				'Coach Name'                => isset($source->lead_coach_name) ? $source->lead_coach_name : $default,
			];
			$results[$i] = $row;
		}
		return $results;
	}

	/**
	 * @param $source
	 *
	 * @return string
	 */
	private function is_Engaged($source){
		$engaged = 'N';
		$completed_activity = [];

		// Check if patient is Disabled or DEACTIVE
		info('Patient: '.@$source->patient_id.' MRN: '.$source->mrn);
		$mrn_upper = trim(strtoupper(@$source->mrn));
		if ( (0 === strpos($mrn_upper, 'DEACTIVE')) || (0 === strpos($mrn_upper, 'DIS')) ){
			info('Patient is in disabled state');
			$engaged = 'D';
			return $engaged;
		}

		// Checking engagement criteria AS BELOW
		// if completed 2 or more from below activity, consider engaged.

		// Complete 2 education modules
		if(isset($source->viewed_units) && ($source->viewed_units) >= 2 ){
			$completed_activity[] = 'education-module';
		}
		// done a 1:1
		if(isset($source->one_to_one_sessions) && intval($source->one_to_one_sessions)){
			$completed_activity[] = 'one-on-one';
		}
		// sent at least 2 msg or 2 group wall post
		if(isset($source->sent_msg) && (intval($source->sent_msg)+(isset($source->post_count)? intval($source->post_count): 0)) >= 2){
			$completed_activity[] = 'sent-msg';
		}
		// set at least 1 behaviour goal or logs the goal behaviour at least 2 times
        $no_of_goal_logged = isset($source->no_of_goal_logged) ? intval($source->no_of_goal_logged) : 0;
        $glasses_of_water_consumed = isset($source->glasses_of_water_consumed) ? intval($source->glasses_of_water_consumed) : 0;
		if(($no_of_goal_logged + $glasses_of_water_consumed) > 1 ){
			$completed_activity[] = 'no-of-goal-logged';
		}
		// logged at least 3 meals
		if(isset($source->no_of_meal_log) && intval($source->no_of_meal_log) >= 3){
			$completed_activity[] = 'no-of-meal-logged';
		}
		// logged physical activity at least 3 times
		if((intval(@$source->no_of_fitbit_activity_logs) + intval(@$source->no_of_manual_activity_log))>= 3){
			$completed_activity[] = 'no-of-activity-minutes-synced';
		}
        //weight log count
        if(isset($source->no_of_weight_log) && intval($source->no_of_weight_log) >= 3){
			$completed_activity[] = 'no-of-weight-logged';
		}

		Log::info('Patient: '.@$source->patient_id.' Engagement criteria: '.count($completed_activity).' ['.implode(', ',$completed_activity).']');
		if(count($completed_activity) >= 2){
			$engaged = 'Y';
		}
		return $engaged;
	}

	/**
	 * Only being used in Week Cohort reports on level 3
	 * @param $source
	 *
	 * @return string
	 */
	private function is_Active_Engaged($source){
		$is_engaged = 'N';

		if ( ( ! empty( $source->completed_week_curriculum ) )
		     && ! empty( $source->last_login_in_week )
		     && ( ! empty( $source->num_group_entries ) || ! empty( $source->num_log_entries ) )
		) {
			$is_engaged = 'Y';
		}
		return $is_engaged;
	}

	/**
	 * @param Validator $validator
	 * @param $facility_id
	 *
	 * @return \Illuminate\Http\JsonResponse|mixed
	 * @throws \Exception
	 * @throws \Throwable
	 */
	public function coachEngagementReport( Validator $validator, $facility_id ){
		$start = request( 'start_date', null );
		$end   = request( 'end_date', null );
		$duration_label   = request( 'duration_label' );

		$validator = $validator::make(
			request()->input(), array(
			'start_date' => 'required|date_format:m/d/Y',
			'end_date'   => 'required|date_format:m/d/Y',
		) );
		$response = [];
		if ( $validator->passes() ) {
			$start = Carbon::createFromFormat( 'm/d/Y', $start )->setTime( 0, 0 );
			$end   = Carbon::createFromFormat( 'm/d/Y', $end )->setTime( 0, 0 )->addDay( 1 );

			$is_download_request = $this->isTableDownloadRequest();
			info( 'Loading Coach Engagement report for: ' . $start . ' - ' . $end );
			$last_week_start = $end->copy()->subWeeks(1);
			info( 'For Last week duration will be: ' . $last_week_start . ' - ' . $end );
			$last_two_week_start = $end->copy()->subWeeks(2);
			info( 'For Last two week duration will be: ' . $last_two_week_start . ' - ' . $end );
			$final_result = [ ];
			// fetching all lead coaches and their patients
			$leads    = collect( $this->healthslate_model->get_facility_coach( $facility_id, true ) );
			$lead_ids = $leads->pluck( 'provider_id' )->toArray();
			info( 'Total Lead Coaches : '.$leads->count() );

			info( 'Fetching info for patients' );
			//@todo We can limit this get all to fetch specific facility only.
			$lead_and_patients = collect( $this->healthslate_model->get_patient_engagement( $start, $end ) );
            $patient_details = collect( $this->common_model->get_patient_manual_activity_log( $start, $end , $lead_and_patients->pluck( 'patient_id' )->toArray()) );

            foreach ($lead_and_patients as $r_key => $row) {
                foreach ($patient_details as $section_data) {
                    if($row->patient_id == $section_data->patient_id)
                    {
                        $row->no_of_manual_activity_log = $section_data->no_of_manual_activity_log;
                        $row->no_of_fitbit_activity_logs = $section_data->no_of_fitbit_activity_logs;
                        $row->current_weight = $section_data->current_weight;
                        $row->no_of_days_weight_logged = $section_data->no_of_days_weight_logged;
                        $row->sent_msg = $section_data->sent_msg;
                        $row->no_of_meal_log = $section_data->no_of_meal_log;
                        $row->no_of_goal_logged = $section_data->no_of_goal_logged;
                        $row->one_to_one_sessions = $section_data->one_to_one_sessions;
                    }
                }
            }

            $lead_and_patients = $lead_and_patients->groupBy('lead_coach_id');

            //dd($lead_and_patients);

			if ( $start->eq( $last_week_start ) ) {
				info('Last week start is equal to start date, No need to query for Last Week duration Data');
				$lead_and_patients_last_week = $lead_and_patients;
			} else {
				$lead_and_patients_last_week = collect( $this->healthslate_model->get_patient_engagement( $last_week_start, $end ) );
                $patient_details_last_week = collect( $this->common_model->get_patient_manual_activity_log( $last_week_start, $end , $lead_and_patients_last_week->pluck( 'patient_id' )->toArray()) );

                foreach ($lead_and_patients_last_week as $r_key => $row) {
                    foreach ($patient_details_last_week as $section_data) {
                        if($row->patient_id == $section_data->patient_id)
                        {
                            $row->no_of_manual_activity_log = $section_data->no_of_manual_activity_log;
                            $row->no_of_fitbit_activity_logs = $section_data->no_of_fitbit_activity_logs;
                            $row->current_weight = $section_data->current_weight;
                            $row->no_of_days_weight_logged = $section_data->no_of_days_weight_logged;
                            $row->sent_msg = $section_data->sent_msg;
                            $row->no_of_meal_log = $section_data->no_of_meal_log;
                            $row->no_of_goal_logged = $section_data->no_of_goal_logged;
                            $row->one_to_one_sessions = $section_data->one_to_one_sessions;
                        }
                    }
                }

				$lead_and_patients_last_week = $lead_and_patients_last_week->groupBy( 'lead_coach_id' );
			}

			if ( $start->eq( $last_two_week_start ) ) {
				info('Last two week start is equal to start date, No need to query for Last two Week duration Data');
				$lead_and_patients_two_week = $lead_and_patients;
			} else {
				$lead_and_patients_two_week = collect( $this->healthslate_model->get_patient_engagement( $last_two_week_start, $end ) );

                $patient_details_two_week = collect( $this->common_model->get_patient_manual_activity_log( $last_two_week_start, $end , $lead_and_patients_two_week->pluck( 'patient_id' )->toArray()) );

                foreach ($lead_and_patients_two_week as $r_key => $row) {
                    foreach ($patient_details_two_week as $section_data) {
                        if($row->patient_id == $section_data->patient_id)
                        {
                            $row->no_of_manual_activity_log = $section_data->no_of_manual_activity_log;
                            $row->no_of_fitbit_activity_logs = $section_data->no_of_fitbit_activity_logs;
                            $row->current_weight = $section_data->current_weight;
                            $row->no_of_days_weight_logged = $section_data->no_of_days_weight_logged;
                            $row->sent_msg = $section_data->sent_msg;
                            $row->no_of_meal_log = $section_data->no_of_meal_log;
                            $row->no_of_goal_logged = $section_data->no_of_goal_logged;
                            $row->one_to_one_sessions = $section_data->one_to_one_sessions;
                        }
                    }
                }

				$lead_and_patients_two_week = $lead_and_patients_two_week->groupBy( 'lead_coach_id' );
			}

			info( 'Collected info for patients for all three durations' );

			$total_leads = $leads->count();
			$g_total_members = 0;
			$g_total_still_in_first_week = 0;
			$g_were_engaged_last_week = 0;
			$g_not_engaged_for_two_weeks = 0;
			$g_recorded_weight_this_week = 0;
			$g_still_no_starting_weight = 0;
			$g_logged_physical_activity_this_week = 0;
			$g_logged_meal_this_week = 0;
			$g_logged_goal_this_week = 0;
			$g_ever_had_one2one = 0;
			$g_ever_viewed_curriculum = 0;

			$g_avg_weight_loss_percent = 0;
			$g_avg_no_of_days_weight_logged = 0;
			$g_avg_no_of_one2one = 0;
			$g_avg_no_of_viewed_units = 0;

			foreach ( $leads as $lead ) {

				info('Collecting info for coach id: ' . $lead->provider_id );
				$patients                    = isset( $lead_and_patients[ $lead->provider_id ] ) ? $lead_and_patients[ $lead->provider_id ] : collect();
				$last_week_patients_data     = isset( $lead_and_patients_last_week[ $lead->provider_id ] ) ? $lead_and_patients_last_week[ $lead->provider_id ] : collect();
				$last_two_week_patients_data = isset( $lead_and_patients_two_week[ $lead->provider_id ] ) ? $lead_and_patients_two_week[ $lead->provider_id ] : collect();
				$count                       = $patients->count();

				$avg_weight_loss_percent   = 0;
				$total_weight_loss_percent = 0;

				$still_no_starting_weight = 0;

				$avg_no_of_days_weight_logged   = 0;
				$total_no_of_days_weight_logged = 0;

				$ever_had_one2one    = 0;
				$avg_no_of_one2one   = 0;
				$total_no_of_one2one = 0;

				$ever_viewed_curriculum   = 0;
				$avg_no_of_viewed_units   = 0;
				$total_no_of_viewed_units = 0;

				// Process result of start and end date duration
				if($count){
					$patients = $this->curriculumServiceProvider->getEducationModulesCompleted( $patients, $start, $end, false );

					foreach ( $patients as $patient ) {
						$patient = $this->getWeightLossData($patient);

						if(isset($patient->weight_loss_percent)){
							$total_weight_loss_percent += floatval($patient->weight_loss_percent);
						}
						if(!isset($patient->starting_weight) || empty($patient->starting_weight)){
							$still_no_starting_weight++;
						}
						if(isset($patient->no_of_days_weight_logged)){
							$total_no_of_days_weight_logged += intval($patient->no_of_days_weight_logged);
						}
						if(isset($patient->one_to_one_sessions) && intval($patient->one_to_one_sessions) > 0){
							$ever_had_one2one++;
							$total_no_of_one2one += intval($patient->one_to_one_sessions);
						}
						if(isset($patient->viewed_units) && ($patient->viewed_units)){
							$ever_viewed_curriculum++;
							$total_no_of_viewed_units += ($patient->viewed_units);
						}
					}
					$avg_weight_loss_percent      = ( $total_weight_loss_percent / $count );
					$avg_no_of_days_weight_logged = ( $total_no_of_days_weight_logged / $count );
					$avg_no_of_one2one            = ( $total_no_of_one2one / $count );
					$avg_no_of_viewed_units       = ( $total_no_of_viewed_units / $count );
				}

				info('Processing Last week data');
				$were_engaged_last_week             = 0;
				$recorded_weight_this_week          = 0;
				$logged_physical_activity_this_week = 0;
				$logged_meal_this_week              = 0;
				$logged_goal_this_week              = 0;
				// processing result of last week duration data
				if(!empty($last_week_patients_data)){
					foreach ( $last_week_patients_data as $patient ) {
						if(isset($patient->current_weight) && !empty($patient->current_weight)){
							$recorded_weight_this_week++;
						}
						if(!empty($patient->no_of_manual_activity_log) || !empty($patient->no_of_fitbit_activity_logs)){
							$logged_physical_activity_this_week++;
						}
						if(isset($patient->no_of_meal_log) && intval($patient->no_of_meal_log)){
							$logged_meal_this_week++;
						}
						if(isset($patient->no_of_goal_logged) && intval($patient->no_of_goal_logged)){
							$logged_goal_this_week++;
						}
						//@todo Does this $patient has curriculum data ?
						$is_engaged = $this->is_Engaged($patient);
						if($is_engaged == 'Y'){
							$were_engaged_last_week++;
						}

					}

				}

				info('Processing Last two week data');
				$not_engaged_for_two_weeks = 0;
				// processing result of last two week duration data
				if(!empty($last_two_week_patients_data)){
					foreach ( $last_two_week_patients_data as $patient ) {
						//@todo Does this $patient has curriculum data ?
						$is_engaged = $this->is_Engaged($patient);
						if($is_engaged == 'N'){
							$not_engaged_for_two_weeks++;
						}
					}

				}

				$row = (object) [
					'lead_coach_id'                      => $lead->provider_id,
					'coach_name'                         => $lead->full_name,
					'total_members'                      => $count,
					'still_in_first_week'                => $patients->filter( function ( $patient ) {
																return intval( $patient->no_of_weeks_in_program ) < 2;
															} )->count(),
					'were_engaged_last_week'             => $were_engaged_last_week,
					'not_engaged_for_two_weeks'          => $not_engaged_for_two_weeks,
					'avg_weight_loss_percent'            => format_weight( $avg_weight_loss_percent ),
					'recorded_weight_this_week'          => $recorded_weight_this_week,
					'still_no_starting_weight'           => $still_no_starting_weight,
					'avg_no_of_days_weight_logged'       => format_weight( $avg_no_of_days_weight_logged, 1 ),
					'logged_physical_activity_this_week' => $logged_physical_activity_this_week,
					'logged_meal_this_week'              => $logged_meal_this_week,
					'logged_goal_this_week'              => $logged_goal_this_week,
					'ever_had_one2one'                   => $ever_had_one2one,
					'avg_no_of_one2one'                  => format_weight( $avg_no_of_one2one, 1 ),
					'ever_viewed_curriculum'             => $ever_viewed_curriculum,
					'avg_no_of_viewed_units'             => format_weight( $avg_no_of_viewed_units, 1 ),

				];

				$g_total_members += $count;
				$g_total_still_in_first_week += $row->still_in_first_week;
				$g_were_engaged_last_week += $row->were_engaged_last_week;
				$g_not_engaged_for_two_weeks += $row->not_engaged_for_two_weeks;
				$g_recorded_weight_this_week += $row->recorded_weight_this_week;
				$g_still_no_starting_weight += $row->still_no_starting_weight;
				$g_logged_physical_activity_this_week += $row->logged_physical_activity_this_week;
				$g_logged_meal_this_week += $row->logged_meal_this_week;
				$g_logged_goal_this_week += $row->logged_goal_this_week;
				$g_ever_had_one2one += $row->ever_had_one2one;
				$g_ever_viewed_curriculum += $row->ever_viewed_curriculum;

				if($total_leads){
					$g_avg_weight_loss_percent += $avg_weight_loss_percent;
					$g_avg_no_of_days_weight_logged += $avg_no_of_days_weight_logged;
					$g_avg_no_of_one2one += $avg_no_of_one2one;
					$g_avg_no_of_viewed_units += $avg_no_of_viewed_units;
				}

				$final_result[] = $row;
			}

			if($total_leads){
				$g_avg_weight_loss_percent      = ( $g_avg_weight_loss_percent / $total_leads );
				$g_avg_no_of_days_weight_logged = ( $g_avg_no_of_days_weight_logged / $total_leads );
				$g_avg_no_of_one2one            = ( $g_avg_no_of_one2one / $total_leads );
				$g_avg_no_of_viewed_units       = ( $g_avg_no_of_viewed_units / $total_leads );
			}

			$this->data['total_records'] = count($final_result);

			$final_result[] = (object) [
				'lead_coach_id'                      => null,
				'coach_name'                         => 'Total',
				'total_members'                      => $g_total_members,
				'still_in_first_week'                => $g_total_still_in_first_week,
				'were_engaged_last_week'             => $g_were_engaged_last_week,
				'not_engaged_for_two_weeks'          => $g_not_engaged_for_two_weeks,
				'avg_weight_loss_percent'            => format_weight( $g_avg_weight_loss_percent ),
				'recorded_weight_this_week'          => $g_recorded_weight_this_week,
				'still_no_starting_weight'           => $g_still_no_starting_weight,
				'avg_no_of_days_weight_logged'       => format_weight( $g_avg_no_of_days_weight_logged, 1 ),
				'logged_physical_activity_this_week' => $g_logged_physical_activity_this_week,
				'logged_meal_this_week'              => $g_logged_meal_this_week,
				'logged_goal_this_week'              => $g_logged_goal_this_week,
				'ever_had_one2one'                   => $g_ever_had_one2one,
				'avg_no_of_one2one'                  => format_weight( $g_avg_no_of_one2one, 1 ),
				'ever_viewed_curriculum'             => $g_ever_viewed_curriculum,
				'avg_no_of_viewed_units'             => format_weight( $g_avg_no_of_viewed_units, 1 ),

			];

			// is_download_request ?
			if ( $is_download_request && ! empty( $final_result ) ) {
				// send all records
				return $this->dataTableService->dataTableResponse( collect( $final_result ), true );
			}
			$this->data['paginated_result'] = $final_result;
			$this->data['test_patient_excluded_note'] = true;
			$this->data['deleted_coach_excluded_note'] = true;
			$this->data['disabled_patient_excluded_note'] = true;
			$this->data['result_headers'] = (object) [
				'coach_name'                         => [ 'name' => "Health Coaches Name" ],
				'total_members'                      => [ 'name' => "Total Members", 'class' => 'text-center' ],
				'still_in_first_week'                => [ 'name' => "Still in First Week", 'class' => 'text-center' ],
				'were_engaged_last_week'             => [ 'name' => "Were Engaged Last Week", 'class' => 'text-center' ],
				'not_engaged_for_two_weeks'          => [ 'name' => "Not Engaged for past two weeks", 'class' => 'text-center' ],
				'avg_weight_loss_percent'            => [ 'name' => "Avg. Weight Loss %", 'class' => 'text-center' ],
				'recorded_weight_this_week'          => [ 'name' => "Recorded Weight this Week", 'class' => 'text-center' ],
				'still_no_starting_weight'           => [ 'name' => "Still No Starting Weight", 'class' => 'text-center' ],
				'avg_no_of_days_weight_logged'       => [ 'name' => "Avg # of days Weight Logged", 'class' => 'text-center' ],
				'logged_physical_activity_this_week' => [ 'name' => "Logged Physical Activity This Week", 'class' => 'text-center' ],
				'logged_meal_this_week'              => [ 'name' => "Logged a Meal this Week", 'class' => 'text-center' ],
				'logged_goal_this_week'              => [ 'name' => "Logged a Goal This Week", 'class' => 'text-center' ],
				'ever_had_one2one'                   => [ 'name' => "Ever Had 1:1", 'class' => 'text-center' ],
				'avg_no_of_one2one'                  => [ 'name' => "Avg # of 1:1's", 'class' => 'text-center' ],
				'ever_viewed_curriculum'             => [ 'name' => "Ever Viewed Curriculum", 'class' => 'text-center' ],
				'avg_no_of_viewed_units'             => [ 'name' => "Avg # of Viewed Units", 'class' => 'text-center' ],
			];

			$response = [
				'start_date'  => $start,
				'end_date'  => $end,
				'html' => view( 'report.blocks.common-duration-report', $this->data )->render(),
				'success' => true,
				'page_description' => $duration_label ? 'Filtered for '.$duration_label : '',
				'total_records' => $this->data['total_records']
			];
		}
		else{
			$response = [
				'error' => $validator->errors()
			];
		}
		return response()->json( $response );
	}

	/**
	 * @param $facility_id
	 *
	 * @return \Illuminate\Http\JsonResponse|mixed
	 */
	public function weekCohortReport( $facility_id = 0 ) {

		$is_download_request = $this->isTableDownloadRequest();
		$result              = collect( $this->healthslate_model->get_week_cohorts( $facility_id ) );
		$result              = $result->groupBy( 'week' );
		$data                = collect();
		// we would have done this using mysql query
		// but it would not possible to calculate and debug Group weight loss percentage.
		foreach ( $result as $week => $group ) {
			info('Week Group: '.$week);
			$row = (object) [
				'week_group'             => $week,
				'patients'               => $group->count(),
				'active_patients'        => $group->filter(function ($item){
					return $this->isActivePatient($item);
				})->count(),
				'num_log_entries'        => $group->sum('num_log_entries'),
				'num_weight_entries'     => $group->sum('num_weight_entries'),
				'weight_loss_percentage' => '',
			];
			if ( ! empty( $group ) ) {
				$weight_loss = $this->getWeightLossByGroup($group);
				$row->weight_loss_percentage = $weight_loss->goal_completion_percentage;
			}
			if(!$is_download_request){
				$row->link = '<a href="'.route('report.week-cohort-patients', ['facility_id' =>  $facility_id, 'week' => $week]).'"> View Detail </a>';
			}
			info('End of Week Group');
			$row = (array) $row;
			$data->push( ( $is_download_request ? $row : array_values( $row ) ) );
		}

		if(!$is_download_request){
			$data->push( array_values( [
				'week_group'             => '<b>All</b>',
				'patients'               => 0,
				'active_patients'        => null,
				'num_log_entries'        => null,
				'num_weight_entries'     => null,
				'weight_loss_percentage' => '',
				'link'                   => '<a href="'.route('report.week-cohort-patients', ['facility_id' =>  $facility_id]).'"> View Detail </a>'
			]));
		}
		return $this->dataTableService->dataTableResponse( $data, $is_download_request );
	}

	private function getWeightLossByGroup($patients, $no_data_message = 'Group target not set'){
		/*$patients = [
			(object) [ 'starting_weight' => 120, 'current_weight' => 110, 'target_weight' => 90],
			(object) [ 'starting_weight' => 130, 'current_weight' => 140, 'target_weight' => 100],
			(object) [ 'starting_weight' => 130, 'current_weight' => 130, 'target_weight' => 100],
			(object) [ 'starting_weight' => 150, 'current_weight' => 100, 'target_weight' => 120],
			(object) [ 'starting_weight' => 200, 'current_weight' => 150, 'target_weight' => 150],
			(object) [ 'starting_weight' => 150, 'current_weight' => 141, 'target_weight' => 100],
		];*/

		$group_weight_loss          = (object) [ ];
		$weight_loss                = 0;
		$target_weight_loss         = 0;
		$goal_completion_percentage = 0;
		foreach ( $patients as $key => $patient ) {

			$starting_weight = floatval( $patient->starting_weight );
			$current_weight  = floatval( $patient->current_weight );
			$target_weight   = floatval( $patient->target_weight );

			if ( ! empty( $current_weight ) && $current_weight > 0 ) {
				// only +ve loss is counted towards group goal
				// ony till 100% is counted if done more than 100% loss.
				if($starting_weight >= $current_weight) {
					if ( $current_weight >= $target_weight ) {
						$weight_loss_temp = $starting_weight - $current_weight;

					} else {
						$weight_loss_temp = $starting_weight - $target_weight;

					}
					$weight_loss        = $weight_loss + $weight_loss_temp;
					$target_weight_loss_temp = $starting_weight - $target_weight;
					$target_weight_loss = $target_weight_loss + $target_weight_loss_temp;
					info( 'Processing : ' . $key );
					info( 'patient_id: ' . @$patient->patient_id . ' weight_loss: ' . $weight_loss_temp . ', target_weight_loss: ' . $target_weight_loss_temp . '  weight loss %: ' . ( $target_weight_loss_temp ? ( ( $weight_loss_temp / $target_weight_loss_temp ) * 100 ) : 0 ) );
				}
			}
		}
		info('Total weight_loss: '.$weight_loss. ' Total target_weight_loss: '.$target_weight_loss);
		if ( $weight_loss && $target_weight_loss ) {
			$goal_completion_percentage = ( $weight_loss / $target_weight_loss ) * 100;
		}
		else if($weight_loss == 0 && $target_weight_loss > 0 ){
			$goal_completion_percentage = 0;
		}
		else {
			$goal_completion_percentage = $no_data_message;
		}

		if ( is_numeric($goal_completion_percentage) && $goal_completion_percentage > 100 ) {
			$goal_completion_percentage = 100;
		}
		$group_weight_loss->total_weight_loss          = format_weight($weight_loss);
		$group_weight_loss->goal_completion_percentage = is_numeric($goal_completion_percentage) ? format_weight($goal_completion_percentage) : $goal_completion_percentage;

		info(json_encode($group_weight_loss));
		return $group_weight_loss;
	}

	/**
	 *
	 * Determine if patient is Active user on basis of last log date
	 * @param $item
	 * @param null|int $log_week
	 *
	 * @return bool
	 */
	private function isActivePatient($item, $log_week = null){
		if ( ! empty( $item->last_entry ) && isset( $item->num_days_last_entry ) ){
			info( 'patient_id: ' . @$item->patient_id . ' Last Log Entry : ' . $item->last_entry.' num_days_last_entry: ' . intval( $item->num_days_last_entry ) );
			return (intval( $item->num_days_last_entry ) <= 3 ) ;
		}
		else if ($log_week && ! empty( $item->last_entry ) && ! empty( $item->password_created_date ) ) {
			// calculation of log week end date
			$password_created_date = Carbon::createFromFormat( 'Y-m-d H:i:s', $item->password_created_date )->setTime( 0, 0 );
			$last_entry            = Carbon::createFromFormat( 'Y-m-d H:i:s', $item->last_entry );
			$end_date              = $password_created_date->copy()->addWeeks( intval( $log_week ) );
			//$start_date          = $end_date->copy()->subWeeks(1);
			$num_days_last_entry = $last_entry->diffInDays( $end_date );
			info( 'patient_id: ' . @$item->patient_id . ' log week end date: ' . $end_date . ' Last Log Entry : ' . $item->last_entry . ' num_days_last_entry: ' . intval( $num_days_last_entry ) );

			return ( $num_days_last_entry <= 3 );
		}
		return false;
	}

	/**
	 * @param $facility_id
	 * @param $week
	 *
	 * @return \Illuminate\Http\JsonResponse|mixed
	 */
	public function weekCohortPatients( $facility_id, $week = 0 ,$coach = 0){
		$is_download_request             = $this->isTableDownloadRequest();
		$patients_of_week                = collect( $this->healthslate_model->get_cohort_patients_of_week( $facility_id, $week, $coach ) );
		$patient_ids                     = $patients_of_week->pluck( 'patient_id' )->toArray();
		$patients_section_completed_data = $this->curriculumServiceProvider->getSectionsCompleted( $patients_of_week );
		$result                          = collect( $this->healthslate_model->get_activity_for_cohort_week( $patient_ids ) );
		$result                          = $result->groupBy( 'log_week' );
		$data                            = collect();

		info('Patients of Week: '.$week.' count: '.count($patient_ids).' are', $patient_ids);
		$not_logged = $result->get( "", false );
		if ( $not_logged ) {

			$row = (object) [
				'week_group'                  => $week,
				'log_week'                    => 'Not yet Logged Anything',
				'patients'                    => $not_logged->count(),
				'active_patients'             => null,
				'num_patients_logging'        => null,
				'num_patients_viewed_content' => null,
				'num_patients_weighing'       => null,
				'num_log_entries'             => null,
				'num_weight_entries'          => null,
				'average_num_weight_entries'  => null,
                                'meal_logged'                 => null, 
				'link'                        => ''
			];
			if(!$is_download_request){
				unset($row->week_group);
			}
			else if ( empty( $week ) ) {
				$row->week_group = 'All';
			}
			$row = (array) $row;
			$data->push( ( $is_download_request ? $row : array_values( $row ) ) );
		}

		// log_week should not be 0, but there are records in DB having
		// log_date_time < password_created_date
		// excluding them, so starting loop from 1
		$loop_till = $week;
		if ( empty( $week ) ) {
			$log_weeks_list = $result->keys();
			$loop_till      = $log_weeks_list->max();
		}
		info('Loop Till '.$loop_till.' week: '.$week);

		for ( $i = 1; $i <= $loop_till; $i ++ ) {
			// group of patient logged in log_week $i
			$group = $result->get( $i, collect() );
			info('Log week '.$i.' patient count: '.$group->count());

			// Check if patient has viewed content of this log_week
			// $i is current log_week
			$patients_viewed_week = [ ];
			$patients_section_completed_data->each( function ( $item, $key ) use ( $i, &$patients_viewed_week ) {
				if ( ! empty( $item->completed_weeks ) && in_array( $i, $item->completed_weeks ) ) {
					$patients_viewed_week[] = $item->patient_id;
				}
			} );
			info('Patients viewed week content : ', $patients_viewed_week);

			$patients_count        = $patients_of_week->count();
			$num_patients_weighing = $group->sum( 'has_weight_entry' );
			$row = (object) [
				'week_group'                  => $week,
				'log_week'                    => $i,
				'patients'                    => $patients_count,
				'active_patients'             => $group->filter(function ($item) use($i){
					return $this->isActivePatient($item, $i);
				})->count(),
				'num_patients_logging'        => $group->count().' ('.format_weight( ($group->count()/$patients_count) *100, 1).'%)',
				'num_patients_viewed_content' => count($patients_viewed_week).' ('.format_weight( (count($patients_viewed_week)/$patients_count) *100, 1).'%)',
				'num_patients_weighing'       => $num_patients_weighing.' ('.format_weight( ($num_patients_weighing/$patients_count) *100, 1).'%)',
				'num_log_entries'             => $group->sum('num_log_entries'), 
				'num_weight_entries'          => $group->sum('num_weight_entries'),
				'average_num_weight_entries'  => 0,
                                'meal_logged'                 => $group->sum('num_meal_entries'),  
			];
			if($row->patients){
				$row->average_num_weight_entries = format_weight($row->num_weight_entries / $row->patients);
			}

			if(!$is_download_request){
				unset($row->week_group);
				$row->link = '<a href="'.route('report.week-cohort-patients-log', ['facility_id' =>  $facility_id, 'week' => $week, 'log_week' => $row->log_week ]).'"> View Records </a>';
			}
			else if ( empty( $week ) ) {
				$row->week_group = 'All';
			}
			info('End of Log week '.$i);
			$row = (array) $row;
			$data->push( ( $is_download_request ? $row : array_values( $row ) ) );
		}
		return $this->dataTableService->dataTableResponse( $data, $is_download_request );
	}

	/**
	 * @param $facility_id
	 * @param $week
	 * @param $log_week
	 *
	 * @return \Illuminate\Http\JsonResponse|mixed
	 */
	public function weekCohortPatientsLog( $facility_id, $week, $log_week ){

		$data                = collect();
		$is_download_request = $this->isTableDownloadRequest();
		$patients_of_week    = collect( $this->healthslate_model->get_cohort_patients_of_week( $facility_id, $week ) );
		$patient_ids         = $patients_of_week->pluck( 'patient_id' )->toArray();
		$user_ids            = $patients_of_week->pluck( 'user_id' )->toArray();

		info('Patients of Week: '.$week.' count: '.count($patient_ids).' Log Week: '.$log_week.' Patients are', $patient_ids);
		$result                          = collect( $this->healthslate_model->get_patients_activity_of_log_week( $patient_ids, $log_week ) );
		$patient_login                   = collect( $this->healthslate_model->get_patients_login_in_week( $patient_ids, $log_week ) );
		$patient_steps                   = collect( $this->healthslate_model->get_patients_synced_steps_in_week( $patient_ids, $log_week ) );
		$patient_group_posts             = collect( $this->healthslate_model->get_patients_group_posts_in_week( $user_ids, $log_week ) );
		$patients_section_completed_data = $this->curriculumServiceProvider->getSectionsCompleted( $patients_of_week );

		// Check if patient has viewed content of this log_week
		$patients_viewed_week = [ ];
		$patients_section_completed_data->each( function ( $item, $key ) use ( $log_week, &$patients_viewed_week ) {
			if ( ! empty( $item->completed_weeks ) && in_array( $log_week, $item->completed_weeks ) ) {
				$patients_viewed_week[] = $item->patient_id;
			}
		} );

		// We need all patients of Week group to be listed here
		foreach ( $patients_of_week as $patient ) {
			info( 'iterating for patient_id: ' . $patient->patient_id );
			$completed_week_curriculum = ( in_array( $patient->patient_id, $patients_viewed_week ) ) ? true : false;
			$row                       = $result->where( 'patient_id', $patient->patient_id )->first();
			$login                     = $patient_login->where( 'patient_id', $patient->patient_id )->first();
			$steps_synced              = $patient_steps->where( 'patient_id', $patient->patient_id )->first();
			$patient_post              = $patient_group_posts->where( 'user_id', $patient->user_id )->first();

			if(empty($row)){
				info( 'Patient is in week group, but has no logs. Building with meta info' );
				$row = (object) [
					'patient_id'              => $patient->patient_id,
					'user_id'                 => $patient->user_id ,
					'password_created_date'   => $patient->password_created_date,
					'num_log_entries'         => 0,
					'num_weight_entries'      => 0,
					'last_weight_logged_on'   => null,
					'current_weight'          => null,
					'last_login_in_week'      => '',
					'manual_activity_minutes' => null,
					'total_steps_synced'      => null,
					'num_group_entries'       => 0,
				];
			}

			$row->starting_weight = $patient->starting_weight;
			$row->target_weight   = $patient->target_weight;

			if(!empty($login)){
				$row->last_login_in_week = $login->last_login_in_week;
			}
			if(!empty($steps_synced)){
				$row->total_steps_synced = $steps_synced->total_steps_synced;
			}
			if(!empty($patient_post)){
				$row->num_group_entries = $patient_post->num_group_entries;
			}


			// calculation of week start and end data for debug purpose
			//$password_created_date = Carbon::createFromFormat( 'Y-m-d H:i:s', $row->password_created_date )->setTime( 0, 0 );
			// $end_date = $password_created_date->copy()->addWeeks(intval($log_week));
			// $start_date = $end_date->copy()->subWeeks(1);
			// info('patient_id : '.$row->patient_id.' user_id: '.$row->user_id.' Week Start Date '.$start_date.'  ===>  end_date: '.$end_date);

			$row->completed_week_curriculum = $completed_week_curriculum;
			info( 'completed_week_curriculum : ' . $completed_week_curriculum . ' Last Login During week: ' . @$row->last_login_in_week );
			info( 'num_log_entries : ' . $row->num_log_entries . ' num_group_entries: ' . @$row->num_group_entries );
			$row               = $this->getWeightLossData( $row );
			$is_active_engaged = $this->is_Active_Engaged( $row );
			info('is_active_engaged : '.$is_active_engaged);

			$tr = [
				"week_group"              => $week,
				"log_week"                => $log_week,
				"patient_id"              => $is_download_request ? $patient->patient_id : '<a href="'.route('patient.detail', ['patient_id' => $patient->patient_id ]).'">'.$patient->patient_id.'</a>',
				"num_log_entries"         => $row->num_log_entries,
				"num_weight_entries"      => $row->num_weight_entries,
				"last_weight_logged_on"   => $row->last_weight_logged_on,
				"starting_weight"         => $row->starting_weight,
				"weight_loss_percent"     => $row->weight_loss_percent,
				"viewed_content"          => $completed_week_curriculum ? ( $is_download_request ? 'Y' : '<div class="text-center">Y</div>' ) : '',
				"last_login_in_week"      => $row->last_login_in_week,
				"manual_activity_minutes" => $row->manual_activity_minutes,
				"total_steps_synced"      => $row->total_steps_synced,
				"num_group_post"          => $row->num_group_entries,
				"active_and_engaged"      => $is_active_engaged ? ( $is_download_request ? $is_active_engaged : '<div class="text-center">'.$is_active_engaged.'</div>') : '',
			];

			if ( ! $is_download_request ) {
				unset( $tr['week_group'] );
				unset( $tr['log_week'] );
			}else if ( empty( $week ) ) {
				$tr['week_group'] = 'All';
			}
			$data->push( ( $is_download_request ? $tr : array_values( $tr ) ) );
		}
		return $this->dataTableService->dataTableResponse( $data, $is_download_request );
	}

	/**
	 * @param $facility_id
	 *
	 * @return \Illuminate\Http\JsonResponse|mixed
	 */
	public function facilityPatientPreferences( $facility_id ) {
		$is_download_request = $this->isTableDownloadRequest();
		$result              = $this->healthslate_model->get_patient_preferences( $facility_id );
		$data                = collect();
		foreach ( $result as $patient ) {
			$row = (object) [
				'patient_id'             => $patient->patient_id,
				'name'                   => $patient->full_name,
				'diabetes_type'          => $patient->diabetes_type_name,
				'email_communication'    => ( ! empty( $patient->email_communication ) && strtolower( $patient->email_communication ) == 'yes' ) ? 'Yes' : 'No',
			];
			if(!$is_download_request){
				$row->patient_id = '<a href="'.route('patient.detail', ['patient_id' =>  $patient->patient_id ]).'"> '.$patient->patient_id.' </a>';
			}
			$row = (array) $row;
			$data->push( ( $is_download_request ? $row : array_values( $row ) ) );
		}
		return $this->dataTableService->dataTableResponse( $data, $is_download_request );
	}

	/**
	 * @param Validator $validator
	 * @param $facility_id
	 *
	 * @return \Illuminate\Http\JsonResponse|mixed
	 * @throws \Exception
	 * @throws \Throwable
	 */
	public function facilityShippingAddresses( Validator $validator, $facility_id ) {
		$start          = request( 'start_date', null );
		$end            = request( 'end_date', null );
		$duration_label = request( 'duration_label' );

		$validator = $validator::make(
			request()->input(), array(
			'start_date' => 'required|date_format:m/d/Y',
			'end_date'   => 'required|date_format:m/d/Y',
		) );
		$response  = [ ];
		if ( $validator->passes() ) {
			$start = Carbon::createFromFormat( 'm/d/Y', $start )->setTime( 0, 0 );
			$end   = Carbon::createFromFormat( 'm/d/Y', $end )->setTime( 0, 0 )->addDay( 1 );

			Log::info( 'Loading Shipping Addresses Report for: ' . $start . ' - ' . $end );
			$is_download_request = $this->isTableDownloadRequest();

			$results = $this->healthslate_model->get_shipping_addresses( $facility_id, $start, $end );

			// is_download_request ?
			if ( $is_download_request && ! empty( $results ) ) {
				return $this->dataTableService->dataTableResponse( collect( $results ), true );
			}

			$this->data['paginated_result']           = $results;
			$this->data['init_datatable']             = true;
			$this->data['test_patient_excluded_note'] = true;
			$this->data['result_headers'] = (object) [
				'patient_id'            => [ 'name' => "Patient Id" ],
				"first_name"            => [ 'name' => "First Name" ],
				"last_name"             => [ 'name' => "Last Name" ],
				"address"               => [ 'name' => "Address" ],
				"city"                  => [ 'name' => "City" ],
				"state"                 => [ 'name' => "State" ],
				"zip_code"              => [ 'name' => "Zip" ],
				"phone"                 => [ 'name' => "Phone" ],
				"password_created_date" => [ 'name' => "Enrolled On" ],
				"today_date"            => [ 'name' => "Today's Date" ],
				"diabetes_type_name"    => [ 'name' => "Diabetes Type" ],
				"lead_coach_name"       => [ 'name' => "Coach Name" ],
				"sent_msg"              => [ 'name' => "# of Sent Msg" ],
				"logged_meals"          => [ 'name' => "# of Logged Meals" ],
			];
			$response                                 = [
				'start_date'       => $start,
				'end_date'         => $end,
				'html'             => view( 'report.blocks.common-duration-report', $this->data )->render(),
				'page_description' => $duration_label ? 'Filtered for ' . $duration_label : '',
				'success'          => true,
				'total_records'    => count( $results )
			];
		} else {
			$response = [
				'error' => $validator->errors()
			];
		}

		return response()->json( $response );
	}

	/**
	 * @param Validator $validator
	 * @param $facility_id
	 *
	 * @return \Illuminate\Http\JsonResponse|mixed
	 */
	public function facilityMessagesAjax( Validator $validator, $facility_id ) {
		$start          = request( 'start_date', null );
		$end            = request( 'end_date', null );
		$duration_label = request( 'duration_label' );

		$validator = $validator::make(
			request()->input(), array(
			'start_date' => 'required|date_format:m/d/Y',
			'end_date'   => 'required|date_format:m/d/Y',
		) );
		$response  = [];
		if ( $validator->passes() ) {
			$start = Carbon::createFromFormat( 'm/d/Y', $start )->setTime( 0, 0 );
			$end   = Carbon::createFromFormat( 'm/d/Y', $end )->setTime( 0, 0 )->addDay( 1 );

			$results        = $this->healthslate_model->get_message_activity_list( $facility_id, $start, $end );
			$result_headers = [
				'patient_id' => [ 'name' => "Patient Id" ],
				'name'       => [ 'name' => "Name" ],
				'mrn'        => [ 'name' => "MRN" ],
				'sent'       => [ 'name' => "Sent", 'class' => 'text-center' ],
				'received'   => [ 'name' => "Received", 'class' => 'text-center' ],
			];

			$is_download_request = $this->isTableDownloadRequest();
			if ( $is_download_request && ! empty( $results ) ) {
				// send all records
				return $this->dataTableService->dataTableResponse( collect( $results ), true );
			}

			$paginated_result = $this->makeLengthAwarePaginator( collect( $results ), config( 'healthslate.table_row_per_page' ) );

			$this->data['paginated_result']           = $paginated_result;
			$this->data['total_records']              = count( $results );
			$this->data['test_patient_excluded_note'] = true;
			$this->data['result_headers']             = (object) $result_headers;

			$response = [
				'start_date'       => $start,
				'end_date'         => $end,
				'html'             => view( 'report.blocks.common-duration-report', $this->data )->render(),
				'success'          => true,
				'page_description' => $duration_label ? 'Filtered for ' . $duration_label : '',
				'total_records'    => count( $results )
			];
		} else {
			$response = [
				'error' => $validator->errors()
			];
		}

		return response()->json( $response );
	}

	/**
	 * @param Validator $validator
	 * @param $facility_id
	 *
	 * @return \Illuminate\Http\JsonResponse|mixed
	 */
	public function weightLossReportAjax( Validator $validator, $facility_id ) {
		$start          = request( 'start_date', null );
		$end            = request( 'end_date', null );
		$duration_label = request( 'duration_label' );

		$validator = $validator::make(
			request()->input(), array(
			'start_date' => 'required|date_format:m/d/Y',
			'end_date'   => 'required|date_format:m/d/Y',
		) );
		$response  = [];
		if ( $validator->passes() ) {
			$start = Carbon::createFromFormat( 'm/d/Y', $start )->setTime( 0, 0 );
			$end   = Carbon::createFromFormat( 'm/d/Y', $end )->setTime( 0, 0 )->addDay( 1 );

			$results        = $this->healthslate_model->get_weight_loss_patient( $facility_id, $start, $end );
			$result_headers = [
				'patient_id'              => [ 'name' => "Patient Id" ],
				'name'                    => [ 'name' => "Name" ],
				'mrn'                     => [ 'name' => "MRN" ],
				'current_weight'          => [ 'name' => "Current Weight", 'class' => 'text-center' ],
				'last_day_weight_logged'  => [ 'name' => "Date Weighed", 'class' => 'text-center' ],
				'starting_weight'         => [ 'name' => "Start Weight", 'class' => 'text-center' ],
				'minimum_weight_achieved' => [ 'name' => "Minimum Weight achieved", 'class' => 'text-center' ],
				'date_achieved'           => [ 'name' => "Date Achieved", 'class' => 'text-center' ],
			];

			$is_download_request = $this->isTableDownloadRequest();
			if ( $is_download_request && ! empty( $results ) ) {
				// send all records
				return $this->dataTableService->dataTableResponse( collect( $results ), true );
			}

			$paginated_result = $this->makeLengthAwarePaginator( collect( $results ), config( 'healthslate.table_row_per_page' ) );

			$this->data['paginated_result']           = $paginated_result;
			$this->data['total_records']              = count( $results );
			$this->data['test_patient_excluded_note'] = true;
			$this->data['result_headers']             = (object) $result_headers;

			$response = [
				'start_date'       => $start,
				'end_date'         => $end,
				'html'             => view( 'report.blocks.common-duration-report', $this->data )->render(),
				'success'          => true,
				'page_description' => $duration_label ? 'Filtered for ' . $duration_label : '',
				'total_records'    => count( $results )
			];
		} else {
			$response = [
				'error' => $validator->errors()
			];
		}

		return response()->json( $response );
	}

	/**
	 * @param Validator $validator
	 * @param $facility_id
	 *weightLossReportAjax
	 * @return array|\Illuminate\Http\JsonResponse|mixed
	 */
	public function patientFitbitActivity(Validator $validator, $facility_id ){
		$start          = request( 'start_date', null );
		$end            = request( 'end_date', null );
		$duration_label = request( 'duration_label' );

		$validator = $validator::make(
			request()->input(), array(
			'start_date' => 'required|date_format:m/d/Y',
			'end_date'   => 'required|date_format:m/d/Y',
		) );
		$response  = [];
		if ( $validator->passes() ) {
			$start = Carbon::createFromFormat( 'm/d/Y', $start )->setTime( 0, 0 );
			$end   = Carbon::createFromFormat( 'm/d/Y', $end )->setTime( 0, 0 )->addDay( 1 );

			$results = $this->healthslate_model->get_patient_fitbit_activity($facility_id, $start, $end);
			foreach($results  as $key => $row){
				if(!empty($row->total_fitbit_steps) && !empty($row->no_of_fitbit_activity_days)){
					$results[$key]->avg_fitbit_steps_per_day = floor($row->total_fitbit_steps/$row->no_of_fitbit_activity_days);
				}
			}
			$result_headers = [
				'patient_id'                 => [ 'name' => "Patient Id" ],
				'name'                       => [ 'name' => "Name" ],
				'mrn'                        => [ 'name' => "MRN" ],
				'uuid'                       => [ 'name' => "UUID" ],
				'diabetes_type'              => [ 'name' => "Diabetes Type" ],
				'is_tracker_linked'          => [ 'name' => "Tracker Linked", 'class' => 'text-center' ],
				'date_tracker_linked'        => [ 'name' => "Tracker Linked Date", 'tooltip' => 'Day When First Activity was Synced' ],
				'last_time_tracker_synced'   => [ 'name' => "Last Date Tracker sync’d", 'tooltip' => 'Day When Last Activity was Synced' ],
				'no_of_times_synced'         => [ 'name' => "# of times sync’d", 'tooltip' => '# of times sync’d (Total Entries)' ],
				'total_fitbit_steps'         => [ 'name' => "Fitbit steps", 'tooltip' => 'total Fitbit Steps' ],
				'total_fitbit_minutes'         => [ 'name' => "Fitbit Minutes", 'tooltip' => 'Total Fitbit Activity Minutes' ],
				'no_of_fitbit_activity_days' => [ 'name' => "# of Activity Days in Fitbit", 'tooltip' => '# of Activity Days in Fitbit' ],
				'avg_fitbit_steps_per_day'   => [ 'name' => "Avg. Steps per Day", 'tooltip' => 'Avg. Steps per Day' ],
				'last_time_manual_logged'    => [ 'name' => "Last Date Manually Logged", 'tooltip' => 'Last Day manual activity logged' ],
				'no_of_times_manual_logged'  => [ 'name' => "Manually Logged", 'tooltip' => '# of times manual activity logged' ],
				'total_minutes_logged'       => [ 'name' => "Manually Logged Minutes", 'tooltip' => 'Total Minutes Manually logged' ],
			];

			$is_download_request = $this->isTableDownloadRequest();
			if ( $is_download_request && ! empty( $results ) ) {
				// send all records
				return $this->dataTableService->dataTableResponse( collect( $results ), true );
			}

			$paginated_result = $this->makeLengthAwarePaginator( collect( $results ), config( 'healthslate.table_row_per_page' ) );

			$this->data['paginated_result']           = $paginated_result;
			$this->data['total_records']              = count( $results );
			$this->data['test_patient_excluded_note'] = true;
			$this->data['result_headers']             = (object) $result_headers;

			$response = [
				'start_date'       => $start,
				'end_date'         => $end,
				'html'             => view( 'report.blocks.common-duration-report', $this->data )->render(),
				'success'          => true,
				'page_description' => $duration_label ? 'Filtered for ' . $duration_label : '',
				'total_records'    => count( $results )
			];
		}
		else {
			$response = [
				'error' => $validator->errors()
			];
		}

		return $response;
	}



	/**
	 * Ajax Handler for Sessions Completed Report
	 *
	 * @param Validator $validator
	 * @param $facility_id
	 *
	 * @return \Illuminate\Http\JsonResponse
	 * @throws \Exception
	 * @throws \Throwable
	 */
	public function sessionsCompletedReport(Validator $validator, $facility_id ) {
		$start = request( 'start_date', null );
		$end   = request( 'end_date', null );
		$duration_label   = request( 'duration_label' );

		$validator = $validator::make(
			request()->input(), array(
			'start_date' => 'required|date_format:m/d/Y',
			'end_date'   => 'required|date_format:m/d/Y',
			'coach'   => 'numeric'
		) );
		$response = [];
		if ( $validator->passes() ) {
			$start = Carbon::createFromFormat( 'm/d/Y', $start )->setTime( 0, 0 );
			$end   = Carbon::createFromFormat( 'm/d/Y', $end )->setTime( 0, 0 )->addDay( 1 );

			Log::info( 'Loading Sessions Completed report for: ' . $start . ' - ' . $end );
			$is_download_request = $this->isTableDownloadRequest();

			$sessions_completed_report = $this->healthslate_model->get_sessions_completed_report_for_duration( $facility_id, $start, $end );
			Log::info( 'Total records in Sessions Completed report are: ' . count( $sessions_completed_report ) );

			$sessions_completed_report = $this->curriculumServiceProvider->getEducationModulesSessionsCompletedReport( $sessions_completed_report, $start, $end, false );

			// is_download_request ?
			if ( $is_download_request && ! empty( $sessions_completed_report ) ) {
				// send all records
				info( 'Building data source for export' );
				$time = microtime( true );
				foreach ( $sessions_completed_report as $row ) {

					if(!is_null(request('_patient_hyperlink'))){
						unset( $row->last_name );
					}

					$row->name       		= ( $row->first_name . ' ' . $row->last_name );
					$row->complete_status 	= (@$row->complete_status == 1 && $row->no_of_weight_log > 0) ? 'Complete' : 'Incomplete';
					$row->viewed_units    	= isset( $row->viewed_units ) ? ( $row->viewed_units ) : '';

					//unset( $row->patient_id );
					unset( $row->no_of_weeks_in_program );
					unset( $row->user_id );
					unset( $row->uuid );
					unset( $row->diabetes_type_id );
					unset( $row->no_of_weight_log );
					unset( $row->email );
					unset( $row->first_name );
					unset( $row->last_name );

				}

				info('Created data set of '.count($sessions_completed_report)." time: " . round( ( microtime( true ) - $time ) * 1000, 2 ) . " ms");
				return $this->dataTableService->dataTableResponse( collect( $sessions_completed_report ), true );
			}

			info( 'Building data source for table' );
			$time = microtime( true );
			foreach ( $sessions_completed_report as $row ) {
				$row->full_name       = ( $row->first_name . ' ' . $row->last_name );
				$row->complete_status = (@$row->complete_status == 1 && $row->no_of_weight_log > 0) ? 'Complete' : 'Incomplete';
				$row->viewed_units    = isset( $row->viewed_units ) ? ( $row->viewed_units ) : '';
			}

			info('Created data set of '.count($sessions_completed_report)." time: " . round( ( microtime( true ) - $time ) * 1000, 2 ) . " ms");
			$this->data['paginated_result'] = $sessions_completed_report;
			$this->data['init_datatable'] = true;
			$this->data['test_patient_excluded_note'] = true;
			$this->data['result_headers'] = (object) [
				'patient_id'     => [ 'name' => "Patient Id" ],
				'full_name'      => [ 'name' => "Name" ],
				'complete_status'   => [ 'name' => "Session Complete", 'class' => 'text-center', 'tooltip' => 'Session Complete' ],
				'viewed_units'      => [ 'name' => "Viewed Units", 'class' => 'text-center', 'tooltip' => '# of Unique Units Viewed' ],
			];
			$response = [
				'start_date'  => $start,
				'end_date'  => $end,
				'html' => view( 'report.blocks.common-duration-report', $this->data )->render(),
				'page_description' => $duration_label ? 'Filtered for '.$duration_label : '',
				'success' => true,
				'total_records' => count($sessions_completed_report)
			];
		}
		else{
			$response = [
				'error' => $validator->errors()
			];
		}
		return response()->json( $response );
	}



    /**
     * Ajax Handler for Self Register Report
     *
     * @param Validator $validator
     * @param $facility_id
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Throwable
     */
    public function selfRegisterReport(Validator $validator, $facility_id ) {
        $start = request( 'start_date', null );
        $end   = request( 'end_date', null );
        $duration_label   = request( 'duration_label' );

        $validator = $validator::make(
            request()->input(), array(
            'start_date' => 'required|date_format:m/d/Y',
            'end_date'   => 'required|date_format:m/d/Y',
            'coach'   => 'numeric'
        ) );
        $response = [];
        if ( $validator->passes() ) {
            $start = Carbon::createFromFormat( 'm/d/Y', $start )->setTime( 0, 0 );
            $end   = Carbon::createFromFormat( 'm/d/Y', $end )->setTime( 0, 0 )->addDay( 1 );

            Log::info( 'Loading Self Register report for: ' . $start . ' - ' . $end );
            $is_download_request = $this->isTableDownloadRequest();

            $sessions_completed_report = $this->healthslate_model->get_self_register_report( $facility_id, $start, $end );
            Log::info( 'Total records in Self Register report are: ' . count( $sessions_completed_report ) );

            // is_download_request ?
            if ( $is_download_request && ! empty( $sessions_completed_report ) ) {
                // send all records
                info( 'Building data source for export' );
                $time = microtime( true );
                foreach ( $sessions_completed_report as $row ) {
                    if(!is_null(request('_patient_hyperlink'))){
                        unset( $row->last_name );
                    }
                    $row->name  = ( $row->first_name . ' ' . $row->last_name );
                    unset( $row->first_name );
                    unset( $row->last_name );
                }

                info('Created data set of '.count($sessions_completed_report)." time: " . round( ( microtime( true ) - $time ) * 1000, 2 ) . " ms");
                return $this->dataTableService->dataTableResponse( collect( $sessions_completed_report ), true );
            }

            info( 'Building data source for table' );
            $time = microtime( true );
            foreach ( $sessions_completed_report as $row ) {
                $row->full_name  = ( $row->first_name . ' ' . $row->last_name );
            }

            info('Created data set of '.count($sessions_completed_report)." time: " . round( ( microtime( true ) - $time ) * 1000, 2 ) . " ms");
            $this->data['paginated_result'] = $sessions_completed_report;
            $this->data['init_datatable'] = true;
            $this->data['hide_footer_note'] = true;
            $this->data['test_patient_excluded_note'] = true;
            $this->data['result_headers'] = (object) [
                'patient_id'            => [ 'name' => "Patient Id" ],
                'full_name'             => [ 'name' => "Name" ],
                'email'                 => [ 'name' => "Email", 'class' => 'text-center', 'tooltip' => 'Email Address' ],
                'mrn'                   => [ 'name' => "MRN", 'class' => 'text-center', 'tooltip' => 'MRN' ],
                'registration_date'     => [ 'name' => "Registration Date" ],
            ];

            $response = [
                'start_date'  => $start,
                'end_date'  => $end,
                'html' => view( 'report.blocks.common-duration-report', $this->data )->render(),
                'page_description' => $duration_label ? 'Filtered for '.$duration_label : '',
                'success' => true,
                'total_records' => count($sessions_completed_report)
            ];
        }
        else{
            $response = [
                'error' => $validator->errors()
            ];
        }
        return response()->json( $response );
    }




    /**
     * Ajax Handler for Self Register Report
     *
     * @param Validator $validator
     * @param $facility_id
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Throwable
     */
    public function usersRegistration(Validator $validator, $facility_id ) {
        $start = request( 'start_date', null );
        $end   = request( 'end_date', null );
        $duration_label   = request( 'duration_label' );
        $coach            = request( 'coach' , null);
        $user_timezone    = request( 'user_timezone' , null);
        $timezone = '';
        /*
        if(isset($_COOKIE['user_timezone'])) {
            $timezone =  $_COOKIE['user_timezone'];
        }*/
        if(isset($user_timezone)) {
            $timezone =  $user_timezone;
        }

        ini_set('memory_limit','-1');
        ini_set('max_execution_time', 0);

        $validator = $validator::make(
            request()->input(), array(
            'start_date' => 'required|date_format:m/d/Y',
            'end_date'   => 'required|date_format:m/d/Y',
            'coach'   => 'numeric'
        ) );
        $response = [];
        if ( $validator->passes() ) {

            $start = Carbon::createFromFormat( 'm/d/Y', $start )->setTime( 0, 0 );
            $end   = Carbon::createFromFormat( 'm/d/Y', $end )->setTime( 0, 0 )->addDay( 1 );

            $start =  ( ! empty( $timezone ) ) ? Carbon::createFromFormat('Y-m-d H:i:s', $start)->timezone($timezone) : $start;
            $end =  ( ! empty( $timezone ) ) ? Carbon::createFromFormat('Y-m-d H:i:s', $end)->timezone($timezone) : $end;

            Log::info( 'Loading Self Register report for: ' . $start . ' - ' . $end );
            $is_download_request = $this->isTableDownloadRequest();

            $report_data = $this->healthslate_model->get_user_registeration_report( $facility_id, $start, $end , $coach);

            if(!empty($report_data)) {
                foreach ($report_data as $row) {
                    if ($row->invitation_sent_date != '') {
                        $row->invitation_sent_date = ( ! empty( $timezone ) ) ? Carbon::createFromFormat('Y-m-d H:i:s', $row->invitation_sent_date)->timezone($timezone) : $row->invitation_sent_date;
                    }
                    if ($row->registration_date != '') {
                        $row->registration_date = ( ! empty( $timezone ) ) ? Carbon::createFromFormat('Y-m-d H:i:s', $row->registration_date)->timezone($timezone) : $row->registration_date;
                    }
                }
            }

           // dd($sessions_completed_report);
            Log::info( 'Total records in User Register report are: ' . count( $report_data ) );

            // is_download_request ?
            if ( $is_download_request && ! empty( $report_data ) ) {
                // send all records
                info( 'Building data source for export' );
                $time = microtime( true );
                foreach ( $report_data as $row ) {
                    unset($row->first_name);
                    unset($row->last_name);
                    if ($row->invitation_sent_date != '') {
                        $row->invitation_sent_date = Carbon::createFromFormat( 'Y-m-d H:i:s', $row->invitation_sent_date )->format(  config( 'healthslate.default_date_format' ) );

                    }
                    if ($row->registration_date != '') {
                        $row->registration_date = Carbon::createFromFormat( 'Y-m-d H:i:s', $row->registration_date )->format(  config( 'healthslate.default_date_format' ) );
                    }
                }

                //dd($report_data);

                info('Created data set of '.count($report_data)." time: " . round( ( microtime( true ) - $time ) * 1000, 2 ) . " ms");
                return $this->dataTableService->dataTableResponse( collect( $report_data ), true );
            }

            info( 'Building data source for table' );
            $time = microtime( true );

            info('Created data set of '.count($report_data)." time: " . round( ( microtime( true ) - $time ) * 1000, 2 ) . " ms");
            $this->data['paginated_result'] = $report_data;
            $this->data['init_datatable'] = true;
            $this->data['test_patient_excluded_note'] = true;
            $this->data['note_deleted_patient_excluded'] = true;
            $this->data['result_headers'] = (object) [
                'patient_id'           => [ 'name' => "Patient Id" ],
                'email'                => [ 'name' => "Email" ],
                'name'                 => [ 'name' => "Name" ],
                'mrn'                  => [ 'name' => "MRN", 'class' => 'text-center' ],
                'invitation_sent_date' => [ 'name' => "Invitation Sent On", 'class' => 'text-center', 'format_date' => config( 'healthslate.default_date_format' ) ],
                'registration_date'    => [ 'name' => "Registration Date", 'class' => 'text-center', 'format_date' => config( 'healthslate.default_date_format' ) ],
                'password'             => [ 'name' => "Password", 'class' => 'text-center' ],
                'self_register'        => [ 'name' => "Self Registered", 'class' => 'text-center' ],
                'days_since_reg'       => [ 'name' => "Days Since Registered", 'class' => 'text-center' ],
                'days_since_activate'  => [ 'name' => "Days Since Activated", 'class' => 'text-center', 'tooltip' => 'Days since Last password changed' ],
            ];

            $response = [
                'start_date'  => $start,
                'end_date'  => $end,
                'html' => view( 'report.blocks.common-duration-report', $this->data )->render(),
                'page_description' => $duration_label ? 'Filtered for '.$duration_label : '',
                'success' => true,
                'total_records' => count($report_data)
            ];
        }
        else{
            $response = [
                'error' => $validator->errors()
            ];
        }
        return response()->json( $response );
    }



    /**
     * Ajax Handler for Import Patient Report
     *
     * @param Validator $validator
     * @param $facility_id
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Throwable
     */
    public function importPatient(Validator $validator ) {
        $start = request( 'start_date', null );
        $end   = request( 'end_date', null );
        $duration_label   = request( 'duration_label' );
        $filter           = request( 'coach' , null);

        $timezone = '';
        if(isset($_COOKIE['user_timezone'])) {
            $timezone =  $_COOKIE['user_timezone'];
        }

        $validator = $validator::make(
            request()->input(), array(
            'start_date' => 'required|date_format:m/d/Y',
            'end_date'   => 'required|date_format:m/d/Y',
            'coach'   => 'numeric'
        ) );
        $response = [];
        if ( $validator->passes() ) {

            $start = Carbon::createFromFormat( 'm/d/Y', $start )->setTime( 0, 0 );
            $end   = Carbon::createFromFormat( 'm/d/Y', $end )->setTime( 0, 0 )->addDay( 1 );

            $start =  ( ! empty( $timezone ) ) ? Carbon::createFromFormat('Y-m-d H:i:s', $start)->timezone($timezone) : $start;
            $end =  ( ! empty( $timezone ) ) ? Carbon::createFromFormat('Y-m-d H:i:s', $end)->timezone($timezone) : $end;

            Log::info( 'Loading Bilk Import Patient Register report for: ' . $start . ' - ' . $end );
            $is_download_request = $this->isTableDownloadRequest();

            $report_data = $this->healthslate_model->get_bulk_import_patient_report($start, $end , $filter);

            if(!empty($report_data)) {
                foreach ($report_data as $row) {
                    if ($row->import_id != null) {
                        $date_import = ( ! empty( $timezone ) ) ? Carbon::createFromFormat('Y-m-d h:i:s A', $row->import_id)->timezone($timezone) : $row->import_id;
                        $row->import_id =  $date_import->format('Y-m-d h:i:s A');
                    }
                }
            }

            // dd($sessions_completed_report);
            Log::info( 'Total records in Import Patient report are: ' . count( $report_data ) );

            // is_download_request ?
            if ( $is_download_request && ! empty( $report_data ) ) {
                // send all records
                info( 'Building data source for export' );
                $time = microtime( true );
                foreach ( $report_data as $row ) {
                    if(!is_null(request('_patient_hyperlink'))){
                        unset( $row->last_name );
                    }
                    unset( $row->first_name );
                    unset( $row->last_name );
                }

                info('Created data set of '.count($report_data)." time: " . round( ( microtime( true ) - $time ) * 1000, 2 ) . " ms");
                return $this->dataTableService->dataTableResponse( collect( $report_data ), true );
            }

            info( 'Building data source for table' );
            $time = microtime( true );

            info('Created data set of '.count($report_data)." time: " . round( ( microtime( true ) - $time ) * 1000, 2 ) . " ms");
            $this->data['paginated_result'] = $report_data;
            $this->data['init_datatable'] = true;
            $this->data['test_patient_excluded_note'] = true;
            $this->data['result_headers'] = (object) [
                'name'                 => [ 'name' => "Patient Name" ],
                'email'                => [ 'name' => "Email" ],
                'solera_id'            => [ 'name' => "MRN", 'class' => 'text-center' ],
                'facility_id'          => [ 'name' => "FacilityId", 'class' => 'text-center'],
                'diabetes_type_id'     => [ 'name' => "TypeId", 'class' => 'text-center' ],
                'failure_cause'        => [ 'name' => "Status", 'class' => 'text-center' ],
                'import_id'            => [ 'name' => "Import Date", 'class' => 'text-center'],
            ];

            $response = [
                'start_date'  => $start,
                'end_date'  => $end,
                'html' => view( 'report.blocks.common-duration-report', $this->data )->render(),
                'page_description' => $duration_label ? 'Filtered for '.$duration_label : '',
                'success' => true,
                'total_records' => count($report_data)
            ];
        }
        else{
            $response = [
                'error' => $validator->errors()
            ];
        }
        return response()->json( $response );
    }


}// End of class