<?php

namespace HealthSlateAdmin\Http\Controllers;

use File;
use HealthSlateAdmin\Helpers\MailServiceHelper;
use HealthSlateAdmin\Models\Healthslate;
use HealthSlateAdmin\Models\ReportService\ReportTriggerQuery;
use HealthSlateAdmin\Models\ReportService\Schedule;
use HealthSlateAdmin\Models\ReportService\ScheduleRunLog;
use Illuminate\Http\Request;
use Illuminate\Console\Scheduling\Schedule as TaskSchedule;
use Validator;
use Carbon\Carbon;
use HealthSlateAdmin\Http\Requests;
use HealthSlateAdmin\Http\Controllers\Controller;

class ReportSchedule extends Controller {

	/**
	 *  common variable for passing data to view
	 * @var array
	 */
	protected $data = array();

	/**
	 * @var Schedule
	 */
	protected $schedule;

	/**
	 * @var ReportTriggerQuery
	 */
	protected $reportTriggerQuery;

	/**
	 * @var ScheduleRunLog
	 */
	protected $scheduleRunLog;

	/**
	 * @var Healthslate
	 */
	protected $healthslate;

	/**
	 * @var MailServiceHelper
	 */
	protected $mailServiceHelper;

	/**
	 * @var array
	 */
	protected $available_reports = [
		'unprocessed-meals' => 'Unprocessed Meals'
	];

	/**
	 * @var array
	 */
	protected $available_occurrence = [
		'0 * * * *' => 'Hourly',
		'0 0 * * *' => 'Daily @ 12.00 AM',
		'0 0 * * 0' => 'Weekly',
		'0 0 1 * *' => 'Monthly'
	];

	/**
	 * @param Schedule $schedule
	 * @param ReportTriggerQuery $reportTriggerQuery
	 * @param ScheduleRunLog $scheduleRunLog
	 * @param Healthslate $healthslate
	 * @param MailServiceHelper $mailServiceHelper
	 */
	function __construct( Schedule $schedule, ReportTriggerQuery $reportTriggerQuery, ScheduleRunLog $scheduleRunLog, Healthslate $healthslate, MailServiceHelper $mailServiceHelper ) {
		$this->schedule           = $schedule;
		$this->reportTriggerQuery = $reportTriggerQuery;
		$this->scheduleRunLog     = $scheduleRunLog;
		$this->healthslate        = $healthslate;
		$this->mailServiceHelper  = $mailServiceHelper;

	}

	/**
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	function manageReportSchedule() {
		$this->data['css'] = [ 'select2' ];
		$this->data['js']  = [ 'select2' ];

		$this->data['scheduled_alerts'] = $this->schedule
			->with( [ 'reportTrigger' ] )
			->get();

		$send_to_list = [ ];
		foreach ( $this->data['scheduled_alerts'] as &$schedule ) {
			if ( isset( $schedule->send_to ) ) {
				$ids = explode( ',', $schedule->send_to );
				foreach ( $ids as $id ) {
					$send_to_list[ $id ] = $id;
				}
				$schedule->send_to = implode( ', ', $ids );
			}
		}
		$this->data['send_to_list']         = $send_to_list;
		$this->data['available_reports']    = [ '0' => 'Select Report' ] + $this->available_reports;
		$this->data['available_occurrence'] = [ '0' => 'Select Occurrence' ] + $this->available_occurrence;

		$this->data['available_report_triggers'] = collect( $this->reportTriggerQuery->lists( 'query_description', 'report_trigger_query_id' ) )->prepend( 'Select Trigger Event', '' );

		return view( 'schedule.manage-schedule', $this->data );
	}

	/**
	 * @param Validator $validator
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	function scheduleAlert(Validator $validator) {
		info( 'Processing scheduleAlert request' );
		Validator::extend('isArrayOfEmails', function ($attribute, $value, $parameters) {
			// define closure separately for readability
		    $checkEmails = function($carry, $email){
		        return $carry && filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
		    };
			// validate that the value given is an array of valid email addresses
            return (is_array($value) && array_reduce($value,$checkEmails,true));
		},'Please provide a valid list of :attribute.');

		$validator = $validator::make(
			request()->input(), array(
			'schedule_id' => 'integer|exists:report_service.schedule,schedule_id',
			'report_name'   => 'required|in:'.('"'.implode('","',array_keys($this->available_reports)).'"'),
			'send_to' => 'required|isArrayOfEmails',
			'occurrence'   => 'required|in:'.('"'.implode('","',array_keys($this->available_occurrence)).'"'),
			'trigger' => 'integer|exists:report_service.report_trigger_query,report_trigger_query_id',
		));

		$response = [];
		if ( $validator->passes() ) {
			$schedule_id = request( 'schedule_id', null );
			$report_name = request( 'report_name', null );
			$send_to = request( 'send_to',[]);
			$occurrence   = request( 'occurrence', null );
			$trigger   = request( 'trigger', null );

			if($schedule_id){
				$schedule = $this->schedule->find($schedule_id);
			}else{
				$schedule = new Schedule();
			}

			$schedule->report_name = $report_name;
			$schedule->report_trigger = !empty($trigger) ? $trigger : null;
			$schedule->send_to = implode(',',$send_to);
			$schedule->occurrence = $occurrence;
			$schedule->start_date = date('Y-m-d H:i:s');
			$schedule->end_date = null;
			$schedule->scheduled_by = @session('user')->userName;

			$schedule->save();
			$response = [
				'success'     => trans_choice( 'common.record_stored_updated_success', 1, [ 'action' => ($schedule_id ? 'updated' : 'stored') ] ),
				'send_to' => route( 'manage-schedule' ),
				'redirect_msg'  => trans('common.redirecting_please_wait',['location' => route('manage-schedule')])
			];
		}else{

			$messages      = $validator->errors();
			info('Validation Rules failed',$messages);
			$errorMessages = [ ];
			foreach ( $messages->all( '<p class="text-danger">:message</p>' ) as $message ) {
				$errorMessages[] = $message;
			}
			$response = [
				'error' => $errorMessages
			];
		}
		return response()->json( $response );
	}

	/**
	 * Deactivate Action Handler
	 *
	 * @param Validator $validator
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function deleteSchedule(Validator $validator){
		info( 'Processing deleteSchedule request' );
		$validator = $validator::make(
			request()->input(), array(
			'schedule_id' => 'required|integer|exists:report_service.schedule,schedule_id'
		));
		if ( $validator->fails() ) {
			return response()->json( [
				'error' => '<p class="text-danger">'.trans('common.invalid_request_data').'</p>'
			] );
		}
		$schedule_id = request( 'schedule_id', null );
		$schedule = $this->schedule->find($schedule_id);
		$schedule->end_date = date('Y-m-d H:i:s');
		$schedule->save();
		return response()->json( [
			'success' => trans_choice('common.record_stored_updated_success',1,['action' => 'Deactivate']),
            'redirect_to' => route('manage-schedule'),
			'wait'  => 1
		] );
	}

	/**
	 * @param TaskSchedule $schedule
	 */
	public function runScheduleAlerts( TaskSchedule $schedule ) {
		info( 'Scheduled Task Runner running' );
		$now = Carbon::now();

		// fetching active scheduled alerts
		$scheduled_alerts = $this->schedule
			->with( [ 'reportTrigger' ] )
			->where( function ( $q ) use ( $now ) {
				$q->whereNull( 'end_date' );
				$q->orWhere( 'end_date', '>', $now );

			} )->where( 'start_date', '<', $now )->get();

		if ( ! empty( $scheduled_alerts ) ) {

			info( 'Total Scheduled Alerts: ' . count( $scheduled_alerts ) );

			foreach ( $scheduled_alerts as $row ) {

				info( 'Scheduled Task ID: ' . $row->schedule_id . ' occurrence: ' . $row->occurrence );

				$schedule->call( function () use ( $row ) {
					$start_time = microtime( true );
					info( 'Scheduled Task ID: ' . $row->schedule_id . ' Started ' );
					$send_alert     = true;
					$trigger_result = [ ];

					if ( ! empty( $row->reportTrigger ) ) {
						info( 'Scheduled Task has additional trigger: "' . $row->reportTrigger->query . '", result_threshold > ' . $row->reportTrigger->result_threshold );
						$result = $this->healthslate->get_query_result( $row->reportTrigger->query );

						if(count($result) == 0){
							$send_alert = false;
						}
						else if(count($result) == 1){
							if ( isset( $result[0] ) && ( @$result[0]->record_count <= $row->reportTrigger->result_threshold ) ) {
								$send_alert = false;
							} elseif ( isset( $result[0] ) ) {
								$trigger_result = $result[0];
							}
						}else{
							$trigger_result = $result;
						}
					}
					if ( $row->report_name == 'unprocessed-meals' && $send_alert ) {
						$this->sendUnprocessedMealsAlert( $row, $trigger_result );
					}

					info( "Scheduled Task ID: " . $row->schedule_id . " Completed. Execution time : " . round( ( microtime( true ) - $start_time ) * 1000, 4 ) . " ms" );

				} )->cron( $row->occurrence );
			}
		}
		if ( defined( 'LARAVEL_START' ) ) {
			info( "Scheduled Task Runner Completed. Execution time : " . round( ( microtime( true ) - LARAVEL_START ) * 1000, 4 ) . " ms" );
		}

	}

	/**
	 * @param $schedule_row
	 * @param array $trigger_result
	 *
	 * @return array
	 */
	private function sendUnprocessedMealsAlert( $schedule_row, $trigger_result = null ) {
		$title = isset( $this->available_reports[ $schedule_row->report_name ] ) ? $this->available_reports[ $schedule_row->report_name ] : $schedule_row->report_name;

		info( 'Executing sendUnprocessedMealsAlert ' );

		$subject = $title . ' Report';
		$body    = [
			'content'        => $subject,
			'table'             => is_array($trigger_result) ? $trigger_result : null,
			'footer_message' => ( ! empty( $schedule_row->reportTrigger ) ? 'Report Scheduled with Additional Trigger : ' . $schedule_row->reportTrigger->query_description : '' )
		];
		if ( ! empty( $schedule_row->reportTrigger ) && ! empty( $trigger_result ) && is_object($trigger_result) ) {
			$subject         = trans( 'common.email_subject_unprocessed_meal_alert', [ 'record' => $trigger_result->record_count ] );
			$body['content'] = trans( 'common.email_message_unprocessed_meal_alert', [ 'record' => $trigger_result->record_count ] );
		}
		$to_list = explode( ',', $schedule_row->send_to );

		foreach ( $to_list as $to ) {
			$status = $this->mailServiceHelper->sendEmail( $subject, $body, trim( $to ) );
			$this->scheduleRunLog->create( [
				'schedule_id'    => $schedule_row->schedule_id,
				'sent_to'        => $to,
				'success_status' => $status
			] );

		}
		info( 'Execution completed sendUnprocessedMealsAlert ' );
	}
}
