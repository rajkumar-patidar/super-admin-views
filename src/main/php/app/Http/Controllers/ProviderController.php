<?php

namespace HealthSlateAdmin\Http\Controllers;

use HealthSlateAdmin\Http\Controllers\Controller;
use HealthSlateAdmin\Models\User;
use Validator;

class ProviderController extends Controller {
	/**
	 *  common variable for passing data to view
	 * @var array
	 */
	protected $data = array();

	/**
	 * @var User
	 */
	protected $user;

	/**
	 * @param User $user
	 */
	function __construct( User $user ) {
		$this->user = $user;
	}

	/**
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function providerWeeklyParticipationReport() {
		$this->data['css'] = [ 'datetime' ];
		$this->data['js']  = [ 'datetime' ];

		$this->data['show_duration_filter'] = true;
		$this->data['report_src']           = route( 'ajax.report.coach-weekly-participation' );

		$this->data['report_meta'] = (object) [
			'title'      => 'Weekly Participation Report',
			'body_class' => 'weekly-participation coach-weekly-participation'

		];

		return view( 'report.provider.my-weekly-participation', $this->data );
	}

	/**
	 * @param Validator $validator
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function ajaxProviderWeeklyParticipationReport( Validator $validator ) {
		$provider = $this->user->where( 'email', '=', session( 'user' )->userName )->first()->provider()->first();
		// is user is not a $provider?
		if ( empty( $provider ) ) {
			return response()->json( [ 'error' => 'Coach provider not found' ] );
		}
		info( 'Processing Request for ProviderController@ajaxProviderWeeklyParticipationReport ' );
		request()->merge( [ 'coach' => $provider->provider_id, '_patient_hyperlink' => true ] );

		return app( 'HealthSlateAdmin\Http\Controllers\ReportingServiceAjaxController' )->weeklyParticipationReport( $validator, $provider->facility_id );
	}

}
