<?php
/**
 * User: spatel
 * Date: 11/1/16
 */

namespace HealthSlateAdmin\Http\Controllers;

use HealthSlateAdmin\Helpers\DataTableServiceProvider;
use HealthSlateAdmin\Http\Controllers\Controller;
use HealthSlateAdmin\Models\Facility;
use HealthSlateAdmin\Models\Healthslate;
use HealthSlateAdmin\Models\Patient;
use HealthSlateAdmin\Models\Provider;
use HealthSlateAdmin\Models\PatientAppServiceProvider;
use HealthSlateAdmin\Helpers\MailServiceHelper;
use Log;

class AjaxServiceController extends Controller {

	/**
	 * @var Facility
	 */
	protected $facility;

	/**
	 * @var Provider
	 */
	protected $provider;

	/**
	 * @var Patient
	 */
	protected $patient;
	/**
	 * @var Healthslate
	 */
	protected $healthslate_model;

	/**
	 * @var DataTableServiceProvider
	 */
	protected $dataTableService;

    /**
     * @var MailServiceHelper
     */
    protected $mailServiceHelper;

	function __construct( Facility $facility, Patient $patient, Provider $provider, Healthslate $healthslate, DataTableServiceProvider $dataTableServiceProvider, MailServiceHelper $mailServiceHelper) {
		parent::__construct();
		$this->facility          = $facility;
		$this->provider          = $provider;
		$this->patient           = $patient;
		$this->healthslate_model = $healthslate;
		$this->dataTableService  = $dataTableServiceProvider;
        $this->mailServiceHelper  = $mailServiceHelper;
	}

	/**
	 * facilities
	 *
	 * @return mixed
	 */
	public function facilities() {

		$facilities = $this->facility->with( 'providers')->get();
		info( 'Total Facility : ' . count( $facilities ) );
		$data       = collect();
		foreach ( $facilities as $facility ) {
			$row = [
				'facility_id'             => $facility->facility_id,
				'name'                    => $facility->name,
				'contact_person_name'     => $facility->contact_person_name,
				'address_info'            => $facility->address_info,
				'providers'               => '<a class="pointer text-light-blue" href="' . route( 'facility.providers', [ 'facility_id' => $facility->facility_id ] ) . '">' . $facility->providers->count() . '</a>',
				'patients'                => '<a class="pointer text-light-blue" href="' . route( 'facility.patients', [ 'facility_id' => $facility->facility_id ] ) . '">' . $facility->patients->count() . '</a>',
				'is_notification_enabled' => '<span class="label ' . ( bit_to_int( $facility->is_notification_enabled ) ? 'label-success' : 'label-danger' ) . '"> ' . ( bit_to_int( $facility->is_notification_enabled ) ? 'Y' : 'N' ) . ' </span>',
				'manage'            	  => '<a class="pointer text-light-blue" href="' . route( 'facility.setting', [ 'facility_id' => $facility->facility_id ] ) . '">Manage</a>',
				'num_providers'  => $facility->providers->count(),
				'num_patients'  => $facility->patients->count(),
			];
            info( 'Facility Data: ' . json_encode($row) );
			$data->push( array_values( $row ) );
		}
        info( 'Sending Facility Data For DataTable : ' . count($data) );
		return $this->dataTableService->dataTableResponse($data);
	}

	/**
	 * facilityProviders
	 *
	 * @param $facility_id
	 *
	 * @return mixed
	 */
	public function facilityProviders( $facility_id ) {
		$data = collect();

		$facility = $this->facility->find( $facility_id );
		// is facility id is invalid?
		if ( empty( $facility ) ) {
			// no records found
			$providers = [];
		}else {
			$providers = $facility->providers()->orderBy( 'provider_id' )->with( [
				'user',
				'user.authority',
				'patientCountRelation'=> function($q){
					$q->isDeleted(false);
				}
			] )->get();
		}

		foreach ( $providers as $provider ) {
			$row = [
				'provider_id'   => $provider->provider_id,
				'full_name'     => ! empty( $provider->user ) ? $provider->user->full_name : '',
				'authority'     => ( ! empty( $provider->user ) && ! empty( $provider->user->authority ) ) ? $provider->user->authority->authority : '',
				'type'          => $provider->type,
				'patients'      => $provider->patientCount,
				'is_access_all' => bit_to_int( $provider->is_access_all ) ? 'Y' : '-',
				'is_deleted'    => '<span class="is_deleted ' . ( bit_to_int( $provider->is_deleted ) ? 'label label-danger' : '' ) . '" data-is-deleted="' . bit_to_int( $provider->is_deleted ) . '">' . ( bit_to_int( $provider->is_deleted ) ? 'Deleted' : '-' ) . '</span>',
				'num_patients'  => $provider->patientCount,
			];
			$data->push( array_values( $row ) );
		}

		return $this->dataTableService->dataTableResponse($data);
	}

	/**
	 * facilityPatients
	 *
	 * @param $facility_id
	 *
	 * @return mixed
	 */
	public function facilityPatients( $facility_id ) {

		$facility = $this->facility->find( $facility_id );

		// is facility id is invalid?
		if ( empty( $facility ) ) {
			// no records found
			$patients = [];
		}else {
			// status filter
			$status = request( 'status', null );
			if(!in_array($status, ['invited_patient','invitation_acceptance_pending','accepted_invitation_not_registered','registered','registered_no_activity','performed_activity'])){
				$status = null;
			}
			// days filter
			$days = intval(request( 'days', null ));

			// fetch patients
			$patients = $this->healthslate_model->get_facility_patients($facility_id, $status, $days);
		}

		info( 'Total Facility Patients : ' . count( $patients ) );
		/*$patients_app_v_log = collect();
		if(!empty($patients)){
			$patients_app_v_log = collect($this->healthslate_model->get_patients_app_version_log());
			$patients_app_v_log = $patients_app_v_log->groupBy('patient_id');
		}
		info( 'Total App version logs: ' . ( $patients_app_v_log->count() ) );*/

		$data = collect();
		foreach ( $patients as $key => $patient ) {
			//$patient_app_v_log = isset($patients_app_v_log[$patient->patient_id]) ? $patients_app_v_log[$patient->patient_id][0] : null;
			// creating row from result set
			$full_name = '<a href="' . route( 'patient.detail', [
					'patient_id'  => $patient->patient_id,
					'facility_id' => $facility_id
				] ) . '">' . $patient->full_name . '</a>';
			$user_type = $patient->user_type;
			$leadCoach  = ( ! empty( $patient->lead_coach_id ) ? '<span data-lead-coach-id="' . $patient->lead_coach_id . '">' . @$patient->lead_coach_full_name . '</span>' : '' );
			$is_deleted = '<span class="is_deleted ' . ( bit_to_int( $patient->is_deleted ) ? 'label label-danger' : '' ) . '" data-is-deleted="'.bit_to_int( $patient->is_deleted ).'">' . ( bit_to_int( $patient->is_deleted ) ? 'Deleted' : '-' ) . '</span>';

			// row object for datatable
			$row = [
				'patient_id' => $patient->patient_id,
				'full_name'  => $full_name,
				'app_version' => trim(( ! empty( $patient->device_type ) ? $patient->device_type . ' - ' : '' ) . ( !empty($patient->app_version) ? @$patient->app_version : '' ), '- '),
				'mrn'        => $patient->mrn,
				'leadCoach'  => $leadCoach,
				'view_link'  => '<a href="' . route( 'patient.detail', [
						'patient_id'  => $patient->patient_id,
						'facility_id' => $facility_id
					] ) . '">View</a>',
				'is_deleted' => $is_deleted,
			];
			$data->push( array_values( $row ) );
		}

		return $this->dataTableService->dataTableResponse($data);
	}

	/**
	 * @param $patient_id
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function patientLogs($patient_id){

		$patient = $this->patient->with(['user','diabetesType'])->find( $patient_id );

		// is patient id is invalid?
		if ( empty( $patient ) ) {
			return response()->json(['success' => true, 'html' => '']);
		}

		$start = request('start_date');
		$end = request('end_date');

		$logs = $patient->logs()
			->logsBetween($start, $end)
			->whereRaw($patient->getConnection()->raw('(ORD(is_suggested) = 0 OR is_suggested IS NULL)'))
			->orderBy('log_time','desc')->with([
			'activityLog',
			'exerciseLogs',
			'foodLogSummary',
			'glucoseLog',
			'logMoods',
			'medicationLog',
			'miscLog',
			'reportErrors',
			'notes'
		])->get();

		$patient->logs = $logs;
		$this->data ['patient'] = $patient;
		$this->data ['HS_CONSTANTS'] = $this->HS_CONSTANTS;
		$this->data['log_group'] = $logs->groupBy('readable_log_time');

		$html = view( 'blocks.patient-log-timeline-div', $this->data )->render();
		return response()->json(['success' => true, 'html' => $html]);
	}

	/**
	 * search patient by id
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function searchPatient(){

		$search = request('q');
		$patients = collect();
		if(!empty($search)) {

			$result = $this->patient
				->with( [ 'user', 'facilities' ] )
				->where( function ( $query ) use ( $search ) {
					$query->where( 'patient_id', '=', $search );
				} )->get();

			foreach ( $result as $patient ) {
				$row = [
					"patient_id"  => $patient->patient_id,
					"id"          => $patient->patient_id,
					"user_id"     => ! empty( $patient->user ) ? $patient->user->user_id : '',
					"text"        => ! empty( $patient->user ) ? $patient->user->full_name : '-',
					"facility_id" => ! empty( $patient->facilities && $patient->facilities->count() ) ? $patient->facilities->first()->name : '-',
					"url"         => route( 'patient.detail',['patient_id' => $patient->patient_id ] )
				];
				$patients->push( $row );
			}
		}
		$data = [
			"total_count" => $patients->count(),
			 "items" => $patients
		];
		return response()->json($data);
	}


    /**
     * search patient by id
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendMessages(){
        ini_set("memory_limit",-1);
        info('Calling facility Table');
        $facility_patients = collect($this->healthslate_model->get_facility_patients(request('send_message_facility_id')));
        info('Responce from facility Table');
        $patient_list = [];
        foreach($facility_patients as $patient){
            if($patient->uuid != '')
            {
                $patient_list[] = (object) [
                    'id'    => $patient->uuid,
                    'text'    => $patient->full_name . ' (' . $patient->patient_id .')',
                ];
            }
        }
        $data['coach_list'] = $patient_list;

        return response()->json($data);
    }


    /**
     * search patient by id
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendMessagesToUser(PatientAppServiceProvider $patientAppServiceProvider){

        $facility_id = request('send_message_facility_id');
        $patient_ids = request('send_message_to_patient');
        $notes = request('notes');

        //dd($patient_ids);

        $data['patientIds'] = $patient_ids;
        $data['message'] = $notes;

        if ( !empty( $data ) ) {
            $result = $patientAppServiceProvider->sendMessagesToUser($data);
            if ($result === true) {
                return $response = [
                    'success' => 'Message Sent Successfully',
                ];
            } else {
                return $response = [
                    'error' => $result->errorDetail,
                ];
            }
        }

    }

}