<?php
/**
 * Created by PhpStorm.
 * User: spatel
 * Date: 4/1/16
 * Time: 7:19 PM
 */

namespace HealthSlateAdmin\Http\Controllers;

use Carbon\Carbon;
use HealthSlateAdmin\Helpers\HealthSlateObfuscator;
use HealthSlateAdmin\Models\DiabetesType;
use HealthSlateAdmin\Models\Patient;
use HealthSlateAdmin\Models\PatientAppServiceProvider;
use HealthSlateAdmin\Models\PendingPatientSignUp;
use HealthSlateAdmin\Models\Provider;
use HealthSlateAdmin\Models\Facility;
use HealthSlateAdmin\Models\User;
use Log;
use Exception;
use File;
use HealthSlateAdmin\Helpers\SqlExportHelper;
use Maatwebsite\Excel\Collections\RowCollection;
use Maatwebsite\Excel\Collections\SheetCollection;
use Symfony\Component\Process\Process;
use Validator;
use DB;

class HomeController extends Controller {
	/**
	 *  common variable for passing data to view
	 * @var array
	 */
	protected $data = [];

	/**
	 * @var Facility
	 */
	protected $facility;

	/**
	 * @var Patient
	 */
	protected $patient;

	/**
	 * @var Provider
	 */
	protected $provider;

	/**
	 * @param Facility $facility
	 * @param Patient $patient
	 * @param Provider $provider
	 */
	function __construct( Facility $facility, Patient $patient, Provider $provider ) {
		parent::__construct();
		$this->facility = $facility;
		$this->patient  = $patient;
		$this->provider = $provider;
	}

	/**
	 * index
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index() {
		return view( 'welcome', $this->data );
	}

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function facilityList() {
		return view( 'facility.facility-list', $this->data );
	}

	/**
	 * @param $facility_id
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
	 */
	public function facilityProviders( $facility_id ) {

		$facility = $this->facility->find( $facility_id );

		// is facility id is invalid?
		if ( empty( $facility ) ) {
			return $this->redirectBack();
		}
		$this->data ['facility']  = $facility;

		return view( 'facility.facility-providers', $this->data );
	}

	/**
	 * @param $facility_id
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
	 */
	public function facilityPatients( $facility_id ) {

		$facility = $this->facility->find( $facility_id );

		// is facility id is invalid?
		if ( empty( $facility ) ) {
			return $this->redirectBack();
		}
		$this->data ['facility'] = $facility;
		$this->data ['status_filter'] = [
			0                                    => 'All Patients',
			'invited_patient'                    => 'Is Invited Patient',
			'invitation_acceptance_pending'      => 'Acceptance Pending',
			'accepted_invitation_not_registered' => 'Accepted Invitation',
			'registered'                         => 'Enrolled',
			'registered_no_activity'             => 'Registered but No Activity',
			'performed_activity'                 => 'Performed Activity Once'
		];
		$this->data['days_list'] = [ '0' => 'Any', '3' => 3, '5' => 5, '7' => 7, '10' => 10, '20' => 20, '30' => 30 ];

		return view( 'facility.facility-patients', $this->data );
	}

	/**
	 * @param Validator $validator
	 * @param           $facility_id
	 *
	 * @return array|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
	 */
	public function facilitySetting( Validator $validator, $facility_id )
	{
		$form_fields = [
			'phone'                   => 'Phone',
			'email'                   => 'Email',
			'doctors_name'            => 'Doctor\'s Name',
			'hospital_name'           => 'Hospital Name',
			'dob'                     => 'DoB',
			'age'                     => 'Age',
			'ethnicity'               => 'Ethnicity',
			'height'                  => 'Height',
			'weight'                  => 'Weight',
			'is_physically_active'    => 'Is physically active',
			'relatives_have_diabetes' => 'Relatives have Diabetes',
			'health_conditions'       => 'Health Conditions',
		];

        $hide_form_fields = [
            'discount'       => 'Includes Discounts Content',
            'header'         => 'Standard Header',
            'help'           => 'HealthSlate Help Content',
            'eligibility'    => 'Eligibility',
            'faq'            => 'FAQ Link',
            'quiz'           => 'Quiz',
        ];

		$facility = $this->facility->find( $facility_id );

		if (request()->getMethod() == 'POST') {
			info( 'Processing Data:', request()->except( [ 'brand_name_logo', 'logo' ] ) );
			// is facility id is invalid?
			if (empty( $facility )) {
				return [ 'error' => '<p class="text-danger">' . trans( 'common.invalid_request_data' ) . '</p>', ];
			}

			// Custom Validator for Footer link
			Validator::extend( 'isValidLinkArray', function ( $attribute, $value, $parameters ) {
				if (empty( $value )) {
					return true;
				}
				// define closure separately for readability
				$checkFooterLinksArray = function ( $carry, $link ) {

					return $carry && isset( $link['title'] ) && ! empty( $link['title'] );
				};
				// validate that the value given is an array of valid Footer Link array
				return ( is_array( $value ) && array_reduce( $value, $checkFooterLinksArray, true ) );
			}, 'Each footer link must have a title.' );

			$validation = $validator::make( request()->input(), [
				'identifier'               => 'required',
				'brand_name'               => 'required',
				'page_title'               => 'required',
				'logo'                     => 'required',
				'footer_copyright_message' => 'required',
				'footer_links'             => 'isValidLinkArray',
				'disabled_fields'          => 'array',
			], [
				'identifier.required' => 'Identifier is required.',
				'logo.required'       => 'Brand Logo is required.',
			] );

			if ($validation->fails()) {
				info( 'Validation failed', $validation->errors()->all() );
				// Send error response
				return [
					'error' => '<p class="text-danger">' . trans( 'common.invalid_request_data' ) . implode( '', $validation->errors()->all( '<br/> :message' ) ) . '</p>',
				];
			}

			$footer_links = [ ];
			$links        = request()->input( 'footer_links', [ ] );
			foreach ($links as $link) {
				if ( ! empty( $link['title'] )) {
					$footer_links[$link['title']] = ( ( ! empty( $link['href'] ) ) ? $link['href'] : '' );
				}
			}

			//info('Disabled Fields ', request( 'disabled_fields' ));
			// Building json data object
			$data                          = [
				'brand_name'               => request( 'brand_name' ),
				'page_title'               => request( 'page_title' ),
				'footer_copyright_message' => request( 'footer_copyright_message' ),
				'custom_style'             => request( 'custom_style' ),
				'footer_links_list'        => $footer_links,
				'logo'                     => request( 'logo' ),
				'brand_name_logo'          => request( 'brand_name_logo' ),
				'disabled_fields'          => request( 'disabled_fields' ),
                'landing_page'             => request( 'landing_page' ),
                'hide_form_fields'         => request( 'hide_form_fields' ),
                'cobrand_charge'           => request( 'cobrand_charge' ),
                'logo_link'                => request( 'logo_link' )
			];
			$facility->onboarding_url      = request( 'identifier' );
			$facility->onboarding_app_data = json_encode( $data );

			$saved = $facility->save();
			return [
				'success'     => '<p class="text-success">' . trans_choice( 'common.record_stored_updated_success', 1, [ 'action' => 'stored' ] ) . '</p>',
				// reload page?
				//'wait'        => 5, 'redirect_to' => request()->getUri()
			];

		} else {
			// is facility id is invalid?
			if (empty( $facility )) {
				return $this->redirectBack();
			}
			$co_brand_data                = (object) json_decode( $facility->onboarding_app_data );
			$co_brand_data->identifier    = $facility->onboarding_url;
			$this->data ['co_brand_data'] = $co_brand_data;
			$this->data ['facility']      = $facility;
			$this->data ['form_fields']   = $form_fields;
            $this->data ['hide_form_fields']   = $hide_form_fields;
			return view( 'facility.manage-setting', $this->data );
		}
	}

	/**
	 * @param $patient_id
	 * @param null $facility_id
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
	 */
	public function patientDetail( $patient_id, $facility_id = null ) {

		$patient = $this->patient->with([
			'leadCoach',
			'primaryFoodCoach',
			'user',
			'providers'=> function($query){
				$query->orderBy('is_deleted','asc' );
			},
			'providers.user',
			'facilities',
			'messages' => function($query){
				$query->orderBy('timestamp','desc' );
			},
			'messages.providerUser',
			'patientMotivationImages',
			'diabetesType',
			'serviceFilterLogs' => function($query){
				$query->orderBy('logging_id','desc')->limit(1);
			},
			'device'
		])->find( $patient_id );
		// is patient id is invalid?
		if ( empty( $patient ) || empty($patient->user) ) {
			return $this->redirectBack();
		}
		$grouped_messages = $patient->messages->groupBy('user_id');

		$messages = collect();
		foreach($grouped_messages as $message_list){
			$messages->push($message_list->first());
		}
		$patient->messages = $messages;
		$patient->logs = $patient->logs()->recentWeeks()->orderBy('log_time','desc')->get();

		$this->data ['facility_id'] = $facility_id;
		$this->data ['patient']     = $patient;

		if ( ! empty( $patient->providers ) && $patient->providers->count() ) {

			// all providers except coaches are team members
			$team = $patient->providers->filter( function ( $provider ) {
				return in_array( $provider->type, [ 'Food Coach', 'Tech Support' ] );
			} );
			if ( ! empty( $patient->leadCoach ) ) {
				$patient->leadCoach->type = "Lead Coach";
				$team->prepend( $patient->leadCoach );
			}
			$this->data ['patient_team'] = $team;
		}

		return view( 'patient.patient-detail', $this->data );
	}

	/**
	 * patientUserMessages
	 *
	 * @param $patient_id
	 * @param null $user_id
	 *
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
	 */
	public function patientUserMessages($patient_id, $user_id = null){

		$patient = $this->patient->find( $patient_id );

		// is patient id is invalid?
		if ( empty( $patient ) ) {
			return $this->redirectBack();
		}

		$this->data ['patient'] = $patient;
		$messages = $patient->messages()->orderBy('timestamp','asc' )->with( ['providerUser'])->get()->groupBy('user_id');

		// associative list of user messages.
		$this->data ['all_messages'] = $messages;

		if(!empty($user_id) && isset($messages[$user_id])){
			$this->data ['messages'] = $messages[$user_id];
		}
		else{
			$this->data ['messages'] = $messages->count() ? $messages->first() : collect();
		}

		if(!empty($this->data ['messages']) && $this->data ['messages']->count() && !empty($this->data ['messages'][0]->providerUser)){
			$this->data ['coach'] = $this->data ['messages'][0]->providerUser;
		}
		return view( 'patient.patient-provider-messages', $this->data );
	}

	/**
	 * @param $patient_id
	 *
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
	 */
	public function patientLogs($patient_id){
		$this->data['css'] = ['datetime'];
		$this->data['js'] = ['datetime'];

		$patient = $this->patient->with(['user'])->find( $patient_id );

		// is patient id is invalid?
		if ( empty( $patient ) ) {
			return $this->redirectBack();
		}
		$this->data ['patient'] = $patient;

		return view( 'patient.patient-logs', $this->data );
	}

	public function searchPatient(){
		$this->data['css'] = ['select2'];
		$this->data['js'] = ['select2'];
		return view( 'patient.search-patient', $this->data );
	}

	/**
	 * @param DiabetesType         $diabetesType
	 * @param PendingPatientSignUp $pendingPatientSignUp
	 *
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function patientImport( DiabetesType $diabetesType, PendingPatientSignUp $pendingPatientSignUp )
	{

		$this->data['facilities'] = $this->facility->orderBy( 'name' )->get()->pluck( 'name', 'facility_id' )->prepend( 'Select a Facility', '' );

		$facilityArray = $this->facility->orderBy( 'name' )->with( [
			'providers'      => function ( $query ) {
				$query->whereIn( 'type', [ 'Food Coach', 'Coach' ] )
				      ->where( 'is_deleted', 0 )
				      ->orWhere( 'is_deleted', null );
			},
			'providers.user' => function ( $query ) {
				$query->where( 'is_enabled', 1 );
			}
		] )->get();

		$resultArray = $this->_coachListBuilder( $facilityArray );
		//$this->data['lead_coach'] = $resultArray['lead_coach'];
		//$this->data['food_coach'] = $resultArray['food_coach'];
		$this->data['lead_coach'] = array();
		$this->data['food_coach'] = array();

		$this->data['lead_coach'] = $resultArray['lead_coach'];
		$this->data['food_coach'] = $resultArray['food_coach'];

		$this->data['diabetes_type']           = $diabetesType->get()->pluck( 'name', 'diabetes_type_id' )->prepend( 'Select a Type', '' );
		$this->data['pending_patient_sign_up'] = $pendingPatientSignUp->with( [
			'diabetesType',
			'facility'
		] )->paginate( config( 'healthslate.table_row_per_page' ) );

		$this->data['on_list_page'] = request()->has( 'page' );
		// Default diabetes_type should be DPP-Cal request in HS-2292
		$this->data['default_diabetes_type'] = 3;
		return view( 'patient.import-patient', $this->data );
	}

	/**
	 * @param $facilities
	 *
	 * @return array
	 */
	private function _coachListBuilder( $facilities )
	{
		$foodCoach = [ '' => trans('common.select_a_food_coach') ];
		$leadCoach = [ '' => trans('common.select_a_lead_coach') ];
		foreach ($facilities as $facility) {
			$providers     = [ ];
			$providersLead = [ ];
			foreach ($facility->providers as $provider) {
				if ($provider->type == "Coach") {
					$providersLead[$provider->provider_id] = @$provider->user->first_name . " " . @$provider->user->last_name;
				} else {
					$providers[$provider->provider_id] = @$provider->user->first_name . " " . @$provider->user->last_name;
				}
			}
			if ( ! empty( $providers )) {
				$foodCoach[$facility->name] = $providers;
			}
			if ( ! empty( $providersLead )) {
				$leadCoach[$facility->name] = $providersLead;
			}
		}
		return [ 'food_coach' => $foodCoach, 'lead_coach' => $leadCoach ];
	}

	/**
	 * @param Validator $validator
	 * @param User $user
	 * @param PatientAppServiceProvider $patientAppServiceProvider
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function patientEnrollment( Validator $validator, User $user, PatientAppServiceProvider $patientAppServiceProvider ) {

		$file      = request()->file( 'data_file' );
		$delimiter = request( 'delimiter', null );
		info('Uploaded file extension : '. (!empty($file) ? strtolower($file->getClientOriginalExtension()) : '') );

		$validation = $validator::make( [
			'facility'  => request( 'facility' ),
			//'primary_food_coach'  => request('food_coach'),
			//'lead_coach'  => request( 'lead_coach' ),
			'type'  => request( 'type' ),
			'data_file' => $file,
			'extension' => (!empty($file) ? strtolower($file->getClientOriginalExtension()) : ''),
			'delimiter' => $delimiter,
		] , [
			'data_file' => 'required',
			'extension' => 'in:txt,csv,xls,xlsx',
			'facility'  => 'required|exists:facility,facility_id',
			//'primary_food_coach'  => 'required|exists:provider,provider_id',
			//'lead_coach'  => 'required|exists:provider,provider_id',
			'type'  => 'required|exists:diabetes_type,diabetes_type_id',
			'delimiter' => [ 'required', 'in:",","|"' ],
		], [
			'data_file.required' => 'Data file is required',
			'facility.required'  => 'Facility name is required',
			//'lead_coach.required'  => 'Lifestyle Coach name is required',
			//'primary_food_coach.required'  => 'Food Coach name is required',
			'delimiter.required'  => 'Data File Delimiter is required',
		] );

		if ( $validation->fails() ) {
			// send back to the page with the input data and errors
			$errorMessages = [ '<p>'.trans('common.patient_import_unsuccessful').' '.trans('common.invalid_request_data').'</p>' ];
			foreach ( $validation->errors()->all( '<p>:message</p>' ) as $message ) {
				$errorMessages[] = $message;
			}

			return $this->redirectBack( route( 'patient.import' ) )->withInput()->with( 'errorMsg', implode( '', $errorMessages ) );
		}
		$facility_id           = request( 'facility' );
		$primary_food_coach_id = request( 'food_coach' );
		$lead_coach            = request( 'lead_coach' );
		$diabetes_type_id      = request( 'type' );
		//info( 'Form Validation Successful, Lead_coach_id: ' . $lead_coach . ' food_coach_id: ' . $primary_food_coach_id );
		info( 'Processing file for facility_id: ' . $facility_id . ' diabetes_type_id: ' . $diabetes_type_id . ' delimiter: ' . $delimiter );
		$sheet = app( 'excel' )
			->setDelimiter($delimiter)
			->load( $file->getPathname() )
			->get();
		if ( $sheet instanceof SheetCollection ) {
			info( 'instance of SheetCollection, get first RowCollection object' );
			$sheet = $sheet->first();
		}

		$data = [ ];
		$members_without_address = [];
		$members_with_invalid_status = [];
		$date_format = 'Ymd';
		if ( $sheet instanceof RowCollection ) {

			// Iterating, Validating each row.
			// Building request DTO, camelCase
			info( 'instance of RowCollection, using sheet: "' . $sheet->getTitle() . '"' );


			//$not_in_email_rule = $user->get(['email'])->pluck('email')->prepend('not_in')->toArray();

            /*$users = DB::table('users')->Join('patient', 'patient.user_id', '=', 'users.user_id')->whereNull('patient.self_register')->orWhere('patient.self_register','=', 0)->orWhere('patient.self_register','!=', 1)->get(['email']);
            $not_in_email_rule = array('not_in');
            foreach($users as $user)
            {
                $not_in_email_rule[] = $user->email;
            }*/
            $uniqid = date('Y-m-d h:i:s A');
			foreach ( $sheet as $row_number => $cellCollection ) {
				$row            = ( $cellCollection->all() );

				//@WARNING: Can not use keys from $row, directly
				// because if delimited is chosen incorrect.
				// It will cause Error `Undefined index`

				// trim whitespace if any in date fields
				$row['birthdate']             = trim( @$row['birthdate'] );
				$row['commited_date']         = trim( @$row['commited_date'] );
				$row['participant_term_date'] = trim( @$row['participant_term_date'] );

				$row_validation = $validator::make( $row, [
					'first_name'            => 'required',
					'last_name'             => 'required',
					'email'                 => [ 'required', 'email', 'regex:' . $this->email_validation_regex ],
					'solera_id'             => 'required',
					'birthdate'             => [ 'date_format:' . $date_format ],
					'commited_date'         => [ 'date_format:' . $date_format ],
					'participant_term_date' => [ 'date_format:' . $date_format ],
					// we will skip unsupported status as of now.
					//'status'     => 'in:new,New,NEW'
				], [
					'email.not_in'                      => trans( 'common.email_already_registered', [ 'email' => @$row['email'] ] ),
					'status.in'                         => @$row['status'] . ' status is not supported',
					'birthdate.date_format'             => 'Birth date should be in Date format (YYYYMMDD)',
					'commited_date.date_format'         => 'Committed date should be in Date format (YYYYMMDD)',
					'participant_term_date.date_format' => 'Participant term date should be in Date format (YYYYMMDD)',
				] );

				if ( $row_validation->fails() ) {
					info( 'Data File validation failed for Row: ' . ( $row_number + 2 ) );
					// send back to the page with the input data and errors
					$rowErrorMessages = [
						'Data File Validation failed at row: ' . ( $row_number + 2 ).' please correct.'
					];
					foreach ( $row_validation->errors()->all( '<p>:message</p>' ) as $message ) {
						$rowErrorMessages[] = $message;
					}
					$rowErrorMessages[] = trans('common.patient_import_unsuccessful').' '.trans('common.invalid_request_data');

					info( 'Validation Error are:', $rowErrorMessages );
					return $this->redirectBack( route( 'patient.import' ) )
					            ->withInput()->with( 'errorMsg', implode( '', $rowErrorMessages ) );
				}
				$row = (object) $row;

				if ( isset( $row->status ) && strtolower( $row->status ) == 'new' ) {

					// process gender to healthslate constants.
					$gender = isset( $row->gender ) ? strtolower( trim( $row->gender ) ) : null;
					if ($gender == 'm') {
						$gender = $this->HS_CONSTANTS->GENDER_MALE;
					} elseif ($gender == 'f') {
						$gender = $this->HS_CONSTANTS->GENDER_FEMALE;

					}

					$data[] = (object) [
						'payerId'                      => isset( $row->payer_id ) ? strtolower($row->payer_id) : null,
						'soleraId'                     => isset( $row->solera_id ) ? $row->solera_id : null,
						'lastName'                     => $row->last_name,
						'firstName'                    => $row->first_name,
						'middleName'                   => isset( $row->middle_name ) ? $row->middle_name : null,
						'birthdate'                    => ( isset( $row->birthdate ) && !empty($row->birthdate) ) ? Carbon::createFromFormat($date_format, $row->birthdate)->timestamp * 1000 : null,
						'gender'                       => $gender,
						'mailaddress1'                 => isset( $row->mailaddress1 ) ? $row->mailaddress1 : null,
						'mailaddress2'                 => isset( $row->mailaddress2 ) ? $row->mailaddress2 : null,
						'city'                         => isset( $row->city ) ? $row->city : null,
						'state'                        => isset( $row->state ) ? $row->state : null,
						'zipCode'                      => isset( $row->zip_code ) ? $row->zip_code : null,
						'preferredPhoneNumber'         => isset( $row->preferred_phone_number ) ? $row->preferred_phone_number : null,
						'homeNumber'                   => isset( $row->home_number ) ? $row->home_number : null,
						'workPhone'                    => isset( $row->work_phone ) ? $row->work_phone : null,
						'cellNum'                      => isset( $row->cell_num ) ? $row->cell_num : null,
						'email'                        => isset( $row->email ) ? $row->email : null,
						'preferredCommunicationMethod' => isset( $row->preferred_communication_method ) ? $row->preferred_communication_method : null,
						'commitedDate'                 => ( isset( $row->commited_date ) && !empty($row->commited_date) ) ? Carbon::createFromFormat($date_format, $row->commited_date)->timestamp * 1000 : null,
						'language'                     => isset( $row->language ) ? $row->language : null,
						'visuallyImpaired'             => isset( $row->visually_impaired ) ? $row->visually_impaired : null,
						'hearingImpaired'              => isset( $row->hearing_impaired ) ? $row->hearing_impaired : null,
						'participantTermDate'          => ( isset( $row->participant_term_date ) && !empty($row->participant_term_date) ) ? Carbon::createFromFormat($date_format, $row->participant_term_date)->timestamp * 1000 : null,
						'status'                       => isset( $row->status ) ? $row->status : null,
						'facilityId'                   => $facility_id,
						'diabetesTypeId'               => $diabetes_type_id,
						'primaryFoodCoachId'           => isset($primary_food_coach_id) ? $primary_food_coach_id : null,
						'leadCoachId'                  => isset($lead_coach) ? $lead_coach : null,
                        'importId'                     => isset($uniqid) ? $uniqid : null,
					];

					// We have to have an address, city, state and zipcode. If not, we need to still register the member,
					// but somehow note that there was no shipping address given.
					if (empty( $row->mailaddress1 ) || empty( $row->city ) || empty( $row->state ) || empty( $row->zip_code ) ) {
						$members_without_address[] = $row->solera_id;
					}
				}
				else{
					$members_with_invalid_status[] = $row->solera_id;
					logger('Data File unsupported status for Row: ' . ( $row_number + 2 ).' skipping status: '. strtolower( $row->status ) );
				}

			}

			info( 'Total requested ' . count( $data ).' from '.$sheet->count().' rows' );
			info( 'Members without address info ' . count( $members_without_address ) );
			info( 'Members without valid status ' . count( $members_with_invalid_status ) );

			$info_msg = 'Among them ' . count( $members_without_address ) . ' members are without Address';
			$info_msg .= ' and '. count( $members_with_invalid_status ) . ' are skipped due to Unsupported status';

			if ( !empty( $data ) ) {

				$result = $patientAppServiceProvider->enrollPatients( $data );
				if ( $result === true ) {
					return $this->redirectBack( route( 'patient.import' ) )
					            ->with( [
						            'success' => 'Successfully queued ' . count( $data ) . ' patients for import from '.$sheet->count().' rows',
						            'infoMsg' => $info_msg ]);
				}
				return $this->redirectBack( route( 'patient.import' ) )->withInput()->with( 'errorMsg', @$result->errorDetail );
			}

		}
		logger( 'Unexpected error case!!' );
		return $this->redirectBack( route( 'patient.import' ) )->withInput()->with( 'errorMsg', trans('common.invalid_request_data') );
	}

	/**
	 * @param SqlExportHelper $sqlExportHelper
	 * @param $patient_id
	 *
	 * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
	 */
	public function patientDebugInfo(HealthSlateObfuscator $healthSlateObfuscator, SqlExportHelper $sqlExportHelper, $patient_id){
		Log::info('Call for dump patient debug info, patient_id: '. $patient_id);

		$patient = $this->patient->with([
			'leadCoach',
			'facilities',
			'providers',
			'primaryFoodCoach',
			'user',
			'diabetesType'
		])->find( $patient_id );

		// is patient id is invalid?
		if ( empty( $patient ) || empty($patient->user) ) {
			Log::info('Invalid Patient Id, patient not found');
			return $this->redirectBack();
		}

		// Check if the required programs are available
		$requiredBinaries = ['tar'];
		foreach ($requiredBinaries as $command) {
			$path = trim( shell_exec( 'which '.$command ) );
			if ( $path == '' ) {
				die( sprintf( '<b>%s</b> not available. It needs to be installed on the server for this script to work.', $command ) );
			}
		}

		$connection_info = config( 'database.connections.mysql' );
		$storage         = storage_path( 'tmp' . DIRECTORY_SEPARATOR . $patient_id . DIRECTORY_SEPARATOR );
		$zip_storage     = storage_path( 'tmp' . DIRECTORY_SEPARATOR );
		$file_prefix     = $patient_id . '_' . date( 'Y-m-d-H-i-s' ) . '_';
		if ( ! File::isDirectory( $storage ) ) {
			Log::info( 'Creating storage path: ' . $storage );
			File::makeDirectory( $storage, 0755, true );
		}

		// obfuscate patient's user data
		$healthSlateObfuscator->obfuscateUserObject($patient->user);
		$healthSlateObfuscator->obfuscatePatientObject($patient);

		$patient_content = ($sqlExportHelper->get_export_string($patient));
		Log::info('Generated content for patient table');
		@file_put_contents($storage . $file_prefix . 'patient.sql',$patient_content);

		$user_content = ($sqlExportHelper->get_export_string($patient->user));
		Log::info('Generated content for user table');
		@file_put_contents($storage . $file_prefix . 'user.sql',$user_content);


		//$base_command = sprintf( 'mysqldump %s -u %s -p\'%s\' -t ', $connection_info['database'], $connection_info['username'], $connection_info['password'] );

		$commands    = [
			//sprintf( '%s %s -w\'%s\' > %s', $base_command, 'users', 'user_id="' . $patient->user->user_id . '"', $storage . $file_prefix . 'user.sql' ),

			// zip all sql to single file for download
			sprintf( "tar -czf '%s' --directory='%s' '%s'"  , $zip_storage . $file_prefix.'.tar.gz' , $zip_storage , $patient_id . DIRECTORY_SEPARATOR )
		];

		try {
			foreach($commands as $cmd){
				Log::info( 'Running : ' . $cmd );
				$process = new Process($cmd);
				$process->run();
				if (!$process->isSuccessful()) {
					Log::error('Process RuntimeException: ' . $process->getErrorOutput());
				}
				$out = $process->getOutput();
			}
		} catch (Exception $error) {
			Log::error($error->getMessage());
			Log::error($error->getTraceAsString());
		}

		Log::info('Cleaning storage path: '.$storage);
		File::deleteDirectory($storage);

		if(File::isFile($zip_storage . $file_prefix.'.tar.gz')){
			Log::info('Sending File Download Request : '.($zip_storage . $file_prefix.'.tar.gz') );
			return response()->download($zip_storage . $file_prefix.'.tar.gz', $file_prefix.'.tar.gz')->deleteFileAfterSend(true);
		}
		else{
			echo "File Not Exists";
		}
	}

	/**
	 * redirectBack
	 *      Common redirect handler
	 *
	 * @param string $redirect_to
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	private function redirectBack($redirect_to = 'dashboard'){
		return redirect( $redirect_to );
	}

	/**
	 * Application config dumper
	 * Note: for debug purpose only
	 *
	 * @param string $key
	 */
	public function dumpConfig($key = ''){
		\Log::info('Call for dumpConfig with key: '. $key);
		$dumper = ( new \Illuminate\Support\Debug\Dumper );
		if($key == config('app.key')) {
			echo 'Database: ';
			$dumper->dump( config( 'database.connections.mysql' ) );
			echo 'Rest: ';
			$dumper->dump( config( 'rest' ) );
			echo 'Healthslate: ';
			$dumper->dump( config( 'healthslate' ) );
			echo 'App: ';
			$dumper->dump( config( 'app' ) );
			echo 'Session: ';
			$dumper->dump( config( 'session' ) );
			echo 'All: ';
			$dumper->dump(config());
			die(1);
		}
		abort(404);
	}
}