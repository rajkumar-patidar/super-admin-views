<?php

namespace HealthSlateAdmin\Http\Controllers;

use Illuminate\Http\Request;

use Cookie;
use HealthSlateAdmin\Http\Controllers\Controller;
use HealthSlateAdmin\Models\AuthServiceProvider;
use Input;
use Log;
use Redirect;
use Session;
use Validator;

class AuthController extends Controller {

	/**
	 *  common variable for passing data to view
	 * @var array
	 */
	protected $data = array(
		'page_title' => 'Sign in'
	);

	/**
	 * @var AuthServiceProvider
	 */
	protected $authServiceProvider;

	/**
	 * @param AuthServiceProvider $authServiceProvider
	 */
	function __construct( AuthServiceProvider $authServiceProvider ) {
		$this->authServiceProvider = $authServiceProvider;
	}

	/**
	 * index
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index() {
		return view( 'auth.login', $this->data );
	}

	/**
	 * loginHandler
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function loginHandler() {
		$validator = Validator::make(
			Input::all(), array(
			'username' => 'required|min:4',
			'password' => 'required|min:4',
		) );
		if ( $validator->passes() ) {
			$user = $this->authServiceProvider->getAccessToken( Input::get( 'username' ), Input::get( 'password' ) );

			return $this->loginUser($user);
		}

		$messages      = $validator->errors();
		$errorMessages = [ ];
		foreach ( $messages->all( '<p>:message</p>' ) as $message ) {
			$errorMessages[] = $message;
		}

		return Redirect::route( 'login' )->withInput( Input::except( 'password' ) )->with( 'errorMsg', implode( '', $errorMessages ) );
	}

	/**
	 * login by user access token handler
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function loginByTokenHandler(){

		$validator = Validator::make(
			Input::all(), array(
			'token' => 'required',
			'expiresIn' => 'required|integer',
		) );

		if ( $validator->failed() ) {
			Log::info("Invalid Request for Login By Token: ".$validator->errors()->toJson());
			return redirect()->route('login');
		}

		// redirect to specific route if exists in request
		$redirect_to = request( 'redirect_to' );
		if ( ! empty( $redirect_to ) && \Route::has( $redirect_to ) ) {
			info( 'Intended redirect to request found: ' . ( $redirect_to ) );
			session( [ 'url.intended' => route( $redirect_to ) ] );
		}

		$user = $this->authServiceProvider->getAdminUserByToken(request('token'), null, request('expiresIn'));
		return $this->loginUser($user);
	}
	/**
	 * logout
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function logout() {
		Log::info( 'Processing logout request.' );
		Session::flush();
                Cookie::queue(Cookie::forget('access_token'));
                Cookie::queue(Cookie::forget('expires_in'));
                Cookie::queue(Cookie::forget('hs_report_f_id'));

		return Redirect::route( 'login' );
	}

	/**
	 * login user by Token Info object
	 *
	 * @param $user
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	private function loginUser( $user ) {
		if ( isset( $user->accessToken ) ) {
			Session::put( 'user', $user );
                        Cookie::queue(Cookie::make('access_token', $user->accessToken, $user->expiresIn));
                        Cookie::queue(Cookie::make('expires_in', $user->expiresIn, $user->expiresIn));

                        return redirect()->intended('/dashboard');
		}
		return redirect()->route( 'login' )->withInput( Input::except( 'password' ) )->with( 'errorMsg', @$user->errorDetail );
	}
}
