<?php

namespace HealthSlateAdmin\Http\Controllers;

use HealthSlateAdmin\Http\Controllers\Controller;
use HealthSlateAdmin\Models\CurriculumServiceProvider;
use HealthSlateAdmin\Models\Facility;
use HealthSlateAdmin\Models\Healthslate;
use Carbon\Carbon;
use HealthSlateAdmin\Models\Provider;
use Illuminate\Support\Str;
use HealthSlateAdmin\Helpers\DataTableServiceProvider;
use Log;

class ReportController extends Controller {

	/**
	 *  common variable for passing data to view
	 * @var array
	 */
	protected $data = [];

	/**
	 * @var Healthslate
	 */
	protected $healthslate_model;

	/*
	 * @var Da*taTableServiceProvider
	 */
	protected $dataTableService;

	/**
	 * @var Facility
	 */
	protected $facility;

	/**
	 * @var Provider
	 */
	protected $provider;

	/**
	 * @var CurriculumServiceProvider
	 */
	protected $curriculumServiceProvider;

	/**
	 * @param Healthslate $healthslate
	 * @param DataTableServiceProvider $dataTableService
	 * @param Facility $facility
	 * @param Provider $provider
	 * @param CurriculumServiceProvider $curriculumServiceProvider
	 */
	function __construct( Healthslate $healthslate, DataTableServiceProvider $dataTableService,
		Facility $facility, Provider $provider, CurriculumServiceProvider $curriculumServiceProvider
	) {
		$this->healthslate_model         = $healthslate;
		$this->dataTableService          = $dataTableService;
		$this->facility                  = $facility;
		$this->curriculumServiceProvider = $curriculumServiceProvider;
		$this->provider                  = $provider;
	}

	/**
	 * index
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index() {
		return view( 'report.reports-home', $this->data );
	}

	/**
	 * @param null $facility_id
	 *
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function facilityReports($facility_id = null) {
		$facilities = $this->facility->orderBy( 'name' )->get();
		$facilities = $facilities->pluck('name','facility_id');
		$this->data['facilities'] = $facilities->prepend('Select Facility','0');
		$this->data['facility_id'] = $facility_id;
		return view( 'report.facility-reports', $this->data );
	}

	/**
	 * getCommonReportView
	 *
	 * Common method to generate table view for various reports
	 *
	 * @param $report_meta
	 * @param string $page_description
	 *
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	private function getCommonReportView( $report_meta, $page_description = "" ) {
		$this->data['report']           = $report_meta;
		$this->data['page_description'] = $page_description;

		return view( 'report.common-reports-detail-view', $this->data );
	}

	/**
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function feedbackMessages() {
		$report           = (object) [
			"visibleName"    => "Patient Feedback Messages",
			"table_src"      => route( 'ajax.report.feedbackMessages' ),
			"result_headers" => [ "Patient id", "Description", "Time" ]
		];
		$page_description = "Most recent feedback messages";

		return $this->getCommonReportView( $report, $page_description );
	}

	/**
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function patientWithoutLeadCoachOneOnOne() {
		$report           = (object) [
			"visibleName"    => "Patient without Lead Coach One on One Session",
			"table_src"      => route( 'ajax.report.patientWithoutLeadCoachOneOnOne' ),
			"result_headers" => [ "Patient id", "Days", "Lifestyle Coach" ]
		];
		$page_description = "No 1:1 in db with lead coach for patient > 7days registered";

		$this->data['deleted_patient_excluded_note'] = true;
		$this->data['test_patient_excluded_note']    = true;

		return $this->getCommonReportView( $report, $page_description );
	}

	/**
	 * @deprecated request to remove in HS-2299
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function patientWithoutFoodCoachOneOnOne() {
		$report           = (object) [
			"visibleName"    => "Patient without Food Coach One on One Session",
			"table_src"      => route( 'ajax.report.patientWithoutFoodCoachOneOnOne' ),
			"result_headers" => [ "Patient id", "Days", "Food Coach Id", "Coach" ]
		];
		$page_description = "No 1:1 in db for food coach with patient >7 days registered";

		return $this->getCommonReportView( $report, $page_description );
	}

	/**
	 * @deprecated request to remove in HS-2299
	 *
	 * @param $duration
	 *
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function unProcessedMealsOverDuration($duration) {
		if($duration == "4d"){
			$string = '4 Days';
		}else{
			$string = '6 Hours';
		}
		$report           = (object) [
			"visibleName"    => "Unprocessed Meals - ".$string,
			"table_src"      => route( 'ajax.report.unProcessedMealsOverDuration' ,['duration' => $duration]),
			"result_headers" => [ "count", "processed", "unprocessed" ]
		];
		$page_description = "Meals in the last $string that have not been processed for more the $string";

		return $this->getCommonReportView( $report, $page_description );
	}

	/**
	 * @deprecated request to remove in HS-2299
	 *
	 * @param $duration
	 *
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function hourlyMealProcessingInDuration($duration) {
		if($duration == "3d"){
			$string = '3 Days';
		}else{
			$string = '';
		}
		$report           = (object) [
			"visibleName"    => "Meal Processing Details - ".$string,
			"table_src"      => route( 'ajax.report.hourlyMealProcessingInDuration' ,['duration' => $duration]),
			"result_headers" => ["hours", "count", "processed", "unprocessed" ]
		];
		$page_description = "Hourly flow of meal process queue $string";

		return $this->getCommonReportView( $report, $page_description );
	}

	/**
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function unRepliedMessages() {
		$report           = (object) [
			"visibleName"    => "Unreplied Messages",
			"table_src"      => route( 'ajax.report.unRepliedMessages'),
			"result_headers" => ["Message", "Patient Id", "To email", "Time", "Days Ago" ]
		];
		$page_description = "Messages that may be unreplied to";

		return $this->getCommonReportView( $report, $page_description );
	}

	/**
	 * @deprecated request to remove in HS-2299
	 *
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function patientSurveyData() {
		$report           = (object) [
			"visibleName"    => "Patient Survey Data",
			"table_src"      => route( 'ajax.report.patientSurveyData'),
			"table_class"      => "survey-data",
			"result_headers" => ["Email", "Form Name", "Form Id", "Patient Id", "When to show", "Data", "Scheduled Form Id" ]
		];
		$page_description = "Patient survey and assessments";

		return $this->getCommonReportView( $report, $page_description );
	}

	//=================== Ajax handler of above report views ========================

	public function ajaxFeedbackMessages() {
		$is_download_request = $this->isTableDownloadRequest();
		$query               = $this->healthslate_model->get_query_for_report( 'feedbackMessages' );
		$result              = $this->healthslate_model->get_query_result( $query );
		$data                = collect();
		foreach ( $result as $row ) {
			$tr = [
				"Patient id"  => $row->patient_id,
				"Description" => $row->description,
				"Time"        => Carbon::createFromFormat( 'Y-m-d H:i:s', $row->time )->format( config( 'healthslate.default_date_time_format' ) ),
			];
			$data->push( ( $is_download_request ? $tr : array_values( $tr ) ) );
		}

		return $this->dataTableService->dataTableResponse( $data, $is_download_request );
	}

	public function ajaxPatientWithoutLeadCoachOneOnOne() {
		$is_download_request = $this->isTableDownloadRequest();
		$query               = $this->healthslate_model->get_query_for_report( 'patientWithoutLeadCoach' );
		$result              = $this->healthslate_model->get_query_result( $query );
		$data                = collect();
		foreach ( $result as $row ) {
			$tr = [
				"Patient id"      => $row->patient_id,
				"Days"            => $row->days,
				"Lifestyle Coach" => $row->lifestyle_coach
			];
			$data->push( ( $is_download_request ? $tr : array_values( $tr ) ) );
		}

		return $this->dataTableService->dataTableResponse( $data, $is_download_request );
	}

	/**
	 * @deprecated request to remove in HS-2299
	 * @return \Illuminate\Http\JsonResponse|mixed
	 */
	public function ajaxPatientWithoutFoodCoachOneOnOne() {
		$is_download_request = $this->isTableDownloadRequest();
		$query               = $this->healthslate_model->get_query_for_report( 'patientWithoutFoodCoach' );
		$result              = $this->healthslate_model->get_query_result( $query );
		$data                = collect();
		foreach ( $result as $row ) {
			$tr = [
				"Patient id"            => $row->patient_id,
				"Days"                  => $row->days,
				"Primary Food Coach Id" => $row->primary_food_coach_id,
				"Coach"                 => $row->coach,
			];
			$data->push( ( $is_download_request ? $tr : array_values( $tr ) ) );
		}

		return $this->dataTableService->dataTableResponse( $data, $is_download_request );
	}

	/**
	 * @deprecated request to remove in HS-2299
	 *
	 * @param $duration
	 *
	 * @return \Illuminate\Http\JsonResponse|mixed
	 */
	public function ajaxUnProcessedMealsOverDuration( $duration ) {
		$is_download_request = $this->isTableDownloadRequest();

		if ( $duration == "4d" ) {
			$key = 'unProcessedMealsOver4d';
		} else {
			$key = 'unProcessedMealsOver6hrs';
		}
		$query  = $this->healthslate_model->get_query_for_report( $key );
		$result = $this->healthslate_model->get_query_result( $query );
		$data   = collect();
		foreach ( $result as $row ) {
			$tr = [
				"count"       => $row->count,
				"processed"   => $row->processed,
				"unprocessed" => $row->unprocessed,
			];
			$data->push( ( $is_download_request ? $tr : array_values( $tr ) ) );
		}

		return $this->dataTableService->dataTableResponse( $data, $is_download_request );
	}

	/**
	 * @deprecated request to remove in HS-2299
	 *
	 * @param $duration
	 *
	 * @return \Illuminate\Http\JsonResponse|mixed
	 */
	public function ajaxHourlyMealProcessingInDuration( $duration ) {
		$is_download_request = $this->isTableDownloadRequest();

		if ( $duration == "3d" ) {
			$key = 'hourlyMealProcessingInDuration3d';
		} else {
			$key = '';
		}
		$query  = $this->healthslate_model->get_query_for_report( $key );
		$result = $this->healthslate_model->get_query_result( $query );
		$data   = collect();
		foreach ( $result as $row ) {
			$tr = [
				"hours"       => $row->h,
				"count"       => $row->count,
				"processed"   => $row->processed,
				"unprocessed" => $row->unprocessed,
			];

			$data->push( ( $is_download_request ? $tr : array_values( $tr ) ) );
		}

		return $this->dataTableService->dataTableResponse( $data, $is_download_request );
	}

	public function ajaxUnRepliedMessages() {
		$is_download_request = $this->isTableDownloadRequest();

		$query  = $this->healthslate_model->get_query_for_report( "unRepliedMessages" );
		$result = $this->healthslate_model->get_query_result( $query );
		$data   = collect();
		foreach ( $result as $row ) {
			$shown_str = Str::words(strip_tags($row->message, config('healthslate.allowed_html_tags')), 10, '');
			$remaining_desc = substr_replace(strip_tags($row->message, config('healthslate.allowed_html_tags')),'',0,strlen($shown_str));
			$message = '<text>'.Str::words(strip_tags($row->message, config('healthslate.allowed_html_tags')), 10, '<div class="more-ellipse inline">...<a href="javascript:void(0);" class="more-desc"> More</a></div>').
			'<div class="hide inline">'.$remaining_desc.'</div></text>';
			$tr = [
				"Message"    => $message,
				"Patient Id" => $row->patient_id,
				"To email"   => $row->to_email,
				"Time"       => Carbon::createFromFormat( 'Y-m-d H:i:s', $row->datetime )->format( config( 'healthslate.default_date_time_format' ) ),
				"Days Ago"   => $row->days_ago,
			];
			if($is_download_request){
				$tr['Message'] = $row->message;
			}
			$data->push( ( $is_download_request ? $tr : array_values( $tr ) ) );
		}

		return $this->dataTableService->dataTableResponse( $data, $is_download_request );
	}

	/**
	 * @deprecated request to remove in HS-2299
	 *
	 * @return mixed
	 *
	 */
	public function ajaxPatientSurveyData() {
		$is_download_request = $this->isTableDownloadRequest();

		$query  = $this->healthslate_model->get_query_for_report( "patientSurveyData" );
		$result = $this->healthslate_model->get_query_result( $query );
		$data   = collect();
		foreach ( $result as $row ) {
			$tr = [
				"Email"             => $row->email,
				"Form Name"         => $row->form_name,
				"Form Id"           => $row->form_id,
				"Patient Id"        => $row->patient_id,
				"When to show"      => $row->whentoshow,
				"Data"              => ($is_download_request ? '' : '<pre class="json">' ) . json_encode( json_decode( $row->data ), JSON_PRETTY_PRINT ) . ($is_download_request ? '' : '</pre>' ),
				"Scheduled Form Id" => $row->scheduled_form_id,
			];
			$data->push( ( $is_download_request ? $tr : array_values( $tr ) ) );
		}

		return $this->dataTableService->dataTableResponse( $data, $is_download_request );
	}

	/**
	 * @param $facility_id
	 * @param int $days
	 *
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function activeUsers( $facility_id=40, $days = 0 ) {

		$this->data['facility_id'] = $facility_id;

		$binding['facility_id'] = $facility_id;
		$query[]                = "select patient.patient_id,from_unixtime(max(log.log_time/1000)) as last_log_entry,
										concat(users.first_name, ' ' ,users.last_name) as name
										from users,patient, log
										 WHERE users.user_id=patient.user_id
										 AND log.patient_id=patient.patient_id
										 AND users.email not like '\\_%' AND patient.facility_id= :facility_id
										 AND log_time is not null";

		if ( $days > 0 ) {
			$query[]         = "AND unix_timestamp()-(log_time/1000) < (86400 * :days )";
			$binding['days'] = $days;
		}
		$query[] = "group by log.patient_id Order by patient.patient_id asc";

		$report_meta = [
			'title'          => 'Active Users',
			"result_headers" => [
				'patient_id'     => [ 'name' => "Patient Id" ],
				'name'           => [ 'name' => "Name" ],
				'last_log_entry' => [ 'name' => "Last Log Entry", 'class' => 'text-center', 'format_date' => config('healthslate.default_date_time_format') ]
			]
		];

		return $this->getFacilityReportView( implode( " ", $query ), $binding, $report_meta );
	}

	/**
	 * @param $facility_id
	 * @param int $days
	 *
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function userEntries( $facility_id, $days = 0 ) {

		$this->data['facility_id'] = $facility_id;

		$binding['facility_id'] = $facility_id;
		$query[]                = "select patient.patient_id, count(*) as entries,
									concat(users.first_name, ' ' ,users.last_name) as name,
									from_unixtime(max(log_time)/1000) as last_log,
									from_unixtime(min(log_time)/1000) as first_log
									from users,patient,log
									where users.user_id=patient.user_id
									AND log.patient_id=patient.patient_id AND
									users.email not like '\\_%' AND
									patient.facility_id = :facility_id AND
									log_time is not null ";

		if ( $days > 0 ) {
			$query[]         = "AND unix_timestamp()-(log_time/1000) < (86400 * :days )";
			$binding['days'] = $days;
		}
		$query[] = "group by log.patient_id Order by patient.patient_id asc";

		$report_meta = [
			'title'          => 'Users Log Entries',
			"result_headers" => [
				'patient_id'  => [ 'name' => "Patient Id" ],
				'name'        => [ 'name' => "Name" ],
				'entries'     => [ 'name' => "Log Entries", 'class' => 'text-center' ],
				'last_log'    => [ 'name' => "Last Log", 'class' => 'text-center', 'format_date' => config('healthslate.default_date_time_format') ],
				'first_log'   => [ 'name' => "First Log", 'class' => 'text-center', 'format_date' => config('healthslate.default_date_time_format') ],
			]
		];

		return $this->getFacilityReportView( implode( " ", $query ), $binding, $report_meta );
	}

	/**
	 * @param $facility_id
	 * @param int $days
	 *
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function userRegistrations( $facility_id, $days = 0 ) {

		$this->data['facility_id'] = $facility_id;

		$binding['facility_id'] = $facility_id;
		$query[]                = "select patient.patient_id,patient.uuid,users.email, users.first_name, users.last_name,
									concat(users.first_name, ' ' ,users.last_name) as name,
									patient.mrn,ORD(is_consent_accepted) as consent_accepted,is_approved,
									from_unixtime(patient.invitation_sent_date/1000) AS invitation_sent_date,
									IF(password is not null,'set','none') as password,registration_date,
									timestampdiff(day,users.registration_date,curdate()) as days_since_reg,
									timestampdiff(day,from_unixtime(password_modified_time/1000),curdate()) as days_since_activate
									from users,patient where
									users.user_id=patient.user_id AND
									users.email not like '\\_%' AND
									patient.facility_id= :facility_id  ";

		if ( $days > 0 ) {
			$query[]         = "AND ( timestampdiff(day,users.registration_date,curdate()) < :days )";
			$binding['days'] = $days;
		}
		$query[] = "Order by patient.patient_id asc";

		$report_meta = [
			'title'          => 'User Registrations',
			"result_headers" => [
				'patient_id'           => [ 'name' => "Patient Id" ],
				'email'                => [ 'name' => "Email" ],
				'name'                 => [ 'name' => "Name" ],
				'mrn'                  => [ 'name' => "MRN", 'class' => 'text-center' ],
				'invitation_sent_date' => [ 'name' => "Invitation Sent On", 'class' => 'text-center', 'format_date' => config( 'healthslate.default_date_format' ) ],
				'consent_accepted'     => [ 'name' => "Consent Accepted", 'class' => 'text-center' ],
				'is_approved'          => [ 'name' => "Is Approved", 'class' => 'text-center' ],
				'password'             => [ 'name' => "Password", 'class' => 'text-center' ],
				'registration_date'    => [ 'name' => "Registration Date", 'class' => 'text-center', 'format_date' => config( 'healthslate.default_date_format' ) ],
				'days_since_reg'       => [ 'name' => "Days Since Registered", 'class' => 'text-center' ],
				'days_since_activate'  => [ 'name' => "Days Since Activated", 'class' => 'text-center', 'tooltip' => 'Days since Last password changed' ],
			]
		];

		return $this->getFacilityReportView( implode( " ", $query ), $binding, $report_meta );
	}

	/**
	 * @param $facility_id
	 * @param int $days
	 *
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function userMisfitSteps( $facility_id, $days = 0 ) {

		$this->data['facility_id'] = $facility_id;

		$binding['facility_id'] = $facility_id;
		$query[]                = "select  patient.patient_id, sum(activity_log_summary.steps) as steps,
									concat(users.first_name, ' ' ,users.last_name) as name
									from users,patient,activity_log_summary where
									users.user_id=patient.user_id AND
									activity_log_summary.patient_id=patient.patient_id AND
									users.email not like '\\_%' AND
									patient.facility_id= :facility_id ";

		if ( $days > 0 ) {
			$query[]         = "AND unix_timestamp()-(activity_log_summary.timestamp/1000) < (86400 * :days )";
			$binding['days'] = $days;
		}
		$query[] = " group by activity_log_summary.patient_id Order by patient.patient_id asc";

		$report_meta = [
			'title'          => 'User misfit Activity',
			"result_headers" => [
				'patient_id'       => [ 'name' => "Patient Id" ],
				'name'             => [ 'name' => "Name" ],
				'steps'             => [ 'name' => "Steps", 'class' => 'text-center' ],
			]
		];

		return $this->getFacilityReportView( implode( " ", $query ), $binding, $report_meta );
	}

	/**
	 * @param $facility_id
	 *
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function userMisfitNeverActive( $facility_id ) {

		$this->data['facility_id'] = $facility_id;
		$binding['facility_id']    = $facility_id;
		$query[]                   = "select patient.patient_id,users.registration_date,
 										concat(users.first_name, ' ' ,users.last_name) as name
										from users,patient
										LEFT JOIN activity_log_summary on activity_log_summary.patient_id=patient.patient_id
										where activity_log_summary.activity_log_summary_id is null
										AND users.user_id=patient.user_id
										AND users.email not like '\\_%'
										AND patient.facility_id= :facility_id
										Order by patient.patient_id asc";

		$report_meta = [
			'title'            => 'User Never Active via misfit',
			'show_days_filter' => false,
			"result_headers"   => [
				'patient_id'        => [ 'name' => "Patient Id" ],
				'name'              => [ 'name' => "Name" ],
				'registration_date' => [ 'name'        => "Registration Date",
				                         'class'       => 'text-center',
				                         'format_date' => config( 'healthslate.default_date_time_format' )
				],
			]
		];

		return $this->getFacilityReportView( implode( " ", $query ), $binding, $report_meta );
	}

	/**
	 * @param $facility_id
	 * @param int $days
	 *
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function facilityMessages( $facility_id) {                
                $this->data['css'] = ['datetime'];
		$this->data['js'] = ['datetime'];

		$this->data['facility_id'] = $facility_id;
		$this->data['show_duration_filter'] = true;
		$this->data['report_src'] = route('ajax.report.facility.messages',['facility_id' =>  $facility_id] );

		$this->data['report_meta'] = (object) [
			'title'      => 'Messages Activity',
			'body_class' => 'weekly-participation common-facility-report'

		];
		return view( 'report.common-duration-report', $this->data );
	}

	/**
	 * @param $facility_id
	 * @param int $days
	 *
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function facilityVideoCalls( $facility_id, $days = 0 ) {

		$this->data['facility_id'] = $facility_id;

		$binding['facility_id'] = $facility_id;
		$query[]                = "select patient.patient_id, date(from_unixtime(session_time/1000)) as GMTsession,
									provider_id, concat(users.first_name, ' ' ,users.last_name) as name
									from users,patient, one_to_one_sessions where
									users.user_id=patient.user_id AND
									one_to_one_sessions.patient_id=patient.patient_id AND
									users.email not like '\\_%' AND
									patient.facility_id= :facility_id ";

		if ( $days > 0 ) {
			$query[]         = "AND unix_timestamp()-(one_to_one_sessions.session_time/1000) < (86400 * :days )
								AND unix_timestamp()>one_to_one_sessions.session_time/1000";
			$binding['days'] = $days;
		}

		$report_meta = [
			'title'          => 'Video Calls Activity',
			"result_headers" => [
				'patient_id'  => [ 'name' => "Patient Id" ],
				'name'        => [ 'name' => "Name" ],
				'GMTsession'  => [
					'name'        => "GMT Session Date",
					'class'       => 'text-center',
					'format_date1' => config( 'healthslate.default_date_time_format' )
				],
				'provider_id' => [ 'name' => "Provider Id" ],
			]
		];

		return $this->getFacilityReportView( implode( " ", $query ), $binding, $report_meta );
	}

	/**
	 * @param $facility_id
	 *
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function facilityUsersLeadCoach( $facility_id ) {

		$this->data['facility_id'] = $facility_id;
		$binding['facility_id']    = $facility_id;
		$query[]                   = "select pts.patient_id,pts.lead_coach_id,
 										concat(users.first_name, ' ' ,users.last_name) as lead_coach_name,
										pts.email as patient_email,users.email from
											(
											select patient_id,lead_coach_id,email
											from patient,users
											where users.user_id=patient.user_id
											AND users.email not like '\\_%'
											AND patient.facility_id= :facility_id
											) as pts,
										provider, users
										where provider.provider_id=pts.lead_coach_id
										AND provider.user_id=users.user_id ";

		$report_meta = [
			'title'            => 'Patient - Lead Coach',
			'show_days_filter' => false,
			"result_headers"   => [
				'patient_id'        => [ 'name' => "Patient Id" ],
				'patient_email'        => [ 'name' => "Patient Email" ],
				'lead_coach_id'              => [ 'name' => "Lead Coach Id" ],
				'email'        => [ 'name' => "Lead Coach Email" ],
				'lead_coach_name'        => [ 'name' => "Lead Coach Name" ],
			]
		];

		return $this->getFacilityReportView( implode( " ", $query ), $binding, $report_meta );
	}

	/**
	 * @param $facility_id
	 *
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function facilityPatientPreferences( $facility_id ) {

		$this->data['facility_id'] = $facility_id;
		$report           = (object) [
			"visibleName"    => "Patient Preferences",
			"table_src"      => route('ajax.report.facility.patient-preferences',['facility_id' =>  $facility_id] ),
			"table_class"    => 'patient-preferences',
			"result_headers" => [ "Patient Id", "Name", "Diabetes Type", "Email Communication Allowed" ]
		];
		$page_description = "";

		return $this->getCommonReportView( $report, $page_description );
	}

	/**
	 * @param $facility_id
	 *
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function facilityCurriculumActivity( $facility_id ) {
		$is_download_request = $this->isTableDownloadRequest();

		$query  = "select patient.patient_id,patient.uuid, users.email, diabetes_type.name as diabetes_type_name, 
					concat(users.first_name, ' ' ,users.last_name) as name, patient.diabetes_type_id
					from users,patient,diabetes_type
					where users.user_id=patient.user_id AND
					diabetes_type.diabetes_type_id = patient.diabetes_type_id AND
					users.email not like '\\_%' AND ORD(users.is_registration_completed) = 1 AND
					patient.facility_id= :facility_id Order by patient.patient_id asc";
		$result = $this->healthslate_model->get_query_result( $query, [ 'facility_id' => $facility_id ] );

		if($is_download_request && !empty($result)){
			// @todo below method should be called for TYPE-2 users only
			$result_with_curriculum = $this->curriculumServiceProvider->getCurriculumUsersWeekReport( $result, true );
			$result_with_curriculum = $this->curriculumServiceProvider->getDPPCurriculumUsersWeekReport( $result_with_curriculum, true );
			return $this->dataTableService->dataTableResponse( collect($result_with_curriculum), true );
		}
		$paginated_result = $this->makeLengthAwarePaginator( collect( $result ), config( 'healthslate.table_row_per_page' ) );
		// @todo below method should be called for TYPE-2 users only
		$paginated_result = $this->curriculumServiceProvider->getCurriculumUsersWeekReport( $paginated_result );
		$paginated_result = $this->curriculumServiceProvider->getDPPCurriculumUsersWeekReport( $paginated_result );
		// get report for current page patient only
		$this->data['paginated_report'] = $paginated_result;
		$this->data['total_records'] = count($result);
		$this->data['facility_id']      = $facility_id;

		return view( 'report.patient-curriculum-report', $this->data );
	}

	/**
	 * @param string $query
	 * @param array $binding
	 * @param array $report_meta
	 *
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	private function getFacilityReportView($query, $binding, $report_meta = []){
		$is_download_request = $this->isTableDownloadRequest();

		$result  = $this->healthslate_model->get_query_result( $query, $binding );
		if($is_download_request && !empty($result)){
			return $this->dataTableService->dataTableResponse( collect($result), true );
		}
		$paginated_result = $this->makeLengthAwarePaginator(collect($result), config('healthslate.table_row_per_page'));

		$this->data['paginated_result'] = $paginated_result;
		$this->data['total_records'] = count($result);
		$this->data['show_days_filter'] = isset($report_meta['show_days_filter']) ? $report_meta['show_days_filter'] : true;
		$this->data['days_list'] = [ '0' => 'Any', '3' => 3, '5' => 5, '10' => 10, '20' => 20, '30' => 30 ];
		$this->data['days'] = !empty(request()->days) ? request()->days : 0;
		$this->data['report_meta'] = (object) $report_meta;
		return view( 'report.common-facility-report', $this->data );
	}

	/**
	 * @param int $days
	 *
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View|mixed
	 */
	public function enrollmentReport($days = 0){
		$is_download_request = $this->isTableDownloadRequest();
		$this->data['days_list'] = [ '0' => 'Any', '3' => 3, '5' => 5, '7' => 7, '10' => 10, '20' => 20, '30' => 30 ];
		$this->data['days'] = $days;
		$this->data['page_description'] = $days ? "Filtered by $days days since invitation sent" : "";

		$report = $this->healthslate_model->get_enrollment_report($days);
		// is_download_request ?
		if($is_download_request && !empty($report)){
			return $this->dataTableService->dataTableResponse( collect($report), true );
		}
		// paginate results for view
		$this->data['paginated_report'] = $this->makeLengthAwarePaginator( collect( $report ), config( 'healthslate.table_row_per_page' ) );
		return view( 'report.daily-enrollment', $this->data );
	}

	/**
	 * @param $facility_id
	 *
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
	 */
	public function facilityCoaches($facility_id){

		$facility = $this->facility->find( $facility_id );

		// is facility id is invalid?
		if ( empty( $facility ) ) {
			return redirect(route('report.facility-reports'));
		}
		$this->data ['facility']  = $facility;

		return view( 'report.facility-providers', $this->data );
	}

	/**
	 * @param $facility_id
	 *
	 * @return \Illuminate\Http\JsonResponse|mixed
	 */
	public function ajaxFacilityCoaches( $facility_id ) {
		$is_download_request = $this->isTableDownloadRequest();
		$data = collect();

		$facility = $this->facility->find( $facility_id );
		// is facility id is invalid?
		if ( empty( $facility ) ) {
			// no records found
			$providers = [];
		}else {
			$providers = $facility->providers()->orderBy( 'provider_id' )->with( [
				'user',
				'patientCountRelation'=> function($q){
					$q->isDeleted(false);
				}
			] )->get();
		}

		foreach ( $providers as $provider ) {
			$row = [
				'Provider Id'   => $provider->provider_id,
				'Provider Name'     => ! empty( $provider->user ) ? $provider->user->full_name : '',
				'Provider type'          => $provider->type,
				'patients'      => '<a class="pointer text-light-blue" href="' . route( 'report.coach.patient-interaction', [ 'provider_id' => $provider->provider_id ] ) . '"> '.$provider->patientCount.' </a>',
				'is_access_all' => bit_to_int( $provider->is_access_all ) ? 'Y' : '-',
				'is_deleted'    => '<span class="is_deleted ' . ( bit_to_int( $provider->is_deleted ) ? 'label label-danger' : '' ) . '" data-is-deleted="' . bit_to_int( $provider->is_deleted ) . '">' . ( bit_to_int( $provider->is_deleted ) ? 'Deleted' : '-' ) . '</span>',
				'Num of patients'  => $provider->patientCount,
			];
			if($is_download_request){
				$row['is_deleted'] = ( bit_to_int( $provider->is_deleted ) ? 'Deleted' : '-' );
				$row['provider_email'] = ! empty( $provider->user ) ? $provider->user->email : '';
				unset($row['patients']);
			}
			$data->push( ( $is_download_request ? $row : array_values( $row ) ) );
		}

		return $this->dataTableService->dataTableResponse($data, $is_download_request);
	}

	/**
	 * @param $provider_id
	 *
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
	 */
	public function coachPatientInteraction( $provider_id, $week = 0) {
		$provider = $this->provider->with( 'user', 'facility' )->find( $provider_id );

		// is provider id is invalid?
		if ( empty( $provider ) ) {
			return redirect(route('report.facility-reports'));
		}
		$this->data ['provider'] = $provider;
		$this->data ['page_description'] = "Provider Type: ".$provider->type;

		return view( 'provider.provider-patients', $this->data );

	}

	/**
	 * @param $provider_id
	 *
	 * @return \Illuminate\Http\JsonResponse|mixed
	 */
	public function ajaxCoachPatientInteraction($provider_id ) {
		$is_download_request = $this->isTableDownloadRequest();
		$provider = $this->provider->find( $provider_id );
		// is provider id is invalid?
		if ( empty( $provider ) ) {
			// no records found
			$patients = [];
		}else {

			$patients = $this->healthslate_model->get_provider_patients($provider_id, request('week',0));
		}

		$thresholds = [
			'last_active'  => 7,
			'sent_msg'     => 2,
			'1_to_1_calls' => 1,

		];
		$week = request('week',0);

		// Calculate warning & danger thresholds
		// warning: below 60%
		// danger: below 90% & above 60%

		$warn_thresholds_last_active = ($thresholds['last_active'] * $week) * 0.60;
		$danger_thresholds_last_active = ($thresholds['last_active'] * $week) * 0.90;

		$warn_thresholds_sent_msg = ($thresholds['sent_msg'] * $week) * 0.90;
		$danger_thresholds_sent_msg = ($thresholds['sent_msg'] * $week) * 0.60;

		$warn_thresholds_1_to_1_calls = ($thresholds['1_to_1_calls'] * $week) * 0.90;
		$danger_thresholds_1_to_1_calls = ($thresholds['1_to_1_calls'] * $week) * 0.60;

		// table class for warning & danger.
		$warning = "warning";
		$danger = "danger";

		\Log::info(compact(
			'warn_thresholds_last_active','danger_thresholds_last_active',
			'warn_thresholds_sent_msg', 'danger_thresholds_sent_msg',
			'warn_thresholds_1_to_1_calls', 'danger_thresholds_1_to_1_calls'
		));

		$data = collect();
		foreach ( $patients as $key => $patient ) {
			// creating row from result set
			$full_name = '<a href="' . route( 'patient.detail', [
					'patient_id'  => $patient->patient_id
				] ) . '">' . $patient->full_name . '</a>';

			// row object for datatable
			$row = [
				'patient_id' => $patient->patient_id,
				'full_name'  => $full_name,
				'mrn'        => $patient->mrn,
				'last_active'  => !is_null($patient->last_log_days) ? $patient->last_log_days : '-',
				'received_msg_link'  => '<a href="'.route('patient.user.messages',['patient_id' => $patient->patient_id, 'user_id' => $provider->user_id]).'" title="Click to View Messages">'.$patient->received_msg.'</a>',
				'sent_msg_link'  => '<a href="'.route('patient.user.messages',['patient_id' => $patient->patient_id, 'user_id' => $provider->user_id]).'" title="Click to View Messages">'.$patient->sent_msg.'</a>',
				'1_to_1_calls' => $patient->one_to_one_sessions,
				'is_deleted' => ( bit_to_int( $patient->is_deleted ) ? 'Deleted' : '' ) ,
				'received_msg' => $patient->received_msg,
				'sent_msg' => $patient->sent_msg,
			];
			if($is_download_request){
				$row['patient_email'] = $patient->email;
				$row['full_name'] = $patient->full_name;
				unset($row['received_msg_link']);
				unset($row['sent_msg_link']);
			}

			$is_need_attention = false;
			// filter for duration is used?
			// temporarily disabling this logic
			if (0 && $week > 0 ) {
				// either patient was not active yet / was active long days back
				if ( is_null( $patient->last_log_days ) || floatval( $patient->last_log_days ) >= $danger_thresholds_last_active ) {
					$is_need_attention = $danger;
				}
				// either patient has not sent msg / sent very few messages.
				else if ( ! floatval( $patient->sent_msg ) || floatval( $patient->sent_msg ) <= $danger_thresholds_sent_msg ) {
					$is_need_attention = $danger;
				}
				// either patient has not received 1:1 call / done very few 1:1 calls.
				else if ( ! floatval( $patient->one_to_one_sessions ) || floatval( $patient->one_to_one_sessions ) <= $danger_thresholds_1_to_1_calls ) {
					$is_need_attention = $danger;
				}
				// patient was active few days back but not now.
				else if ( floatval( $patient->last_log_days ) >= $warn_thresholds_last_active ) {
					$is_need_attention = $warning;
				}
				// patient has sent messages but not much.
				else if ( floatval( $patient->sent_msg ) <= $warn_thresholds_sent_msg ) {
					$is_need_attention = $warning;
				}
				// patient has received 1:1 calls but not much.
				else if ( floatval( $patient->one_to_one_sessions ) <= $warn_thresholds_1_to_1_calls ) {
					$is_need_attention = $warning;
				}
			}
			$row['is_need_attention'] = $is_need_attention;

			$data->push( ( $is_download_request ? $row : array_values( $row ) ) );

		}
		return $this->dataTableService->dataTableResponse($data, $is_download_request );
	}

	/**
	 * Patient Weekly Participation report
	 * @param $facility_id
	 *
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View|mixed
	 */
	public function weeklyParticipation( $facility_id ) {
		$this->data['css'] = ['datetime', 'select2'];
		$this->data['js'] = ['datetime', 'select2'];

		$this->data['facility_id'] = $facility_id;
		$this->data['show_duration_filter'] = true;
		$this->data['report_src'] = route('ajax.report.weekly.participation',['facility_id' =>  $facility_id] );

		$this->data['report_meta'] = (object) [
			'title'      => 'Weekly Participation Report',
			'body_class' => 'weekly-participation'

		];
		$coaches = collect($this->healthslate_model->get_facility_coach($facility_id))->sortBy('full_name');
		$coach_list = [];
		foreach($coaches as $coach){
			$coach_list[] = (object) [
				'id'    => $coach->provider_id,
				'text'    => $coach->full_name,
				'first_name'    => $coach->first_name,
				'last_name'    => $coach->last_name,
				'type'    => $coach->type
			];
		}
		$this->data['coach_list'] = $coach_list;
		return view( 'report.common-duration-report', $this->data );
	}

	public function engagementReport( $facility_id ){
		$this->data['css'] = ['datetime'];
		$this->data['js'] = ['datetime'];

		$this->data['facility_id'] = $facility_id;
		$this->data['show_duration_filter'] = true;
		$this->data['report_src'] = route('ajax.report.engagement',['facility_id' =>  $facility_id] );

		$this->data['report_meta'] = (object) [
			'title'      => 'Engagement Report',
			'body_class' => 'weekly-participation engagement-report'

		];
		return view( 'report.common-duration-report', $this->data );
	}

	/**
	 * @param $facility_id
	 *
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function coachEngagementReport( $facility_id ){
		$this->data['css'] = ['datetime'];
		$this->data['js'] = ['datetime'];

		$this->data['facility_id'] = $facility_id;
		$this->data['show_duration_filter'] = true;
		$this->data['duration_info'] = (object) [
			'label'         => 'Last 30 Days',
			'subtract_days' => 29,
		];
		$this->data['report_src'] = route('ajax.report.coach.engagement',['facility_id' =>  $facility_id] );

		$this->data['report_meta'] = (object) [
			'title'      => 'Coach Report',
			'body_class' => 'weekly-participation coach-report'

		];
		return view( 'report.common-duration-report', $this->data );
	}

	/**
	 * @param $facility_id
	 *
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function weekCohortReport( $facility_id ) {
		$this->data['facility_id'] = $facility_id;
		$report           = (object) [
			"visibleName"    => "Week Cohort Report",
			"table_src"      => route( 'ajax.report.week-cohort', ['facility_id' =>  $facility_id] ),
			"table_class"    => 'week-cohort',
			"result_headers" => [ "Week Group", "Users", "Active Users(<= 3d)", "# of Log Entries", "# of Weight Entries", "Weight Loss %", "View" ]
		];
		$page_description = "";

		return $this->getCommonReportView( $report, $page_description );
	}

	/**
	 * @param $facility_id
	 * @param $week
	 *
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function weekCohortPatients( $facility_id, $week = null , $coach = null ) {
		$this->data['facility_id'] = $facility_id;
		$this->data['breadcrumb'] = [
			(object) [
				'href' => route( 'report.week-cohort', [ 'facility_id' => $facility_id ] ),
				'text' => 'Week Cohort Report'
			]
		];
		$report           = (object) [
			"visibleName"    => "Week Cohort Patients",
			"table_src"      => route( 'ajax.report.week-cohort-patients', ['facility_id' =>  $facility_id, 'week' => $week, 'coach' => $coach ] ),
			"table_class"    => 'week-cohort',
			"result_headers" => [ "Log Week", "Patients", "Active Patients(<= 3d)", "# of Patients Logging",
				"# of Patients Viewed Content", "# of Patients Weighing", "# of Log Entries", "# of Weight Entries", "Average # of Weight Entries","# of Meals Logged", "View"
			]
		];
		$page_description =  $week ? "Patients in week ".$week : 'All Patients';

		return $this->getCommonReportView( $report, $page_description );
	}

	/**
	 * @param $facility_id
	 * @param $week
	 * @param $log_week
	 *
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function weekCohortPatientsLog( $facility_id, $week , $log_week ) {
		$this->data['facility_id'] = $facility_id;
		$this->data['breadcrumb'] = [
			(object) [
				'href' => route( 'report.week-cohort', [ 'facility_id' => $facility_id ] ),
				'text' => 'Week Cohort Report'
			],
			(object) [
				'href' => route( 'report.week-cohort-patients', [ 'facility_id' => $facility_id, 'week' => $week ? $week : null ] ),
				'text' => 'Week Cohort Patients'
			],
		];
		$report           = (object) [
			"visibleName"    => "Week Cohort Patients Logs",
			"table_src"      => route( 'ajax.report.week-cohort-patients-log', ['facility_id' =>  $facility_id, 'week' => $week, 'log_week' => $log_week ] ),
			"table_class"    => 'week-cohort',
			"result_headers" => [ "Patient Id", "# of Log Entries", "# of Weight Entries",
				"Last Weight Recorded on", "Starting Weight", "Weight Loss %", "Viewed Content",
				"Last Login in Week",
				"Manual Activity Minutes", "Total Steps Synced (Fitbit)", "# of Group Post", "Active & Engaged"
			]
		];
		$page_description = !empty($week) ?  "Patients in week ".$week.", Logs of Week ".$log_week : "All Patients' Logs of Week ".$log_week;

		return $this->getCommonReportView( $report, $page_description );
	}

	/**
	 * Patient Activity Sync Report (Fitbit + Manual)
	 * @param $facility_id
	 *
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function patientFitbitActivity($facility_id){

		$this->data['css'] = ['datetime'];
		$this->data['js'] = ['datetime'];

		$this->data['facility_id'] = $facility_id;
		$this->data['show_duration_filter'] = true;
		$this->data['report_src'] = route('ajax.report.patient.fitbit.activity',['facility_id' =>  $facility_id] );

		$this->data['report_meta'] = (object) [
			'title'      => 'Patient Fitbit Activity Report',
			'body_class' => 'weekly-participation patient-fitbit-activity'

		];
		return view( 'report.common-duration-report', $this->data );
	}

	/**
	 * Patient Weight Sync Report (Aria + Manual)
	 *
	 * @param $facility_id
	 * @param int $days
	 *
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View|mixed
	 */
	public function patientFitbitWeightReport($facility_id, $days = 1){

		$is_download_request = $this->isTableDownloadRequest();
		$fitbit_report = $this->healthslate_model->get_patient_fitbit_weight_report($facility_id, $days);
		// is_download_request ?
		if($is_download_request && !empty($fitbit_report)){
			// send all records
			return $this->dataTableService->dataTableResponse( collect($fitbit_report), true );
		}
		$paginated_result = $this->makeLengthAwarePaginator(collect($fitbit_report), config('healthslate.table_row_per_page'));

		$this->data['facility_id'] = $facility_id;
		$this->data['paginated_result'] = $paginated_result;
		$this->data['total_records'] = count($fitbit_report);
		$this->data['show_days_filter'] = true;
		$this->data['days_list'] = [ '0' => 'Any', '1' => 1, '3' => 3, '5' => 5, '7' => 7, '10' => 10, '20' => 20, '30' => 30 ];
		$this->data['days'] = $days;
		$this->data['show_week_filter'] = false;
		$this->data['test_patient_excluded_note'] = true;
		$this->data['page_description'] = '';
		$this->data['report_meta'] = (object) [
			'title'          => 'Patient Fitbit Weight Report',
			"result_headers" => [
				'patient_id'     => [ 'name' => "Patient Id" ],
				'name'      => [ 'name' => "Name" ],
				'mrn'      => [ 'name' => "MRN" ],
				'uuid'      => [ 'name' => "UUID" ],
				'diabetes_type'      => [ 'name' => "Diabetes Type" ],
				'is_tracker_linked'      => [ 'name' => "Tracker Linked", 'class' => 'text-center'  ],
				'date_tracker_linked'      => [ 'name' => "Tracker Linked Date", 'tooltip' => 'Day When First Activity was Synced'  ],
				'starting_weight'      => [ 'name' => "Starting Weight", 'tooltip' => 'Starting Weight Logged By Lead Coach / First Weight Synced'  ],
				'last_time_tracker_synced'      => [ 'name' => "Last Date Tracker sync’d", 'tooltip' => 'Day When Last Weight was Synced'  ],
				'no_of_times_synced'      => [ 'name' => "# of times sync’d", 'tooltip' => '# of times sync’d'  ],
				'last_synced_weight'      => [ 'name' => "Last sync’d Weight", 'tooltip' => 'Last sync’d Weight'  ],

				'last_time_bodyTrace_synced'      => [ 'name' => "Last time BodyTrace sync’d", 'tooltip' => 'Day When Last Weight was Synced From BodyTrace'  ],
				'no_of_times_bodyTrace_synced'      => [ 'name' => "# of times BodyTrace sync’d", 'tooltip' => '# of times BodyTrace Device sync’d'  ],
				'last_bodyTrace_synced_weight'      => [ 'name' => "Last BodyTrace sync’d Weight", 'tooltip' => 'Last BodyTrace sync’d Weight'  ],

				'last_time_manual_logged'      => [ 'name' => "Last Date Manually Logged", 'tooltip' => 'Last Day manual weight logged'  ],
				'no_of_times_manual_logged'      => [ 'name' => "# of Manually Logged", 'tooltip' => '# of times manual weight logged'  ],
				'last_manually_logged_weight'      => [ 'name' => "Last Manually Logged Weight", 'tooltip' => 'Last Manually logged Weight'  ],
			]
		];
		return view( 'report.common-facility-report', $this->data );
	}

	/**
	 * @param $facility_id
	 *
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View|mixed
	 */
	public function missingPatientProviderMapping($facility_id){
		$is_download_request = $this->isTableDownloadRequest();

		$facility = $this->facility->find($facility_id);
		if(empty($facility)){
			return $this->redirectBack();
		}
		$missing_mappings = $this->healthslate_model->get_missing_provider_patient_mapping($facility_id);

		Log::info('Total missing mapping for facility_id: '.$facility_id.' are '.count($missing_mappings));
		// is_download_request ?
		if($is_download_request && !empty($missing_mappings)){
			// send all records
			return $this->dataTableService->dataTableResponse( collect($missing_mappings), true );
		}
		$paginated_result = $this->makeLengthAwarePaginator(collect($missing_mappings), config('healthslate.table_row_per_page'));

		$this->data['facility_id'] = $facility_id;
		$this->data['facility'] = $facility;
		$this->data['show_days_filter'] = false;
		$this->data['show_week_filter'] = false;
		$this->data['paginated_result'] = $paginated_result;
		$this->data['total_records'] = count($missing_mappings);
		$this->data['page_description'] = 'Records missing in `provider_patient` table';
		$this->data['report_meta'] = (object) [
			'title'          => 'Missing Patient Provider Mappings',
			"result_headers" => [
				'patient_id'     => [ 'name' => "Patient Id" ],
				'provider_id'     => [ 'name' => "Provider Id" ],
			]
		];
		return view( 'report.common-facility-report', $this->data );
	}

	/**
	 * @param int $days
	 *
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View|mixed
	 */
	public function missingFitbitActivityLog($days = 0){
		$is_download_request    = $this->isTableDownloadRequest();
		$per_page               = config( 'healthslate.table_row_per_page' );
		$page                   = $this->getCurrentPage();
		$skip                   = $per_page * ( $page - 1 );

		$total_missing_mappings = $this->healthslate_model->get_missing_fitbit_activity_log( true, $days );
		info('Total missing Fitbit Activity Log mapping are: '.($total_missing_mappings).'. Limit ' . $skip . ', ' . $per_page);
		$missing_mappings = $this->healthslate_model->get_missing_fitbit_activity_log( false, $days, $per_page, $skip );

		// is_download_request ?
		if($is_download_request && !empty($missing_mappings)){
			// send all records
			return $this->dataTableService->dataTableResponse( collect($missing_mappings), true );
		}
		$paginated_result = $this->makePaginatorFrom(collect($missing_mappings), $total_missing_mappings ,$per_page);

		$this->data['show_days_filter'] = true;
		$this->data['current_page'] = $page;
		$this->data['days_list'] = [ '0' => 'Any', '1' => 1, '3' => 3, '5' => 5, '7' => 7, '10' => 10, '20' => 20, '30' => 30 ];
		$this->data['days'] = $days;
		$this->data['show_week_filter'] = false;
		$this->data['hide_footer_note'] = true;
		$this->data['paginated_result'] = $paginated_result;
		$this->data['total_records'] = $total_missing_mappings;
		$this->data['page_description'] = 'Records missing in `patient_app` DB';
		$this->data['report_meta'] = (object) [
			'title'          => 'Missing Fitbit Activity Logs',
			"result_headers" => [
				'patient_id'     => [ 'name' => "Patient Id" ],
				'name'     => [ 'name' => "Patient Name" ],
				'steps'     => [ 'name' => "Steps" ],
				'activity_start_date'     => [ 'name' => "Timestamp" ,'format_date' => config('healthslate.default_date_time_format')],
				'callback_time'     => [ 'name' => "Upload Time" ,'format_date' => config('healthslate.default_date_time_format')],
			]
		];
		return view( 'report.common-facility-report', $this->data );
	}

	/**
	 * @param int $days
	 *
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View|mixed
	 */
	public function missingFitbitWeightLog($days = 0){
		$is_download_request = $this->isTableDownloadRequest();
		$missing_mappings = $this->healthslate_model->get_missing_fitbit_weight_log($days);

		Log::info('Total missing Fitbit Weight Log mapping are: '.count($missing_mappings));
		// is_download_request ?
		if($is_download_request && !empty($missing_mappings)){
			// send all records
			return $this->dataTableService->dataTableResponse( collect($missing_mappings), true );
		}
		$paginated_result = $this->makeLengthAwarePaginator(collect($missing_mappings), config('healthslate.table_row_per_page'));

		$this->data['show_days_filter'] = true;
		$this->data['days_list'] = [ '0' => 'Any', '1' => 1, '3' => 3, '5' => 5, '7' => 7, '10' => 10, '20' => 20, '30' => 30 ];
		$this->data['days'] = $days;
		$this->data['show_week_filter'] = false;
		$this->data['hide_footer_note'] = true;
		$this->data['paginated_result'] = $paginated_result;
		$this->data['total_records'] = count($missing_mappings);
		$this->data['page_description'] = 'Records missing in `patient_app` DB';
		$this->data['report_meta'] = (object) [
			'title'          => 'Missing Fitbit Weight Logs',
			"result_headers" => [
				'patient_id'     => [ 'name' => "Patient Id" ],
				'name'     => [ 'name' => "Patient Name" ],
				'weight'     => [ 'name' => "Weight" ],
				'activity_start_date'     => [ 'name' => "Log Time" ,'format_date' => config('healthslate.default_date_time_format')],
				'callback_time'     => [ 'name' => "Upload Time" ,'format_date' => config('healthslate.default_date_time_format')],
			]
		];
		return view( 'report.common-facility-report', $this->data );
	}


	/**
	 * @param $facility_id
	 *
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function facilityShippingAddresses( $facility_id ) {
		$this->data['css'] = ['datetime'];
		$this->data['js'] = ['datetime'];

		$this->data['facility_id'] = $facility_id;
		$this->data['show_duration_filter'] = true;

		$this->data['report_src'] = route('ajax.report.facility.shipping-addresses',['facility_id' =>  $facility_id] );

		$this->data['report_meta'] = (object) [
			'title'      => 'Shipping Addresses for New Scales',
			'body_class' => 'weekly-participation shipping-addresses'

		];
		return view( 'report.common-duration-report', $this->data );
	}


	/**
	 * @param $facility_id
	 *
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View|mixed
	 */
	public function memberCoachesListReport( $facility_id ) {
		$is_download_request = $this->isTableDownloadRequest();
		$per_page            = config( 'healthslate.table_row_per_page' );
		$page                = $this->getCurrentPage();
		$skip                = $per_page * ( $page - 1 );

		// is_download_request ?
		if ( $is_download_request ) {
			// send all records
			$records = $this->healthslate_model->get_patient_coaches( $facility_id, false, - 1 );

			return $this->dataTableService->dataTableResponse( collect( $records ), true );
		}
		$count = $this->healthslate_model->get_patient_coaches( $facility_id, true );
		info( 'Total Patient Coach Records are: ' . ( $count ) . '. Limit ' . $skip . ', ' . $per_page );
		$records          = $this->healthslate_model->get_patient_coaches( $facility_id, false, $per_page, $skip );
		$paginated_result = $this->makePaginatorFrom( collect( $records ), $count, $per_page );

		$this->data['facility_id']      = $facility_id;
		$this->data['show_days_filter'] = false;
		$this->data['show_week_filter'] = false;
		$this->data['paginated_result'] = $paginated_result;
		$this->data['total_records']    = $count;
		$this->data['page_description'] = '';
		$this->data['report_meta']      = (object) [
			'title'          => 'Patient\'s Coach List',
			"result_headers" => [
				'patient_id'      => [ 'name' => "Patient Id" ],
				'full_name'       => [ 'name' => "Patient Name" ],
				'provider_id'     => [ 'name' => "Coach Id" ],
				'coach_full_name' => [ 'name' => "Coach Name" ],
				'coach_email'     => [ 'name' => "Coach Email" ],
			]
		];

		return view( 'report.common-facility-report', $this->data );
	}

	/**
	 * redirectBack
	 *      Common redirect handler
	 *
	 * @param string $redirect_to
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	private function redirectBack($redirect_to = 'dashboard'){
		return redirect( $redirect_to );
	}
        
    public function weightLossReport( $facility_id) {
        $this->data['css'] = ['datetime'];
		$this->data['js'] = ['datetime'];

		$this->data['facility_id'] = $facility_id;
		$this->data['show_duration_filter'] = true;
		$this->data['report_src'] = route('ajax.report.weight.loss',['facility_id' =>  $facility_id] );

		$this->data['report_meta'] = (object) [
			'title'      => 'Weight Loss Report',
			'body_class' => 'weekly-participation common-duration-report'

		];
		return view( 'report.common-duration-report', $this->data );
	}


	/**
	 * Patient Sessions Completed report
	 * @param $facility_id
	 *
	 * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View|mixed
	 */
	public function sessionsCompletedReport( $facility_id ) {

		$this->data['css'] = ['datetime'];
		$this->data['js'] = ['datetime'];

		$this->data['facility_id'] = $facility_id;
		$this->data['show_duration_filter'] = true;
		$this->data['report_src'] = route('ajax.report.sessions.completed',['facility_id' =>  $facility_id] );

		$this->data['report_meta'] = (object) [
			'title'      => 'Sessions Completed Report',
			'body_class' => 'weekly-participation engagement-report'
		];
		return view( 'report.common-duration-report', $this->data );
	}

    /**
     * Patient Self Register report
     * @param $facility_id
     *
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View|mixed
     */
    public function selfRegisterReport( $facility_id ) {

        $this->data['css'] = ['datetime'];
        $this->data['js'] = ['datetime'];

        $this->data['facility_id'] = $facility_id;
        $this->data['show_duration_filter'] = true;
        $this->data['report_src'] = route('ajax.report.self.register',['facility_id' =>  $facility_id] );

        $this->data['report_meta'] = (object) [
            'title'      => 'Self Register Report',
            'body_class' => 'weekly-participation engagement-report'
        ];
        return view( 'report.common-duration-report', $this->data );
    }


    /**
     * Patient Users Register report
     * @param $facility_id
     *
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View|mixed
     */
    public function usersRegistration( $facility_id ) {

        $this->data['css'] = ['datetime', 'select2'];
        $this->data['js'] = ['datetime', 'select2'];

        $this->data['facility_id'] = $facility_id;
        $this->data['show_duration_filter'] = true;
        $this->data['duration_info'] = (object) [
            'label'         => 'Today',
            'subtract_days' => 0,
        ];

        $self_user_filter[] = (object) ['id' => 1, 'text' => 'Users Self Registered'];
        $self_user_filter[] = (object) ['id' => 2, 'text' => 'Users who have not Downloaded the APP'];
        $self_user_filter[] = (object) ['id' => 3, 'text' => 'Users who have not been Reconciled'];

        foreach (get_timezone_list() as $key => $value)
        {
            $timezone_filter[] = (object) ['id' => $value, 'text' => $key];
        }

        $this->data['timezone_list'] = $timezone_filter;


        $this->data['coach_list'] = $self_user_filter;
        $this->data['self_user_filter'] = 'Filter By User';

        $this->data['report_src'] = route('ajax.report.user.registration',['facility_id' =>  $facility_id] );

        $this->data['report_meta'] = (object) [
            'title'      => 'User Registrations',
            'body_class' => 'weekly-participation engagement-report'
        ];
        return view( 'report.common-duration-report', $this->data );
    }


    /**
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function sendMessages() {

        $patient_id = request( 'patient_id', null );
        $this->data['css'] = [ 'select2' ];
        $this->data['js']  = [ 'select2' ];
        $this->data['patient_id']  = $patient_id;

        //$facilities = $this->facility->orderBy( 'name' )->get();
        //$facilities = $facilities->pluck('name','facility_id');
        //$this->data['facilities'] = $facilities->prepend('Select Facility','0');
        //$this->data['facility_id'] = $facility_id;

        return view( 'message.message', $this->data );
    }



    /**
     * Patient Import Patient Report
     * @param $facility_id
     *
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View|mixed
     */
    public function importPatient( ) {

        $this->data['css'] = ['datetime', 'select2'];
        $this->data['js'] = ['datetime', 'select2'];

        $this->data['show_duration_filter'] = true;

        $self_user_filter[] = (object) ['id' => 1, 'text' => 'New Patient'];
        $self_user_filter[] = (object) ['id' => 2, 'text' => 'Reconciled Patient'];
        $self_user_filter[] = (object) ['id' => 3, 'text' => 'Already Exist Patient'];

        $this->data['coach_list'] = $self_user_filter;
        $this->data['self_user_filter'] = 'Filter By Import Status';

        $this->data['report_src'] = route('ajax.report.import.patient');

        $this->data['report_meta'] = (object) [
            'title'      => 'Patient Bulk Import Report',
            'body_class' => 'weekly-participation engagement-report'
        ];
        return view( 'report.common-duration-report', $this->data );
    }



    /**
     * @param $facility_id
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function fitbitInvalidToken() {
        ini_set("memory_limit",-1);

        $device_support_db_name = config('healthslate.device_support_db_name');
        //$this->data['facility_id'] = $facility_id;

        //$binding['facility_id'] = $facility_id;
        $binding['is_expired_refresh_token'] = 1;

        $query[] = 'select patient_id, users.email,concat(users.first_name, " " ,users.last_name) as name, max(activity_start_date) as activity_start_date , max(third_party_user_id) as  fitbit_user_id, max(refresh_token) as refresh_token
                  from `'.$device_support_db_name.'`.`patient_device` ps, patient, `'.$device_support_db_name.'`.`patient_service_log` psl, users  where ps.patient_uuid = uuid and mrn not like "%test%" and psl.patient_uuid = uuid
				  and is_expired_refresh_token= :is_expired_refresh_token and patient.user_id = users.user_id AND users.email not like \'\\_%\' 
                  group by uuid order by max(activity_start_date) desc';

        $report_meta = [
            'title'          => 'Fitbit Invalid Token (All Facility Members)',
            'show_days_filter' => false,
            "result_headers" => [
                'patient_id'          => [ 'name' => "Patient Id" ],
                'email'               => [ 'name' => "Email" ],
                'name'                => [ 'name' => "Name" ],
                'activity_start_date' => [ 'name' => "Last Activity Date" ],
            ]
        ];

        return $this->getFacilityReportView( implode( " ", $query ), $binding, $report_meta );
    }

}
