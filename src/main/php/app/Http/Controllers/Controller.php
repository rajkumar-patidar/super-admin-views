<?php

namespace HealthSlateAdmin\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    /**
     * @var string
     */
    protected $email_validation_regex = "/^[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z]*[a-z])$/i";
    /**
     * @var object
     */
    protected $HS_CONSTANTS;

    /**
     *
     */
    function __construct(){
        $this->HS_CONSTANTS = (object) config( 'healthslate.hs_constants', [ ] );
    }
    /**
     * @return bool
     * @deprecated In favour of `DataTableServiceProvider->isExportRequest()` since 28th Dec 2016
     */
    protected function isTableDownloadRequest() {
        $action = request( 'action', null );

        return ( in_array( $action, [ 'csv' ] ) ? true : false );
    }

    /**
     * @param \Illuminate\Support\Collection $collection
     * @param $perPage
     *
     * @return LengthAwarePaginator
     */
    protected function makeLengthAwarePaginator(\Illuminate\Support\Collection $collection, $perPage ) {
        $page = $this->getCurrentPage();
        $slice     = $collection->slice( $perPage * ( $page - 1 ), $perPage );
        $paginator = new LengthAwarePaginator( $slice, $collection->count(), $perPage, $page,
            [ 'path' => Paginator::resolveCurrentPath() ] );

        return $paginator;
    }

    /**
     * @param \Illuminate\Support\Collection $collection
     * @param $recordCount
     * @param $perPage
     *
     * @return LengthAwarePaginator
     */
    protected function makePaginatorFrom(\Illuminate\Support\Collection $collection, $recordCount, $perPage){
        $paginator = new LengthAwarePaginator( $collection, $recordCount, $perPage, Paginator::resolveCurrentPage(),
            [ 'path' => Paginator::resolveCurrentPath() ] );

        return $paginator;
    }

    /**
     * @return int
     */
    protected function getCurrentPage(){
        return Paginator::resolveCurrentPage();
    }
}
