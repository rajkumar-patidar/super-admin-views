<?php
/**
 * Created by PhpStorm.
 * User: spatel
 * Date: 28/12/16
 * Time: 4:36 PM
 */

namespace HealthSlateAdmin\Http\Controllers\v2;

use Carbon\Carbon;
use HealthSlateAdmin\Http\Controllers\Controller;
use HealthSlateAdmin\Models\SoleraApi\SoleraApiAuthProvider;
use HealthSlateAdmin\Models\PatientAppServiceProvider;
use HealthSlateAdmin\Models\User;
use HealthSlateAdmin\Models\ReportService\Solera_bulk_import;
use HealthSlateAdmin\Models\Facility;
use HealthSlateAdmin\Models\Healthslate;
use HealthSlateAdmin\Models\Patient;
use HealthSlateAdmin\Models\Provider;
use HealthSlateAdmin\Models\CurriculumServiceProvider;
use Cookie;
use Log;
use Validator;


/**
 * Class ReportController
 * @package HealthSlateAdmin\Http\Controllers\v2
 */
class SoleraApiIntegration extends Controller {
	/**
	 * @var array
	 */
	protected $data = [ ];

	/**
	 * @var AuthServiceProvider
	 */
	protected $soleraAuthProvider;
	protected $validator;
	protected $user;
	protected $patientAppServiceProvider;
	protected $solera_bulk_import;
	protected $facility;
	protected $provider;
	protected $patient;
	protected $healthslate_model;
	/**
	 * @var CurriculumServiceProvider
	 */
	protected $curriculumServiceProvider;


	/**
	 * @param AuthServiceProvider $authServiceProvider
	 */
	function __construct(Facility $facility, Patient $patient, Provider $provider, Healthslate $healthslate, Solera_bulk_import $solera_bulk_import ,SoleraApiAuthProvider $soleraAuthProvider, Validator $validator, User $user, PatientAppServiceProvider $patientAppServiceProvider,
		CurriculumServiceProvider $curriculumServiceProvider) {
		$this->soleraAuthProvider = $soleraAuthProvider;
		$this->validator = $validator;
		$this->user = $user;
		$this->solera_bulk_import = $solera_bulk_import;
		$this->patientAppServiceProvider = $patientAppServiceProvider;
		$this->facility          = $facility;
		$this->provider          = $provider;
		$this->patient           = $patient;
		$this->healthslate_model = $healthslate;
		$this->curriculumServiceProvider = $curriculumServiceProvider;
	}

	/**
	 * @param
	 * @Desc: Enroll new patient from Solera api
	 * @return
	 */
	public function index($arguments) {

		//Log for command
		$output = new \Symfony\Component\Console\Output\ConsoleOutput(2);
		info( 'Command Arguments:', (array) $arguments) ;

		$facilityId = $arguments['facilityId'];
		$facility = $this->facility->find( $facilityId );

		// is facility id is invalid?
		if ( empty( $facility ) ) {
			// no records found
			$output->writeln( 'Invalid FacilityId: ' . $facilityId) ;
			info( 'Invalid FacilityId', (array) $arguments) ;
			die;
		}


		$startDate =  $this->solera_bulk_import->max('end_date');
		$start_date = $startDate;
		$endDate  =  date('Y-m-d\TH:i:s.Z\Z', time() );
		if(empty($startDate)) {
			//Set startDate flag -1 day to get data from solera api
			$startDate  = date('Y-m-d\T00:00:00.Z\Z', (strtotime('-2 day', time())));
			$start_date = date('Y-m-d H:i:s', (strtotime('-2 day', time())));
		}
		else{
			$startDate = date('Y-m-d\TH:i:s.Z\Z', strtotime($startDate));
		}

		$end_date = date('Y-m-d H:i:s', strtotime($endDate));

		// To get access token using refresh token
		$tokenInfo = $this->soleraAuthProvider->getAccessToken();
		if($tokenInfo)
		{
			$access_token = $tokenInfo->access_token;
			// Calling Daily Participant API

			$output->writeln('Calling Daily Participant Status api');

			$dailyParticipantStatus = $this->soleraAuthProvider->getDailyParticipantStatus($access_token, $startDate,$endDate);

			$output->writeln('Total Members Received From Participant API:'. count($dailyParticipantStatus->commitments) );

			$data = [ ];
			$members_without_address = [];
			$members_with_invalid_status = [];
			$date_format = 'Y-m-d';

			// Iterating, Validating each row.
			// Building request DTO, camelCase
			$not_in_email_rule = $this->user->get(['email'])->pluck('email')->prepend('not_in')->toArray();
			if(!empty ($dailyParticipantStatus->commitments)) {
				foreach ($dailyParticipantStatus->commitments as $row_number => $value) {

					// trim whitespace if any in date fields
					$row['birthdate'] = $value->Birthdate;
					$row['commited_date'] = $value->Committed_Date;
					$row['participant_term_date'] = $value->Committed_Time;
					$row['first_name'] = $value->First_Name;
					$row['last_name'] = $value->Last_Name;
					$row['email'] = $value->Email;
					$row['solera_id'] = $value->Solera_ID;

					$row_validation = Validator::make($row, [
						'first_name' => 'required',
						'last_name' => 'required',
						'email' => ['required', 'email', 'regex:' . $this->email_validation_regex, $not_in_email_rule],
						'solera_id' => 'required',
						'birthdate' => ['date_format:' . $date_format],
						'commited_date' => ['date_format:' . $date_format],
						// we will skip unsupported status as of now.
						//'status'     => 'in:new,New,NEW'
					], [
						'email.not_in' => trans('common.email_already_registered', ['email' => @$row['email']]),
						'birthdate.date_format' => 'Birth date should be in Date format (YYYY-MM-DD)',
						'commited_date.date_format' => 'Committed date should be in Date format (YYYY-MM-DD)',
					]);


					if ($row_validation->fails()) {
						// send back to the page with the input data and errors
						foreach ($row_validation->errors()->all('<p>:message</p>') as $message) {
							$rowErrorMessages[] = $message;
						}
						//info( 'Validation Error are:', $rowErrorMessages );
						continue;
					}

					// process gender to healthslate constants.
					$gender = isset($value->Gender) ? strtolower(trim($value->Gender)) : null;
					if ($gender == 'm') {
						$gender = $this->HS_CONSTANTS->GENDER_MALE;
					} elseif ($gender == 'f') {
						$gender = $this->HS_CONSTANTS->GENDER_FEMALE;

					}

					$data[] = (object)[
						'payerId' => isset($value->Payer_ID) ? strtolower($value->Payer_ID) : null,
						'soleraId' => isset($value->Solera_ID) ? $value->Solera_ID : null,
						'lastName' => $value->Last_Name,
						'firstName' => $value->First_Name,
						'middleName' => isset($value->Middle_Name) ? $value->Middle_Name : null,
						'birthdate' => (isset($value->Birthdate) && !empty($value->Birthdate)) ? Carbon::createFromFormat($date_format, $value->Birthdate)->timestamp * 1000 : null,
						'gender' => $gender,
						'mailaddress1' => isset($value->Mailing_Address->street) ? $value->Mailing_Address->street : null,
						'mailaddress2' => isset($value->mailaddress2) ? $value->mailaddress2 : null,
						'city' => isset($value->Mailing_Address->city) ? $value->Mailing_Address->city : null,
						'state' => isset($value->Mailing_Address->state) ? $value->Mailing_Address->state : null,
						'zipCode' => isset($value->Mailing_Address->postalCode) ? $value->Mailing_Address->postalCode : null,
						'preferredPhoneNumber' => isset($value->Preferred_Phone_Number) ? $value->Preferred_Phone_Number : null,
						'homeNumber' => isset($value->Home_Number) ? $value->Home_Number : null,
						'workPhone' => isset($value->Work_Number) ? $value->Work_Number : null,
						'cellNum' => isset($value->Cell_Number) ? $value->Cell_Number : null,
						'email' => isset($value->Email) ? $value->Email : null,
						'preferredCommunicationMethod' => isset($value->preferred_communication_method) ? $value->preferred_communication_method : null,
						'commitedDate' => (isset($value->Committed_Date) && !empty($value->Committed_Date)) ? Carbon::createFromFormat($date_format, $value->Committed_Date)->timestamp * 1000 : null,
						'language' => isset($value->Language) ? $value->Language : null,
						'visuallyImpaired' => isset($value->visually_impaired) ? $value->visually_impaired : null,
						'hearingImpaired' => isset($value->hearing_impaired) ? $value->hearing_impaired : null,
						'participantTermDate' => (isset($value->participant_term_date) && !empty($value->participant_term_date)) ? Carbon::createFromFormat($date_format, $value->participant_term_date)->timestamp * 1000 : null,
						'status' => isset($value->status) ? $value->status : null,
						'facilityId' => $arguments['facilityId'],
						'diabetesTypeId' => $arguments['diabetesTypeId'],
						'primaryFoodCoachId' => isset($arguments['lifestyleCoachId']) ? $arguments['lifestyleCoachId'] : null,
						'leadCoachId' => isset($arguments['foodCoachId']) ? $arguments['foodCoachId'] : null,
					];

					// We have to have an address, city, state and zipcode. If not, we need to still register the member,
					// but somehow note that there was no shipping address given.
					if (empty($value->Mailing_Address->street) || empty($value->Mailing_Address->city) || empty($value->Mailing_Address->state) || empty($value->Mailing_Address->postalCode)) {
						$members_without_address[] = $value->Solera_ID;
					}

					break;

				}
				//Log for laravel log file
				info( 'Total Members: ' . count( $dailyParticipantStatus->commitments ));
				info( 'Members without address: ' . count( $members_without_address ) );
				info( 'Members already exist: ' . (count( $dailyParticipantStatus->commitments ) - count( $data ))  );
				info( 'Members going to import: ' . count( $data ) );

				//Log for console command
				$output->writeln('Total Members: ' . count( $dailyParticipantStatus->commitments ));
				$output->writeln('Members without address: ' . count( $members_without_address ) );
				$output->writeln('Members already exist: ' . (count( $dailyParticipantStatus->commitments ) - count( $data )) );
				$output->writeln('Members going to import: ' . count( $data ) );

				if ( !empty( $data ) ) {
					$result = $this->patientAppServiceProvider->enrollPatients( $data );
					if ( $result === true ) {
						info( 'Successfully queued ' . count( $data ) . ' patients for bulk import');
						$output->writeln( 'Successfully queued ' . count( $data ) . ' patients for bulk import');

						$this->solera_bulk_import->insert([
							['start_date' 	=> $start_date, 'end_date' => $end_date, 'total_records' => count($dailyParticipantStatus->commitments), 'total_success' => count( $data ),'total_failed' => (count( $dailyParticipantStatus->commitments ) - count( $data )),
							'status' => $dailyParticipantStatus->success]
						]);
					}
				}
			}
			else {

				$this->solera_bulk_import->insert([
					['start_date' 	=> $start_date, 'end_date' => $end_date, 'total_records' => count($dailyParticipantStatus->commitments), 'status' => $dailyParticipantStatus->success,'message' => $dailyParticipantStatus->message ]
				]);

				info('Total Members:' . count($dailyParticipantStatus->commitments));
			}

			// Calling Solera Get api for disenrolled user between specific timeframe

			$start = Carbon::createFromFormat( 'm/d/Y', date('m/d/Y') )->setTime( 0, 0 )->addDay( -1 );
			$startDate = date('Y-m-d\TH:i:s.Z\Z', strtotime($start));
			$output->writeln('Calling disenrolled Solera api');
			$dailyParticipantStatus = $this->soleraAuthProvider->getParticipantEnrollmentStatus($access_token, $startDate);

			if($dailyParticipantStatus->message == '')
			{

			}
			else
			{
				info($dailyParticipantStatus->message);
				$output->writeln($dailyParticipantStatus->message);
			}

		}
	}



	/**
	 * @param
	 * @Desc: To enroll new member from Solera
	 * @return
	 */
	public function enrollmentDataToSolera($arguments) {

		//Log for command
		$output = new \Symfony\Component\Console\Output\ConsoleOutput(2);

		info( 'Command Arguments:', (array) $arguments) ;
		$facilityId = $arguments['facilityId'];
		$facility = $this->facility->find( $facilityId );

		// Check facility id is invalid?
		if ( empty( $facility ) ) {
			// no records found
			$patients = [];
			$output->writeln( 'Invalid FacilityId: ' . $facilityId) ;
			info( 'Invalid FacilityId', (array) $arguments) ;
		}
		else
		{
			// days filter
			$days = 1;
			// fetch patients
			$output->writeln( 'Getting patients for facility..') ;
			info( 'Getting patients for facility..') ;

			$patients = $this->soleraAuthProvider->getEnrollmentDataToSolera($facilityId, $days);

			if(!empty($patients))
			{
				// To get access token using refresh token
				$tokenInfo = $this->soleraAuthProvider->getAccessToken();
				if($tokenInfo) {
					$access_token = $tokenInfo->access_token;

					foreach($patients as $patient_details)
					{
						$data = array();

						if($patient_details->is_deleted == 1)
							$data['enrolled'] 		= 'false';
						else
							$data['enrolled'] 		= 'true';

						$data['partnerId'] 		= $patient_details->payer_id;
						$data['soleraId'] 		= $patient_details->mrn;
						$data['enrolledDate'] 	= date('Y-m-d\TH:i:s.Z\Z', strtotime($patient_details->registration_date));
						$data['startDate'] 		= date('Y-m-d\TH:i:s.Z\Z', strtotime($patient_details->registration_date));

						if($patient_details->is_deleted == 1) {
							$data['enrollmentReasonCode'] = '07';
							$data['enrollmentReasonText'] = '';
						}

						//Dummy data
						/*$data['enrolled'] 		= 'true';
						$data['partnerId'] 		= '1000008';
						$data['soleraId'] 		= '10005';
						$data['enrolledDate'] 	= "2016‐12‐07T22:09:16.277Z";
						$data['startDate'] 		= "2016‐12‐07T22:09:16.277Z";
						$data['enrollmentReasonCode'] = '07';
						$data['enrollmentReasonText'] = '';*/

						$dailyParticipantStatus = $this->soleraAuthProvider->postEnrollmentDataToSolera($access_token, $data);
						//echo json_encode($data);die;
					}
				}
				else{
					$output->writeln( 'Access token not generated.');
					info( 'Access token not generated.');
				}
			}
			else
			{
				$output->writeln( 'No Patients registed today.');
				info( 'No Patients registered today.');
			}




		}
	}


	/**
	 * @param
	 * @Desc: Send engagement report data to Solera api
	 * @return
	 */
	public function engagementDataToSolera( $arguments )
	{
		//Log for command
		$output = new \Symfony\Component\Console\Output\ConsoleOutput(2);

		info( 'Command Arguments:', (array) $arguments) ;
		$facilityId = $arguments['facilityId'];
		$facility = $this->facility->find( $facilityId );

		// is facility id is invalid?
		if ( empty( $facility ) ) {
			// no records found
			$patients = [];
			$output->writeln( 'Invalid FacilityId: ' . $facilityId) ;
			info( 'Invalid FacilityId', (array) $arguments) ;
		}
		else
		{
			// Current day -1 day
			$start = Carbon::createFromFormat( 'm/d/Y', date('m/d/Y') )->setTime( 0, 0 )->addDay( -1 );
			$end   = Carbon::createFromFormat( 'm/d/Y', date('m/d/Y') )->setTime( 0, 0 );

			Log::info( 'Loading Engagement report for: ' . $start . ' - ' . $end );

			$results = $this->soleraAuthProvider->get_weekly_participation_report_for_duration( $facilityId, $start, $end );

			Log::info( 'Total records in Engagement report are: ' . count( $results ) );

			$results = $this->curriculumServiceProvider->getEducationModulesCompleted( $results, $start, $end, false );
			$results = $this->buildEngagementReport($results, $start , $end);

			// To get access token using refresh token
			$tokenInfo = $this->soleraAuthProvider->getAccessToken();
			if($tokenInfo) {
				$access_token = $tokenInfo->access_token;

				if(!empty($results)) {
					foreach($results as $patient_details) {
						$response = $this->soleraAuthProvider->postEngagementDataToSolera( $access_token, $patient_details );
						break;
					}
				}
				else{
					$output->writeln( 'Empty result.');
					info( 'Empty result.');
				}
			}
			else
			{
				$output->writeln( 'Access token not generated.');
				info( 'Access token not generated.');
			}

		}
	}


	/**
	 * @param $results
	 * @param string $default
	 *
	 * @return mixed
	 */
	private function buildEngagementReport( &$results,  $start , $end ) {
		// get collection for patient id pluck
		$result_collection = ( $results instanceof \Illuminate\Pagination\LengthAwarePaginator ) ? $results->getCollection() : collect( $results );
		$patient_ids       = $result_collection->pluck( 'patient_id' )->toArray();
		$user_ids          = $result_collection->pluck( 'user_id' )->toArray();
		info( 'All Patients are: ', $patient_ids );

		$start_date = $start;
		$end_date   = $end;
		// get previous weight logs for all patients
		$patients_weight_log = collect( $this->healthslate_model->get_patient_recent_weight_log( $patient_ids, $start_date ) );
		$patients_weight_log = $patients_weight_log->groupBy("patient_id");

		info('Collected all information, Building Engagement Report');
		for($i = 0; $i < count($results); $i++){
			$source = $results[$i];

			$physical_activity_minutes = 0;
			if(isset($source->manual_activity_minutes)){
				$physical_activity_minutes += intval($source->manual_activity_minutes);
			}
			// @todo spatel
			//  If Fitbit Minutes are not yet tracked, use (# of FitbitSteps/105) as an approximation
			if(isset($source->total_steps_synced)){
				$physical_activity_minutes += (intval($source->total_steps_synced)/105);
			}

			// Weight Info
			$recent_weight = null;
			$weight_loss = null;
			$weight_loss_percent = null;


			if(!empty($source->current_weight)){
				$recent_weight = $source->current_weight;
			}else{
				$weight_log = isset($patients_weight_log[$source->patient_id]) ? $patients_weight_log[$source->patient_id][0] : null;
				if(!empty($weight_log) && isset($weight_log->weight)){
					$recent_weight = $weight_log->weight;
				}
			}

			if ( ! empty( $source->starting_weight ) && ! is_null( $recent_weight )  ) {
				$weight_loss         = format_weight(( $source->starting_weight - $recent_weight ));
				$weight_loss_percent = format_weight(( $weight_loss ? ( $weight_loss / $source->starting_weight ) * 100 : '' ));

			}
			if ( ! empty( $source->starting_weight ) ){
				$source->starting_weight = format_weight($source->starting_weight);
			}
			if ( ! empty( $recent_weight ) ){
				$recent_weight = format_weight($recent_weight);
			}

			$coachInteractions = 0;
			if ( ! empty( $source->one_to_one_sessions ) ){
				$coachInteractions += $source->one_to_one_sessions;
			}
			if ( ! empty( $source->sent_msg ) ){
				$coachInteractions += $source->sent_msg;
			}

			$row = (object) [
				'activityDate'              => date('Y-m-d\TH:i:s.Z\Z', strtotime($source->user_time)),
				'partnerId'                 => $source->patient_id,
				'soleraId'                  => $source->mrn,
				'physicalActivityMinutes'   => floor($physical_activity_minutes),
				'weight'                    => $recent_weight,
				'mealsLogged'               => isset($source->no_of_meal_log) ? $source->no_of_meal_log : 0,
				'coachName'                 => isset($source->lead_coach_name) ? $source->lead_coach_name : '',
				'modulesCompleted'          => isset($source->viewed_units) ? $source->viewed_units : 0,
				'groupInteractions'         => isset($source->no_of_goal_logged) ? $source->no_of_goal_logged : 0,

				'coachDuration'             => 0,
				'coachInteractions'         => $coachInteractions,
				'goalBehaviorSet'           => 0,


				'videoWatchedPercent'       => 0,
				'appPoints'                 => 0,
				'articlesRead'              => 0,
				'coachType'                 => 0,
				'groupType'                 => 0,
				'surveysCompleted'          => 0
			];
			$results[$i] = $row;
		}
		return $results;
	}

}