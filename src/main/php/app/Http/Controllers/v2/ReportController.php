<?php
/**
 * Created by PhpStorm.
 * User: spatel
 * Date: 28/12/16
 * Time: 4:36 PM
 */

namespace HealthSlateAdmin\Http\Controllers\v2;

use Carbon\Carbon;
use HealthSlateAdmin\Helpers\DataTableServiceProvider;
use HealthSlateAdmin\Http\Controllers\Controller;
use HealthSlateAdmin\Models\PatientApp\DatatableQueryProvider;

/**
 * Class ReportController
 * @package HealthSlateAdmin\Http\Controllers\v2
 */
class ReportController extends Controller {
	/**
	 * @var array
	 */
	protected $data = [ ];

	/**
	 * @var DatatableQueryProvider
	 */
	protected $datatableQueryProvider;

	/**
	 * @var DataTableServiceProvider
	 */
	protected $dataTableServiceProvider;

	/**
	 * @var bool
	 */
	protected $isAjax = false;

	/**
	 * @var bool
	 */
	protected $isExportRequest = false;

	/**
	 * @param DatatableQueryProvider $datatableQueryProvider
	 * @param DataTableServiceProvider $dataTableServiceProvider
	 */
	function __construct(
		DatatableQueryProvider $datatableQueryProvider,
		DataTableServiceProvider $dataTableServiceProvider
	) {
		$this->datatableQueryProvider   = $datatableQueryProvider;
		$this->dataTableServiceProvider = $dataTableServiceProvider;
		$this->isAjax                   = request()->ajax();
		$this->isExportRequest          = $this->dataTableServiceProvider->isExportRequest();
	}

	/**
	 * @param $facility_id
	 *
	 * @return \BladeView|\Illuminate\Http\JsonResponse
	 */
	public function memberGroupReport( $facility_id ) {

		if ( $this->isAjax || $this->isExportRequest ) {
			// get DB Query for building Datatable
			$builderQuery = $this->datatableQueryProvider->getMemberGroupReportQuery( $facility_id );
			// get search Closure
			$searchClosure = $this->datatableQueryProvider->getMemberGroupReportSearchClosure();

			return $this->dataTableServiceProvider->usingQueryBuilder( $builderQuery, $searchClosure,
				// pre-processing result data of Datatable
				function ( $datatable ) {
					$datatable->editColumn( 'member_id', function ( $row ) {
						return ( $row->member_id && ! $this->isExportRequest ) ? link_to_route( 'patient.detail', $row->member_id, [ 'patient_id' => $row->member_id ] ) : $row->member_id;
					} );
					$datatable->editColumn( 'in_person', function ( $row ) {
						return $row->in_person ? 'Yes' : '-';
					} );
				} );
		} else {

			// Render View for DataTable

			$this->data['facility_id'] = $facility_id;
			$this->data['report']      = (object) [
				"visibleName"    => "Member Groups",
				"table_src"      => route( 'report.member-group', [ 'facility_id' => $facility_id ] ),
				"table_class"    => 'member-group',
				"result_headers" => [
					'Group Name',
					'Member Id',
					'MRN',
					'First Name',
					'Group Visibility',
					'Lifestyle Coach',
					'Food Coach',
					'Facility Admin',
					'Created On',
					'Started On',
					'In Person'
				]
			];

			$this->data['page_description']              = "";
			$this->data['deleted_patient_excluded_note'] = true;
			$this->data['test_patient_excluded_note']    = true;
			$this->data['note_dates_are_in_timezone']    = true;

			return view( 'report.common-reports-detail-view', $this->data );
		}
	}

	/**
	 * Request Handler for Member Missing Facility Mapping report.
	 *
	 * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
	 */
	public function memberMissingFacilityMapping() {

		if ( $this->isAjax || $this->isExportRequest ) {
			// get DB Query for building Datatable
			$builderQuery = $this->datatableQueryProvider->getMemberMissingFacilityMappingQuery();
			// get search Closure
			$searchClosure = $this->datatableQueryProvider->getMemberMissingFacilityMappingReportSearchClosure();

			return $this->dataTableServiceProvider->usingQueryBuilder( $builderQuery, $searchClosure,
				// pre-processing result data of Datatable
				function ( $datatable ) {
					$datatable->editColumn( 'member_id', function ( $row ) {
						return ( $row->member_id && ! $this->isExportRequest ) ? link_to_route( 'patient.detail', $row->member_id, [ 'patient_id' => $row->member_id ] ) : $row->member_id;
					} );
					$datatable->editColumn( 'is_registration_completed', function ( $row ) {
						return $row->is_registration_completed ? 'Yes' : '-';
					} );
				} );
		} else {
			// Render View for DataTable

			$this->data['report'] = (object) [
				"visibleName"    => "Member Missing Facility Mapping Report",
				"table_src"      => route( 'report.members-missing-facility-mapping' ),
				"table_class"    => 'members-missing-facility',
				"result_headers" => [
					'Member Id',
					'First Name',
					'MRN',
					'Diabetes Type',
					'Is Registration Completed'
				]
			];

			$this->data['page_description']              = "Members missing mapping for facility in `patient_facility` table";
			$this->data['deleted_patient_excluded_note'] = true;

			return view( 'report.common-reports-detail-view', $this->data );
		}
	}

	/**
	 * show patient gone inactive report
	 *
	 * @return \BladeView|\Illuminate\Http\JsonResponse|\Illuminate\View\View
	 */
	public function memberGoneInactive()
	{
		if ($this->isAjax || $this->isExportRequest) {
			// get DB Query for building Datatable
			$builderQuery = $this->datatableQueryProvider->getMemberGoneInactiveReportQuery();
			// get search Closure
			$searchClosure = $this->datatableQueryProvider->getMemberGoneInactiveReportSearchClosure();

			return $this->dataTableServiceProvider->usingQueryBuilder( $builderQuery, $searchClosure,
				// pre-processing result data of Datatable
				function ( $datatable ) {
					$datatable->removeColumn( 'is_registration_completed', 'email' );
					$datatable->editColumn( 'member_id', function ( $row ) {
						return ( $row->member_id && ! $this->isExportRequest ) ? link_to_route( 'patient.detail', $row->member_id,
							[ 'patient_id' => $row->member_id ] ) : $row->member_id;
					} );
					$datatable->editColumn( 'last_time', function ( $row ) {
						return ( ! empty( $row->last_time ) ) ? Carbon::createFromFormat( 'Y-m-d H:i:s',
							$row->last_time )->format( config( 'healthslate.default_date_format' ) ) : $row->last_time;
					} );
				} );
		} else {
			// Render View for DataTable

			$this->data['report'] = (object) [
				"visibleName"    => "Member gone inactive",
				"table_src"      => route( request()->route()->getName() ),
				"table_class"    => 'members-gone-inactive',
				"result_headers" => [
					'Member Id',
					'Member Name',
					'Last log days',
					'Last time',
					'Lifestyle Coach Name',
					'Lifestyle Coach Id',
					'Lifestyle Coach Facility'
				]
			];

			$this->data['page_description']              = "Members that stopped logging > 7 days ago";
			$this->data['deleted_patient_excluded_note'] = true;
			$this->data['test_patient_excluded_note']    = true;

			return view( 'report.common-reports-detail-view', $this->data );
		}
	}
}