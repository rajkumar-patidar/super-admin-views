<?php
/**
 * Created by PhpStorm.
 * User: spatel
 * Date: 28/12/16
 * Time: 4:36 PM
 */

namespace HealthSlateAdmin\Http\Controllers\v2;

use Carbon\Carbon;
use HealthSlateAdmin\Http\Controllers\Controller;
use HealthSlateAdmin\Models\SoleraApi\SoleraApiAuthProvider;
use HealthSlateAdmin\Models\PatientAppServiceProvider;
use HealthSlateAdmin\Models\User;
use HealthSlateAdmin\Models\ReportService\Solera_bulk_import;
use HealthSlateAdmin\Models\Facility;
use HealthSlateAdmin\Models\Healthslate;
use HealthSlateAdmin\Models\Patient;
use HealthSlateAdmin\Models\Provider;
use HealthSlateAdmin\Models\CurriculumServiceProvider;
use HealthSlateAdmin\Helpers\DataTableServiceProvider;
use Cookie;
use Log;
use Validator;


/**
 * Class ReportController
 * @package HealthSlateAdmin\Http\Controllers\v2
 */
class FitbitActivitySteps extends Controller {
    /**
     * @var array
     */
    protected $data = [ ];

    /*
	 * @var Da*taTableServiceProvider
	 */
    protected $dataTableService;


    /**
     * @var AuthServiceProvider
     */
    protected $soleraAuthProvider;
    protected $validator;
    protected $user;
    protected $patientAppServiceProvider;
    protected $solera_bulk_import;
    protected $facility;
    protected $provider;
    protected $patient;
    protected $healthslate_model;
    /**
     * @var CurriculumServiceProvider
     */
    protected $curriculumServiceProvider;


    /**
     * @param AuthServiceProvider $authServiceProvider
     */
    function __construct(Facility $facility, Patient $patient, Provider $provider, Healthslate $healthslate, Solera_bulk_import $solera_bulk_import ,SoleraApiAuthProvider $soleraAuthProvider, Validator $validator, User $user, PatientAppServiceProvider $patientAppServiceProvider,
                         CurriculumServiceProvider $curriculumServiceProvider, DataTableServiceProvider $dataTableService) {
        $this->soleraAuthProvider = $soleraAuthProvider;
        $this->validator = $validator;
        $this->user = $user;
        $this->solera_bulk_import = $solera_bulk_import;
        $this->patientAppServiceProvider = $patientAppServiceProvider;
        $this->facility          = $facility;
        $this->provider          = $provider;
        $this->patient           = $patient;
        $this->healthslate_model = $healthslate;
        $this->curriculumServiceProvider = $curriculumServiceProvider;
        $this->dataTableService          = $dataTableService;
    }

    /**
     * search patient by id
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendMessagesToUser(PatientAppServiceProvider $patientAppServiceProvider){
        ini_set("memory_limit",-1);

        //Get patient details form Patient Table
        $p_detail = $this->healthslate_model->get_patient_detail_using_id(request('patient_id'));

        if(!empty(@$p_detail[0]->uuid))
        {
            $patient_uuid = $p_detail[0]->uuid;

            //To get details from Device Support DB
            $patient_details = $this->healthslate_model->get_patient_detail_from_device_db($patient_uuid);

            if(!empty(@$patient_details))
            {
                $patient_details = $patient_details[0];

                // To get fitbit steps from Fitbit server for a patient
                $fitbit_data = $this->soleraAuthProvider->getPatientActivitySteps($patient_details);

                if(@$fitbit_data->errors[0]->errorType == 'insufficient_permissions' || @$fitbit_data->errors[0]->errorType == 'system'
                    || @$fitbit_data->errors[0]->errorType == 'invalid_token' || @$fitbit_data->errors[0]->errorType == 'insufficient_scope')
                {
                    return $response = [
                        'error' => @$fitbit_data->errors[0]->message,
                    ];
                }

                // To get Synced steps from our server for a patient
                $steps_details = $this->healthslate_model->get_patient_steps_from_activity_log_summary($patient_uuid);

                if(@$fitbit_data->errors[0]->errorType != 'expired_token') {
                    foreach ($fitbit_data as $key => $value) {
                        foreach ($value as &$values) {
                            $values->steps_from_db = 0;
                        }
                    }

                    if (!empty($steps_details)) {
                        foreach ($fitbit_data as $key => $value) {
                            foreach ($value as &$values) {
                                foreach ($steps_details as $steps) {
                                    if ($steps->date_format == $values->dateTime) {
                                        $values->steps_from_db = $steps->steps;
                                    }
                                }
                            }
                        }
                    }
                }

                if(@$fitbit_data->errors[0]->errorType == 'expired_token')
                {
                    // To get new patient accesstoken from fitbit server
                    $fitbit_res = $this->soleraAuthProvider->getPatientNewAccessToken($patient_details);
                    if(@$fitbit_res->refresh_token)
                    {
                        // To get update patient accesstoken into our server
                        $this->healthslate_model->updatePatientAccessToken($patient_details, $fitbit_res);

                        $patient_details->access_token = @$fitbit_res->access_token;
                        $fitbit_data = $this->soleraAuthProvider->getPatientActivitySteps($patient_details);

                        if(@$fitbit_data->errors[0]->errorType != 'expired_token') {
                            foreach ($fitbit_data as $key => $value) {
                                foreach ($value as &$values) {
                                    $values->steps_from_db = 0;
                                }
                            }

                            if (!empty($steps_details)) {
                                foreach ($fitbit_data as $key => $value) {
                                    foreach ($value as &$values) {
                                        foreach ($steps_details as $steps) {
                                            if ($steps->date_format == $values->dateTime) {
                                                $values->steps_from_db = $steps->steps;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        return $response = [
                            'success' => $fitbit_data,
                        ];
                    }
                    else{
                        return $response = [
                            'error' => @$fitbit_res->errors[0]->message,
                        ];
                    }
                }
                else{
                    return $response = [
                        'success' => $fitbit_data,
                    ];
                }
            }
            else
            {
                return $response = [
                    'error' => "Patient details not found into Device Support database.",
                ];
            }
        }
        else
        {
            return $response = [
                'error' => "Patient detail not found.",
            ];
        }

    }


    /**
     * @param $facility_id
     *
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function fitbitStepsMismatchReport() {
        $days = request( 'days', null );
        $this->data['css'] = ['datetime', 'select2'];
        $this->data['js'] = ['datetime', 'select2'];
        $this->data['days'] = $days;

        $this->data['show_duration_filter'] = true;
        $this->data['hide_duration_filter_dropdown'] = false;

        $this->data['report_src'] = route('ajax.report.fitbit-steps-mismatch' );
        $this->data['show_days_filter'] = true;
        $this->data['days_list'] = [ '7' => 7, '30' => 30 ];
        $this->data['days'] = !empty(request()->days) ? request()->days : 0;
        $this->data['report_meta'] = (object) [
            'title'      => 'Fitbit Steps Mismatch',
            'body_class' => 'weekly-participation'
        ];

        return view( 'report.common-duration-report', $this->data );
    }

    /**
     * search patient by id
     * @return \Illuminate\Http\JsonResponse
     */
    public function fitbitStepsMismatch(PatientAppServiceProvider $patientAppServiceProvider){
        ini_set("memory_limit", -1);
        ini_set('max_execution_time', 0);
        $days = request( 'days', 30 );
        //Get patient details form Patient Table
        $p_detail = $this->healthslate_model->get_patient_detail_using_id();
        $patient_detail = collect($this->healthslate_model->get_patient_detail_from_device_db());
        if($days == 7)
        {
            $steps_details = collect($this->healthslate_model->get_patient_steps_from_activity_log_summary(false , 8));
        }
        else
        {
            $steps_details = collect($this->healthslate_model->get_patient_steps_from_activity_log_summary());
        }
        $detail = collect();
        foreach ($p_detail as $key => $patient_id) {
            $counter = 0;
            $last_mismatch_date = [];
            $patient_uuid_exist = $patient_detail->contains('patient_uuid', $patient_id->uuid);

            if ($patient_uuid_exist) {
                $patient_details = $patient_detail->where('patient_uuid', $patient_id->uuid)->first();
                $patient_synced_steps = $steps_details->where('patient_id', $patient_id->patient_id);
                // To get fitbit steps from Fitbit server for a patient
                if($days == 7)
                {
                    $fitbit_data = $this->soleraAuthProvider->getPatientActivitySteps($patient_details, 7);
                }
                else
                {
                    $fitbit_data = $this->soleraAuthProvider->getPatientActivitySteps($patient_details);
                }

                if (@$fitbit_data->errors[0]->errorType == 'insufficient_permissions' || @$fitbit_data->errors[0]->errorType == 'system'
                    || @$fitbit_data->errors[0]->errorType == 'invalid_token' || @$fitbit_data->errors[0]->errorType == 'insufficient_scope'
                ) {
                    continue;
                } else {
                    if (@$fitbit_data->errors[0]->errorType != 'expired_token') {
                        // To get Synced steps from our server for a patient

                        foreach ($fitbit_data as $key => $value) {
                            foreach ($value as &$values) {
                                if ($values->value != 0) {
                                    $counter += 1;
                                }
                            }
                        }

                        if (!empty($patient_synced_steps)) {
                            foreach ($fitbit_data as $key => $value) {
                                foreach ($value as &$values) {
                                    foreach ($patient_synced_steps as $steps) {
                                        if ($steps->date_format == $values->dateTime) {
                                            if ($values->value == $steps->steps) {
                                                $counter -= 1;
                                            }
                                            else
                                            {
                                               $last_mismatch_date[] = $steps->date_format;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if($counter != 0)
                        {
                            $temp = (object)[
                                'patient_id' => $patient_id->patient_id,
                                'uuid' => $patient_id->uuid,
                                'email' => $patient_id->email,
                                'total_mismatch' => $counter,
                                'mismatch_last_data' => @$last_mismatch_date[0],
                                'link' => route('fitbit-steps', ['patient_id' => $patient_id->patient_id, 'days' => 30]),
                                'link_view' => route('report.fitbit-steps-mismatch-detail', ['patient_id' => $patient_id->patient_id]),
                            ];
                            $detail->push($temp);
                        }

                    }
                }
            }
        }

        $is_download_request = $this->isTableDownloadRequest();
        if ($is_download_request && !empty($detail)) {
            foreach ($detail as $key => &$value)
            {
                unset($value->link);
            }
            return $this->dataTableService->dataTableResponse($detail, true);
        }

        $this->data['paginated_result'] = $detail;
        $this->data['init_datatable'] = true;
        $this->data['test_patient_excluded_note'] = true;
        $this->data['result_headers'] = (object) [
            'patient_id'        => ['name' => "Patient Id"],
            'email'             => ['name' => "Email"],
            'uuid'              => ['name' => "UUID"],
            'total_mismatch'    => ['name' => "Total Mismatch Records"],
            'mismatch_last_data'=> ['name' => "Last Mismatch Date"],
            'link'              => ['name' => "View Steps"],
            'link_view'         => ['name' => "View Hourly Data"],
        ];
        $response = [
            'start_date'  => '',
            'end_date'  => '',
            'html' => view( 'report.blocks.common-duration-report', $this->data )->render(),
            'page_description' => 'Note: We are comparing Fitbit last '.$days.' days with our synced up database.',
            'success' => true,
            'total_records' => count($detail)
        ];

        return response()->json( $response );
    }



    /**
     * search patient by id
     * @return \Illuminate\Http\JsonResponse
     */
    public function fitbitStepsMismatchGet(PatientAppServiceProvider $patientAppServiceProvider){
        ini_set("memory_limit",-1);
        ini_set('max_execution_time', 0);
        //Get patient details form Patient Table
        $p_detail = $this->healthslate_model->get_patient_detail_using_id();
        $patient_detail = collect($this->healthslate_model->get_patient_detail_from_device_db());
        $steps_details  = collect($this->healthslate_model->get_patient_steps_from_activity_log_summary());

        $detail = collect();

        foreach ($p_detail as $key => $patient_id)
        {
            $counter = 0;
            $patient_uuid_exist = $patient_detail->contains('patient_uuid',$patient_id->uuid);

            if($patient_uuid_exist)
            {
                $patient_details        = $patient_detail->where('patient_uuid', $patient_id->uuid)->first();
                $patient_synced_steps   = $steps_details->where('patient_id', $patient_id->patient_id);
                // To get fitbit steps from Fitbit server for a patient
                $fitbit_data = $this->soleraAuthProvider->getPatientActivitySteps($patient_details);

                if(@$fitbit_data->errors[0]->errorType == 'insufficient_permissions' || @$fitbit_data->errors[0]->errorType == 'system'
                    || @$fitbit_data->errors[0]->errorType == 'invalid_token' || @$fitbit_data->errors[0]->errorType == 'insufficient_scope')
                {
                    continue;
                }
                else{
                    if(@$fitbit_data->errors[0]->errorType != 'expired_token') {
                        // To get Synced steps from our server for a patient

                        foreach ($fitbit_data as $key => $value) {
                            foreach ($value as &$values) {
                                if($values->value != 0)
                                {
                                    $counter += 1;
                                }
                            }
                        }

                        if (!empty($patient_synced_steps)) {
                            foreach ($fitbit_data as $key => $value) {
                                foreach ($value as &$values) {
                                    foreach ($patient_synced_steps as $steps) {
                                        if ($steps->date_format == $values->dateTime) {
                                            if($values->value == $steps->steps)
                                            {
                                                $counter -=  1;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        $temp = (object) [
                            'patient_id' => $patient_id->patient_id,
                            'email' => $patient_id->email,
                            'uuid' => $patient_id->uuid,
                            'total_mismatch' => $counter,
                        ];
                        $detail->push($temp);
                    }
                }
            }
        }
        echo json_encode($detail);
    }




    /**
     * @param $facility_id
     *
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function fitbitStepsMismatchReportDetail() {

        $this->data['css'] = ['datetime', 'select2'];
        $this->data['js'] = ['datetime', 'select2'];

        $this->data['show_duration_filter'] = true;
        $this->data['hide_duration_filter_dropdown'] = false;

        $this->data['report_src'] = route('ajax.report.fitbit-steps-mismatch-detail' );

        $this->data['report_meta'] = (object) [
            'title'      => 'Fitbit Steps Mismatch Detail',
            'body_class' => 'weekly-participation'
        ];

        return view( 'report.common-duration-report', $this->data );
    }



    /**
     * search patient by id
     * @return \Illuminate\Http\JsonResponse
     */
    public function fitbitStepsMismatchDetail(PatientAppServiceProvider $patientAppServiceProvider){
        ini_set("memory_limit", -1);
        ini_set('max_execution_time', 0);
        //Get patient details form Patient Table
        $p_detail = $this->healthslate_model->get_patient_detail_using_id(request('patient_id'));
        $patient_detail = collect($this->healthslate_model->get_patient_detail_from_device_db());
        $steps_details = collect($this->healthslate_model->get_patient_steps_from_misfit_activity_log_summary($p_detail));

        $detail = collect();

        foreach ($p_detail as $key => $patient_id) {
            $counter = 0;
            $patient_uuid_exist = $patient_detail->contains('patient_uuid', $patient_id->uuid);

            if ($patient_uuid_exist) {
                $patient_details = $patient_detail->where('patient_uuid', $patient_id->uuid)->first();
                $patient_synced_steps = $steps_details->where('patient_id', $patient_id->patient_id);
                // To get fitbit steps from Fitbit server for a patient
                $fitbit_data = $this->soleraAuthProvider->getPatientActivitySteps($patient_details,7);

                if (@$fitbit_data->errors[0]->errorType == 'insufficient_permissions' || @$fitbit_data->errors[0]->errorType == 'system'
                    || @$fitbit_data->errors[0]->errorType == 'invalid_token' || @$fitbit_data->errors[0]->errorType == 'insufficient_scope'
                ) {
                    continue;
                } else {
                    if (@$fitbit_data->errors[0]->errorType != 'expired_token') {
                        // To get Synced steps from our server for a patient

                        foreach ($fitbit_data as $key => $value) {
                            foreach ($value as &$values) {

                                if (!empty($patient_synced_steps)) {

                                    $patient_steps = $patient_synced_steps->contains('date', $values->dateTime);
                                    if ($patient_steps) {
                                        foreach ($patient_synced_steps as $steps) {
                                            if ($steps->date == $values->dateTime) {
                                                $temp = (object)[
                                                    'patient_id'        => $patient_id->patient_id,
                                                    'fitbit'            => $values->dateTime . ' ' . '(' . $values->value .')',
                                                    'synced'            => $steps->date_format.':00:00' . ' ' . '(' . $steps->steps .')',
                                                ];
                                                $detail->push($temp);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        $temp = (object)[
                                            'patient_id'        => $patient_id->patient_id,
                                            'fitbit'            => $values->dateTime . ' ' . '(' . $values->value .')',
                                            'synced'            => '',
                                        ];
                                        $detail->push($temp);
                                    }

                                }
                                else
                                {
                                    $temp = (object)[
                                        'patient_id'        => $patient_id->patient_id,
                                        'fitbit'            => $values->dateTime . ' ' . '(' . $values->value .')',
                                        'synced'            => '',
                                    ];
                                    $detail->push($temp);
                                }

                            }
                        }



                    }
                }
            }
        }

        $is_download_request = $this->isTableDownloadRequest();
        if ($is_download_request && !empty($detail)) {
            foreach ($detail as $key => &$value)
            {
                unset($value->link);
            }
            return $this->dataTableService->dataTableResponse($detail, true);
        }

        $this->data['paginated_result'] = $detail;
        $this->data['init_datatable'] = true;
        $this->data['test_patient_excluded_note'] = true;
        $this->data['result_headers'] = (object) [
            'patient_id'        => ['name' => "Patient Id"],
            'fitbit'        => ['name' => "Fitbit Steps"],
            'synced'            => ['name' => "Synced Steps DateTime"],
        ];
        $response = [
            'start_date'  => '',
            'end_date'  => '',
            'html' => view( 'report.blocks.common-duration-report', $this->data )->render(),
            'page_description' => 'Note: We are comparing Fitbit last 7 Day data with our synced up database.',
            'success' => true,
            'total_records' => count($detail)
        ];

        return response()->json( $response );
    }



    /**
     * @param $facility_id
     *
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function fitbitStepsMismatchReportDetails() {

        $this->data['css'] = ['datetime', 'select2'];
        $this->data['js'] = ['datetime', 'select2'];

        $this->data['show_duration_filter'] = true;
        $this->data['hide_duration_filter_dropdown'] = false;

        $this->data['report_src'] = route('ajax.report.fitbit-steps-mismatch-details' );

        $this->data['report_meta'] = (object) [
            'title'      => 'Fitbit Steps Mismatch Details',
            'body_class' => 'weekly-participation'
        ];

        return view( 'report.common-duration-report', $this->data );
    }




    /**
     * search patient by id
     * @return \Illuminate\Http\JsonResponse
     */
    public function fitbitStepsMismatchDetails(PatientAppServiceProvider $patientAppServiceProvider){
        ini_set("memory_limit", -1);
        ini_set('max_execution_time', 0);
        $date = request( 'days', '2017-08-10' );
        //Get patient details form Patient Table
        $p_detail = $this->healthslate_model->get_patient_detail_using_id(request('patient_id'));
        $patient_detail = collect($this->healthslate_model->get_patient_detail_from_device_db());
        $steps_details = collect($this->healthslate_model->get_patient_steps_from_misfit_activity_log_summary_signle_days($p_detail));
        $detail = collect();
        $p_data = collect();
        foreach ($p_detail as $key => $patient_id) {
            $counter = 0;
            $patient_uuid_exist = $patient_detail->contains('patient_uuid', $patient_id->uuid);

            if ($patient_uuid_exist) {
                $patient_details = $patient_detail->where('patient_uuid', $patient_id->uuid)->first();
                $patient_synced_steps = $steps_details->where('patient_id', $patient_id->patient_id);
                $patient_synced_steps = $patient_synced_steps->where('date', $date);
                // To get fitbit steps from Fitbit server for a patient
                $fitbit_data = $this->soleraAuthProvider->getPatientActivityStepsSignleDays($patient_details, $date);

                if (@$fitbit_data->errors[0]->errorType == 'insufficient_permissions' || @$fitbit_data->errors[0]->errorType == 'system'
                    || @$fitbit_data->errors[0]->errorType == 'invalid_token' || @$fitbit_data->errors[0]->errorType == 'insufficient_scope'
                ) {
                    continue;
                } else {
                    if (@$fitbit_data->errors[0]->errorType != 'expired_token') {

                        foreach ($fitbit_data as $key => $value) {
                            if($key == 'activities-steps')
                            {
                                foreach ($value as &$values) {
                                        $temp = (object)[
                                            'time'              => $values->dateTime,
                                            'fitbit'            => $values->value,
                                            'synced'            => $patient_synced_steps->sum('steps')
                                        ];
                                }
                                $p_data->push($temp);
                            }
                            else
                            {
                                foreach ($value->dataset as &$values) {
                                    if($values->value != 0)
                                    {
                                        $p_steps = $patient_synced_steps->where('datetime', $values->time)->first();
                                        $temp = (object)[
                                            'time'              => $values->time,
                                            'fitbit'            => $values->value,
                                            'synced'            => isset($p_steps) ? $p_steps->steps : ''
                                        ];
                                        $p_data->push($temp);
                                    }
                                }
                            }
                        }

                        if(isset($p_data))
                        {
                            foreach ($p_data as $values) {
                                $temp = (object)[
                                    'patient_id'        => $patient_id->patient_id,
                                    'time'              => $values->time,
                                    'fitbit'            => $values->fitbit,
                                    'synced'            => $values->synced,
                                ];
                                $detail->push($temp);
                            }
                        }

                    }
                }
            }
        }

        $is_download_request = $this->isTableDownloadRequest();
        if ($is_download_request && !empty($detail)) {
            return $this->dataTableService->dataTableResponse($detail, true);
        }

        $this->data['paginated_result'] = $detail;
        $this->data['init_datatable'] = true;
        $this->data['test_patient_excluded_note'] = true;
        $this->data['result_headers'] = (object) [
            'patient_id'        => ['name' => "Patient Id"],
            'time'              => ['name' => "DateTime"],
            'fitbit'            => ['name' => "Fitbit Steps"],
            'synced'            => ['name' => "Synced Steps"],
        ];
        $response = [
            'start_date'  => '',
            'end_date'  => '',
            'html' => view( 'report.blocks.common-duration-report', $this->data )->render(),
            'page_description' => 'Note: We are comparing Fitbit last 1 Day data with our synced up database.',
            'success' => true,
            'total_records' => count($detail)
        ];

        return response()->json( $response );
    }



}// EOF