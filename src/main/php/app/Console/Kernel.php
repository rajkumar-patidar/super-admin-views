<?php

namespace HealthSlateAdmin\Console;

use HealthSlateAdmin\Console\Commands\CleanCacheDirectory;
use HealthSlateAdmin\Console\Commands\EnrollSoleraMembers;
use HealthSlateAdmin\Console\Commands\EnrollmentDataToSolera;
use HealthSlateAdmin\Console\Commands\EngagementDataToSolera;
use Illuminate\Console\Scheduling\Schedule;
use Symfony\Component\Console\Input\ArgvInput;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {
	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		CleanCacheDirectory::class,
		EnrollSoleraMembers::class,
		EnrollmentDataToSolera::class,
		EngagementDataToSolera::class
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule $schedule
	 * @return void
	 */
	protected function schedule( Schedule $schedule ) {

		$current_command = (new ArgvInput())->getFirstArgument();
		if($current_command  == "schedule:run") {
			// preventing for DB connection requirement on jenkins
			app( 'HealthSlateAdmin\Http\Controllers\ReportSchedule' )->runScheduleAlerts( $schedule );

			// Clean CSV Export files.
			$schedule->command('cache:export-cache-clean')->daily();

			// To enroll new patinets from Solera api
			//$schedule->command('solera:enroll-solera-member --facilityId=200 --diabetesTypeId=3')->everyMinute();

			// To send engagement report to Solera
			//$schedule->command('solera:engagement-data-to-solera  --facilityId=26)->daily();

			// To send enroll disenroll details of the patient to Solera
			//$schedule->command('solera:enrollment-data-to-solera  --facilityId=26)->daily();
		}
	}
}
