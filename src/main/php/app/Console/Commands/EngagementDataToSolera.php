<?php

namespace HealthSlateAdmin\Console\Commands;

use Illuminate\Console\Command;
use Validator;

class EngagementDataToSolera extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'solera:engagement-data-to-solera {--facilityId= : Enter Facility ID}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Sending Engagement report data to Solera for each patient.';


	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	function __construct( ) {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$data = array();
		$data['facilityId'] = $this->option('facilityId');

		$row_validation = Validator::make($data, [
			'facilityId' => 'required|numeric'
		]);

		if ($row_validation->fails()) {
			$this->error('Something went wrong!');
			foreach ($row_validation->errors()->all('<p>:message</p>') as $message) {
				$this->error($message);
			}
			die;
		}

		$this->info('facilityId: ' . $data['facilityId']);
		$this->info('Calling access token api...');

		app( 'HealthSlateAdmin\Http\Controllers\v2\SoleraApiIntegration' )->engagementDataToSolera($data);
	}
}
