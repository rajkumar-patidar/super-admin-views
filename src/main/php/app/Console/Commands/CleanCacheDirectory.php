<?php
/**
 * User: spatel
 * Date: 1/02/17
 */
namespace HealthSlateAdmin\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;


class CleanCacheDirectory extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'cache:export-cache-clean';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Clean all export files from storage folder older than a week';

	/**
	 * @var Filesystem
	 */
	protected $filesystem;
	/**
	 * @var string
	 */
	protected $directory;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct( Filesystem $filesystem )
	{
		parent::__construct();
		$this->filesystem = $filesystem;
		$this->directory  = config( 'excel.export.store.path', storage_path( 'exports' ) );
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		if ($this->filesystem->exists( $this->directory )) {
			info( 'Cleaning files in folder ' . $this->directory );
			$past_date = Carbon::now()->subDays( 7 );
			info( 'Cleaning files older than', [ $past_date ] );
			$files = $this->deleteFilesOlderThan( $past_date );
			info( 'Removed file, count', [ $files->count() ] );
			return $files;
		}

	}

	/**
	 * @param Carbon $timeInPast
	 *
	 * @return \Illuminate\Support\Collection
	 */
	public function deleteFilesOlderThan( Carbon $timeInPast )
	{
		return collect( $this->filesystem->files( $this->directory ) )
			->filter( function ( $file ) use ( $timeInPast ) {
				// If file create time is less than $timeInPast
				return Carbon::createFromTimestamp( filemtime( $file ) )
				             ->lt( $timeInPast );
			} )
			->each( function ( $file ) {
				$this->filesystem->delete( $file );
			} );
	}
}
