<?php

namespace HealthSlateAdmin\Console\Commands;

use Illuminate\Console\Command;
use Validator;

class EnrollSoleraMembers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'solera:enroll-solera-members
        {--facilityId= : Enter Facility ID}
        {--diabetesTypeId= : Enter Patient Diabetes Type  1:TYPE2, 2:DPP, 3:DPP-CALORIES }
        {--LifestyleCoachId= : Enter Lifestyle Coach Id ID }
        {--FoodCoachId= : Enter Food Coach ID }
        ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get a list of Patients from Solera and Register into our system. It will check daily';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = array();
        $data['facilityId']         = $this->option('facilityId');
        $data['diabetesTypeId']     = $this->option('diabetesTypeId');
        $data['lifestyleCoachId']   = $this->option('LifestyleCoachId');
        $data['foodCoachId']        = $this->option('FoodCoachId');

        $row_validation = Validator::make($data, [
            'facilityId' => 'required|numeric',
            'diabetesTypeId' => 'required|numeric',
        ]);

        if ($row_validation->fails()) {
            $this->error('Something went wrong!');
            foreach ($row_validation->errors()->all('<p>:message</p>') as $message) {
                $this->error($message);
            }
            die;
        }

        $this->info('facilityId: ' . $data['facilityId']);
        $this->info('diabetesTypeId: ' . $data['diabetesTypeId']);
        $this->info('Calling access token api...');

        app( 'HealthSlateAdmin\Http\Controllers\v2\SoleraApiIntegration' )->index($data);

    }



}
