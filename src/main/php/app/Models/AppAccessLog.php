<?php

namespace HealthSlateAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class AppAccessLog extends Model {
	protected $table = 'app_access_log';
	protected $primaryKey = 'access_log_id';
	public $timestamps = false;
}
