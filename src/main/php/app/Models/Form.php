<?php

namespace HealthSlateAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class Form extends Model {
	protected $table = 'form';
	protected $primaryKey = 'form_id';
	public $timestamps = false;
	public $incrementing = false;
}
