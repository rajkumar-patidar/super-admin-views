<?php

namespace HealthSlateAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class FormSchedule extends Model {

	protected $table = 'form_schedule';
	protected $primaryKey = 'id';
	public $timestamps = false;
}
