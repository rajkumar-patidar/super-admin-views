<?php

namespace HealthSlateAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class PatientMedication extends Model {

	protected $table = 'patient_medication';
	protected $primaryKey = 'patient_medication_id';
	public $timestamps = false;
}
