<?php

namespace HealthSlateAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class FilledForm extends Model {
	protected $table = 'filled_form';
	protected $primaryKey = 'filled_form_id';
	public $timestamps = false;
}
