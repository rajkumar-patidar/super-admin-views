<?php

namespace HealthSlateAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class CoachGoalAssessment extends Model {
	protected $table = 'coach_goal_assessment';
	protected $primaryKey = 'coach_goal_assessment_id';
	public $timestamps = false;
}
