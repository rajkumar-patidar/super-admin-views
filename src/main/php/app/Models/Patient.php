<?php

namespace HealthSlateAdmin\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Patient extends Model {

	protected $table = 'patient';
	protected $primaryKey = 'patient_id';
	public $timestamps = false;

	public function getReadableDobAttribute() {
		return empty($this->dob) ? "" : Carbon::createFromFormat('Y-m-d H:i:s', $this->dob)->format('M, d Y');
	}

	/**
	 * Filter Deleted Patients
	 * @param $query
	 * @param bool $is_deleted
	 *
	 * @return mixed
	 */
	public function scopeIsDeleted($query, $is_deleted = true){
		if($is_deleted){
			return $query->whereRaw($this->getConnection()->raw('ORD(is_deleted) = 1'));
		}
		return $query->whereRaw($this->getConnection()->raw('(ORD(is_deleted) = 0 OR is_deleted IS NULL)'));
	}

	public function user() {
		return $this->belongsTo( 'HealthSlateAdmin\Models\User', 'user_id', 'user_id' );
	}

	public function leadCoach() {
		return $this->hasOne( 'HealthSlateAdmin\Models\Provider', 'provider_id', 'lead_coach_id' )->whereNotNull( 'provider_id' );
	}

	public function primaryFoodCoach() {
		return $this->hasOne( 'HealthSlateAdmin\Models\Provider', 'provider_id', 'primary_food_coach_id' )->whereNotNull( 'provider_id' );
	}

	public function cohort() {
		return $this->hasOne( 'HealthSlateAdmin\Models\Cohort', 'cohort_id', 'cohort_id' )->whereNotNull( 'cohort_id' );
	}

	public function diabetesType() {
		return $this->hasOne( 'HealthSlateAdmin\Models\DiabetesType', 'diabetes_type_id', 'diabetes_type_id' )->whereNotNull( 'diabetes_type_id' );
	}

	public function patientDiabetesInfo() {
		return $this->hasOne( 'HealthSlateAdmin\Models\PatientDiabetesInfo', 'patient_id', 'patient_id' );
	}

	public function insulinType() {
		return $this->hasOne( 'HealthSlateAdmin\Models\InsulinType', 'insulin_type_id', 'insulin_type_id' )->whereNotNull( 'insulin_type_id' );
	}

	public function facilities() {
		//return $this->belongsToMany( 'HealthSlateAdmin\Models\Facility', 'patient_facility', 'patients_patient_id', 'facilities_facility_id' );
		return $this->hasMany( 'HealthSlateAdmin\Models\Facility', 'facility_id', 'facility_id' );
	}

	public function providers() {
		return $this->belongsToMany( 'HealthSlateAdmin\Models\Provider', 'provider_patient', 'patients_patient_id', 'providers_provider_id' );
	}

	// This table is delete in recent db.
	/*public function techSupportMessages() {
		return $this->hasMany( 'HealthSlateAdmin\Models\TechSupportMessage', 'patient_id' ,'patient_id')->whereNotNull('patient_id');
	}*/

	public function filledForms() {
		return $this->hasMany( 'HealthSlateAdmin\Models\FilledForm', 'patient_id', 'patient_id' )->whereNotNull( 'patient_id' );
	}

	public function logs() {
		return $this->hasMany( 'HealthSlateAdmin\Models\Log', 'patient_id', 'patient_id' )->whereNotNull( 'patient_id' );
	}

	public function patientMedications() {
		return $this->hasMany( 'HealthSlateAdmin\Models\PatientMedication', 'patient_id', 'patient_id' )->whereNotNull( 'patient_id' );
	}

	public function target() {
		return $this->hasOne( 'HealthSlateAdmin\Models\Target', 'patient_id', 'patient_id' )->whereNotNull( 'patient_id' );
	}

	// It is storing messages b/w patient and user_id (a provider).
	public function messages() {
		return $this->hasMany( 'HealthSlateAdmin\Models\Message', 'patient_id', 'patient_id' )->whereNotNull( 'patient_id' );
	}

	public function feedbacks() {
		return $this->hasMany( 'HealthSlateAdmin\Models\Feedback', 'patient_id', 'patient_id' );
	}

	public function oneToOneSessions() {
		return $this->hasMany( 'HealthSlateAdmin\Models\OneToOneSession', 'patient_id', 'patient_id' );
	}

	public function patientEatingPreferences() {
		return $this->hasMany( 'HealthSlateAdmin\Models\PatientEatingPreference', 'patient_id', 'patient_id' );
	}

	public function patientSessionPreferences() {
		return $this->hasMany( 'HealthSlateAdmin\Models\PatientSessionPreference', 'patient_id', 'patient_id' );
	}

	public function patientMotivationImages() {
		return $this->hasMany( 'HealthSlateAdmin\Models\PatientMotivationImage', 'patient_id', 'patient_id' );
	}

	public function serviceFilterLogs() {
		return $this->hasMany( 'HealthSlateAdmin\Models\ServiceFilterLog', 'patient_id', 'patient_id' );
	}

	public function smsReplies() {
		return $this->hasMany( 'HealthSlateAdmin\Models\SmsReply', 'patient_id', 'patient_id' );
	}

	public function device() {
		return $this->hasOne( 'HealthSlateAdmin\Models\Device', 'device_mac_address', 'device_mac_address' )->whereNotNull( 'device_mac_address' );
	}

	public function foodMasters() {
		return $this->hasMany( 'HealthSlateAdmin\Models\FoodMaster', 'patient_id', 'patient_id' )->whereNotNull( 'patient_id' );
	}
	// hasMany or hasOne care_giver ?
	// no need to map coach_notes with patient?
	// patient_forms ?
}