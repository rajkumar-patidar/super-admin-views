<?php

namespace HealthSlateAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class PatientEatingPreference extends Model {

	protected $table = 'patient_eating_preference';
	protected $primaryKey = 'session_id';
	public $timestamps = false;

	public function patient() {
		return $this->belongsTo( 'HealthSlateAdmin\Models\Patient', 'patient_id', 'patient_id' );
	}
}
