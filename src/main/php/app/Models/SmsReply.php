<?php

namespace HealthSlateAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class SmsReply extends Model {

	protected $table = 'sms_reply';
	protected $primaryKey = 'sms_reply_id';
	public $timestamps = false;

	public function patient() {
		return $this->belongsTo( 'HealthSlateAdmin\Models\Patient', 'patient_id', 'patient_id' );
	}
}
