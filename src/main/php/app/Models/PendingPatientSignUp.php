<?php

namespace HealthSlateAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class PendingPatientSignUp extends Model {

	protected $table = 'pending_patient_sign_up';

	protected $primaryKey = 'solera_id';

	public $timestamps = false;

	public function diabetesType() {
		return $this->hasOne( 'HealthSlateAdmin\Models\DiabetesType', 'diabetes_type_id', 'diabetes_type_id' );
	}

	public function facility() {
		return $this->hasOne( 'HealthSlateAdmin\Models\Facility', 'facility_id', 'facility_id' );
	}
}
