<?php

namespace HealthSlateAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class FoodLogSummary extends Model {

	protected $table = 'food_log_summary';
	protected $primaryKey = 'food_log_summary_id';
	public $timestamps = false;

}
