<?php

namespace HealthSlateAdmin\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Message extends Model {

	protected $table = 'message';
	protected $primaryKey = 'message_id';
	public $timestamps = false;

	// user_id is storing provider's user_id.
	public function providerUser() {
		return $this->belongsTo( 'HealthSlateAdmin\Models\User', 'user_id', 'user_id' );
	}

	public function getIsSentAttribute() {
		return ($this->owner == 'PATIENT') ? true : false;
	}

	public function getSentOnAttribute() {
		if(empty($this->timestamp)){
			return "";
		}
		return Carbon::createFromTimestamp($this->timestamp/1000)->format('M, d Y h:i A');
	}

	public function getReadedAttribute() {
		return (ord($this->read_status)) ? true : false;
	}

	public function patient() {
		return $this->belongsTo( 'HealthSlateAdmin\Models\Patient', 'patient_id', 'patient_id' );
	}
}
