<?php

namespace HealthSlateAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class GlucoseLog extends Model {

	protected $table = 'glucose_log';
	protected $primaryKey = 'glucose_log_id';
	public $timestamps = false;
}
