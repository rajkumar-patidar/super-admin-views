<?php
/**
 * User: spatel
 * Date: 19/1/16
 */

namespace HealthSlateAdmin\Models;

use Carbon\Carbon;
use DB;

class Healthslate {

	/**
	 * @var \Illuminate\Database\Connection
	 */
	protected $connection;

	/**
	 * @var array
	 */
	protected $coach_provider_types = [ 'Coach', 'Food Coach' ];
	/**
	 *
	 */
	function __construct() {
		$this->connection = DB::connection();
	}

	/**
	 * @param $query
	 * @param array $bindings
	 *
	 * @return array
	 */
	public function get_query_result( $query, $bindings = [ ] ) {
		return $this->connection->select( $query, $bindings );
	}

	/**
	 * @param $report_name
	 *
	 * @return bool|string
	 */
	public function get_query_for_report( $report_name ) {
		$reports = [

			'goneInactive'                     => "SELECT l.patient_id, concat(pu.first_name, ' ' ,pu.last_name) as patient_name,
													from_unixtime(max(l.log_time)/1000) as last_time,
													datediff(now(),from_unixtime(l.log_time/1000)) as last_log_days ,
													pr.provider_id, concat(pru.first_name, ' ' ,pru.last_name) as provider_name,
													pr.facility_id, pru.user_id as provider_user_id, p.mrn
													FROM log as l

													left join patient as p ON p.patient_id = l.patient_id
													left join users as pu ON pu.user_id = p.user_id

													left join provider_patient as pp ON pp.patients_patient_id = l.patient_id
													left join provider as pr ON pr.provider_id = pp.providers_provider_id
													left join users as pru ON pru.user_id = pr.user_id

													group by l.patient_id having datediff(now(),last_time)>7 order by log_time desc",

			'feedbackMessages'                 => "SELECT f.feedback_id, f.patient_id, f.description, FROM_UNIXTIME( f.timestamp /1000 ) AS time, f.provider_id, f.type
													FROM feedback f
												 	left join patient as p ON p.patient_id = f.patient_id
													order by f.timestamp desc",

			"patientWithoutLeadCoach"          => "select patient_id,days,users.email as lifestyle_coach
													from(
														SELECT u.email,p.patient_id,lead_coach_id,datediff(now(),u.registration_date) as days
														FROM users as u, patient as p
														left join one_to_one_sessions as o on p.patient_id=o.patient_id and p.lead_coach_id=o.provider_id
														where u.user_id=p.user_id AND u.email not like '\\_%' AND p.is_deleted is null
														AND p.is_consent_accepted=1 AND u.password is not null AND datediff(now(),u.registration_date)>7
														and p.mrn not like '%test%'
														and session_id is null
														) t1,
													users, provider
													where provider.provider_id=lead_coach_id AND users.user_id=provider.user_id
													ORDER by days asc",

			"patientWithoutFoodCoach"          => "select patient_id,days,primary_food_coach_id,users.email as coach
													from (
														SELECT u.email,p.patient_id,p.primary_food_coach_id,datediff(now(),u.registration_date) as days
														FROM users as u, patient as p
														left join one_to_one_sessions as o on p.patient_id=o.patient_id and p.primary_food_coach_id=o.provider_id
														where u.user_id=p.user_id AND u.email not like '\\_%' AND p.is_deleted is null
														AND p.is_consent_accepted=1 AND u.password is not null AND datediff(now(),u.registration_date)>7
														and p.is_deleted is null and session_id is null
														) t1,
													users, provider
													where provider.provider_id=primary_food_coach_id
													AND users.user_id=provider.user_id
													ORDER by days",

			"unProcessedMealsOver6hrs"         => "SELECT count(*) as count,sum(is_processed) as processed,
													sum(case when is_processed=0 then 1 else 0 end) as unprocessed
													FROM log
												 	where log_type='meal'
													AND timestampdiff(hour,from_unixtime(last_modified/1000),now()) >6
													AND datediff(now(),from_unixtime(last_modified/1000))<1 ",

			"unProcessedMealsOver4d"           => "SELECT count(*) as count, sum(is_processed) as processed,
													sum(case when is_processed=0 then 1 else 0 end) as unprocessed
													FROM log where log_type='meal'
													AND datediff(now(),from_unixtime(last_modified/1000))<4",

			"hourlyMealProcessingInDuration3d" => "SELECT timestampdiff(hour,from_unixtime(last_modified/1000),now()) as h, count(*) as count,
													sum(is_processed) as processed, sum(case when is_processed=0 then 1 else 0 end) as unprocessed
													FROM log where log_type='meal'
													group by h having h<72",

			"unRepliedMessages"                => "select message as message,
													m.patient_id,u.email as to_email,from_unixtime(m.timestamp/1000) as datetime,
													datediff(now(),from_unixtime(m.timestamp/1000)) days_ago
													from message m JOIN (
																		SELECT patient_id,max(timestamp) as ts FROM message group by patient_id
													) as t ON m.patient_id=t.patient_id AND t.ts=m.timestamp AND m.owner='patient'
													JOIN users u on u.user_id=m.user_id order by m.timestamp desc ",

			"patientSurveyData"                => "select email,form_name,form.form_id,patient.mrn,patient.patient_id,
													whentoshow,data,patient_forms.scheduled_form_id
													from patient_forms,form_schedule,form,patient,users
													where patient_forms.scheduled_form_id=form_schedule.scheduled_form_id
													AND form_schedule.form_id=form.form_id and patient.uuid=patient_forms.patient_id
													and users.user_id=patient.user_id and patient.mrn not like '%test%'
													and patient.is_deleted is null order by email,whentoshow asc",
		];

		return ( isset( $reports[ $report_name ] ) ? $reports[ $report_name ] : false );

	}

	/**
	 * get_enrollment_report
	 *  get patient enrollment report for a duration of invitation sent
	 * @param null $duration
	 * @param string $unit
	 *
	 * @return array|static[]
	 */
	public function get_enrollment_report($duration = null, $unit = 'day') {
		$query = $this->connection
			->table( 'patient AS p' )
			->select( [
				'f.facility_id',
				'f.name AS facility_name',
				// Total Invitations Sent
				$this->connection->raw( "
					count( DISTINCT
					    IF(
					        ".$this->get_patient_status_where_query('invited_patient')."
					        ,p.patient_id,null
					    )
					) as invited_patient"
				),
				// Invitations Sent, But Not Yet accepted TOS and NOT Yet Registered
				$this->connection->raw( "
					count( DISTINCT
					    IF(
					        ".$this->get_patient_status_where_query('invitation_acceptance_pending')."
					        ,p.patient_id,null
					    )
					) as invitation_acceptance_pending"
				),
				// accepted_invitation but, not yet downloaded app / not registered
				$this->connection->raw( "
					count( DISTINCT
					    IF(
					        ".$this->get_patient_status_where_query('accepted_invitation_not_registered')."
					        ,p.patient_id,null
					    )
					) as accepted_invitation_not_registered"
				),
				// accepted_invitation & registered
				$this->connection->raw( "
					count( DISTINCT
					    IF(
					        ".$this->get_patient_status_where_query('registered')."
					        ,p.patient_id,null
					    )
					) as registered"
				),
				// registered but no activity
				$this->connection->raw( "
					count( DISTINCT
					    IF(
					        ".$this->get_patient_status_where_query('registered_no_activity')."
					        ,p.patient_id,null
					    )
					) as registered_no_activity"
				),
				// performed activity after registration
				$this->connection->raw( "
					count( DISTINCT
					    IF(
					        ".$this->get_patient_status_where_query('performed_activity')."
					        ,p.patient_id,null
					    )
					) as performed_activity"
				)
			] )
			->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
			->leftJoin( 'facility AS f', 'f.facility_id', '=', 'p.facility_id' )
			->leftJoin( 'log AS l', function ( $query ) {
				$query->on( 'l.patient_id', '=', 'p.patient_id' )
					->whereNotNull( 'l.log_time' );
			} )
			->where( 'u.email', 'not like', '\\_%' )
			->whereNotNull( 'p.facility_id' );


		if($duration){
			$query->whereRaw($this->get_patient_days_where_raw($unit),['duration' => $duration]);
		}
		return $query->groupBy( 'p.facility_id' )->get();
	}

	/**
	 * @param string $unit
	 *
	 * @return \Illuminate\Database\Query\Expression
	 */
	public function get_patient_days_where_raw($unit = 'day'){
		return $this->connection->raw('( timestampdiff( '.$unit.', from_unixtime(p.invitation_sent_date/1000),curdate()) < ? )');
	}

	/**
	 * get patient where_clause_query for a status
	 *
	 * @param $status
	 *
	 * @return string
	 */
	public function get_patient_status_where_query( $status ) {
		if ( $status == 'invited_patient' ) {
			return "(
					            -- is approved from coach admin
					         p.is_approved = '3' AND
					            -- is invited patient
					         ORD(p.is_invited) = 1
					        )";
		} else if ( $status == 'invitation_acceptance_pending' ) {
			return "(
					            -- is approved from coach admin
					         p.is_approved = '3' AND
					            -- is invited patient
					         ORD(p.is_invited) = 1 AND
					            -- not yet accepted invitation (null,0)
					         ( (p.is_consent_accepted IS NULL) OR ORD(p.is_consent_accepted) = 0 ) AND
					            -- not yet registered (null,0)
					         ( (u.is_registration_completed IS NULL) OR ORD(u.is_registration_completed) = 0 )
					        )";
		} else if ( $status == 'accepted_invitation_not_registered' ) {
			return "(
					            -- is approved from coach admin
					         p.is_approved = '3' AND
					            -- is invited patient
					         ORD(p.is_invited) = 1 AND
					            -- accepted invitation
					         ORD(p.is_consent_accepted) = 1 AND
					            -- not yet registered (null,0)
					         ( (u.is_registration_completed IS NULL) OR ORD(u.is_registration_completed) = 0 )
					        )";
		} else if ( $status == 'registered' ) {
			return "(
					            -- is approved from coach admin
					         p.is_approved = '3' AND
					            -- is invited patient
					         ORD(p.is_invited) = 1 AND
					            -- accepted invitation
					         ORD(p.is_consent_accepted) = 1 AND
					            -- registered
					         ORD(u.is_registration_completed) = 1 AND
					            -- is enabled for oauth
					         ORD(u.is_enabled) = 1 AND ( (p.is_deleted IS NULL) OR ORD(p.is_deleted) = 0 )
					        )";
		} else if ( $status == 'registered_no_activity' ) {
			return "(
					            -- is approved from coach admin
					         p.is_approved = '3' AND
					            -- is invited patient
					         ORD(p.is_invited) = 1 AND
					            -- accepted invitation
					         ORD(p.is_consent_accepted) = 1 AND
					            -- registered
					         ORD(u.is_registration_completed) = 1 AND
					            -- is enabled for oauth
					         ORD(u.is_enabled) = 1 AND ( (p.is_deleted IS NULL) OR ORD(p.is_deleted) = 0 ) AND
					         -- has no log
					         (l.log_id is null)
					        )";
		} else if ( $status == 'performed_activity' ) {
			return "(
					            -- is approved from coach admin
					         p.is_approved = '3' AND
					            -- is invited patient
					         ORD(p.is_invited) = 1 AND
					            -- accepted invitation
					         ORD(p.is_consent_accepted) = 1 AND
					            -- registered
					         ORD(u.is_registration_completed) = 1 AND
					            -- is enabled for oauth
					         ORD(u.is_enabled) = 1 AND ( (p.is_deleted IS NULL) OR ORD(p.is_deleted) = 0 ) AND
					            -- has log
					         (l.log_id is not null)
					        )";
		}
	}

	/**
	 * get_facility_patients
	 *
	 * @param $facility_id
	 * @param null $statusFilter
	 * @param int $daysFilter
	 *
	 * @return array|static[]
	 */
	public function get_facility_patients( $facility_id, $statusFilter = null, $daysFilter = 0 ) {
		$query = $this->connection
			->table( 'patient AS p' )
			->select( [
				'p.*',
				'u.*',
				$this->connection->raw( 'concat(u.first_name, " " ,u.last_name) as full_name' ),
				'pr.provider_id',
				'pr.facility_id AS lead_coach_facility_id',
				'pr.user_id AS lead_coach_user_id',
				'd.device_type',
				$this->connection->raw( 'concat(pru.first_name, " " ,pru.last_name) as lead_coach_full_name' ),
			] )
			->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
			->leftJoin( 'provider AS pr', 'pr.provider_id', '=', 'p.lead_coach_id' )
			->leftJoin( 'users AS pru', 'pru.user_id', '=', 'pr.user_id' )
			->leftJoin( 'device AS d', 'd.device_mac_address', '=', 'p.device_mac_address' );

		if ( $statusFilter ) {
			if($statusFilter == 'registered_no_activity' || $statusFilter == 'performed_activity'){
				$query->leftJoin( 'log AS l', function ( $q ) {
					$q->on( 'l.patient_id', '=', 'p.patient_id' )
					      ->whereNotNull( 'l.log_time' );
				} );
				$query->groupBy( 'p.patient_id' );
			}
			$query->whereRaw( $this->connection->raw( $this->get_patient_status_where_query( $statusFilter ) ) );
		}
		if ( $daysFilter ) {
			$query->whereRaw( $this->get_patient_days_where_raw(), [ 'duration' => $daysFilter ] );
		}

		return $query
			->whereRaw($this->connection->raw('(ORD(p.is_deleted) = 0 OR p.is_deleted IS NULL)'))
			// this might lead to incorrect data, so commenting it.
			//->where( 'u.email', 'not like', '\\_%' )
			->where( 'p.facility_id', '=', $facility_id )
			->orderBy( 'p.patient_id' )
			->get();
	}

	/**
	 * @param $provider_id
	 * @param int $week
	 *
	 * @return array|static[]
	 */
	public function get_provider_patients( $provider_id, $week = 1 ) {
		$last_date = null;
		if ( $week > 0 ) {
			$last_date = Carbon::today()->subWeek( $week )->addDay( 1 );
		}
		$query = $this->connection
			->table( 'patient AS p' )
			->select( [
				'p.*',
				$this->connection->raw( 'concat(u.first_name, " " ,u.last_name) as full_name' ),
				'pr.provider_id',
				'pr.facility_id AS provider_facility_id',
				'pr.user_id AS provider_user_id',
				$this->connection->raw( 'datediff(now(),max(l.log_date_time)) as last_log_days' ),
				$this->connection->raw( 'count(DISTINCT IF( (m.owner = "PROVIDER" AND m.user_id = pr.user_id),m.message_id,NULL) ) as received_msg' ),
				$this->connection->raw( 'count(DISTINCT IF( (m.owner = "PATIENT" AND m.user_id = pr.user_id),m.message_id,NULL) ) as sent_msg' ),
				$this->connection->raw( 'count(DISTINCT o2o.session_id) as one_to_one_sessions' )
			] )
			->leftJoin( 'provider_patient AS pp', 'p.patient_id', '=', 'pp.patients_patient_id' )
			->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
			->leftJoin( 'provider AS pr', 'pr.provider_id', '=', 'pp.providers_provider_id' )
			->leftJoin( 'log as l', function ( $q ) use ( $week, $last_date ) {
				$q->on( 'l.patient_id', '=', 'p.patient_id' );
				if ( $week > 0 ) {
					$q->where( 'l.log_date_time', '>', $last_date->format( 'Y-m-d H:i:s' ) );
				}
			})
			->leftJoin( 'message as m', function ( $q ) use ( $week, $last_date ) {
				$q->on( 'm.patient_id', '=', 'p.patient_id' );
				$q->on( 'm.user_id', '=', 'pr.user_id' );
				if ( $week > 0 ) {
					$q->where( 'm.timestamp', '>', $last_date->timestamp * 1000 );
				}
			} )
			->leftJoin( 'one_to_one_sessions as o2o', function ( $q ) use ( $week, $last_date ) {
				$q->on( 'o2o.patient_id', '=', 'p.patient_id' );
				$q->on( 'o2o.provider_id', '=', 'pr.provider_id' );
				if ( $week > 0 ) {
					$q->where( 'o2o.session_time', '>', $last_date->timestamp * 1000 );
				}
			} );

		return $query
			->whereRaw( $this->connection->raw( '(ORD(p.is_deleted) = 0 OR p.is_deleted IS NULL)' ) )
			// this might lead to incorrect data, so commenting it.
			//->where( 'u.email', 'not like', '\\_%' )
			->where( 'pp.providers_provider_id', '=', $provider_id )
			->groupBy( 'p.patient_id' )
			->orderBy( 'l.log_date_time', 'desc' )
			->orderBy( 'p.patient_id' )
			->get();
	}

	/**
	 * get_patient_fitbit_activity
	 * @param $facility_id
	 * @param Carbon $start
	 * @param Carbon $end
	 *
	 * @return array
	 */
	public function get_patient_fitbit_activity($facility_id, Carbon $start, Carbon $end) {
		$device_support_db_name = config('healthslate.device_support_db_name');
		$additional_patient_service_log_join = ' AND ds_psl.activity_start_date >= :activity_start_date AND ds_psl.activity_start_date < :activity_end_date  ';
		$additional_log_join = ' AND pa_l.log_date_time >= :log_date_time AND pa_l.log_date_time < :log_end_date_time ';
		$binding = [
			'facility_id' => $facility_id,
			'activity_start_date' => $start->format('Y-m-d H:i:s'),
			'activity_end_date' => $end->format('Y-m-d H:i:s'),
			'log_date_time' => $start->format('Y-m-d H:i:s'),
			'log_end_date_time' => $end->format('Y-m-d H:i:s'),
		];
		$raw_query = "SELECT pa_p.patient_id, pa_p.uuid, pa_p.mrn,
						CONCAT(pa_u.first_name, ' ' ,pa_u.last_name) AS name,
						pa_u.offset_key,
						pa_dt.name AS diabetes_type,

						IF(ds_pd.patient_device_id IS NOT NULL, '1',NULL) as is_tracker_linked,
						-- Date Tracker Linked is not available yet
						DATE_FORMAT(MAX(ds_psl.activity_start_date),'".config('healthslate.mysql_default_date_format')."') as last_time_tracker_synced,
						count(DISTINCT ds_psl.patient_service_log_id) AS no_of_times_synced,
						-- As SUM is getting multiplied by activityMinutes count
						FLOOR( SUM(IF(ds_psl.attribute_type = 'steps', ds_psl.activity_value, NULL))
						*count(DISTINCT IF(ds_psl.attribute_type = 'steps', ds_psl.patient_service_log_id, NULL))
						 / count(IF(ds_psl.attribute_type = 'steps', ds_psl.patient_service_log_id, NULL)) ) as total_fitbit_steps,
						-- 
						FLOOR( SUM(IF(ds_psl.attribute_type IN ('minutesFairlyActive', 'minutesVeryActive'), ds_psl.activity_value, NULL))
						*count(DISTINCT IF(ds_psl.attribute_type IN ('minutesFairlyActive', 'minutesVeryActive'), ds_psl.patient_service_log_id, NULL))
						 / count(IF(ds_psl.attribute_type IN ('minutesFairlyActive', 'minutesVeryActive'), ds_psl.patient_service_log_id, NULL)) ) as total_fitbit_minutes,
						count(DISTINCT DATE_FORMAT(ds_psl.activity_start_date,'%Y-%m-%d')) AS no_of_fitbit_activity_days,
						'' AS avg_fitbit_steps_per_day,

						DATE_FORMAT(MAX(pa_l.log_date_time),'".config('healthslate.mysql_default_date_format')."') as last_time_manual_logged,
						count(DISTINCT pa_al.activity_log_id) AS no_of_times_manual_logged,
						-- As SUM is getting multiplied by fitbit steps count
						FLOOR(SUM(pa_al.minutes_performed)*count(DISTINCT pa_al.activity_log_id) / count(*)) as total_minutes_logged

						FROM `patient` pa_p
						LEFT JOIN `users` pa_u ON pa_p.user_id = pa_u.user_id
						LEFT JOIN `diabetes_type` pa_dt ON pa_p.diabetes_type_id = pa_dt.diabetes_type_id

						LEFT JOIN `".$device_support_db_name."`.`patient_device` ds_pd ON pa_p.uuid = ds_pd.patient_uuid
						LEFT JOIN `".$device_support_db_name."`.`patient_service_log` ds_psl 
						ON ( ds_pd.patient_uuid = ds_psl.patient_uuid 
						AND ds_psl.service_name = 'activity' 
						".$additional_patient_service_log_join." )

						LEFT JOIN `log` pa_l 
						ON (pa_p.patient_id = pa_l.patient_id 
						AND pa_l.log_type = 'ActivityMinute' 
						".$additional_log_join.")
						LEFT JOIN `activity_log` pa_al ON (pa_l.log_id = pa_al.log_id)
						WHERE pa_p.facility_id = :facility_id
						AND (pa_p.is_deleted IS NULL OR ORD(pa_p.is_deleted) = 0)
						AND ORD(pa_u.is_registration_completed) = 1
						AND pa_p.mrn NOT LIKE '%test%'
						GROUP BY pa_p.patient_id
						ORDER BY pa_p.patient_id";

		return $this->connection->select($this->connection->raw($raw_query),$binding);
	}

	/**
	 * get_patient_fitbit_weight_report
	 *
	 * @param $facility_id
	 * @param int $days
	 *
	 * @return array
	 */
	public function get_patient_fitbit_weight_report($facility_id, $days = 0) {
		$device_support_db_name = config('healthslate.device_support_db_name');
		$binding = ['facility_id' => $facility_id ];
		$additional_patient_service_log_join = '';
		$additional_log_join = '';
		if(intval($days) > 0){
			$last_date = Carbon::today()->subDay($days)->addDay(1);
			$additional_patient_service_log_join = ' AND ds_psl.callback_time > :callback_time ';
			$additional_log_join = ' AND pa_l.log_date_time > :log_date_time ';
			$binding = [
				'facility_id' => $facility_id,
				'callback_time' => $last_date->format('Y-m-d H:i:s'),
				'log_date_time' => $last_date->format('Y-m-d H:i:s')
			];
		}
		$raw_query = "SELECT pa_p.patient_id, pa_p.uuid, pa_p.mrn,
						CONCAT(pa_u.first_name, ' ' ,pa_u.last_name) AS name,
						pa_u.offset_key,
						pa_dt.name AS diabetes_type,

						IF(ds_pd.patient_device_id IS NOT NULL, '1',NULL) as is_tracker_linked,
						IF( (pa_t.starting_weight IS NOT NULL AND pa_t.starting_weight != 0), pa_t.starting_weight, NULL) as starting_weight,
						-- Date Tracker Linked is not available yet
						DATE_FORMAT(MAX(ds_psl.callback_time),'".config('healthslate.mysql_default_date_format')."') as last_time_tracker_synced,
						count(DISTINCT ds_psl.patient_service_log_id) AS no_of_times_synced,
						SUBSTRING_INDEX(
							GROUP_CONCAT(CAST(ds_psl.activity_value AS CHAR) ORDER BY ds_psl.callback_time DESC), ',', 1
						) as last_synced_weight,

						-- BodyTrace related logs
						FROM_UNIXTIME(MAX(IF( pa_l.log_source = 'BodyTrace', pa_ml.timestamp, NULL ) )/1000,'".config('healthslate.mysql_default_date_format')."') as last_time_bodyTrace_synced,
						count(DISTINCT IF( pa_l.log_source = 'BodyTrace', pa_ml.misc_log_id, NULL ) ) AS no_of_times_bodyTrace_synced,
						SUBSTRING_INDEX(
							GROUP_CONCAT( IF( pa_l.log_source = 'BodyTrace', CAST(pa_ml.weight AS CHAR), NULL) ORDER BY pa_l.log_date_time DESC ), ',', 1
						) as last_bodyTrace_synced_weight,

						FROM_UNIXTIME(MAX(IF( ( pa_l.log_source IS NULL OR pa_l.log_source = 'Manual' ), pa_ml.timestamp, NULL ) )/1000,'".config('healthslate.mysql_default_date_format')."') as last_time_manual_logged,
						count(DISTINCT IF( (pa_l.log_source IS NULL OR pa_l.log_source = 'Manual' ), pa_ml.misc_log_id, NULL ) ) AS no_of_times_manual_logged,
						SUBSTRING_INDEX(
							GROUP_CONCAT( IF( (pa_l.log_source IS NULL OR pa_l.log_source = 'Manual' ), CAST(pa_ml.weight AS CHAR), NULL) ORDER BY pa_l.log_date_time DESC ), ',', 1
						) as last_manually_logged_weight

						FROM `patient` pa_p
						LEFT JOIN `users` pa_u ON pa_p.user_id = pa_u.user_id
						LEFT JOIN `diabetes_type` pa_dt ON pa_p.diabetes_type_id = pa_dt.diabetes_type_id

						LEFT JOIN `".$device_support_db_name."`.`patient_device` ds_pd ON pa_p.uuid = ds_pd.patient_uuid
						LEFT JOIN `".$device_support_db_name."`.`patient_service_log` ds_psl ON ( ds_pd.patient_uuid = ds_psl.patient_uuid AND ds_psl.service_name = 'weight' ".$additional_patient_service_log_join." )

						LEFT JOIN `log` pa_l ON (pa_p.patient_id = pa_l.patient_id AND pa_l.log_type = 'Weight'  ".$additional_log_join.")
						LEFT JOIN `misc_log` pa_ml ON (pa_l.log_id = pa_ml.log_id)
						LEFT JOIN `targets` pa_t ON (pa_p.patient_id = pa_t.patient_id)

						WHERE pa_p.facility_id = :facility_id
						AND (pa_p.is_deleted IS NULL OR ORD(pa_p.is_deleted) = 0)
						AND ORD(pa_u.is_registration_completed) = 1
						AND pa_p.mrn NOT LIKE '%test%'
						GROUP BY pa_p.patient_id
						ORDER BY pa_dt.name";

		return $this->connection->select($this->connection->raw($raw_query),$binding);
	}

	/**
	 * @return array|static[]
	 */
	public function get_patients_app_version_log(){
		return $this->connection
			->table( 'service_filter_log AS sfl' )
			->select( [ 'sfl.*' ] )
			->rightJoin(
				$this->connection->raw( ' (SELECT max(logging_id) logging_id FROM service_filter_log 
				GROUP BY patient_id, service, status 
				HAVING status = "Access Granted" AND service IN ("auth/login", "/services/v2/auth/login" ) ) AS l
			' ), 'l.logging_id', '=', 'sfl.logging_id' )
			->get();
	}

	/**
	 * get_atoz_preferences
	 *
	 * @param null $preference_name
	 *
	 * @return array|mixed|static|static[]
	 */
	public function get_atoz_preferences($preference_name = null){
		if($preference_name){
			return $this->connection->table('atoz_preferences')->where('preference_name',$preference_name)->first();
		}
		return $this->connection->table('atoz_preferences')->get();
	}

	/**
	 * get_missing_provider_patient_mapping for a facility
	 *
	 * @param $facility_id
	 *
	 * @return array
	 */
	public function get_missing_provider_patient_mapping($facility_id){

		$super_facility = $this->get_atoz_preferences('healthslateFacilityId');

		if(!empty($super_facility) && !empty($super_facility->preference_value) ){
			$super_facility_id = $super_facility->preference_value;

			if($facility_id == $super_facility_id){
				// searching for missing mappings of super facility ( fetch records for $facility_id (all - facility_admin) )
				$raw_query = 'select patient_id,provider_id from (
							    select * from
							        (
							        select * From
							            (
							                select patient_id from patient p where facility_id = :facility_id and (is_deleted is null or ord(is_deleted) = 0)
							            ) patients,
							            (
							                select provider_id From provider where (facility_id = :facility_id_2 AND type NOT IN ("Facility Admin")) and (is_deleted is null or ord(is_deleted) = 0)
							            ) providers
							        ) expected_entries

							    left outer join
							    (
							        select patients_patient_id , providers_provider_id from provider_patient
							    ) current_mappings

							    on (expected_entries.patient_id = current_mappings.patients_patient_id and expected_entries.provider_id = current_mappings.providers_provider_id)
							) x
							where patients_patient_id is null';
				$binding = ['facility_id' => $facility_id, 'facility_id_2' => $facility_id ];
			}else{
				// searching for missing mappings of non super facility ( fetch records for $facility_id(all - facility_admin) + $super_facility_id(partial) )

				$raw_query = 'select patient_id,provider_id from (
							    select * from
							        (
							        select * From
							            (
							                select patient_id from patient p where facility_id = :facility_id and (is_deleted is null or ord(is_deleted) = 0)
							            ) patients,
							            (
							                select provider_id From provider
							                where (
											    (facility_id = :super_facility_id AND type IN ("Food Coach", "Tech Support"))
											    OR
											    (facility_id = :facility_id_2 AND type NOT IN ("Facility Admin"))
											) and (is_deleted is null or ord(is_deleted) = 0)
							            ) providers
							        ) expected_entries

							    left outer join
							    (
							        select patients_patient_id , providers_provider_id from provider_patient
							    ) current_mappings

							    on (expected_entries.patient_id = current_mappings.patients_patient_id and expected_entries.provider_id = current_mappings.providers_provider_id)
							) x
							where patients_patient_id is null';
				$binding = ['facility_id' => $facility_id, 'super_facility_id' => $super_facility_id, 'facility_id_2' => $facility_id ];
			}
			return $this->connection->select($this->connection->raw($raw_query),$binding);
		}
		return [];
	}

	/**
	 * @param bool $count
	 * @param int $days
	 * @param int $limit
	 * @param int $offset
	 *
	 * @return array|int
	 */
	public function get_missing_fitbit_activity_log($count = false, $days = 0, $limit = 50, $offset = 0){
		$additional_where = '';
		if(intval($days) > 0){
			$last_date = Carbon::today()->subDay(intval($days))->addDay(1);
			$additional_where = ' AND activity_start_date_time > '.($last_date->timestamp * 1000).' ';
		}
		$device_support_db_name = config('healthslate.device_support_db_name');

		$select = $count ? ' count(*) AS records ' : ' psl.patient_id,
						psl.name,
						psl.activity_value AS steps,
						activity_start_date_time AS timestamp,
						UNIX_TIMESTAMP(psl.callback_time)*1000 AS upload_Time,
						psl.log_time_offset,
						psl.activity_start_date,
						psl.callback_time ';

		$limit_string = $count ? '' : ' Limit '.$offset.', '.$limit.' ';
		$order_string = $count ? '' : ' ORDER BY activity_start_date_time DESC ';

		$raw_query = 'SELECT ' . $select . '
						FROM
							(
								SELECT ds_psl.*,UNIX_TIMESTAMP(ds_psl.activity_start_date)*1000 AS activity_start_date_time,
								pa_p.patient_id, concat(pa_u.first_name, " " ,pa_u.last_name) AS name, pa_u.log_time_offset AS log_time_offset
								FROM `' . $device_support_db_name . '`.`patient_service_log` ds_psl
								LEFT JOIN `patient` pa_p ON ds_psl.patient_uuid = pa_p.uuid
								LEFT JOIN `users`  pa_u ON pa_p.user_id = pa_u.user_id
								WHERE pa_p.uuid IS NOT NULL
								AND ds_psl.service_name = "activity"
							) AS psl
						LEFT JOIN `misfit_activity_log` pa_mal ON pa_mal.patient_id  = psl.patient_id
						AND pa_mal.timestamp = activity_start_date_time
						WHERE pa_mal.activity_log_id IS NULL ' . $additional_where . '
						'.$order_string.' '.$limit_string;

		$result = $this->connection->select($this->connection->raw($raw_query));
		if($count){
			return (!empty($result)) ? @$result[0]->records : 0;
		}
		return $result;
	}

	/**
	 * @param int $days
	 *
	 * @return array
	 */
	public function get_missing_fitbit_weight_log($days = 0){
		$additional_where = '';
		if(intval($days) > 0){
			$last_date = Carbon::today()->subDay(intval($days))->addDay(1);
			$additional_where = ' AND activity_start_date_time > '.($last_date->timestamp * 1000).' ';
		}
		$device_support_db_name = config('healthslate.device_support_db_name');
		$raw_query = 'SELECT
						psl.patient_id,
						psl.name,
						psl.activity_value AS weight,
						activity_start_date_time AS log_time,
						UNIX_TIMESTAMP(psl.callback_time)*1000 AS upload_time,
						psl.log_time_offset,
						psl.activity_start_date,
						psl.callback_time
						FROM
						(
							SELECT ds_psl.*,UNIX_TIMESTAMP(ds_psl.activity_start_date)*1000 AS activity_start_date_time,
							pa_p.patient_id, concat(pa_u.first_name, " " ,pa_u.last_name) AS name, pa_u.log_time_offset AS log_time_offset
							FROM `'.$device_support_db_name.'`.`patient_service_log` ds_psl
							LEFT JOIN `patient` pa_p ON ds_psl.patient_uuid = pa_p.uuid
							LEFT JOIN `users`  pa_u ON pa_p.user_id = pa_u.user_id
							WHERE pa_p.uuid IS NOT NULL
							AND ds_psl.service_name = "weight"
						) AS psl
						LEFT JOIN `log` pa_l ON
						(
							pa_l.patient_id  = psl.patient_id AND
							pa_l.log_time = activity_start_date_time AND
							pa_l.log_type = "Weight"
						)
						 WHERE pa_l.log_id IS NULL '.$additional_where.'
						 ORDER BY activity_start_date_time DESC ';
		return $this->connection->select($this->connection->raw($raw_query));
	}

	/**
	 * get_weekly_participation_report_for_duration
	 *
	 * @param $facility_id
	 * @param null|Carbon $start
	 * @param null|Carbon $end
	 * @param null $lead_coach
	 *
	 * @return array|static[]
	 */
	public function get_weekly_participation_report_for_duration( $facility_id, $start = null, $end = null, $lead_coach = null) {
		if(!$start instanceof Carbon || !$end instanceof Carbon){
			$start = Carbon::today()->subWeek(1)->addDay(1);
			$end = null;
		}
		$query = $this->connection
			->table( 'patient AS p' )
			->select( [
				'f.facility_id',
				'f.name AS facility_name',
				'p.patient_id',
				'p.uuid',
				'p.mrn',
				'dt.name AS diabetes_type_name',
				'p.diabetes_type_id',
				'u.first_name',
				'u.last_name',
				'u.email',
				'u.user_id',
				// invite, commit and enrollment_date
				$this->connection->raw('FROM_UNIXTIME(p.invitation_sent_date/1000,"'.config('healthslate.mysql_default_date_format').'") AS invite_date'),
				$this->connection->raw('DATE_FORMAT(p.approval_date,"'.config('healthslate.mysql_default_date_format').'") AS commit_date'),
				$this->connection->raw('DATE_FORMAT(u.password_created_date,"'.config('healthslate.mysql_default_date_format').'") AS enrollment_date'),
				$this->connection->raw( 'IF(DATE(' . ( $end ? '"'.$end->format( 'Y-m-d H:i:s' ).'"' : 'now()' ) . ') > DATE(u.password_created_date), FLOOR(DATEDIFF(DATE(' . ( $end ? '"'.$end->format( 'Y-m-d H:i:s' ).'"' : 'now()' ) . '),DATE(u.password_created_date))/7), 0) as no_of_weeks_in_program' ),
				$this->connection->raw( ' "" AS no_of_weeks_logged_weight'),
				$this->connection->raw( 'concat(lcu.first_name, " " ,lcu.last_name) as lead_coach_name' ),
				$this->connection->raw( 'count(DISTINCT l.log_id) as total_logs' ),

				// Manual Activity Info
				$this->connection->raw( '
					 FLOOR(
					 sum(IF(l.log_type = "ActivityMinute", al.minutes_performed, NULL))*count(DISTINCT IF(l.log_type = "ActivityMinute", al.activity_log_id, NULL))/count(IF(l.log_type = "ActivityMinute", al.activity_log_id, NULL))
					) as manual_activity_minutes' ),
				$this->connection->raw( 'count(DISTINCT IF(l.log_type = "ActivityMinute",FROM_UNIXTIME(l.log_time/1000,"%Y-%m-%d"), NULL)) as no_of_manual_activity_days' ),
				$this->connection->raw( '
					count(DISTINCT
						IF(l.log_type = "ActivityMinute", l.log_id, NULL)
					) as no_of_manual_activity_log' ),

				// Fitbit Activity Info
				$this->connection->raw( 'mal.no_of_synced AS no_of_fitbit_activity_logs' ),
				$this->connection->raw( 'mal.total_steps_synced' ),
				$this->connection->raw( 'mal.no_of_fitbit_activity_days' ),
				$this->connection->raw( 'FLOOR(mal.total_steps_synced/mal.no_of_fitbit_activity_days) AS avg_steps_per_day' ),

				// weight info
				$this->connection->raw( 'IF( (t.starting_weight IS NOT NULL AND t.starting_weight != 0), t.starting_weight, NULL) as starting_weight' ),
				$this->connection->raw( '
					  SUBSTRING_INDEX(
					    GROUP_CONCAT(CAST(ml.weight AS CHAR) ORDER BY l.log_time DESC), ",", 1
					  ) as current_weight'
				),
				$this->connection->raw( 'count(DISTINCT FROM_UNIXTIME(ml.timestamp/1000,"%Y-%m-%d")) as no_of_days_weight_logged' ),

				// message info
				$this->connection->raw( 'count(DISTINCT IF( (m.owner = "PATIENT"),m.message_id,NULL) ) as sent_msg' ),

				// meal info
				$this->connection->raw( '
					count(DISTINCT
						IF( (l.log_type = "Meal" AND (ORD(l.is_suggested) = 0 OR l.is_suggested IS NULL) ), l.log_id, NULL)
					) as no_of_meal_log' ),
				//weight log count
				$this->connection->raw( '
					count(DISTINCT
						IF( (l.log_type = "Weight" AND (ORD(l.is_removed) = 0 OR l.is_removed IS NULL) ), l.log_id, NULL)
					) as no_of_weight_log' ),

                //water log count
                $this->connection->raw( '
					count(DISTINCT
						IF( (l.log_type = "Water" AND (ORD(l.is_removed) = 0 OR l.is_removed IS NULL) ), l.log_id, NULL)
					) as glasses_of_water_consumed' ),


                // goal and 1:1 sessions
				$this->connection->raw( 'count(DISTINCT ff.filled_form_id) as no_of_goal_logged ' ),
				$this->connection->raw( 'count(DISTINCT o2o.session_id) as one_to_one_sessions' )
			] )
			->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
			->leftJoin( 'diabetes_type AS dt', 'dt.diabetes_type_id', '=', 'p.diabetes_type_id' )
			->leftJoin( 'facility AS f', 'f.facility_id', '=', 'p.facility_id' )

			->leftJoin( 'provider AS lc', 'lc.provider_id', '=', 'p.lead_coach_id' )
			->leftJoin( 'users AS lcu', 'lcu.user_id', '=', 'lc.user_id' )

			->leftJoin( 'targets AS t', 'p.patient_id', '=', 't.patient_id' )
			->leftJoin( 'log AS l', function ( $q ) use ( $start, $end ){
				$q->on( 'l.patient_id', '=', 'p.patient_id' );
				if($start){
					$q->where( 'l.log_date_time', '>=', $start->format( 'Y-m-d H:i:s' ) );
				}
				if($end){
					$q->where( 'l.log_date_time', '<', $end->format( 'Y-m-d H:i:s' ) );
				}
			} )
			->leftJoin( 'activity_log AS al', 'al.log_id', '=', 'l.log_id' )
			->leftJoin( 'misc_log AS ml', function ( $q ) {
				$q->on( 'ml.log_id', '=', 'l.log_id' )
					->where( 'l.log_type', '=', 'Weight' );
			} )
			->leftJoin( $this->connection->raw( ' (SELECT patient_id,
				count(DISTINCT activity_log_summary_id) AS no_of_synced,
				SUM(steps) AS total_steps_synced,
				count(DISTINCT FROM_UNIXTIME(timestamp/1000,"%Y-%m-%d")) AS no_of_fitbit_activity_days,
				MAX(timestamp) AS last_timestamp,
				MAX(upload_Time) AS last_upload_Time
				FROM activity_log_summary
				WHERE timestamp >= ' . ( $start->timestamp * 1000 ) . '
				' . ( $end ? " AND timestamp < ".($end->timestamp * 1000)." " : '' ) . '
				AND attribute_type = "steps"
				GROUP BY patient_id) AS mal
			' ), 'p.patient_id', '=', 'mal.patient_id' )
			->leftJoin( 'message as m', function($q) use ( $start, $end ){
				$q->on( 'm.patient_id', '=', 'p.patient_id' );
				if($start){
					$q->where( 'm.timestamp', '>=', $start->timestamp*1000 );
				}
				if($end){
					$q->where( 'm.timestamp', '<', $end->timestamp*1000 );
				}
			} )
			->leftJoin( 'filled_form as ff', function ( $q ) use ( $start, $end ) {
				$q->on( 'ff.patient_id', '=', 'p.patient_id' );
				$q->where( 'ff.type', '=', 'GOAL' );
				if($start){
					$q->where( 'ff.created_date', '>=', $start->format( 'Y-m-d H:i:s' ) );
				}
				if($end){
					$q->where( 'ff.created_date', '<', $end->format( 'Y-m-d H:i:s' ) );
				}
			} )
			->leftJoin( 'one_to_one_sessions as o2o', function ( $q ) use ( $start, $end ) {
				$q->on( 'o2o.patient_id', '=', 'p.patient_id' );
				if($start){
					$q->where( 'o2o.session_time', '>=', $start->timestamp*1000 );
				}
				if($end){
					$q->where( 'o2o.session_time', '<', $end->timestamp*1000 );
				}
			} )
			//->where( 'u.email', 'not like', '\\_%' )
			->whereRaw( $this->connection->raw('ORD(u.is_registration_completed) = 1') )
			->where( 'p.mrn', 'not like', '%test%' )
			->where( 'p.facility_id', '=', $facility_id );

		if(!empty($lead_coach)){
			$query->where('p.lead_coach_id', '=', $lead_coach);
		}
		return $query
			->groupBy( 'p.patient_id' )
			->get();
	}

	/**
	 * get_weekly_participation_report_for_duration
	 *
	 * @param $facility_id
	 * @param null|Carbon $start
	 * @param null|Carbon $end
	 * @param null $lead_coach
	 *
	 * @return array|static[]
	 */
	public function get_weekly_participation_report_for_duration_engagement( $facility_id, $start = null, $end = null, $lead_coach = null, $patient_ids) {
		if(!$start instanceof Carbon || !$end instanceof Carbon){
			$start = Carbon::today()->subWeek(1)->addDay(1);
			$end = null;
		}
		$query = $this->connection
			->table( 'patient AS p' )
			->select( [
				'f.facility_id',
				'f.name AS facility_name',
				'p.patient_id',
				'p.uuid',
				'p.mrn',
				'dt.name AS diabetes_type_name',
				'p.diabetes_type_id',
				'u.first_name',
				'u.last_name',
				'u.email',
                'u.user_id',
				// invite, commit and enrollment_date
				$this->connection->raw('FROM_UNIXTIME(p.invitation_sent_date/1000,"'.config('healthslate.mysql_default_date_format').'") AS invite_date'),
				$this->connection->raw('DATE_FORMAT(p.approval_date,"'.config('healthslate.mysql_default_date_format').'") AS commit_date'),
				$this->connection->raw('DATE_FORMAT(u.password_created_date,"'.config('healthslate.mysql_default_date_format').'") AS enrollment_date'),
				$this->connection->raw( 'IF(DATE(' . ( $end ? '"'.$end->format( 'Y-m-d H:i:s' ).'"' : 'now()' ) . ') > DATE(u.password_created_date), FLOOR(DATEDIFF(DATE(' . ( $end ? '"'.$end->format( 'Y-m-d H:i:s' ).'"' : 'now()' ) . '),DATE(u.password_created_date))/7), 0) as no_of_weeks_in_program' ),
				$this->connection->raw( ' "" AS no_of_weeks_logged_weight'),
				$this->connection->raw( 'concat(lcu.first_name, " " ,lcu.last_name) as lead_coach_name' ),
				$this->connection->raw( 'count(DISTINCT l.log_id) as total_logs' ),

				// Manual Activity Info
				$this->connection->raw( '
					 FLOOR(
					 sum(IF(l.log_type = "ActivityMinute", al.minutes_performed, NULL))*count(DISTINCT IF(l.log_type = "ActivityMinute", al.activity_log_id, NULL))/count(IF(l.log_type = "ActivityMinute", al.activity_log_id, NULL))
					) as manual_activity_minutes' ),
				$this->connection->raw( 'count(DISTINCT IF(l.log_type = "ActivityMinute",FROM_UNIXTIME(l.log_time/1000,"%Y-%m-%d"), NULL)) as no_of_manual_activity_days' ),
				$this->connection->raw( '
					count(DISTINCT
						IF(l.log_type = "ActivityMinute", l.log_id, NULL)
					) as no_of_manual_activity_log' ),

				// Fitbit Activity Info
				$this->connection->raw( 'mal.no_of_synced AS no_of_fitbit_activity_logs' ),
				$this->connection->raw( 'mal.total_steps_synced' ),
				$this->connection->raw( 'mal.no_of_fitbit_activity_days' ),
				$this->connection->raw( 'FLOOR(mal.total_steps_synced/mal.no_of_fitbit_activity_days) AS avg_steps_per_day' ),

				// weight info
				$this->connection->raw( 'IF( (t.starting_weight IS NOT NULL AND t.starting_weight != 0), t.starting_weight, NULL) as starting_weight' ),
				$this->connection->raw( '
					  SUBSTRING_INDEX(
					    GROUP_CONCAT(CAST(ml.weight AS CHAR) ORDER BY l.log_time DESC), ",", 1
					  ) as current_weight'
				),
				$this->connection->raw( 'count(DISTINCT FROM_UNIXTIME(ml.timestamp/1000,"%Y-%m-%d")) as no_of_days_weight_logged' ),

				// message info
				$this->connection->raw( 'count(DISTINCT IF( (m.owner = "PATIENT"),m.message_id,NULL) ) as sent_msg' ),

				// meal info
				$this->connection->raw( '
					count(DISTINCT
						IF( (l.log_type = "Meal" AND (ORD(l.is_suggested) = 0 OR l.is_suggested IS NULL) ), l.log_id, NULL)
					) as no_of_meal_log' ),
                //weight log count
                $this->connection->raw( '
					count(DISTINCT
						IF( (l.log_type = "Weight" AND (ORD(l.is_removed) = 0 OR l.is_removed IS NULL) ), l.log_id, NULL)
					) as no_of_weight_log' ),

                //water log count
                $this->connection->raw( '
					count(DISTINCT
						IF( (l.log_type = "Water" AND (ORD(l.is_removed) = 0 OR l.is_removed IS NULL) ), l.log_id, NULL)
					) as glasses_of_water_consumed' ),


                // goal and 1:1 sessions
				$this->connection->raw( 'count(DISTINCT ff.filled_form_id) as no_of_goal_logged ' ),
				$this->connection->raw( 'count(DISTINCT o2o.session_id) as one_to_one_sessions' )
			] )
			->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
			->leftJoin( 'diabetes_type AS dt', 'dt.diabetes_type_id', '=', 'p.diabetes_type_id' )
			->leftJoin( 'facility AS f', 'f.facility_id', '=', 'p.facility_id' )

			->leftJoin( 'provider AS lc', 'lc.provider_id', '=', 'p.lead_coach_id' )
			->leftJoin( 'users AS lcu', 'lcu.user_id', '=', 'lc.user_id' )

			->leftJoin( 'targets AS t', 'p.patient_id', '=', 't.patient_id' )
			->leftJoin( 'log AS l', function ( $q ) use ( $start, $end ){
				$q->on( 'l.patient_id', '=', 'p.patient_id' );
				if($start){
					$q->where( 'l.log_date_time', '>=', $start->format( 'Y-m-d H:i:s' ) );
				}
				if($end){
					$q->where( 'l.log_date_time', '<', $end->format( 'Y-m-d H:i:s' ) );
				}
			} )
			->leftJoin( 'activity_log AS al', 'al.log_id', '=', 'l.log_id' )
			->leftJoin( 'misc_log AS ml', function ( $q ) {
				$q->on( 'ml.log_id', '=', 'l.log_id' )
				  ->where( 'l.log_type', '=', 'Weight' );
			} )
			->leftJoin( $this->connection->raw( ' (SELECT patient_id,
				count(DISTINCT activity_log_summary_id) AS no_of_synced,
				SUM(steps) AS total_steps_synced,
				count(DISTINCT FROM_UNIXTIME(timestamp/1000,"%Y-%m-%d")) AS no_of_fitbit_activity_days,
				MAX(timestamp) AS last_timestamp,
				MAX(upload_Time) AS last_upload_Time
				FROM activity_log_summary
				WHERE timestamp >= ' . ( $start->timestamp * 1000 ) . '
				' . ( $end ? " AND timestamp < ".($end->timestamp * 1000)." " : '' ) . '
				AND attribute_type = "steps"
				GROUP BY patient_id) AS mal
			' ), 'p.patient_id', '=', 'mal.patient_id' )
			->leftJoin( 'message as m', function($q) use ( $start, $end ){
				$q->on( 'm.patient_id', '=', 'p.patient_id' );
				if($start){
					$q->where( 'm.timestamp', '>=', $start->timestamp*1000 );
				}
				if($end){
					$q->where( 'm.timestamp', '<', $end->timestamp*1000 );
				}
			} )
			->leftJoin( 'filled_form as ff', function ( $q ) use ( $start, $end ) {
				$q->on( 'ff.patient_id', '=', 'p.patient_id' );
				$q->where( 'ff.type', '=', 'GOAL' );
				if($start){
					$q->where( 'ff.created_date', '>=', $start->format( 'Y-m-d H:i:s' ) );
				}
				if($end){
					$q->where( 'ff.created_date', '<', $end->format( 'Y-m-d H:i:s' ) );
				}
			} )
			->leftJoin( 'one_to_one_sessions as o2o', function ( $q ) use ( $start, $end ) {
				$q->on( 'o2o.patient_id', '=', 'p.patient_id' );
				if($start){
					$q->where( 'o2o.session_time', '>=', $start->timestamp*1000 );
				}
				if($end){
					$q->where( 'o2o.session_time', '<', $end->timestamp*1000 );
				}
			} )
			//->where( 'u.email', 'not like', '\\_%' )
			->whereRaw( $this->connection->raw('ORD(u.is_registration_completed) = 1') )
			->where( 'p.mrn', 'not like', '%test%' )
			->where( 'p.facility_id', '=', $facility_id )
			->whereIn( 'p.patient_id', $patient_ids );

		if(!empty($lead_coach)){
			$query->where('p.lead_coach_id', '=', $lead_coach);
		}
		return $query
			->groupBy( 'p.patient_id' )
			->get();
	}



	/**
	 * get_weekly_participation_report_total_patients
	 *
	 * @param $facility_id
	 * @param null|Carbon $start
	 * @param null|Carbon $end
	 * @param null $lead_coach
	 *
	 * @return array|static[]
	 */
	public function get_weekly_participation_report_total_patients( $facility_id) {

		$query = $this->connection
			->table( 'patient AS p' )
			->select( [
				'p.patient_id',
			] )
			->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
			//->where( 'u.email', 'not like', '\\_%' )
			->whereRaw( $this->connection->raw('ORD(u.is_registration_completed) = 1') )
			->where( 'p.mrn', 'not like', '%test%' )
			->where( 'p.facility_id', '=', $facility_id );

		return $query
			->groupBy( 'p.patient_id' )
			->get();
	}


	/**
	 * group_report_for_duration
	 *
	 * @param $facility_id
	 * @param null|Carbon $start
	 * @param null|Carbon $end
	 * @param null $lead_coach
	 *
	 * @return array|static[]
	 */
	public function get_group_report_for_duration(array $patients, Carbon $start, Carbon $end, Carbon $prior_week_start, Carbon $prior_week_end){
		return $this->connection
			->table( 'patient AS p' )
			->select( [
				'p.patient_id',
				'dt.name AS diabetes_type_name',
				'p.diabetes_type_id',
				'p.uuid',
				'cpt.name as curriculum_program_type_name',

				$this->connection->raw('IF(DAYNAME(registration_date) = "Sunday" , registration_date , date(registration_date + INTERVAL 6 - weekday(registration_date) DAY)) as member_start_date'),
				$this->connection->raw('FLOOR(DATEDIFF(CURDATE(), IF(DAYNAME(registration_date) = "Sunday" , registration_date , date(registration_date + INTERVAL 6 - weekday(registration_date) DAY))) / 7) as member_completed_week'),

				// total logs
				$this->connection->raw('count(
					DISTINCT IF(l.log_date_time >= "'.$start->format( 'Y-m-d H:i:s' ).'", l.log_id, null)
				  ) as total_logs'),
				$this->connection->raw( 'count(
					DISTINCT IF(l.log_date_time >= "'.$start->format( 'Y-m-d H:i:s' ).'", DATE(l.log_date_time), null)
				) as days_logged' ),

				't.starting_weight',
				't.target_weight',
				// This week manual Activity Minutes
				$this->connection->raw( 'SUM(
					IF(
						(l.log_type = "ActivityMinute" AND l.log_date_time >= "'.$start->format( 'Y-m-d H:i:s' ).'"),
						al.minutes_performed, null
					)
				) as manual_activity_minutes' ),

				// Prior week manual Activity Minutes
				$this->connection->raw( 'SUM(
					IF(
						(l.log_type = "ActivityMinute"
						AND l.log_date_time >= "'.$prior_week_start->format( 'Y-m-d H:i:s' ).'"
				        AND l.log_date_time < "'.$start->format( 'Y-m-d H:i:s' ).'"),
						al.minutes_performed, null
					)
				) as prior_week_manual_activity_minutes' ),
			])
			->leftJoin( 'diabetes_type AS dt', 'dt.diabetes_type_id', '=', 'p.diabetes_type_id' )
			->leftJoin( 'targets AS t', 't.patient_id', '=', 'p.patient_id' )
			->leftJoin( 'log AS l', function ( $q ) use ( $prior_week_start, $end ){
				$q->on( 'l.patient_id', '=', 'p.patient_id' );
				$q->where( 'l.log_date_time', '>=', $prior_week_start->format( 'Y-m-d H:i:s' ) );
				$q->where( 'l.log_date_time', '<', $end->format( 'Y-m-d H:i:s' ) );
			} )
			->leftJoin( 'activity_log AS al', 'al.log_id', '=', 'l.log_id' )
			->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
			->leftJoin( 'curriculum_program_type AS cpt', 'cpt.curriculum_program_type_id', '=', 'dt.curriculum_program_type_id' )
			->whereIn( 'p.patient_id', $patients )
			->groupBy( 'p.patient_id' )
			->get();


	}



	/**
	 * get_group_report_for_duration_weight_details
	 *
	 * @param $facility_id
	 * @param null|Carbon $start
	 * @param null|Carbon $end
	 * @param null $lead_coach
	 *
	 * @return array|static[]
	 */
	public function get_group_report_for_duration_weight_details(array $patients, Carbon $start, Carbon $end, Carbon $prior_week_start, Carbon $prior_week_end){
		return $this->connection
			->table( 'patient AS p' )
			->select( [
				'p.patient_id',

				// this_week_weight
				$this->connection->raw('SUBSTRING_INDEX(
				    GROUP_CONCAT(
				        IF(
					        (l.log_type = "Weight" AND l.log_date_time >= "'.$start->format( 'Y-m-d H:i:s' ).'"),
					        CAST(ml.weight AS CHAR), NULL
				        ) ORDER BY l.log_time DESC ), ",", 1
				  ) as current_weight'),
				// prior_week_weight
				$this->connection->raw('SUBSTRING_INDEX(
				    GROUP_CONCAT(
				        IF(
					        (l.log_type = "Weight"
					        AND l.log_date_time >= "'.$prior_week_start->format( 'Y-m-d H:i:s' ).'"
					        AND l.log_date_time < "'.$start->format( 'Y-m-d H:i:s' ).'"),
					        CAST(ml.weight AS CHAR), NULL
				        ) ORDER BY l.log_time DESC ), ",", 1
				  ) as prior_week_weight'),

			])
			->join( 'log AS l', function ( $q ) use ( $prior_week_start, $end ){
				$q->on( 'l.patient_id', '=', 'p.patient_id' );
				$q->where( 'l.log_date_time', '>=', $prior_week_start->format( 'Y-m-d H:i:s' ) );
				$q->where( 'l.log_date_time', '<', $end->format( 'Y-m-d H:i:s' ) );
			} )
			->join( 'misc_log AS ml', 'ml.log_id', '=', 'l.log_id' )
			->whereIn( 'p.patient_id', $patients )
			->groupBy( 'p.patient_id' )
			->get();
	}


	/**
	 * @param array $patients
	 * @param Carbon $start
	 * @param Carbon $end
	 *
	 * @return array|static[]
	 */
	public function get_patients_synced_minutes( array $patients, Carbon $start, Carbon $end ) {

		$start_date = $start->timestamp * 1000;

		$statement = $this->connection
			->table( 'activity_log_summary AS m' )
			->select( [
				'm.patient_id',
				$this->connection->raw( 'FLOOR(SUM(m.steps)) AS total_activity_minutes_synced' ),
			] )
			->leftJoin( 'patient AS p', 'p.patient_id', '=', 'm.patient_id' )
			->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
			->whereIn( 'm.patient_id', $patients )
			->whereIn( 'm.attribute_type', [ 'minutesFairlyActive', 'minutesVeryActive' ] );

		if ( ! empty( $start ) && ! empty( $end ) ) {
			$statement->whereRaw( $this->connection->raw( 'm.timestamp >= IF( '.$start_date.' < (UNIX_TIMESTAMP(password_created_date) * 1000) , (UNIX_TIMESTAMP(password_created_date) * 1000), '.$start_date.' )' ) );
			$statement->where( 'm.timestamp', '<', $end->timestamp * 1000 );
		}

		return $statement->groupBy( 'm.patient_id' )->get();
	}



	/**
	 * @param $patient_ids
	 * @param null $older_than
	 *
	 * @return array|static[]
	 */
	public function get_patient_recent_weight_log( $patient_ids, $older_than = null ) {
		$query = $this->connection
			->table( 'log AS l' )
			->select( [
				'l.patient_id',
				$this->connection->raw('SUBSTRING_INDEX(
				    GROUP_CONCAT(CAST(l.log_id AS CHAR) ORDER BY l.log_time DESC), ",", 1
				  ) as log_id'),
				$this->connection->raw('SUBSTRING_INDEX(
				    GROUP_CONCAT(CAST(l.log_date_time AS CHAR) ORDER BY l.log_time DESC), ",", 1
				  ) as log_date_time'),
				$this->connection->raw('SUBSTRING_INDEX(
				    GROUP_CONCAT(CAST(ml.weight AS CHAR) ORDER BY l.log_time DESC), ",", 1
				  ) as weight'),
			] )
			->leftJoin( 'misc_log AS ml', 'ml.log_id', '=', 'l.log_id' )
			->where( 'l.log_type', '=', 'Weight' )
			->whereNotNull( 'ml.weight' )
			->whereIn( 'l.patient_id', $patient_ids )
			->groupBy( 'l.patient_id' );
		if ( $older_than instanceof Carbon ) {
			$query->where( 'l.log_date_time', '<', $older_than->format( 'Y-m-d H:i:s' ) );
		}

		return $query->get();
	}
        
        /**
         * 
         * @param type $patient_ids
         * 
         * @return type array|static[]
         */
        public function get_patient_minimum_weight($patient_ids) {
            $query = $this->connection
                    ->table( 'misc_log AS ml' )
                    ->select([  
                        'l.patient_id',
                        $this->connection->raw('MIN(ml.weight) as min_weight'),
                    ])
            ->leftJoin( 'log AS l', function ( $q ) {
                    $q->on( 'l.log_id', '=', 'ml.log_id' )
                    ->where( 'l.log_type', '=', 'Weight' );
                    
            } )
            ->whereIn( 'l.patient_id', $patient_ids )
            ->whereNotNull( 'ml.weight' )
            ->groupBy( 'l.patient_id' );
            
            return $query->get();
        }

	/**
	 * @param $facility_id
	 * @param bool $exclude_deleted
	 *
	 * @return array|static[]
	 */
	public function get_facility_coach( $facility_id = false, $exclude_deleted = false ) {
		$query = $this->connection
			->table( 'provider AS pr' )
			->select( [
				'pr.*',
				'u.first_name',
				'u.last_name',
				'u.email',
				'u.user_type',
				'u.display_name',
				$this->connection->raw( 'CONCAT(u.first_name, " ", u.last_name) AS full_name' )
			] )
			->leftJoin( 'users AS u', 'u.user_id', '=', 'pr.user_id' )
			->whereIn( 'pr.type', $this->coach_provider_types );

		if ( $exclude_deleted ) {
			$query->whereRaw( $this->connection->raw( '(pr.is_deleted IS NULL OR ORD(pr.is_deleted) = 0)' ) );
		}
        if ( $facility_id != false ) {
            $query->where( 'pr.facility_id', '=', $facility_id );
        }
		return $query->get();
	}

	/**
	 * @param $patient_ids
	 * @param $end_date
	 * @param array $log_source
	 *
	 * @return array|static[]
	 */
	public function get_patient_weeks_weight_logged($patient_ids, $end_date, $log_source = ['Aria','BodyTrace']){
		return $this->connection
			->table( 'patient AS p' )
			->select( [
				'p.patient_id',
				'u.user_id',
				'u.password_created_date',
				$this->connection->raw( 'count(l.log_id) as weight_logs_in_week' ),
				$this->connection->raw( 'FLOOR( DATEDIFF( from_unixtime(l.log_time/1000) , u.password_created_date ) /7 ) AS log_week' ),
			] )
			->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
			->leftJoin( 'log AS l', 'l.patient_id', '=', 'p.patient_id' )
			->where( 'l.log_type', '=', 'Weight' )
			->whereIn( 'l.log_source', $log_source )
			->where( 'l.log_time', '<', $end_date->timestamp * 1000 )
			->whereIn( 'p.patient_id', $patient_ids )
			->groupBy( 'p.patient_id' )
			->groupBy( 'log_week' )
			->get();
	}

	/**
	 * get_patient_engagement
	 *      get all patients' engagement grouped by lead
	 * @param $start
	 * @param $end
	 *
	 * @return array|static[]
	 */
	public function get_patient_engagement($start, $end){
		// if start and end data are not supplied correctly
		if(!$start instanceof Carbon || !$end instanceof Carbon){
			return [];
		}

		$query = $this->connection
			->table( 'patient AS p' )
			->select( [
				'p.lead_coach_id',
				'p.patient_id',
				'p.uuid',
				'p.mrn',
				'u.first_name',
				'u.last_name',
				'dt.name AS diabetes_type_name',
				'p.diabetes_type_id',
				$this->connection->raw( 'IF(DATE("' . $end->format( 'Y-m-d H:i:s' ) . '") > DATE(u.password_created_date), FLOOR(DATEDIFF(DATE("' .$end->format( 'Y-m-d H:i:s' ). '"),DATE(u.password_created_date))/7), 0) as no_of_weeks_in_program' ),
                $this->connection->raw( 'IF( (t.starting_weight IS NOT NULL AND t.starting_weight != 0), t.starting_weight, NULL) as starting_weight' ),
			])
			->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
			->leftJoin( 'diabetes_type AS dt', 'dt.diabetes_type_id', '=', 'p.diabetes_type_id' )
			->leftJoin( 'targets AS t', 'p.patient_id', '=', 't.patient_id' )
			->whereNotNull( 'p.lead_coach_id')
			->where( 'u.email', 'not like', '\\_%' )
			->whereRaw( $this->connection->raw('ORD(u.is_registration_completed) = 1') )
			->where( 'p.mrn', 'not like', '%test%' )
			->where( 'p.mrn', 'not like', 'dis%' )
			->where( 'p.mrn', 'not like', 'deactive%' );

		return $query
			->groupBy( 'p.lead_coach_id' )
			->groupBy( 'p.patient_id' )
			->get();
	}

	/**
	 * @param $facility_id
	 *
	 * @return array|static[]
	 */
	public function get_week_cohorts($facility_id, $lead_coach = false){
		$query = $this->connection
			->table( 'patient AS p' )
			->select( [
				$this->connection->raw('CEIL( DATEDIFF( NOW() , u.password_created_date ) /7 ) AS week'),
				'p.patient_id',
				$this->connection->raw('COUNT(DISTINCT l.log_id) AS num_log_entries'),
				$this->connection->raw('COUNT(DISTINCT IF(l.log_type = "Weight", l.log_id, NULL)) AS num_weight_entries'),
				$this->connection->raw('FROM_UNIXTIME( MAX( l.log_time ) /1000 ) AS last_entry'),
				$this->connection->raw('DATEDIFF( NOW() , FROM_UNIXTIME( MAX( l.log_time ) /1000 ) ) AS num_days_last_entry'),
				't.starting_weight',
				$this->connection->raw(' SUBSTRING_INDEX( GROUP_CONCAT(CAST(ml.weight AS CHAR) ORDER BY l.log_time DESC), ",", 1 ) as current_weight'),
				't.target_weight',
			] )

			->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
			->leftJoin( 'log AS l', 'l.patient_id', '=', 'p.patient_id' )
			->leftJoin( 'misc_log AS ml', 'ml.log_id', '=', 'l.log_id' )
			->leftJoin( 'targets AS t', 't.patient_id', '=', 'p.patient_id' );

        if ($facility_id != 0) {
            $query->where('p.facility_id', '=', $facility_id );
        }

        if ($lead_coach) {
            $query->where(function ($query) use ($lead_coach) {
                $query->where('p.lead_coach_id', '=', $lead_coach)
                    ->orWhere(function($query) use ($lead_coach) {
                        $query->where('p.primary_food_coach_id', '=', $lead_coach);
                    });
            });
        }

		return $query->where( 'u.email', 'not like', '\\_%' )
			->whereNotNull( 'u.password_created_date' )
			->where( 'p.mrn', 'not like', '%test%' )
			->groupBy( 'week' )
			->groupBy( 'p.patient_id' )
			->get();

	}

	/**
	 * @param $facility_id
	 * @param $week
	 *
	 * @return array|static[]
	 */
	public function get_cohort_patients_of_week($facility_id, $week = null, $lead_coach = false){

		$query = $this->connection
			->table( 'patient AS p' )
			->select( [
				'p.patient_id',
				'p.uuid',
				'p.user_id',
				'p.diabetes_type_id',
				'dt.name AS diabetes_type_name',
				'u.password_created_date',
				't.starting_weight',
				't.target_weight',
			] )
			->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
			->leftJoin( 'diabetes_type AS dt', 'dt.diabetes_type_id', '=', 'p.diabetes_type_id' )
			->leftJoin( 'targets AS t', 't.patient_id', '=', 'p.patient_id' )
			->where( 'u.email', 'not like', '\\_%' )
			->whereNotNull( 'u.password_created_date' )
			->where( 'p.mrn', 'not like', '%test%' );

        if ($facility_id != 0) {
            $query->where('p.facility_id', '=', $facility_id );
        }

        if ($lead_coach) {
            $query->where(function ($query) use ($lead_coach) {
                $query->where('p.lead_coach_id', '=', $lead_coach)
                    ->orWhere(function($query) use ($lead_coach) {
                        $query->where('p.primary_food_coach_id', '=', $lead_coach);
                    });
            });
        }

		if(!empty($week)){
			$query->whereRaw( $this->connection->raw('CEIL( DATEDIFF( NOW() , u.password_created_date ) /7 ) = ?'), ['week' => $week] );
		}

		return $query->orderBy('p.patient_id')->get();
	}

	/**
	 * @param $patients_of_week
	 *
	 * @return array
	 */
	public function get_activity_for_cohort_week($patients_of_week){
		if(empty($patients_of_week)){
			return [];
		}

		return $this->connection
			->table( 'patient AS p' )
			->select( [
				'p.patient_id',
				'p.uuid',
				'dt.name AS diabetes_type_name',
				'u.password_created_date',
				$this->connection->raw('CEIL( DATEDIFF( from_unixtime(l.log_time/1000) , u.password_created_date ) /7 ) AS log_week'),
				$this->connection->raw('COUNT(DISTINCT l.log_id) AS num_log_entries'),
				$this->connection->raw('COUNT(DISTINCT IF(l.log_type = "Weight", p.patient_id, NULL)) AS has_weight_entry'),
				$this->connection->raw('COUNT(DISTINCT IF(l.log_type = "Weight", l.log_id, NULL)) AS num_weight_entries'),
                                $this->connection->raw('COUNT(DISTINCT IF(l.log_type = "Meal", l.log_id, NULL)) AS num_meal_entries'),
				$this->connection->raw('FROM_UNIXTIME( MAX( l.log_time ) /1000 ) AS last_entry'),
			] )
			->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
			->leftJoin( 'diabetes_type AS dt', 'dt.diabetes_type_id', '=', 'p.diabetes_type_id' )
			->leftJoin( 'log AS l', 'l.patient_id', '=', 'p.patient_id' )
			->whereIn( 'p.patient_id', $patients_of_week )
			->groupBy( 'log_week' )
			->groupBy( 'p.patient_id' )
			->orderBy('log_week')
			->get();
	}

	/**
	 * @param $patients_of_week
	 * @param $log_week
	 *
	 * @return array|static[]
	 */
	public function get_patients_activity_of_log_week($patients_of_week, $log_week){
		if(empty($patients_of_week)){
			return [];
		}
		$log_week_start = ( $log_week - 1 ) * 7;
		$log_week_end   = $log_week * 7;
		info('log week start: '.$log_week_start.' end: '.$log_week_end);

		return $this->connection
			->table( 'patient AS p' )
			->select( [
				'p.patient_id',
				'p.uuid',
				'u.user_id',
				'dt.name AS diabetes_type_name',
				'u.password_created_date',
				$this->connection->raw( 'count(distinct l.log_id) num_log_entries' ),
				$this->connection->raw( 'COUNT(DISTINCT IF(l.log_type = "Weight", l.log_id, NULL)) num_weight_entries' ),
				$this->connection->raw( 'SUBSTRING_INDEX( GROUP_CONCAT(CAST(ml.weight AS CHAR) ORDER BY l.log_time DESC), ",", 1 )  current_weight' ),
				$this->connection->raw( 'from_unixtime( MAX(IF(l.log_type = "Weight", l.log_time, NULL)) /1000, "'.config('healthslate.mysql_default_date_format').'" ) as last_weight_logged_on' ),
				$this->connection->raw( 'FLOOR(
					 sum(IF(l.log_type = "ActivityMinute", al.minutes_performed, NULL))
					 *count(DISTINCT IF(l.log_type = "ActivityMinute", al.activity_log_id, NULL))
					 /
					 count(IF(l.log_type = "ActivityMinute", al.activity_log_id, NULL))
				) as manual_activity_minutes' ),
				$this->connection->raw( '"" AS last_login_in_week' ),
				$this->connection->raw( ' NULL AS total_steps_synced' ),
				$this->connection->raw( ' 0 AS num_group_entries' ),

			] )
			->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
			->leftJoin( 'diabetes_type AS dt', 'dt.diabetes_type_id', '=', 'p.diabetes_type_id' )
			->leftJoin( 'log AS l', 'l.patient_id', '=', 'p.patient_id' )
			->leftJoin( 'misc_log AS ml', 'ml.log_id', '=', 'l.log_id' )
			->leftJoin( 'activity_log AS al', 'al.log_id', '=', 'l.log_id' )
			->whereIn( 'p.patient_id', $patients_of_week )
			->whereRaw( $this->connection->raw( ' ( l.log_time >= UNIX_TIMESTAMP(date_add(u.password_created_date,INTERVAL ? DAY))*1000 )
			 AND
			    ( l.log_time < UNIX_TIMESTAMP(date_add(u.password_created_date,INTERVAL ? DAY))*1000 )' ),
				[ $log_week_start, $log_week_end ] )
			->groupBy( 'p.patient_id' )
			->get();
	}

	/**
	 * @param $patients_of_week
	 * @param $log_week
	 *
	 * @return array|mixed|static|static[]
	 */
	public function get_patients_login_in_week($patients_of_week, $log_week){
		if ( empty( $patients_of_week ) ) {
			return [ ];
		}
		$log_week_start = ( $log_week - 1 ) * 7;
		$log_week_end   = $log_week * 7;
		info( 'login week start: ' . $log_week_start . ' end: ' . $log_week_end );

		return $this->connection
			->table( 'service_filter_log AS sfl' )
			->select( [
				'p.patient_id',
				$this->connection->raw( 'from_unixtime( MAX( sfl.timestamp ) / 1000, "' . config( 'healthslate.mysql_default_date_format' ) . '" ) as last_login_in_week' ),
			] )
			->rightJoin( 'patient AS p', 'p.patient_id', '=', 'sfl.patient_id' )
			->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
			->whereIn( 'sfl.service', [ "auth/login", "/services/v2/auth/login" ] )
			->where( 'sfl.status', '=', 'Access Granted' )
			->whereIn( 'p.patient_id', $patients_of_week )
			->whereRaw( $this->connection->raw( ' ( sfl.timestamp >= UNIX_TIMESTAMP(date_add(u.password_created_date,INTERVAL ? DAY))*1000 )
			 AND
			    ( sfl.timestamp < UNIX_TIMESTAMP(date_add(u.password_created_date,INTERVAL ? DAY))*1000 )' ),
				[ $log_week_start, $log_week_end ] )
			->groupBy( 'p.patient_id' )
			->get();

	}

	/**
	 * @param $patients_of_week
	 * @param $log_week
	 *
	 * @return array|static[]
	 */
	public function get_patients_synced_steps_in_week($patients_of_week, $log_week){
		if ( empty( $patients_of_week ) ) {
			return [ ];
		}
		$log_week_start = ( $log_week - 1 ) * 7;
		$log_week_end   = $log_week * 7;
		info( 'sync week start: ' . $log_week_start . ' end: ' . $log_week_end );

		return $this->connection
			->table( 'activity_log_summary AS m' )
			->select( [
				'm.patient_id',
				$this->connection->raw( 'FLOOR(SUM(m.steps)) AS total_steps_synced' ),
			] )
			->leftJoin( 'patient AS p', 'p.patient_id', '=', 'm.patient_id' )
			->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
			->whereIn( 'p.patient_id', $patients_of_week )
			->where( 'm.attribute_type', '=', 'steps' )
			->whereRaw( $this->connection->raw( ' ( m.timestamp >= UNIX_TIMESTAMP(date_add(u.password_created_date,INTERVAL ? DAY))*1000 )
			 AND
			    ( m.timestamp < UNIX_TIMESTAMP(date_add(u.password_created_date,INTERVAL ? DAY))*1000 )' ),
				[ $log_week_start, $log_week_end ] )
			->groupBy( 'p.patient_id' )
			->get();
	}

	/**
	 * @param $users_of_week
	 * @param $log_week
	 *
	 * @return array|static[]
	 */
	public function get_patients_group_posts_in_week( $users_of_week, $log_week ) {
		$group_db_name = config( 'healthslate.group_db_name' );
		if ( empty( $users_of_week ) || empty( $group_db_name ) ) {
			return [ ];
		}
		$log_week_start = ( $log_week - 1 ) * 7;
		$log_week_end   = $log_week * 7;
		info( 'group post week start: ' . $log_week_start . ' end: ' . $log_week_end );

		return $this->connection
			->table( 'users AS p_u' )
			->select( [
				'p_u.user_id',
				$this->connection->raw( 'count(DISTINCT g_p.post_id) AS num_group_entries' ),
			] )
			->leftJoin( $this->connection->raw('`' . $group_db_name . '`.post AS g_p'), 'p_u.user_id', '=', 'g_p.user_id' )
			->whereIn( 'p_u.user_id', $users_of_week )
			->whereRaw( $this->connection->raw( ' ( g_p.timestamp >= date_add(p_u.password_created_date,INTERVAL ? DAY) )
			 AND
			    ( g_p.timestamp < date_add(p_u.password_created_date,INTERVAL ? DAY) )' ),
				[ $log_week_start, $log_week_end ] )
			->groupBy( 'p_u.user_id' )
			->get();
	}

	/**
	 * @param $facility_id
	 *
	 * @return array|static[]
	 */
	public function get_patient_preferences( $facility_id ) {

		$query = $this->connection
			->table( 'patient AS p' )
			->select( [
				'p.*',
				'u.*',
				$this->connection->raw( 'concat(u.first_name, " " ,u.last_name) as full_name' ),
				'dt.name AS diabetes_type_name'
			] )
			->leftJoin( 'diabetes_type AS dt', 'dt.diabetes_type_id', '=', 'p.diabetes_type_id' )
			->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' );
		return $query
			// this might lead to incorrect data, so commenting it.
			//->whereRaw($this->connection->raw('(ORD(p.is_deleted) = 0 OR p.is_deleted IS NULL)'))
			->where( 'u.email', 'not like', '\\_%' )
			->whereRaw( $this->connection->raw('ORD(u.is_registration_completed) = 1') )
			->where( 'p.mrn', 'not like', '%test%' )
			->where( 'p.facility_id', '=', $facility_id )
			->orderBy( 'p.patient_id' )
			->get();
	}

	/**
	 * @param $facility_id
	 * @param null $start
	 * @param null $end
	 *
	 * @return array|static[]
	 */
	public function get_shipping_addresses($facility_id, $start = null, $end = null){
		// if start and end data are not supplied correctly
		if(!$start instanceof Carbon || !$end instanceof Carbon){
			return [];
		}

		$query = $this->connection
			->table( 'patient AS p' )
			->select( [
				'p.patient_id',
				'p.mrn',
				'u.first_name',
				'u.last_name',
				'u.address',
				'u.city',
				'u.state',
				'u.zip_code',
				'u.phone',
				$this->connection->raw( 'DATE_FORMAT(CURDATE(), "'.config('healthslate.mysql_default_date_format').'" ) AS today_date' ),
				$this->connection->raw( 'DATE_FORMAT(u.password_created_date, "'.config('healthslate.mysql_default_date_format').'" ) AS password_created_date' ),
				'dt.name AS diabetes_type_name',
				$this->connection->raw( 'concat(lcu.first_name, " " ,lcu.last_name) as lead_coach_name' ),
				$this->connection->raw( 'count(DISTINCT m.message_id ) as sent_msg' ),
				$this->connection->raw( 'count(DISTINCT l2.log_id ) as logged_meals' ),
			])
			->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
			->leftJoin( 'diabetes_type AS dt', 'dt.diabetes_type_id', '=', 'p.diabetes_type_id' )

			->leftJoin( 'provider AS lc', 'lc.provider_id', '=', 'p.lead_coach_id' )
			->leftJoin( 'users AS lcu', 'lcu.user_id', '=', 'lc.user_id' )

			// joining to get weight logs
			->leftJoin( 'log AS l', function ( $q ) {
				$q->on( 'l.patient_id', '=', 'p.patient_id' );
				$q->where( 'l.log_type', '=', 'Weight' );
				$q->whereIn( 'l.log_source', ['Aria','BodyTrace'] );
			} )
			// joining to get meal logs
			->leftJoin( 'log AS l2', function ( $q ) {
				$q->on( 'l2.patient_id', '=', 'p.patient_id' );
				$q->where( 'l2.log_type', '=', 'Meal' );
				$q->where( $this->connection->raw('ORD(l2.is_suggested)'), '=', '0');
			} )
			// joining to get sent messages
			->leftJoin( 'message AS m', function($q) {
				$q->on( 'm.patient_id', '=', 'p.patient_id' );
				$q->where( 'm.owner', '=', 'PATIENT' );
			} )
			->where( 'u.email', 'not like', '\\_%' )
			->whereRaw( $this->connection->raw('ORD(u.is_registration_completed) = 1') )
			->where( 'p.mrn', 'not like', '%test%' )
			->where( 'p.facility_id', '=', $facility_id )
			// has created password within duration
			->where( 'u.password_created_date', '>=', $start->format( 'Y-m-d H:i:s' ) )
			->where( 'u.password_created_date', '<', $end->format( 'Y-m-d H:i:s' ) )
			// has no weight logs
			->whereNull('l.log_id');
			// we have changed criteria @18th Oct
			// has at least one sent message
			//->whereNotNull( 'm.message_id')
			// has at least one meal log
			//->whereNotNull( 'l2.log_id')

		return $query
			->groupBy( 'p.patient_id' )
			->get();
	}

	/**
	 * @param $facility_id
	 * @param bool $count
	 * @param int $limit
	 * @param int $offset
	 *
	 * @return array|int|static[]
	 */
	public function get_patient_coaches($facility_id, $count = false, $limit = 50, $offset = 0){
		$query = $this->connection
			->table( 'patient AS p' )
			->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
			->leftJoin( 'provider_patient AS pp', 'pp.patients_patient_id', '=', 'p.patient_id' )
			->leftJoin( 'provider AS pr', 'pr.provider_id', '=', 'pp.providers_provider_id' )
			->leftJoin( 'users AS pru', 'pru.user_id', '=', 'pr.user_id' )
			->where( 'u.email', 'not like', '\\_%' )
			->where( 'pru.email', 'not like', '\\_%' )
			->whereRaw( $this->connection->raw( 'ORD(u.is_registration_completed) = 1' ) )
			->where( 'p.facility_id', '=', $facility_id );

		if ( $count ) {
			$records = $query->select( $this->connection->raw( 'count(*) as records' ) )->first();

			return ( ! empty( $records ) ) ? @$records->records : 0;
		}

		$query
			->select( [
				'p.patient_id',
				$this->connection->raw( 'concat(u.first_name, " " ,u.last_name) as full_name' ),
				'pr.provider_id',
				$this->connection->raw( 'concat(pru.first_name, " " ,pru.last_name) as coach_full_name' ),
				'pru.email AS coach_email'
			] )
			->orderBy( 'p.patient_id' );

		if ( $limit > 0 ) {
			$query->skip( $offset )
				->limit( $limit );
		}

		return $query->get();
	}

	/**
	 * @param $facility_id
	 * @param $start
	 * @param $end
	 *
	 * @return array|static[]
	 */
	public function get_message_activity_list( $facility_id, $start, $end ) {

		$query = $this->connection
			->table( 'patient AS p' )
			->select( [
				'p.patient_id',
				$this->connection->raw( 'concat(u.first_name, " " ,u.last_name) as name' ),
				'p.mrn',
				$this->connection->raw( 'sum(IF(owner="PATIENT", 1,NULL)) as sent' ),
				$this->connection->raw( 'sum(IF(owner="PROVIDER", 1,NULL)) as received' ),

			] )
			->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
			->leftJoin( 'message AS m', 'm.patient_id', '=', 'p.patient_id' )
			->where( 'u.email', 'not like', '\\_%' )
			->where( 'p.mrn', 'not like', '%test%' )
			->where( 'p.facility_id', '=', $facility_id )
			->where( 'm.timestamp', '>=', $start->timestamp * 1000 )
			->where( 'm.timestamp', '<', $end->timestamp * 1000 );

		return $query->groupBy( 'm.patient_id' )->orderBy( 'p.patient_id' )->get();
	}


	/**
	 * @param $facility_id
	 * @param $start
	 * @param $end
	 *
	 * @return array|static[]
	 */
	public function get_weight_loss_patient( $facility_id, $start, $end ) {

		$query = $this->connection
			->table( 'patient AS p' )
			->select( [
				'p.patient_id',
				$this->connection->raw( 'concat(u.first_name, " " ,u.last_name) as name' ),
				'p.mrn',
				$this->connection->raw( 'ROUND(SUBSTRING_INDEX(GROUP_CONCAT(CAST(ml.weight AS CHAR) ORDER BY l.log_time DESC), ",", 1), 2) as current_weight' ),
				$this->connection->raw( 'FROM_UNIXTIME(MAX(ml.timestamp)/1000,"' . config( 'healthslate.mysql_default_date_format' ) . '") as last_day_weight_logged' ),
				$this->connection->raw( 'ROUND(IF( (t.starting_weight IS NOT NULL AND t.starting_weight != 0), t.starting_weight, NULL), 2) as starting_weight' ),
				$this->connection->raw( 'ROUND(MIN(ml.weight), 2) as minimum_weight_achieved' ),
				$this->connection->raw( 'FROM_UNIXTIME(SUBSTRING_INDEX(GROUP_CONCAT(CAST(ml.timestamp/1000 AS CHAR) ORDER BY ml.weight ASC), ",", 1),"' . config( 'healthslate.mysql_default_date_format' ) . '") as date_achieved' ),
			] )
			->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
			->leftJoin( 'targets AS t', 't.patient_id', '=', 'p.patient_id' )
			->leftJoin( 'log AS l', function ( $q ) use ( $start, $end ) {
				$q->on( 'l.patient_id', '=', 'p.patient_id' )
				  ->where( 'l.log_type', '=', 'Weight' )
				  ->where( 'l.created_on', '>=', $start->timestamp * 1000 )
				  ->where( 'l.created_on', '<', $end->timestamp * 1000 );
			} )
			->leftJoin( 'misc_log AS ml', 'ml.log_id', '=', 'l.log_id' )
			->where( 'u.email', 'not like', '\\_%' )
			->whereRaw( $this->connection->raw( 'ORD(u.is_registration_completed) = 1' ) )
			->where( 'p.mrn', 'not like', '%test%' )
			->where( 'p.facility_id', '=', $facility_id );


		return $query->groupBy( 'p.patient_id' )->orderBy( 'p.patient_id' )->get();
	}



	/**
	 * get_sessions_completed_report_for_duration
	 *
	 * @param $facility_id
	 * @param null|Carbon $start
	 * @param null|Carbon $end
	 *
	 * @return array|static[]
	 */
	public function get_sessions_completed_report_for_duration( $facility_id, $start = null, $end = null) {
		if(!$start instanceof Carbon || !$end instanceof Carbon){
			$start = Carbon::today()->subWeek(1)->addDay(1);
			$end = null;
		}
		$query = $this->connection
			->table( 'patient AS p' )
			->select( [
				'p.patient_id',
				'p.uuid',
				'p.diabetes_type_id',
				'u.first_name',
				'u.last_name',
				'u.email',
				'u.user_id',
				$this->connection->raw( 'IF(DATE(' . ( $end ? '"'.$end->format( 'Y-m-d H:i:s' ).'"' : 'now()' ) . ') > DATE(u.password_created_date), FLOOR(DATEDIFF(DATE(' . ( $end ? '"'.$end->format( 'Y-m-d H:i:s' ).'"' : 'now()' ) . '),DATE(u.password_created_date))/7), 0) as no_of_weeks_in_program' ),

				//weight log count
				$this->connection->raw( '
					count(DISTINCT
						IF( (l.log_type = "Weight" AND (ORD(l.is_removed) = 0 OR l.is_removed IS NULL) ), l.log_id, NULL)
					) as no_of_weight_log' )

			] )
			->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
			->leftJoin( 'diabetes_type AS dt', 'dt.diabetes_type_id', '=', 'p.diabetes_type_id' )
			->leftJoin( 'facility AS f', 'f.facility_id', '=', 'p.facility_id' )

			->leftJoin( 'provider AS lc', 'lc.provider_id', '=', 'p.lead_coach_id' )
			->leftJoin( 'users AS lcu', 'lcu.user_id', '=', 'lc.user_id' )

			->leftJoin( 'targets AS t', 'p.patient_id', '=', 't.patient_id' )
			->leftJoin( 'log AS l', function ( $q ) use ( $start, $end ){
				$q->on( 'l.patient_id', '=', 'p.patient_id' );
				if($start){
					$q->where( 'l.log_date_time', '>=', $start->format( 'Y-m-d H:i:s' ) );
				}
				if($end){
					$q->where( 'l.log_date_time', '<', $end->format( 'Y-m-d H:i:s' ) );
				}
			} )
			->leftJoin( 'activity_log AS al', 'al.log_id', '=', 'l.log_id' )
			->leftJoin( 'misc_log AS ml', function ( $q ) {
				$q->on( 'ml.log_id', '=', 'l.log_id' )
					->where( 'l.log_type', '=', 'Weight' );
			} )
			->where( 'u.email', 'not like', '\\_%' )
			->whereRaw( $this->connection->raw('ORD(u.is_registration_completed) = 1') )
			->where( 'p.mrn', 'not like', '%test%' )
			->where( 'p.facility_id', '=', $facility_id );

		return $query
			->groupBy( 'p.patient_id' )
			->get();
	}



    /**
     * get_sessions_completed_report_for_duration
     *
     * @param $facility_id
     * @param null|Carbon $start
     * @param null|Carbon $end
     *
     * @return array|static[]
     */
    public function get_self_register_report( $facility_id, $start = null, $end = null) {
        if(!$start instanceof Carbon || !$end instanceof Carbon){
            $start = Carbon::today()->subWeek(1)->addDay(1);
            $end = null;
        }
        $query = $this->connection
            ->table( 'patient AS p' )
            ->select( [
                'p.patient_id',
                'u.first_name',
                'u.last_name',
                'u.email',
                'p.mrn',
                'u.registration_date',
            ] )
            ->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
            ->leftJoin( 'facility AS f', 'f.facility_id', '=', 'p.facility_id' )
            //->where( 'u.email', 'not like', '\\_%' )
            //->where( 'p.mrn', 'not like', '%test%' )
            ->where( 'p.self_register', '=', true )
            ->where( 'u.registration_date', '>=', $start->format( 'Y-m-d H:i:s' ) )
            ->where( 'u.registration_date', '<', $end->format( 'Y-m-d H:i:s' ) )
            ->where( 'p.facility_id', '=', $facility_id );

        return $query
            ->groupBy( 'p.patient_id' )
            ->get();
    }



    /**
     * get_user_registeration_report
     *
     * @param $facility_id
     * @param null|Carbon $start
     * @param null|Carbon $end
     *
     * @return array|static[]
     */
    public function get_user_registeration_report( $facility_id, $start = null, $end = null, $coach = null) {
        if(!$start instanceof Carbon || !$end instanceof Carbon){
            $start = Carbon::today()->subWeek(1)->addDay(1);
            $end = null;
        }
        $query = $this->connection
            ->table( 'patient AS p' )
            ->select( [
                'p.patient_id','p.uuid',
                'u.email',
                $this->connection->raw( 'concat(u.first_name, " " ,u.last_name) as name' ),
                'p.mrn',
                $this->connection->raw( 'from_unixtime(p.invitation_sent_date/1000) AS invitation_sent_date' ),
                'u.registration_date',
                $this->connection->raw( 'IF(password is not null,"Set","No") as password' ),
                $this->connection->raw( 'IF(self_register is not null,"Yes","No") as self_register' ),
                $this->connection->raw( 'timestampdiff(day,u.registration_date,curdate()) as days_since_reg' ),
                $this->connection->raw( 'timestampdiff(day,from_unixtime(password_modified_time/1000),curdate()) as days_since_activate' ),
                //$this->connection->raw( 'ORD(is_consent_accepted) as consent_accepted' ),
                //'p.is_approved'
            ] )
            ->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
            ->leftJoin( 'facility AS f', 'f.facility_id', '=', 'p.facility_id' )
            //->where( 'u.email', 'not like', '\\_%' )
            ->where( 'p.mrn', 'not like', '%test%' )
            ->where( 'p.mrn', 'not like', '%US%' )
            ->where( 'p.facility_id', '=', $facility_id );

        $query->where(function ($query) use($start, $end) {
            $query->where('invitation_sent_date', '>=', strtotime($start->format( 'Y-m-d H:i:s' )) * 1000)
                ->where('invitation_sent_date',  '<=' , strtotime($end->format( 'Y-m-d H:i:s' )) * 1000)
                ->orWhere(function($query) use($start, $end) {
                    $query->where('registration_date', '>=', $start->format( 'Y-m-d H:i:s' ))
                        ->where('registration_date',  '<=' , $end->format( 'Y-m-d H:i:s' ));
                });
        });

        if($coach == 1){
            $query->whereNotNull('p.self_register');
        }
        if($coach == 2){
            $query->whereNull('p.app_version');
        }
        if($coach == 3){
            $query->where( 'p.self_register', '=', true );
        }

        return $query
            ->orderBy('p.patient_id', 'asc')
            ->get();
    }



    /**
     * get_bulk_import_patient_report
     *
     * @param $facility_id
     * @param null|Carbon $start
     * @param null|Carbon $end
     *
     * @return array|static[]
     */
    public function get_bulk_import_patient_report($start = null, $end = null, $filter = null) {
        if(!$start instanceof Carbon || !$end instanceof Carbon){
            $start = Carbon::today()->subWeek(1)->addDay(1);
            $end = null;
        }
        $query = $this->connection
            ->table( 'pending_patient_sign_up AS p' )
            ->select( [
                'p.solera_id','p.last_name',
                'p.first_name',
                'p.facility_id',
                'p.email',
                'p.failure_cause',
                'p.diabetes_type_id','p.import_id',
                $this->connection->raw( 'concat(p.first_name, " " ,p.last_name) as name' ),
            ] );

        $query->where(function ($query) use($start, $end) {
            $query->where('timestamp', '>=', $start->format( 'Y-m-d H:i:s' ))
                ->where('timestamp',  '<=' , $end->format( 'Y-m-d H:i:s' ));
        });

        if($filter == 1){
            $query->where( 'p.failure_cause', '=', 'User saved successfully' );
        }
        if($filter == 2){
            $query->where( 'p.failure_cause', '=', 'User reconciled successfully' );
        }
        if($filter == 3){
            $query->where( 'p.failure_cause', '=', 'Email address already exists' );
        }

        return $query
            ->orderBy('p.pending_patient_sign_up_id', 'desc')
            ->get();
    }


    /**
     * get_patient_detail_using_id
     *
     * @param $patient_id
     *
     * @return object
     */
    public function get_patient_detail_using_id( $patient_id = false ) {

        if($patient_id)
        {
            /*$raw_query = 'SELECT uuid,patient_id FROM patient as p WHERE patient_id = "'.$patient_id.'"';
            return $this->connection->select($this->connection->raw($raw_query));*/

            $raw_query = 'SELECT uuid, patient_id, email FROM patient as p, users as u WHERE patient_id = "'.$patient_id.'" AND p.user_id=u.user_id';
            return $this->connection->select($this->connection->raw($raw_query));
        }
        else
        {
            $raw_query = 'SELECT uuid, patient_id, email FROM patient as p, users as u WHERE  p.user_id=u.user_id and p.mrn not like \'%test%\' AND u.email not like \'\\_%\' AND p.is_deleted is null';
            return $this->connection->select($this->connection->raw($raw_query));
        }
    }


    /**
     * get_patient_detail_using_id
     *
     * @param $patient_id
     *
     * @return object
     */
    public function get_patient_detail_from_device_db( $patient_uuid = false) {
        $device_support_db_name = config('healthslate.device_support_db_name');
        if($patient_uuid == false)
        {
            $raw_query = 'SELECT * FROM `'.$device_support_db_name.'`.`patient_device` as p WHERE patient_uuid != "" group by patient_uuid';
            return $this->connection->select($this->connection->raw($raw_query));
        }
        else
        {
            $raw_query = 'SELECT * FROM `'.$device_support_db_name.'`.`patient_device` as p WHERE patient_uuid = "'.$patient_uuid.'"';
            return $this->connection->select($this->connection->raw($raw_query));
        }
    }


    /**
     * updatePatientAccessToken
     *
     * @param $patient_details, $fitbit_res
     *
     * @return object
     */
    public function updatePatientAccessToken( $patient_details , $fitbit_res) {

        $device_support_db_name = config('healthslate.device_support_db_name');
        $raw_query = 'UPDATE `'.$device_support_db_name.'`.`patient_device` as p SET refresh_token = "'.$fitbit_res->refresh_token.'", access_token = "'.$fitbit_res->access_token.'" WHERE patient_uuid = "'.$patient_details->patient_uuid.'"';
        return $this->connection->select($this->connection->raw($raw_query));
    }


    /**
     * get_patient_steps_from_activity_log_summary
     *
     * @param $patient_details
     *
     * @return object
     */
    public function get_patient_steps_from_activity_log_summary( $patient_details = false , $days = 35) {

        if($patient_details == false)
        {
            $raw_query = "select 
                        patient.patient_id, 
                        sum(activity_log_summary.steps) as steps, 
                        DATE_FORMAT( CONVERT_TZ(FROM_UNIXTIME(activity_log_summary.timestamp/1000),'UTC',IF(users.offset_key != '', users.offset_key, 'UTC')), '%Y-%m-%d') as date_format
                        from patient,activity_log_summary, users 
                        where
                        activity_log_summary.patient_id=patient.patient_id AND patient.user_id=users.user_id
                        AND unix_timestamp()-(activity_log_summary.timestamp/1000) < (86400 * ".$days." ) 
                        AND activity_log_summary.attribute_type = 'steps' AND activity_log_summary.service_name = 'fitbit'
                        group by date_format, patient.uuid";
        }
        else {
            $raw_query = "select 
                        patient.patient_id, 
                        sum(activity_log_summary.steps) as steps, 
                        DATE_FORMAT( CONVERT_TZ(FROM_UNIXTIME(activity_log_summary.timestamp/1000),'UTC',IF(users.offset_key != '', users.offset_key, 'UTC')), '%Y-%m-%d') as date_format
                        from patient,activity_log_summary, users 
                        where
                        activity_log_summary.patient_id=patient.patient_id AND patient.user_id=users.user_id
                        AND patient.uuid = '" . $patient_details . "'
                        AND unix_timestamp()-(activity_log_summary.timestamp/1000) < (86400 * ".$days." ) 
                        AND activity_log_summary.attribute_type = 'steps' AND activity_log_summary.service_name = 'fitbit'
                        group by date_format";
        }

        return $this->connection->select($this->connection->raw($raw_query));

    }



    /**
     * get_patient_steps_from_activity_log_summary
     *
     * @param $patient_details
     *
     * @return object
     */
    public function get_patient_steps_from_misfit_activity_log_summary( $patient_details = false) {

        $raw_query = "select 
                            patient.patient_id, 
                            sum(misfit_activity_log.steps) as steps, 
                            DATE_FORMAT( CONVERT_TZ(FROM_UNIXTIME(misfit_activity_log.timestamp/1000),'UTC', IF(users.offset_key != '', users.offset_key, 'UTC')), '%Y-%m-%d %H') as date_format,
                            DATE_FORMAT( CONVERT_TZ(FROM_UNIXTIME(misfit_activity_log.timestamp/1000),'UTC', IF(users.offset_key != '', users.offset_key, 'UTC')), '%Y-%m-%d') as date
                            from patient, users , misfit_activity_log
                            where patient.user_id=users.user_id AND patient.patient_id=misfit_activity_log.patient_id 
                            AND misfit_activity_log.service_name = 'fitbit' AND misfit_activity_log.attribute_type = 'steps' AND misfit_activity_log.patient_id = '" . $patient_details[0]->patient_id . "'
                            AND unix_timestamp()-(misfit_activity_log.timestamp/1000) < (86400 * 31 ) 
                            group by date_format";

        return $this->connection->select($this->connection->raw($raw_query));
    }


    /**
     * get_patient_steps_from_activity_log_summary
     *
     * @param $patient_details
     *
     * @return object
     */
    public function get_patient_steps_from_misfit_activity_log_summary_signle_days( $patient_details = false) {

        $raw_query = "select 
                            patient.patient_id, 
                            sum(misfit_activity_log.steps) as steps, 
                            DATE_FORMAT( CONVERT_TZ(FROM_UNIXTIME(misfit_activity_log.timestamp/1000),'UTC', IF(users.offset_key != '', users.offset_key, 'UTC')), '%Y-%m-%d %H:%i:%s') as date_format,
                            DATE_FORMAT( CONVERT_TZ(FROM_UNIXTIME(misfit_activity_log.timestamp/1000),'UTC', IF(users.offset_key != '', users.offset_key, 'UTC')), '%Y-%m-%d') as date,
                            DATE_FORMAT( CONVERT_TZ(FROM_UNIXTIME(misfit_activity_log.timestamp/1000),'UTC', IF(users.offset_key != '', users.offset_key, 'UTC')), '%H:%i:%s') as datetime
                            from patient, users , misfit_activity_log
                            where patient.user_id=users.user_id AND patient.patient_id=misfit_activity_log.patient_id 
                            AND misfit_activity_log.service_name = 'fitbit' AND misfit_activity_log.attribute_type = 'steps' AND misfit_activity_log.patient_id = '" . $patient_details[0]->patient_id . "'
                            AND unix_timestamp()-(misfit_activity_log.timestamp/1000) < (86400 * 31 ) 
                            group by date_format";

        return $this->connection->select($this->connection->raw($raw_query));
    }

}