<?php

namespace HealthSlateAdmin\Models;

use Carbon\Carbon;
use HealthSlateAdmin\Helpers\RequestHelper;
use Log;

class CurriculumServiceProvider {

	/**
	 * @var RequestHelper
	 */
	protected $requestHelper;

	/**
	 * Type 2 Diabetes Type DB ID
	 * @var int
	 */
	protected $type_2_diabetes_type_id;

	/**
	 * @param RequestHelper $requestHelper
	 */
	public function __construct( RequestHelper $requestHelper ) {
		$this->requestHelper = $requestHelper;
		$this->type_2_diabetes_type_id = config('healthslate.hs_constants.TYPE_2_DIABETES_TYPE_ID');
	}

	/**
	 * getCurriculumUsersWeekReport
	 *
	 * @param $users
	 * @param bool $encode_json
	 *
	 * @return mixed
	 */
	public function getCurriculumUsersWeekReport( $users, $encode_json = false ) {
		$base_url = config( 'rest.GET_curriculum_users_report_by_id' );

		$request_data = [ ];
		foreach ( $users as $r_key => $row ) {
			// This API is for TYPE-2 members only
			if ( ! empty( $row->uuid ) ) {
				$url            = $base_url . $row->uuid . '/weeks';
				$request_data[$r_key] = (object) [ 'METHOD' => 'GET', 'url' => $url ];
			}
		}

		$results = $this->requestHelper->makeAsyncCall('GET', $base_url, $request_data);

		foreach ( $users as $r_key => $row ) {

			if(isset($results[$r_key])){
				$response = $results[$r_key];

				if ( $response->statusCode === 200 && !empty($response->body) ) {
					// valid response & has data
					$summary = [];
					foreach($response->body as $key  => $week){
						if($key != 'completed'){
							$summary[] = ['week' => $key, 'completed_percent' => @$week->percentage];
						}
					}
					$row->weeks = $encode_json ? json_encode($summary,JSON_PRETTY_PRINT) : $summary;
				}
			}
		}
		return $users;
	}

	/**
	 * @param $users
	 * @param bool $encode_json
	 *
	 * @return mixed
	 */
	public function getDPPCurriculumUsersWeekReport( $users, $encode_json = false ) {
		$patients = (array) $this->getCurriculumV2SessionReport($users);

		foreach ( $users as $r_key => $row ) {
			if ( (  $row->diabetes_type_id != $this->type_2_diabetes_type_id ) && isset( $patients[ $row->uuid ] ) && is_array( $patients[ $row->uuid ] ) ) {
				// It is a DPP or DPP-Cal User
				$summary = [];
				foreach ( $patients[ $row->uuid ] as $section_data ) {
					// section has starting index of 0
					$summary[] = [
						'week'              => intval( $section_data->section ) + 1,
						'units_in_week'     => ( $section_data->unitsInSection ),
						'completed_percent' => floatval( @$section_data->percentCompleted ) * 100,
						'completed'     => ( $section_data->completed_units ),
					];
				}
				$row->weeks = $encode_json ? json_encode($summary,JSON_PRETTY_PRINT) : $summary;
			}
		}
		return $users;
	}

	/**
	 * @param $users
	 * @param null|Carbon $start
	 * @param null|Carbon $end
	 * @param bool $encode_json
	 *
	 * @return mixed
	 */
	public function getEducationModulesCompleted($users, $start = null, $end = null, $encode_json = false ){
		$patients = (array) $this->getCurriculumV2SessionReport( $users, $start, $end );

		foreach ($users as $r_key => $row) {

			if (( $row->diabetes_type_id != $this->type_2_diabetes_type_id ) && isset( $patients[$row->uuid] ) && is_array( $patients[$row->uuid] )) {
				// It is a DPP or DPP-Cal User

				$completed_units = 0;
				foreach ($patients[$row->uuid] as $section_data) {
					$completed_units = $completed_units + intval(@$section_data->completed_units);
				}
				$row->viewed_units = $completed_units;

			}else{
				//@todo we are not calling Curriculum for Type-2 Members, for now. 23rd Jan 2017
			}
		}
		return $users;
	}

	/**
	 * @deprecated In favour of `getCurriculumV2SessionReport` which uses new V2 API.
	 *             Note: There is change in response object also.
	 * @param $users
	 * Call curriculum API for Only DPP & DPP-Cal Users
	 *
	 * @return array
	 */
	private function _getDPPSectionsCompleted( $users ) {
		$base_url = config( 'rest.POST_curriculum_users_sections_completed_by_uuids' );

		$patients = [];
		foreach ( $users as $r_key => $row ) {
			if ( ! empty( $row->uuid ) && (  $row->diabetes_type_id != $this->type_2_diabetes_type_id ) ) {
				$patients[] = $row->uuid;
			}
		}

		info( 'Requesting curriculum, user count: ' . count( $patients ) );
		$response = $this->requestHelper->makeRequest( 'POST', $base_url, [
			'patients' => $patients
		] );

		$data = [];
		if ( $response->statusCode === 200 && ! empty( $response->body ) && ! empty( $response->body->patients ) ) {
			$data = (array) $response->body->patients;
		}
		info( 'Response Result count: ' . count( $data ) );
		return $data;
	}

	/**
	 * Sections are week of curriculum education modules, get list of completed one
	 * @param $users
	 *
	 * @return mixed
	 */
	public function getSectionsCompleted( $users )
	{
		$patients = (array) $this->getCurriculumV2SessionReport( $users );

		foreach ($users as $r_key => $row) {
			$completed_weeks = [ ];
			$sections        = [ ];
			if (isset( $patients[$row->uuid] ) && is_array( $patients[$row->uuid] )) {
				foreach ($patients[$row->uuid] as $section_data) {
					if (isset( $section_data->section ) && floatval( @$section_data->percentCompleted ) == 1) {
						// section has starting index of 0
						$completed_weeks[] = intval( $section_data->section ) + 1;
					}
				}
				$sections = $patients[$row->uuid];
			}

			info( 'Patient ' . $row->uuid . ' [' . @$row->patient_id . '] section completed: ', $completed_weeks );
			$row->completed_weeks  = $completed_weeks;
			$row->curriculum_weeks = $sections;

		}
		return $users;
	}

	/**
	 * @param $users
	 * @param $week_number
	 *
	 * @return mixed
	 */
	public function getUnitsCompletedInSection( $users)
	{
		$results = (array) $this->getCurriculumV2SessionReport( $users );

		foreach ($users as $r_key => $row) {
			if (isset( $results[$row->uuid] ) && is_array( $results[$row->uuid] )) {
				// Response has user data
				foreach ($results[$row->uuid] as $section_data) {

					if($row->member_completed_week < 0)
						$row->member_completed_week = 0;

					if (isset( $section_data->section ) && $section_data->section == $row->member_completed_week) {
						// Response has $week_number data
						//$row->units_in_week        = @$section_data->unitsInSection;
						$row->viewed_units_in_week = @$section_data->completed_units;
					}
				}
			}
		}
		return $users;
	}


	/**
	 * @param $users
	 * @param $week_number
	 *
	 * @return mixed
	 */
	public function getSectionListByPatientType( $users)
	{
		$results = (array) $this->getCurriculumV2SectionList( $users );

		foreach ($users as $r_key => $row) {
			if (isset( $results ) && is_array( $results )) {
				// Response has user data
				foreach ($results as $section_data) {

					if($row->member_completed_week < 0)
						$row->member_completed_week = 0;

					if (isset( $section_data->display_week ) && $section_data->display_week == $row->member_completed_week + 1) {
						// Response has $week_number data
						$row->sectionTitle  	= @$section_data->sectionTitle;
						$row->units_in_week     = @$section_data->numberUnits;
					}
				}
			}
		}
		return $users;
	}

	// =============================== Curriculum V2 Calls ===========================================
	/**
	 * @param array $users
	 * @param null  $start
	 * @param null  $end
	 *
	 * @return array|object
	 */
	public function getCurriculumV2SessionReport( $users = [ ], $start = null, $end = null )
	{
		$patients = [ ];
		foreach ($users as $r_key => $row) {
			if ( ! empty( $row->uuid ) && ( $row->diabetes_type_id != $this->type_2_diabetes_type_id )) {
				// member is not of type-2
				$patients[] = $row->uuid;
			}
		}
		if (empty( $patients )) {
			return [ ];
		}
		$request = [
			'action'     => 'session_report',
			'patient_id' => $patients,
		];
		if ($start instanceof Carbon && $end instanceof Carbon) {
			$request['start'] = $start->timestamp;
			$request['end']   = ( $end->timestamp - 1 );
		}

		//info( 'Session Report Request payload', $request );
		$result = $this->requestCurriculumV2( $request );

		if (isset( $result->patients )) {
			return $result->patients;
		}

		return $result;
	}


	/**
	 * @param array $users
	 * @param null  $start
	 * @param null  $end
	 *
	 * @return array|object
	 */
	public function getCurriculumV2SectionList( $users = [ ], $start = null, $end = null )
	{
		$patients = [ ];
		foreach ($users as $r_key => $row) {
			if ( ! empty( $row->uuid ) && ( $row->diabetes_type_id != $this->type_2_diabetes_type_id )) {
				// member is not of type-2
				$patients = $row->curriculum_program_type_name;
				break;
			}
		}
		if (empty( $patients )) {
			return [ ];
		}
		$request = [
			'action'     => 'section_list',
			'programType' => $patients,
		];
		if ($start instanceof Carbon && $end instanceof Carbon) {
			$request['start'] = $start->timestamp;
			$request['end']   = ( $end->timestamp - 1 );
		}

		//info( 'Session Report Request payload', $request );
		$result = $this->requestCurriculumV2( $request );

		if (isset( $result->patients )) {
			return $result->patients;
		}

		return $result;
	}

	/**
	 * @param array $request
	 * @param null  $url
	 *
	 * @return object
	 */
	private function requestCurriculumV2( $request = [ ], $url = null )
	{
		$base_url = $url ? $url : config( 'rest.POST_curriculum_v2_request_url' );

		info( 'Requesting v2 curriculum, action: ' . ( @$request['action'] ) );
		$response = $this->requestHelper->makeRequest( 'POST', $base_url, $request );

		$data = [ ];
		if ($response->statusCode === 200 && ! empty( $response->body )) {
			$data = $response->body;
		}

		return  (object) $data;
	}

	/**
	 * @param $users
	 * @param null|Carbon $start
	 * @param null|Carbon $end
	 * @param bool $encode_json
	 *
	 * @return mixed
	 */
	public function getEducationModulesSessionsCompletedReport($users, $start = null, $end = null, $encode_json = false ){
		$patients = (array) $this->getCurriculumV2SessionReport( $users, $start, $end );

		foreach ($users as $r_key => $row) {

			if (( $row->diabetes_type_id != $this->type_2_diabetes_type_id ) && isset( $patients[$row->uuid] ) && is_array( $patients[$row->uuid] )) {
				// It is a DPP or DPP-Cal User

				$completed_units = 0;
				$complete_status = 0;
				foreach ($patients[$row->uuid] as $section_data) {
					$completed_units = $completed_units + intval(@$section_data->completed_units);
					$complete_status = @$section_data->complete_status;
				}
				$row->viewed_units 		= $completed_units;
				$row->complete_status 	= $complete_status;

			}else{
				//@todo we are not calling Curriculum for Type-2 Members, for now. 23rd Jan 2017
			}
		}
		return $users;
	}

}
