<?php

namespace HealthSlateAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityLog extends Model {

	protected $table = 'activity_log';
	protected $primaryKey = 'activity_log_id';
	public $timestamps = false;

}
