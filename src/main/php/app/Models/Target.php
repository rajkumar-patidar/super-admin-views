<?php

namespace HealthSlateAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class Target extends Model {

	protected $table = 'targets';
	protected $primaryKey = 'target_id';
	public $timestamps = false;
}
