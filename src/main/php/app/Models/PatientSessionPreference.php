<?php

namespace HealthSlateAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class PatientSessionPreference extends Model {

	protected $table = 'patient_session_preference';
	protected $primaryKey = 'session_id';
	public $timestamps = false;


	public function patient() {
		return $this->belongsTo( 'HealthSlateAdmin\Models\Patient', 'patient_id', 'patient_id' );
	}
}
