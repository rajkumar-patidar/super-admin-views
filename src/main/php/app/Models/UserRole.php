<?php

namespace HealthSlateAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model {

	protected $table = 'user_roles';

	protected $primaryKey = 'user_role_id';

	public $timestamps = false;

	protected $fillable = array('authority', 'user_id');

	public function user() {
		return $this->belongsTo( 'HealthSlateAdmin\Models\User', 'user_id', 'user_id' );
	}
}