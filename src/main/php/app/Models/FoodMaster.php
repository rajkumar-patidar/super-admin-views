<?php

namespace HealthSlateAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class FoodMaster extends Model {

	protected $table = 'food_master';
	protected $primaryKey = 'food_master_id';
	public $timestamps = false;

}
