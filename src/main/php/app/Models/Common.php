<?php
/**
 * User: rpatidar
 * Date: 22/5/17
 */

namespace HealthSlateAdmin\Models;

use Carbon\Carbon;
use DB;

class Common {

	/**
	 * @var \Illuminate\Database\Connection
	 */
	protected $connection;

	/**
	 * @var array
	 */
	protected $coach_provider_types = [ 'Coach', 'Food Coach' ];
	/**
	 *
	 */
	function __construct() {
		$this->connection = DB::connection();
	}

	/**
	 * @param $query
	 * @param array $bindings
	 *
	 * @return array
	 */
	public function get_query_result( $query, $bindings = [ ] ) {
		return $this->connection->select( $query, $bindings );
	}

    /**
     * get_patient_engagement
     *      get all patients' engagement grouped by lead
     * @param $start
     * @param $end
     *
     * @return array|static[]
     */
    public function get_patient_engagement($start, $end){
        // if start and end data are not supplied correctly
        if(!$start instanceof Carbon || !$end instanceof Carbon){
            return [];
        }

        $query = $this->connection
            ->table( 'patient AS p' )
            ->select( [
                'p.lead_coach_id',
                'p.patient_id',
                'p.uuid',
                'p.mrn',
                'u.first_name',
                'u.last_name',
                'dt.name AS diabetes_type_name',
                'p.diabetes_type_id',

                $this->connection->raw( 'IF(DATE("' . $end->format( 'Y-m-d H:i:s' ) . '") > DATE(u.password_created_date), FLOOR(DATEDIFF(DATE("' .$end->format( 'Y-m-d H:i:s' ). '"),DATE(u.password_created_date))/7), 0) as no_of_weeks_in_program' ),

                $this->connection->raw( '
					count(DISTINCT
						IF(l.log_type = "ActivityMinute", l.log_id, NULL)
					) as no_of_manual_activity_log' ),

                $this->connection->raw( 'mal.no_of_synced AS no_of_fitbit_activity_logs' ),

                $this->connection->raw( 'IF( (t.starting_weight IS NOT NULL AND t.starting_weight != 0), t.starting_weight, NULL) as starting_weight' ),

                $this->connection->raw( '
					  SUBSTRING_INDEX(
					    GROUP_CONCAT(CAST(ml.weight AS CHAR) ORDER BY l.log_time DESC), ",", 1
					  ) as current_weight'
                ),
                $this->connection->raw( 'count(DISTINCT FROM_UNIXTIME(ml.timestamp/1000,"%Y-%m-%d")) as no_of_days_weight_logged' ),

                $this->connection->raw( 'count(DISTINCT IF( (m.owner = "PATIENT"),m.message_id,NULL) ) as sent_msg' ),

                $this->connection->raw( '
					count(DISTINCT
						IF( (l.log_type = "Meal" AND (ORD(l.is_suggested) = 0 OR l.is_suggested IS NULL) ), l.log_id, NULL)
					) as no_of_meal_log' ),
                $this->connection->raw( 'count(DISTINCT ff.filled_form_id) as no_of_goal_logged ' ),
                $this->connection->raw( 'count(DISTINCT o2o.session_id) as one_to_one_sessions' )
            ])
            ->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
            ->leftJoin( 'diabetes_type AS dt', 'dt.diabetes_type_id', '=', 'p.diabetes_type_id' )
            ->leftJoin( 'targets AS t', 'p.patient_id', '=', 't.patient_id' )
            ->leftJoin( 'log AS l', function ( $q ) use ( $start, $end ){
                $q->on( 'l.patient_id', '=', 'p.patient_id' );
                $q->where( 'l.log_date_time', '>=', $start->format( 'Y-m-d H:i:s' ) );
                $q->where( 'l.log_date_time', '<', $end->format( 'Y-m-d H:i:s' ) );
            } )
            ->leftJoin( 'misc_log AS ml', function ( $q ) {
                $q->on( 'ml.log_id', '=', 'l.log_id' )
                    ->where( 'l.log_type', '=', 'Weight' );
            } )
            ->leftJoin( $this->connection->raw( ' (SELECT patient_id,
			    count(*) AS no_of_synced
				FROM activity_log_summary
				WHERE timestamp >= ' . ( $start->timestamp * 1000 ) . ' AND timestamp < '.($end->timestamp * 1000). '
				GROUP BY patient_id) AS mal
			' ), 'p.patient_id', '=', 'mal.patient_id' )
            ->leftJoin( 'message as m', function($q) use ( $start, $end ){
                $q->on( 'm.patient_id', '=', 'p.patient_id' );
                $q->where( 'm.timestamp', '>=', $start->timestamp*1000 );
                $q->where( 'm.timestamp', '<', $end->timestamp*1000 );
            } )
            ->leftJoin( 'filled_form as ff', function ( $q ) use ( $start, $end ) {
                $q->on( 'ff.patient_id', '=', 'p.patient_id' );
                $q->where( 'ff.type', '=', 'GOAL' );
                $q->where( 'ff.created_date', '>=', $start->format( 'Y-m-d H:i:s' ) );
                $q->where( 'ff.created_date', '<', $end->format( 'Y-m-d H:i:s' ) );
            } )
            ->leftJoin( 'one_to_one_sessions as o2o', function ( $q ) use ( $start, $end ) {
                $q->on( 'o2o.patient_id', '=', 'p.patient_id' );
                $q->where( 'o2o.session_time', '>=', $start->timestamp*1000 );
                $q->where( 'o2o.session_time', '<', $end->timestamp*1000 );
            } )
            ->whereNotNull( 'p.lead_coach_id')
            ->where( 'u.email', 'not like', '\\_%' )
            ->whereRaw( $this->connection->raw('ORD(u.is_registration_completed) = 1') )
            ->where( 'p.mrn', 'not like', '%test%' )
            ->where( 'p.mrn', 'not like', 'dis%' )
            ->where( 'p.mrn', 'not like', 'deactive%' );

        return $query
            ->groupBy( 'p.lead_coach_id' )
            ->groupBy( 'p.patient_id' )
            ->get();
    }


    /**
     * get_patient_engagement
     *      get all patients' engagement grouped by lead
     * @param $start
     * @param $end
     *
     * @return array|static[]
     */
    public function get_patient_manual_activity_log($start, $end, $patient_ids){
        // if start and end data are not supplied correctly
        if(!$start instanceof Carbon || !$end instanceof Carbon){
            return [];
        }

        $query = $this->connection
            ->table( 'patient AS p' )
            ->select( [
                'p.patient_id',

                $this->connection->raw( '
					count(DISTINCT
						IF(l.log_type = "ActivityMinute", l.log_id, NULL)
					) as no_of_manual_activity_log' ),

                $this->connection->raw( 'mal.no_of_synced AS no_of_fitbit_activity_logs' ),

                $this->connection->raw( '
					  SUBSTRING_INDEX(
					    GROUP_CONCAT(CAST(ml.weight AS CHAR) ORDER BY l.log_time DESC), ",", 1
					  ) as current_weight'
                ),
                $this->connection->raw( 'count(DISTINCT FROM_UNIXTIME(ml.timestamp/1000,"%Y-%m-%d")) as no_of_days_weight_logged' ),

                $this->connection->raw( 'count(DISTINCT IF( (m.owner = "PATIENT"),m.message_id,NULL) ) as sent_msg' ),

                $this->connection->raw( '
					count(DISTINCT
						IF( (l.log_type = "Meal" AND (ORD(l.is_suggested) = 0 OR l.is_suggested IS NULL) ), l.log_id, NULL)
					) as no_of_meal_log' ),
                $this->connection->raw( 'count(DISTINCT ff.filled_form_id) as no_of_goal_logged ' ),
                $this->connection->raw( 'count(DISTINCT o2o.session_id) as one_to_one_sessions' )
            ])
            ->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
            ->leftJoin( 'log AS l', function ( $q ) use ( $start, $end ){
                $q->on( 'l.patient_id', '=', 'p.patient_id' );
                $q->where( 'l.log_date_time', '>=', $start->format( 'Y-m-d H:i:s' ) );
                $q->where( 'l.log_date_time', '<', $end->format( 'Y-m-d H:i:s' ) );
            } )
            ->leftJoin( 'misc_log AS ml', function ( $q ) {
                $q->on( 'ml.log_id', '=', 'l.log_id' )
                    ->where( 'l.log_type', '=', 'Weight' );
            } )
            ->leftJoin( $this->connection->raw( ' (SELECT patient_id,
			    count(*) AS no_of_synced
				FROM activity_log_summary
				WHERE timestamp >= ' . ( $start->timestamp * 1000 ) . ' AND timestamp < '.($end->timestamp * 1000). '
				GROUP BY patient_id) AS mal
			' ), 'p.patient_id', '=', 'mal.patient_id' )
            ->leftJoin( 'message as m', function($q) use ( $start, $end ){
                $q->on( 'm.patient_id', '=', 'p.patient_id' );
                $q->where( 'm.timestamp', '>=', $start->timestamp*1000 );
                $q->where( 'm.timestamp', '<', $end->timestamp*1000 );
            } )
            ->leftJoin( 'filled_form as ff', function ( $q ) use ( $start, $end ) {
                $q->on( 'ff.patient_id', '=', 'p.patient_id' );
                $q->where( 'ff.type', '=', 'GOAL' );
                $q->where( 'ff.created_date', '>=', $start->format( 'Y-m-d H:i:s' ) );
                $q->where( 'ff.created_date', '<', $end->format( 'Y-m-d H:i:s' ) );
            } )
            ->leftJoin( 'one_to_one_sessions as o2o', function ( $q ) use ( $start, $end ) {
                $q->on( 'o2o.patient_id', '=', 'p.patient_id' );
                $q->where( 'o2o.session_time', '>=', $start->timestamp*1000 );
                $q->where( 'o2o.session_time', '<', $end->timestamp*1000 );
            } )
            ->whereIn( 'p.patient_id', $patient_ids );

        return $query
            ->groupBy( 'p.patient_id' )
            ->get();
    }



    /**
     * get_weekly_participation_report_for_patient
     *
     * @param $facility_id
     * @param null|Carbon $start
     * @param null|Carbon $end
     * @param null $lead_coach
     *
     * @return array|static[]
     */
    public function get_weekly_participation_report_for_patient( $facility_id, $start = null, $end = null, $lead_coach = null) {
        if(!$start instanceof Carbon || !$end instanceof Carbon){
            $start = Carbon::today()->subWeek(1)->addDay(1);
            $end = null;
        }
        $query = $this->connection
            ->table( 'patient AS p' )
            ->select( [
                'f.facility_id',
                'f.name AS facility_name',
                'p.patient_id',
                'p.uuid',
                'p.mrn',
                'dt.name AS diabetes_type_name',
                'p.diabetes_type_id',
                'u.first_name',
                'u.last_name',
                'u.email',
                'u.user_id',
                // invite, commit and enrollment_date
                $this->connection->raw('FROM_UNIXTIME(p.invitation_sent_date/1000,"'.config('healthslate.mysql_default_date_format').'") AS invite_date'),
                $this->connection->raw('DATE_FORMAT(p.approval_date,"'.config('healthslate.mysql_default_date_format').'") AS commit_date'),
                $this->connection->raw('DATE_FORMAT(u.password_created_date,"'.config('healthslate.mysql_default_date_format').'") AS enrollment_date'),
                $this->connection->raw( 'IF(DATE(' . ( $end ? '"'.$end->format( 'Y-m-d H:i:s' ).'"' : 'now()' ) . ') > DATE(u.password_created_date), FLOOR(DATEDIFF(DATE(' . ( $end ? '"'.$end->format( 'Y-m-d H:i:s' ).'"' : 'now()' ) . '),DATE(u.password_created_date))/7), 0) as no_of_weeks_in_program' ),

                $this->connection->raw( 'concat(lcu.first_name, " " ,lcu.last_name) as lead_coach_name' ),
                $this->connection->raw( 'IF( (t.starting_weight IS NOT NULL AND t.starting_weight != 0), t.starting_weight, NULL) as starting_weight' ),
            ] )
            ->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
            ->leftJoin( 'diabetes_type AS dt', 'dt.diabetes_type_id', '=', 'p.diabetes_type_id' )
            ->leftJoin( 'facility AS f', 'f.facility_id', '=', 'p.facility_id' )
            ->leftJoin( 'provider AS lc', 'lc.provider_id', '=', 'p.lead_coach_id' )
            ->leftJoin( 'users AS lcu', 'lcu.user_id', '=', 'lc.user_id' )
            ->leftJoin( 'targets AS t', 'p.patient_id', '=', 't.patient_id' )
            //->where( 'u.email', 'not like', '\\_%' )
            ->whereRaw( $this->connection->raw('ORD(u.is_registration_completed) = 1') )
            ->where( 'p.mrn', 'not like', '%test%' )
            ->where( 'p.facility_id', '=', $facility_id );

        if(!empty($lead_coach)){
            $query->where('p.lead_coach_id', '=', $lead_coach);
        }
        return $query
            ->groupBy( 'p.patient_id' )
            ->get();
    }


    /**
     * get_weekly_participation_report_for_patient_by_patientids
     *
     * @param $facility_id
     * @param null|Carbon $start
     * @param null|Carbon $end
     * @param null $lead_coach
     *
     * @return array|static[]
     */
    public function get_weekly_participation_report_for_patient_by_patientids( $facility_id, $start = null, $end = null, $patient_ids = null) {
        if(!$start instanceof Carbon || !$end instanceof Carbon){
            $start = Carbon::today()->subWeek(1)->addDay(1);
            $end = null;
        }
        $query = $this->connection
            ->table( 'patient AS p' )
            ->select( [
                'p.patient_id',
                $this->connection->raw( ' "" AS no_of_weeks_logged_weight'),
                $this->connection->raw( 'count(DISTINCT l.log_id) as total_logs' ),
                // Manual Activity Info
                $this->connection->raw( '
					 FLOOR(
					 sum(IF(l.log_type = "ActivityMinute", al.minutes_performed, NULL))*count(DISTINCT IF(l.log_type = "ActivityMinute", al.activity_log_id, NULL))/count(IF(l.log_type = "ActivityMinute", al.activity_log_id, NULL))
					) as manual_activity_minutes' ),
                $this->connection->raw( 'count(DISTINCT IF(l.log_type = "ActivityMinute",FROM_UNIXTIME(l.log_time/1000,"%Y-%m-%d"), NULL)) as no_of_manual_activity_days' ),
                $this->connection->raw( '
					count(DISTINCT
						IF(l.log_type = "ActivityMinute", l.log_id, NULL)
					) as no_of_manual_activity_log' ),

                // Fitbit Activity Info
                $this->connection->raw( 'mal.no_of_synced AS no_of_fitbit_activity_logs' ),
                $this->connection->raw( 'mal.total_steps_synced' ),
                $this->connection->raw( 'mal.no_of_fitbit_activity_days' ),
                $this->connection->raw( 'FLOOR(mal.total_steps_synced/mal.no_of_fitbit_activity_days) AS avg_steps_per_day' ),

                $this->connection->raw( '
					  SUBSTRING_INDEX(
					    GROUP_CONCAT(CAST(ml.weight AS CHAR) ORDER BY l.log_time DESC), ",", 1
					  ) as current_weight'
                ),
                $this->connection->raw( 'count(DISTINCT FROM_UNIXTIME(ml.timestamp/1000,"%Y-%m-%d")) as no_of_days_weight_logged' ),

                // message info
                $this->connection->raw( 'count(DISTINCT IF( (m.owner = "PATIENT"),m.message_id,NULL) ) as sent_msg' ),

                // meal info
                $this->connection->raw( '
					count(DISTINCT
						IF( (l.log_type = "Meal" AND (ORD(l.is_suggested) = 0 OR l.is_suggested IS NULL) ), l.log_id, NULL)
					) as no_of_meal_log' ),
                //weight log count
                $this->connection->raw( '
					count(DISTINCT
						IF( (l.log_type = "Weight" AND (ORD(l.is_removed) = 0 OR l.is_removed IS NULL) ), l.log_id, NULL)
					) as no_of_weight_log' ),

                //water log count
                $this->connection->raw( '
					count(DISTINCT
						IF( (l.log_type = "Water" AND (ORD(l.is_removed) = 0 OR l.is_removed IS NULL) ), l.log_id, NULL)
					) as glasses_of_water_consumed' ),

                // goal and 1:1 sessions
                $this->connection->raw( 'count(DISTINCT ff.filled_form_id) as no_of_goal_logged ' ),
                $this->connection->raw( 'count(DISTINCT o2o.session_id) as one_to_one_sessions' )
            ] )

            ->leftJoin( 'log AS l', function ( $q ) use ( $start, $end ){
                $q->on( 'l.patient_id', '=', 'p.patient_id' );
                if($start){
                    $q->where( 'l.log_date_time', '>=', $start->format( 'Y-m-d H:i:s' ) );
                }
                if($end){
                    $q->where( 'l.log_date_time', '<', $end->format( 'Y-m-d H:i:s' ) );
                }
            } )
            ->leftJoin( 'activity_log AS al', 'al.log_id', '=', 'l.log_id' )
            ->leftJoin( 'misc_log AS ml', function ( $q ) {
                $q->on( 'ml.log_id', '=', 'l.log_id' )
                    ->where( 'l.log_type', '=', 'Weight' );
            } )
            ->leftJoin( $this->connection->raw( ' (SELECT patient_id,
				count(DISTINCT activity_log_summary_id) AS no_of_synced,
				SUM(steps) AS total_steps_synced,
				count(DISTINCT FROM_UNIXTIME(timestamp/1000,"%Y-%m-%d")) AS no_of_fitbit_activity_days,
				MAX(timestamp) AS last_timestamp,
				MAX(upload_Time) AS last_upload_Time
				FROM activity_log_summary
				WHERE timestamp >= ' . ( $start->timestamp * 1000 ) . '
				' . ( $end ? " AND timestamp < ".($end->timestamp * 1000)." " : '' ) . '
				AND attribute_type = "steps"
				GROUP BY patient_id) AS mal
			' ), 'p.patient_id', '=', 'mal.patient_id' )
            ->leftJoin( 'message as m', function($q) use ( $start, $end ){
                $q->on( 'm.patient_id', '=', 'p.patient_id' );
                if($start){
                    $q->where( 'm.timestamp', '>=', $start->timestamp*1000 );
                }
                if($end){
                    $q->where( 'm.timestamp', '<', $end->timestamp*1000 );
                }
            } )
            ->leftJoin( 'filled_form as ff', function ( $q ) use ( $start, $end ) {
                $q->on( 'ff.patient_id', '=', 'p.patient_id' );
                $q->where( 'ff.type', '=', 'GOAL' );
                if($start){
                    $q->where( 'ff.created_date', '>=', $start->format( 'Y-m-d H:i:s' ) );
                }
                if($end){
                    $q->where( 'ff.created_date', '<', $end->format( 'Y-m-d H:i:s' ) );
                }
            } )
            ->leftJoin( 'one_to_one_sessions as o2o', function ( $q ) use ( $start, $end ) {
                $q->on( 'o2o.patient_id', '=', 'p.patient_id' );
                if($start){
                    $q->where( 'o2o.session_time', '>=', $start->timestamp*1000 );
                }
                if($end){
                    $q->where( 'o2o.session_time', '<', $end->timestamp*1000 );
                }
            } )
            ->where( 'p.facility_id', '=', $facility_id )
            ->whereIn( 'p.patient_id', $patient_ids );


        return $query
            ->groupBy( 'p.patient_id' )
            ->get();
    }

}