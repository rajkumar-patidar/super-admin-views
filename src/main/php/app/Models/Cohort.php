<?php

namespace HealthSlateAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class Cohort extends Model {

	protected $table = 'cohort';
	protected $primaryKey = 'cohort_id';
	public $timestamps = false;
}
