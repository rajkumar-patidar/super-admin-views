<?php

namespace HealthSlateAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class MedicationLog extends Model {

	protected $table = 'medication_log';
	protected $primaryKey = 'medication_log_id';
	public $timestamps = false;

}
