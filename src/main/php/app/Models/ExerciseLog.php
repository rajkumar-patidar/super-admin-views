<?php

namespace HealthSlateAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class ExerciseLog extends Model {

	protected $table = 'exercise_log';
	protected $primaryKey = 'exercise_log_id';
	public $timestamps = false;

}
