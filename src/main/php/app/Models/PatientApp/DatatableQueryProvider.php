<?php
/**
 * Created by PhpStorm.
 * User: spatel
 * Date: 28/12/16
 */

namespace HealthSlateAdmin\Models\PatientApp;

use Carbon\Carbon;
use DB;
use Illuminate\Database\Query\Builder;

class DatatableQueryProvider {

	/**
	 * @var \Illuminate\Database\Connection
	 */
	protected $connection;

	/**
	 *
	 */
	function __construct() {
		$this->connection = DB::connection();
	}

	/**
	 * @param $facility_id
	 *
	 * @return Builder
	 */
	public function getMemberGroupReportQuery( $facility_id ) {
		$group_db_name = config( 'healthslate.group_db_name' );

		$query = $this->connection
			->table( 'patient AS p' )
			->select( [
				'g.name AS group_name',
				'p.patient_id as member_id',
				'p.mrn',
				'u.first_name AS member_first_name',
				'u.group_visibility',
				'lcu.first_name AS lifestyle_coach',
				'fcu.first_name AS food_coach',
				'fau.first_name AS facility_admin',
				$this->connection->raw( 'DATE_FORMAT(g.timestamp, "' . config( 'healthslate.mysql_default_date_format' ) . '" ) AS created_on' ),
				$this->connection->raw( 'DATE_FORMAT(u.password_created_date, "' . config( 'healthslate.mysql_default_date_format' ) . '" ) AS started_on' ),
				'g.is_person AS in_person',
			] )
			->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
			->leftJoin( 'diabetes_type AS dt', 'dt.diabetes_type_id', '=', 'p.diabetes_type_id' )
			->leftJoin( $this->connection->raw( '`' . $group_db_name . '`.group_member AS gm' ), 'u.user_id', '=', 'gm.user_id' )
			->leftJoin( $this->connection->raw( '`' . $group_db_name . '`.groups AS g' ), 'gm.group_id', '=', 'g.group_id' )
			->leftJoin( 'users AS lcu', 'lcu.user_id', '=', 'g.lead_user_id' )
			->leftJoin( 'users AS fcu', 'fcu.user_id', '=', 'g.primary_food_coach_id' )
			->leftJoin( 'users AS fau', 'fau.user_id', '=', '.facility_admin_id' )
			->where( 'u.email', 'not like', '\\_%' )
			->whereRaw( $this->connection->raw( 'ORD(u.is_registration_completed) = 1' ) )
			->where( 'p.mrn', 'not like', '%test%' )
			->where( 'p.facility_id', '=', $facility_id )
			->groupBy( 'p.patient_id' );

		return $query;
	}

	/**
	 * Returns a Closure, responsible for search in report table
	 * @return callable|null
	 */
	public function getMemberGroupReportSearchClosure() {

		return function ( $query ) {
			if ( $keyword = request()->get( 'search' )['value'] ) {

				$query->where( function ( $q ) use ( $keyword ) {
					$q->where( 'g.name', 'like', "%$keyword%" );
					$q->orWhere( 'p.patient_id', 'like', "%$keyword%" );
					$q->orWhere( 'p.mrn', 'like', "%$keyword%" );
					$q->orWhere( 'u.first_name', 'like', "%$keyword%" );
					$q->orWhere( 'u.group_visibility', 'like', "%$keyword%" );
					$q->orWhere( 'lcu.first_name', 'like', "%$keyword%" );
					$q->orWhere( 'fcu.first_name', 'like', "%$keyword%" );
					$q->orWhere( 'fau.first_name', 'like', "%$keyword%" );
					$q->orWhereRaw( "DATE_FORMAT(g.timestamp,'" . config( 'healthslate.mysql_default_date_format' ) . "') like ? ", [ "%$keyword%" ] );
					$q->orWhereRaw( "DATE_FORMAT(u.password_created_date,'" . config( 'healthslate.mysql_default_date_format' ) . "') like ? ", [ "%$keyword%" ] );
					if ( ( strtolower( $keyword ) == 'yes' ) ) {
						$q->orWhere( 'g.is_person', '=', 1 );
					} else {
						$q->orWhere( 'g.is_person', 'like', "%$keyword%" );
					}
				} );
			}
		};
	}

	/**
	 * get members who are missing facility mapping
	 *
	 * @return Builder
	 */
	public function getMemberMissingFacilityMappingQuery() {
		$query = $this->connection
			->table( 'patient AS p' )
			->select( [
				'p.patient_id as member_id',
				'u.first_name AS member_first_name',
				'p.mrn',
				'dt.name AS diabetes_type_name',
				$this->connection->raw( 'ORD(u.is_registration_completed) AS is_registration_completed' )
			] )
			->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
			->leftJoin( 'diabetes_type AS dt', 'dt.diabetes_type_id', '=', 'p.diabetes_type_id' )
			->where( 'u.email', 'not like', '\\_%' )
			->whereNull( 'p.facility_id' );

		return $query;
	}

	/**
	 * Returns a Closure, responsible for search in report table
	 *
	 * @return callable|null
	 */
	public function getMemberMissingFacilityMappingReportSearchClosure() {

		return function ( $query ) {
			if ( $keyword = request()->get( 'search' )['value'] ) {

				$query->where( function ( $q ) use ( $keyword ) {
					$q->where( 'p.patient_id', 'like', "%$keyword%" );
					$q->orWhere( 'u.first_name', 'like', "%$keyword%" );
					$q->orWhere( 'p.mrn', 'like', "%$keyword%" );
					$q->orWhere( 'dt.name', 'like', "%$keyword%" );
					if ( ( strtolower( $keyword ) == 'yes' ) ) {
						$q->orWhereRaw( ' ORD(u.is_registration_completed) = 1 ' );
					} else if ( ( strtolower( $keyword ) == '-' ) ) {
						$q->orWhereRaw( ' ORD(u.is_registration_completed) = 0 ' );
					} else {
						// can't use keyword here, otherwise each keyword will be counted as `TRUE`
					}
				} );
			}
		};
	}

	/**
	 * get members gone inactive
	 *
	 * @param int $days
	 *
	 * @return Builder
	 */
	public function getMemberGoneInactiveReportQuery($days = 7)
	{
		$query = $this->connection
			->table( 'patient AS p' )
			->select( [
				$this->connection->raw( 'p.patient_id as member_id' ),
				$this->connection->raw( 'concat(u.first_name, " " ,u.last_name) as member_name' ),
				$this->connection->raw( 'datediff(now(),from_unixtime(l.log_time/1000)) as last_log_days' ),
				$this->connection->raw( 'from_unixtime(max(l.log_time)/1000) as last_time' ),
				$this->connection->raw( 'concat(pru.first_name, " " ,pru.last_name) as lifestyle_coach_name' ),
				$this->connection->raw( 'pr.provider_id as lifestyle_coach_id' ),
				$this->connection->raw( 'pr.facility_id as lifestyle_coach_facility' ),
				$this->connection->raw( 'ORD(u.is_registration_completed) as is_registration_completed' ),
				'p.mrn',
				'u.email',

			] )
			->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
			->leftJoin( 'log AS l', 'l.patient_id', '=', 'p.patient_id' )
			->leftJoin( 'provider AS pr', 'pr.provider_id', '=', 'p.lead_coach_id' )
			->leftJoin( 'users AS pru', 'pru.user_id', '=', 'pr.user_id' )
			->groupBy( 'p.patient_id' )
			->having( 'u.email', 'not like', '\\_%' )
			->having( 'p.mrn', 'not like', '%test%' )
			->having( 'is_registration_completed', '=', 1 )
			->havingRaw( 'datediff(now(),last_time)> ? ', [ $days ] );

		return $query;
	}

	/**
	 * Returns a Closure, responsible for search in report table
	 *
	 * @return callable|null
	 */
	public function getMemberGoneInactiveReportSearchClosure()
	{

		return function ( $query ) {
			if ($keyword = request()->get( 'search' )['value']) {

				$query->havingRaw( $this->connection->raw( ' (
					member_id like ?
					OR member_name like ?
					OR last_log_days like ?
					OR last_time like ?
					OR lifestyle_coach_name like ?
					OR lifestyle_coach_id like ?
					OR lifestyle_coach_facility like ?
				)' ), [ "%$keyword%", "%$keyword%", "%$keyword%", "%$keyword%", "%$keyword%", "%$keyword%", "%$keyword%", ] );
			}
		};
	}
}