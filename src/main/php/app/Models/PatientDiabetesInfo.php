<?php

namespace HealthSlateAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class PatientDiabetesInfo extends Model {

	protected $table = 'patient_diabetes_info';
	protected $primaryKey = 'diabetes_info_id';
	public $timestamps = false;

	public function patient() {
		return $this->belongsTo( 'HealthSlateAdmin\Models\Patient', 'patient_id', 'patient_id' );
	}
}
