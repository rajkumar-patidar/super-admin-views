<?php

namespace HealthSlateAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model {

	protected $table = 'users';

	protected $primaryKey = 'user_id';

	public $timestamps = false;

	protected $fillable = array('email', 'first_name', 'last_name');

	public function getFullNameAttribute() {
		return $this->first_name . ' ' . $this->last_name;
	}

	public function getAddressInfoAttribute() {
		return $this->address . ' ' . $this->city. ' ' . $this->state;
	}

	public function scopeIsRegistrationCompleted( $query, $is_registration_completed = true ){
		if($is_registration_completed){
			// registered 1
			return $query->whereRaw($this->getConnection()->raw('ORD(is_registration_completed) = 1'));
		}else{
			// not yet registered (null,0)
			return $query->whereRaw($this->getConnection()->raw('( (is_registration_completed IS NULL) OR ORD(is_registration_completed) = 0 )'));
		}
	}

	public function authority() {
		return $this->hasOne( 'HealthSlateAdmin\Models\UserRole', 'user_id', 'user_id' )->whereNotNull('user_id');
	}

	public function appAccessLogs() {
		return $this->hasMany( 'HealthSlateAdmin\Models\AppAccessLog', 'user_id' ,'user_id');
	}

	public function coachGoalAssessments() {
		return $this->hasMany( 'HealthSlateAdmin\Models\CoachGoalAssessment', 'user_id' ,'user_id');
	}

	public function emailNotificationMessages() {
		return $this->hasMany( 'HealthSlateAdmin\Models\EmailNotificationMessage', 'user_id' ,'user_id');
	}

	// Here in this table user_id is for provider
	public function messages() {
		return $this->hasMany( 'HealthSlateAdmin\Models\Message', 'user_id' ,'user_id');
	}

	// a user will have either patient or provider
	public function patient() {
		return $this->hasOne( 'HealthSlateAdmin\Models\Patient', 'user_id', 'user_id' )->whereNotNull('user_id');
	}

	// a user will have either patient or provider
	public function provider() {
		return $this->hasOne( 'HealthSlateAdmin\Models\Provider', 'user_id', 'user_id' );
	}

	// hasOne care_giver ?
	// hasMany log.
	// hasMany server_log?
}