<?php

namespace HealthSlateAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class InsulinType extends Model {
	protected $table = 'insulin_type';
	protected $primaryKey = 'insulin_type_id';
	public $timestamps = false;
}
