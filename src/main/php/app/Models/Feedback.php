<?php

namespace HealthSlateAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model {

	protected $table = 'feedback';
	protected $primaryKey = 'feedback_id';
	public $timestamps = false;
}
