<?php

namespace HealthSlateAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class MiscLog extends Model {

	protected $table = 'misc_log';
	protected $primaryKey = 'misc_log_id';
	public $timestamps = false;

}
