<?php

/**
 * Created by PhpStorm.
 * User: spatel
 * Date: 6/5/16
 * Time: 5:55 PM
 */

namespace HealthSlateAdmin\Models\Groups;

use Carbon\Carbon;
use DB;

class HealthslateGroups {

    /**
     * @var \Illuminate\Database\Connection
     */
    protected $connection;

    /**
     *
     */
    function __construct() {
        $this->connection = DB::connection('groups');
    }

    /**
     * @param $user_id
     * @param $from_date
     * @param $week
     *
     * @return array|static[]
     */
    public function get_user_post_of_week_from($user_id, $from_date, $week) {
        return $this->connection->table('post AS p')
                        ->where('p.user_id', '=', $user_id)
                        ->whereRaw($this->connection->raw('CEIL( DATEDIFF( p.timestamp , ? ) /7 ) = ? '), [$from_date->format('Y-m-d H:i:s'), $week])
                        ->get();
    }

    public function get_patient_group_wall_post_count($user_ids, $start = null, $end = null) {
        $query = $this->connection
                ->table('post')
                ->select([
                        'user_id',
                        $this->connection->raw('COUNT(post_id) as post_count'),
                ]);
                if ($start) {
                    $query->where('timestamp', '>=', $start->format('Y-m-d H:i:s'));
                }
                if ($end) {
                    $query->where('timestamp', '<', $end->format('Y-m-d H:i:s'));
                }
                $query->whereIn('user_id', $user_ids)->groupBy('user_id');

                return $query->get();
            }

}
