<?php

namespace HealthSlateAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class ServerLog extends Model {

	protected $table = 'server_log';
	protected $primaryKey = 'server_log_id';
	public $timestamps = false;
}
