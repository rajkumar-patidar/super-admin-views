<?php

namespace HealthSlateAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class ReportError extends Model {

	protected $table = 'report_error';
	protected $primaryKey = 'report_error_id';
	public $timestamps = false;

}
