<?php

namespace HealthSlateAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class Facility extends Model {

	protected $table = 'facility';

	protected $primaryKey = 'facility_id';

	public $timestamps = false;

	public function getAddressInfoAttribute() {
		return $this->address . ' ' . $this->city. ' ' . $this->state;
	}

	public function providers() {
		return $this->hasMany( 'HealthSlateAdmin\Models\Provider', 'facility_id', 'facility_id' );
	}

	public function patients() {
		//return $this->belongsToMany( 'HealthSlateAdmin\Models\Patient', 'patient_facility', 'facilities_facility_id', 'patients_patient_id' );
		return $this->hasMany( 'HealthSlateAdmin\Models\Patient', 'facility_id', 'facility_id' );
	}

	/**
	 * belongsToMany relation for fetching counts only
	 * @return mixed
	 */
	public function patientCountRelation() {
		return $this->belongsToMany( 'HealthSlateAdmin\Models\Patient', 'patient_facility', 'facilities_facility_id', 'patients_patient_id')
		            ->select($this->getConnection()->raw('count(*) as aggregate'))->groupBy('facilities_facility_id');
	}

	/**
	 * get patient count for a facility
	 * @return int
	 */
	public function getPatientCountAttribute() {
		if ( ! array_key_exists('patientCountRelation', $this->relations)) $this->load('patientCountRelation');

		$related = $this->getRelation('patientCountRelation')->first();
		return ($related) ? $related->aggregate : 0;
	}

	public function forms() {
		return $this->hasMany( 'HealthSlateAdmin\Models\Form', 'facility_id', 'facility_id' );
	}

	public function formSchedules() {
		return $this->hasMany( 'HealthSlateAdmin\Models\FormSchedule', 'facility_id', 'facility_id' );
	}
        
}