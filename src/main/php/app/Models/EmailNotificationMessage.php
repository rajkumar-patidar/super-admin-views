<?php

namespace HealthSlateAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class EmailNotificationMessage extends Model {
	protected $table = 'email_notification_message';
	protected $primaryKey = 'email_notification_id';
	public $timestamps = false;
}
