<?php

namespace HealthSlateAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class DiabetesType extends Model {
	protected $table = 'diabetes_type';
	protected $primaryKey = 'diabetes_type_id';
	public $timestamps = false;

}
