<?php

namespace HealthSlateAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class OneToOneSession extends Model {

	protected $table = 'one_to_one_sessions';
	protected $primaryKey = 'session_id';
	public $timestamps = false;

	public function patient() {
		return $this->belongsTo( 'HealthSlateAdmin\Models\Patient', 'patient_id', 'patient_id' );
	}

	public function provider() {
		return $this->belongsTo( 'HealthSlateAdmin\Models\Provider', 'provider_id', 'provider_id' );
	}
}
