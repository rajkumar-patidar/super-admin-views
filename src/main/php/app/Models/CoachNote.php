<?php

namespace HealthSlateAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class CoachNote extends Model {

	protected $table = 'coach_notes';
	protected $primaryKey = 'coach_notes_id';
	public $timestamps = false;

	public function patient() {
		return $this->belongsTo( 'HealthSlateAdmin\Models\Patient', 'patient_id', 'patient_id' );
	}

	public function provider() {
		return $this->belongsTo( 'HealthSlateAdmin\Models\Provider', 'provider_id', 'provider_id' );
	}
}
