<?php

namespace HealthSlateAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceFilterLog extends Model {

	protected $table = 'service_filter_log';
	protected $primaryKey = 'logging_id';
	public $timestamps = false;

	public function patient() {
		return $this->belongsTo( 'HealthSlateAdmin\Models\Patient', 'patient_id', 'patient_id' );
	}
}
