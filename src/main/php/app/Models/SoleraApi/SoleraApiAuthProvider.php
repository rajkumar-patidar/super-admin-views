<?php
/**
 * User: spatel
 * Date: 4/1/16
 */

namespace HealthSlateAdmin\Models\SoleraApi;

use HealthSlateAdmin\Helpers\RequestHelper;
use Cookie;
use Log;
use DB;
use Carbon\Carbon;


class SoleraApiAuthProvider {

	/**
	 * @var RequestHelper
	 */
	protected $requestHelper;

	/**
	 * @var \Illuminate\Database\Connection
	 */
	protected $connection;

	/**
	 * @param RequestHelper $requestHelper
	 */
	public function __construct( RequestHelper $requestHelper ) {
		$this->requestHelper = $requestHelper;
		$this->connection = DB::connection();
	}

	/**
	 * @param $username
	 * @param $password
	 *
	 * @return object
	 */

	public function getAccessToken() {
		$url 			= config( 'solera.solera_url' );
		$grant_type 	= config( 'solera.grant_type_refresh' );
		$client_id 		= config( 'solera.solera_client_id' );
		$client_secret 	= config( 'solera.solera_client_secret' );
		$refresh_token 	= config( 'solera.refresh_token' );
		info('Calling Refresh Access Token API');
		$response = $this->requestHelper->makeRequestSoleraApi( 'POST', $url, [
			"grant_type" => $grant_type,
			"client_id" => $client_id,
			"refresh_token" => $refresh_token,
		]);

		if ( $response->statusCode === 400 ) {

			// Bad credentials
			info('Bad: ', (array) $response->body);
			return false;
		}
		else
		{
			info('Solera Refresh Token Response: ', (array) $response->body);
			return $response->body;
		}
	}



	/**
	 * @param $username
	 * @param $password
	 *
	 * @return object
	 */

	public function getDailyParticipantStatus( $accessToken , $startDate, $endDate) {
		$url = config( 'solera.daily_Participant_Status' );

		info('Calling getDailyParticipantStatus function to get list of Members from solera api.');

		$response = $this->requestHelper->makeRequestSoleraApi( 'GET', $url, [
			"startDate" => $startDate,
			"endTime" => $endDate,
			"includeBiometric" => 'true',
		], [
			'Authorization' => 'Bearer ' . $accessToken
		] );

		if ( $response->statusCode === 400 ) {

			// Bad credentials
			info('Bad: ', (array) $response->body);
			return false;
		}
		else
		{
			info('Daily Participant Status Response: ',  (array) $response->body );
			return $response->body;
		}
	}


	/**
	 * @param $username
	 * @param $password
	 *
	 * @return object
	 */

	public function getParticipantEnrollmentStatus( $accessToken , $startDate) {
		$url = config( 'solera.enrollmentDataToSolera' );

		info('Calling ParticipantEnrollmentStatus function to get list of Diserolled Members from solera api.');

		$response = $this->requestHelper->makeRequestSoleraApi( 'GET', $url, [
			"startDate" => $startDate,
		], [
			'Authorization' => 'Bearer ' . $accessToken
		] );

		if ( $response->statusCode === 400 ) {

			// Bad credentials
			info('Bad: ', (array) $response->body);
			return false;
		}
		else
		{
			info('ParticipantEnrollmentStatus Response: ',  (array) $response->body );
			return $response->body;
		}
	}


	/**
	 * @param $data, $access_token
	 *
	 * @return bool
	 */
	public function postEnrollmentDataToSolera($accessToken, $data)
	{
		$url = config( 'solera.enrollmentDataToSolera' );

		$response = $this->requestHelper->makeRequestSoleraApiPost( 'POST', $url, $data, [
			'Content-Type' => 'application/json',
			'Authorization' => 'Bearer ' . $accessToken
		] );

		if ( $response->statusCode === 400 ) {
			// Bad credentials
			info('Bad: ', (array) $response->body);
			return false;
		}
		else
		{
			info('Enrollment Data To Solera Response: ', (array) $response->body);
			return $response->body;
		}
	}



	/**
	 * @param $data, $access_token
	 *
	 * @return bool
	 */
	public function postEngagementDataToSolera($accessToken, $data)
	{
		$url = config( 'solera.engagementDataToSolera' );

		$response = $this->requestHelper->makeRequestSoleraApiPost( 'POST', $url, $data, [
			'Content-Type' => 'application/json',
			'Authorization' => 'Bearer ' . $accessToken
		] );

		if ( $response->statusCode === 400 ) {
			// Bad credentials
			info('Bad: ', (array) $response->body);
			return false;
		}
		else
		{
			info('Engagement Data To Solera Response: ', (array) $response->body);
			return $response->body;
		}
	}



	public function getEnrollmentDataToSolera($facilityId, $days)
	{
		$binding['facility_id'] = $facilityId;
		$query[] = "select patient.patient_id,patient.uuid,patient.payer_id,patient.mrn,patient.is_deleted,
						from_unixtime(patient.invitation_sent_date/1000) AS invitation_sent_date,
						IF(password is not null,'set','none') as password,registration_date,
						timestampdiff(day,users.registration_date,curdate()) as days_since_reg,
						timestampdiff(day,from_unixtime(password_modified_time/1000),curdate()) as days_since_activate
						from users,patient where
						users.user_id=patient.user_id AND
						patient.mrn NOT LIKE '%test%' AND
						patient.facility_id= :facility_id  ";

		if ( $days > 0 ) {
			$query[]         = "AND ( timestampdiff(day,users.registration_date,curdate()) < :days )";
			$binding['days'] = $days;
		}
		$query[] = "Order by patient.patient_id asc";

		return $this->connection->select( implode(" ", $query), $binding );
	}



	/**
	 * get_weekly_participation_report_for_duration
	 *
	 * @param $facility_id
	 * @param null|Carbon $start
	 * @param null|Carbon $end
	 * @param null $lead_coach
	 *
	 * @return array|static[]
	 */
	public function get_weekly_participation_report_for_duration( $facility_id, $start = null, $end = null, $lead_coach = null) {
		if(!$start instanceof Carbon || !$end instanceof Carbon){
			$start = Carbon::today()->subWeek(1)->addDay(1);
			$end = null;
		}
		$query = $this->connection
			->table( 'patient AS p' )
			->select( [
				'p.facility_id',
				'f.name AS facility_name',
				'p.patient_id',
				'p.uuid',
				'p.mrn',
				'dt.name AS diabetes_type_name',
				'p.diabetes_type_id',
				'u.first_name',
				'u.last_name',
				'u.email',
				'u.user_id',
				// invite, commit and enrollment_date
				$this->connection->raw('IF(u.offset_key != "" , CONVERT_TZ("'.$start->format( 'Y-m-d H:i:s' ).'","UTC",u.offset_key) , "'.$start->format( 'Y-m-d H:i:s' ).'") as user_time'),
				$this->connection->raw('FROM_UNIXTIME(p.invitation_sent_date/1000,"'.config('healthslate.mysql_default_date_format').'") AS invite_date'),
				$this->connection->raw('DATE_FORMAT(p.approval_date,"'.config('healthslate.mysql_default_date_format').'") AS commit_date'),
				$this->connection->raw('DATE_FORMAT(u.password_created_date,"'.config('healthslate.mysql_default_date_format').'") AS enrollment_date'),
				$this->connection->raw( 'IF(DATE(' . ( $end ? '"'.$end->format( 'Y-m-d H:i:s' ).'"' : 'now()' ) . ') > DATE(u.password_created_date), FLOOR(DATEDIFF(DATE(' . ( $end ? '"'.$end->format( 'Y-m-d H:i:s' ).'"' : 'now()' ) . '),DATE(u.password_created_date))/7), 0) as no_of_weeks_in_program' ),
				$this->connection->raw( ' "" AS no_of_weeks_logged_weight'),
				$this->connection->raw( 'concat(lcu.first_name, " " ,lcu.last_name) as lead_coach_name' ),
				$this->connection->raw( 'count(DISTINCT l.log_id) as total_logs' ),

				// Manual Activity Info
				$this->connection->raw( '
					 FLOOR(
					 sum(IF(l.log_type = "ActivityMinute", al.minutes_performed, NULL))*count(DISTINCT IF(l.log_type = "ActivityMinute", al.activity_log_id, NULL))/count(IF(l.log_type = "ActivityMinute", al.activity_log_id, NULL))
					) as manual_activity_minutes' ),
				$this->connection->raw( 'count(DISTINCT IF(l.log_type = "ActivityMinute",FROM_UNIXTIME(l.log_time/1000,"%Y-%m-%d"), NULL)) as no_of_manual_activity_days' ),
				$this->connection->raw( '
					count(DISTINCT
						IF(l.log_type = "ActivityMinute", l.log_id, NULL)
					) as no_of_manual_activity_log' ),

				// Fitbit Activity Info
				$this->connection->raw( 'mal.no_of_synced AS no_of_fitbit_activity_logs' ),
				$this->connection->raw( 'mal.total_steps_synced' ),
				$this->connection->raw( 'mal.no_of_fitbit_activity_days' ),
				$this->connection->raw( 'FLOOR(mal.total_steps_synced/mal.no_of_fitbit_activity_days) AS avg_steps_per_day' ),

				// weight info
				$this->connection->raw( 'IF( (t.starting_weight IS NOT NULL AND t.starting_weight != 0), t.starting_weight, NULL) as starting_weight' ),
				$this->connection->raw( '
					  SUBSTRING_INDEX(
					    GROUP_CONCAT(CAST(ml.weight AS CHAR) ORDER BY l.log_time DESC), ",", 1
					  ) as current_weight'
				),
				$this->connection->raw( 'count(DISTINCT FROM_UNIXTIME(ml.timestamp/1000,"%Y-%m-%d")) as no_of_days_weight_logged' ),

				// message info
				$this->connection->raw( 'count(DISTINCT IF( (m.owner = "PATIENT"),m.message_id,NULL) ) as sent_msg' ),

				// meal info
				$this->connection->raw( '
					count(DISTINCT
						IF( (l.log_type = "Meal" AND (ORD(l.is_suggested) = 0 OR l.is_suggested IS NULL) ), l.log_id, NULL)
					) as no_of_meal_log' ),
				//weight log count
				$this->connection->raw( '
					count(DISTINCT
						IF( (l.log_type = "Weight" AND (ORD(l.is_removed) = 0 OR l.is_removed IS NULL) ), l.log_id, NULL)
					) as no_of_weight_log' ),

				// goal and 1:1 sessions
				$this->connection->raw( 'count(DISTINCT ff.filled_form_id) as no_of_goal_logged ' ),
				$this->connection->raw( 'count(DISTINCT o2o.session_id) as one_to_one_sessions' )
			] )
			->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
			->leftJoin( 'diabetes_type AS dt', 'dt.diabetes_type_id', '=', 'p.diabetes_type_id' )
			->leftJoin( 'facility AS f', 'f.facility_id', '=', 'p.facility_id' )

			->leftJoin( 'provider AS lc', 'lc.provider_id', '=', 'p.lead_coach_id' )
			->leftJoin( 'users AS lcu', 'lcu.user_id', '=', 'lc.user_id' )



			->leftJoin( 'targets AS t', 'p.patient_id', '=', 't.patient_id' )
			->leftJoin( 'log AS l', function ( $q ) use ( $start, $end ){
				$q->on( 'l.patient_id', '=', 'p.patient_id' );
				if($start){
					$q->where( 'l.log_date_time', '>=', $start->format( 'Y-m-d H:i:s' ) );
				}
				if($end){
					$q->where( 'l.log_date_time', '<', $end->format( 'Y-m-d H:i:s' ) );
				}
			} )
			->leftJoin( 'activity_log AS al', 'al.log_id', '=', 'l.log_id' )
			->leftJoin( 'misc_log AS ml', function ( $q ) {
				$q->on( 'ml.log_id', '=', 'l.log_id' )
				  ->where( 'l.log_type', '=', 'Weight' );
			} )
			->leftJoin( $this->connection->raw( ' (SELECT patient_id,
				count(DISTINCT activity_log_summary_id) AS no_of_synced,
				SUM(steps) AS total_steps_synced,
				count(DISTINCT FROM_UNIXTIME(timestamp/1000,"%Y-%m-%d")) AS no_of_fitbit_activity_days,
				MAX(timestamp) AS last_timestamp,
				MAX(upload_Time) AS last_upload_Time
				FROM activity_log_summary
				WHERE timestamp >= ' . ( $start->timestamp * 1000 ) . '
				' . ( $end ? " AND timestamp < ".($end->timestamp * 1000)." " : '' ) . '
				AND attribute_type = "steps"
				GROUP BY patient_id) AS mal
			' ), 'p.patient_id', '=', 'mal.patient_id' )
			->leftJoin( 'message as m', function($q) use ( $start, $end ){
				$q->on( 'm.patient_id', '=', 'p.patient_id' );
				if($start){
					$q->where( 'm.timestamp', '>=', $start->timestamp*1000 );
				}
				if($end){
					$q->where( 'm.timestamp', '<', $end->timestamp*1000 );
				}
			} )
				->leftJoin( 'filled_form as ff', function ( $q ) use ( $start, $end ) {
				$q->on( 'ff.patient_id', '=', 'p.patient_id' );
				$q->where( 'ff.type', '=', 'GOAL' );
				if($start){
					$q->where( 'ff.created_date', '>=', $start->format( 'Y-m-d H:i:s' ) );
				}
				if($end){
					$q->where( 'ff.created_date', '<', $end->format( 'Y-m-d H:i:s' ) );
				}
			} )
			->leftJoin( 'one_to_one_sessions as o2o', function ( $q ) use ( $start, $end ) {
				$q->on( 'o2o.patient_id', '=', 'p.patient_id' );
				if($start){
					$q->where( 'o2o.session_time', '>=', $start->timestamp*1000 );
				}
				if($end){
					$q->where( 'o2o.session_time', '<', $end->timestamp*1000 );
				}
			} )
			->where( 'u.email', 'not like', '\\_%' )
			->whereRaw( $this->connection->raw('ORD(u.is_registration_completed) = 1') )
			->where( 'p.mrn', 'not like', '%test%' )
			->where( 'p.facility_id', '=', $facility_id );

		if(!empty($lead_coach)){
			$query->where('p.lead_coach_id', '=', $lead_coach);
		}
		return $query
			->groupBy( 'p.patient_id' )
			->get();
	}



    /**
     * @param $username
     * @param $password
     *
     * @return object
     */

    public function getPatientActivitySteps($patient_detail, $days = false) {
        $url 			= config( 'solera.fitbit_url' );
        $fitbit_days 	= $days != false ? config( 'solera.fitbit_days_7' ) : config( 'solera.fitbit_days' );
        info('Calling Patient Activity Steps API');
        $url = $url . $patient_detail->third_party_user_id . '/activities/steps/date/today/' . $fitbit_days . '.json';
        $response = $this->requestHelper->makeRequestSoleraApi( 'GET', $url, [],
        [
            'Authorization' => 'Bearer ' . $patient_detail->access_token
        ] );

        if ( $response->statusCode === 400 ) {

            // Bad credentials
            info('Bad: ', (array) $response->body);
            return false;
        }
        else
        {
            info('Patient Activity Steps Response: ',  (array) $response->body );
            return $response->body;
        }
    }


    /**
     * @param $username
     * @param $password
     *
     * @return object
     */

    public function getPatientActivityStepsSignleDays($patient_detail, $date = false) {
        $url = config( 'solera.fitbit_url' );
        info('Calling Patient Activity Steps API');
        $url = $url . $patient_detail->third_party_user_id . '/activities/steps/date/' . $date . '/1d.json';
        $response = $this->requestHelper->makeRequestSoleraApi( 'GET', $url, [],
            [
                'Authorization' => 'Bearer ' . $patient_detail->access_token
            ] );

        if ( $response->statusCode === 400 ) {

            // Bad credentials
            info('Bad: ', (array) $response->body);
            return false;
        }
        else
        {
            info('Patient Activity Steps Response: ',  (array) $response->body );
            return $response->body;
        }
    }



    /**
     * @param $username
     * @param $password
     *
     * @return object
     */

    public function getPatientNewAccessToken($patient_detail) {
        $url                    = config( 'solera.fitbit_access_token' );
        $fitbit_client_secret   = config( 'solera.fitbit_client_secret' );
        info('Calling Fitbit for New Access Token Generate API');
        $url = $url . $patient_detail->refresh_token;

        $response = $this->requestHelper->makeRequestSoleraApiPost( 'POST', $url, [], [
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Authorization' => 'Basic ' . $fitbit_client_secret
        ] );


        if ( $response->statusCode === 400 ) {
            info('Bad: ', (array) $response->body);
            return $response->body;
        }
        else
        {
            info('Patient New Access Token: ',  (array) $response->body );
            return $response->body;
        }
    }


}