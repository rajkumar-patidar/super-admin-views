<?php

namespace HealthSlateAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model {

	protected $table = 'provider';

	protected $primaryKey = 'provider_id';

	public $timestamps = false;

	public function facility() {
		return $this->hasOne( 'HealthSlateAdmin\Models\Facility', 'facility_id', 'facility_id' );
	}

	public function patients() {
		return $this->belongsToMany( 'HealthSlateAdmin\Models\Patient', 'provider_patient', 'providers_provider_id', 'patients_patient_id');
	}

	/**
	 * belongsToMany relation for fetching counts only
	 * @return mixed
	 */
	public function patientCountRelation() {
		return $this->belongsToMany( 'HealthSlateAdmin\Models\Patient', 'provider_patient', 'providers_provider_id', 'patients_patient_id')
			->select($this->getConnection()->raw('count(*) as aggregate'))->groupBy('providers_provider_id');
	}

	/**
	 * get patient count for a provider
	 * @return int
	 */
	public function getPatientCountAttribute() {
		if ( ! array_key_exists('patientCountRelation', $this->relations)) $this->load('patientCountRelation');

		$related = $this->getRelation('patientCountRelation')->first();
		return ($related) ? $related->aggregate : 0;
	}

	public function user() {
		return $this->belongsTo( 'HealthSlateAdmin\Models\User', 'user_id', 'user_id' );
	}

	// coach has many notes for a patient
	public function coachNotes() {
		return $this->hasMany( 'HealthSlateAdmin\Models\CoachNote', 'provider_id' ,'provider_id');
	}

	public function oneToOneSessions() {
		return $this->hasMany( 'HealthSlateAdmin\Models\OneToOneSession', 'provider_id', 'provider_id' );
	}

}