<?php

namespace HealthSlateAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class LogMood extends Model {

	protected $table = 'log_mood';
	protected $primaryKey = 'log_mood_id';
	public $timestamps = false;
}
