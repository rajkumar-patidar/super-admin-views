<?php

namespace HealthSlateAdmin\Models;

use Illuminate\Database\Eloquent\Model;

class PatientMotivationImage extends Model {

	protected $table = 'patient_motivation_image';
	protected $primaryKey = 'patient_motivation_id';
	public $timestamps = false;

	public function patient() {
		return $this->belongsTo( 'HealthSlateAdmin\Models\Patient', 'patient_id', 'patient_id' );
	}

}
