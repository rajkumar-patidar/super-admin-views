<?php

namespace HealthSlateAdmin\Models\ReportService;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Schedule extends Model {

	protected $connection = 'report_service';
	protected $table = 'schedule';
	protected $primaryKey = 'schedule_id';
	public $timestamps = false;

	protected $dates = ['start_date','end_date'];

	public function reportTrigger() {
		return $this->hasOne( 'HealthSlateAdmin\Models\ReportService\ReportTriggerQuery', 'report_trigger_query_id', 'report_trigger' );
	}

	/**
	 * @return bool
	 */
	public function getIsActiveAttribute(){
		$is_active = false;
		$now = Carbon::now();
		if( !empty($this->start_date) && $this->start_date->lt($now) ){
			$is_active = true;
		}
		if(!empty($this->end_date) && $this->end_date->lt($now) ){
			$is_active = false;
		}
		return $is_active;
	}
}
