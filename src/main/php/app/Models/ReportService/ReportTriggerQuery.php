<?php

namespace HealthSlateAdmin\Models\ReportService;

use Illuminate\Database\Eloquent\Model;

class ReportTriggerQuery extends Model {

	protected $connection = 'report_service';
	protected $table = 'report_trigger_query';
	protected $primaryKey = 'report_trigger_query_id';
	public $timestamps = false;
}
