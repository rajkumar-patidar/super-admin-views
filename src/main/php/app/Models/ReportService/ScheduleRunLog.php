<?php

namespace HealthSlateAdmin\Models\ReportService;

use Illuminate\Database\Eloquent\Model;

class ScheduleRunLog extends Model {

	protected $connection = 'report_service';
	protected $table = 'schedule_run_log';
	protected $primaryKey = 'schedule_run_log_id';
	public $timestamps = false;

	protected $fillable = [ 'schedule_id', 'sent_to', 'success_status', 'timestamp' ];

	public function schedule() {
		return $this->belongsTo( 'HealthSlateAdmin\Models\ReportService\Schedule', 'schedule_id', 'schedule_id' );
	}
}
