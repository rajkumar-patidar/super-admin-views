<?php

namespace HealthSlateAdmin\Models\ReportService;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Solera_bulk_import extends Model
{

	protected $connection = 'report_service';
	protected $table = 'bulk_import_log';
	protected $primaryKey = 'bulk_import_log_id';
	public $timestamps = false;

	protected $dates = ['start_date', 'end_date'];


}
