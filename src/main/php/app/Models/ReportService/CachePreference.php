<?php

namespace HealthSlateAdmin\Models\ReportService;

use Illuminate\Database\Eloquent\Model;

class CachePreference extends Model {

	protected $connection = 'report_service';
	protected $table = 'cache_preference';
	protected $primaryKey = 'cache_preference_id';
	public $timestamps = false;

}
