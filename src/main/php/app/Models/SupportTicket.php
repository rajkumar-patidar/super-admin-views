<?php

namespace HealthSlateAdmin\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class SupportTicket extends Model {
	protected $table = 'support_ticket';
	protected $primaryKey = 'support_ticket_id';
	public $timestamps = false;
	protected $dates = ['date_opened','date_closed'];

	public function getIsActiveAttribute() {
		return empty( $this->date_closed );
	}

	public function scopeStatus( $query, $open = true ) {
		if($open){
			return $query->whereNull( 'date_closed' );
		}else{
			return $query->whereNotNull( 'date_closed' );
		}
	}

	public function techSupportMessage() {
		return $this->hasOne( 'HealthSlateAdmin\Models\TechSupportMessage', 'message_id', 'tech_support_message_id' );
	}
}
