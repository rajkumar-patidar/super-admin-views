<?php
/**
 * Created by PhpStorm.
 * User: spatel
 * Date: 19/7/16
 */

namespace HealthSlateAdmin\Models;

use HealthSlateAdmin\Helpers\RequestHelper;

class PatientAppServiceProvider {

	/**
	 * @var RequestHelper
	 */
	protected $requestHelper;

	/**
	 * @param RequestHelper $requestHelper
	 */
	public function __construct( RequestHelper $requestHelper ) {
		$this->requestHelper = $requestHelper;
	}

	/**
	 * @param $data
	 *
	 * @return bool|object
	 */
	public function enrollPatients($data){
		$url = config( 'rest.POST_bulk_patient_sign_up' );

		$response = $this->requestHelper->makeRequest( 'POST', $url, $data);

		if ( $response->statusCode === 201 ) {
			// Success !
			return true;
		} else {
			info('Response Body: ', (array) $response->body);
			return (object) [ 'errorDetail' => trans( 'common.internal_server_error' ) ];
		}
	}

    public function sendMessagesToUser($data){
        $url = config( 'rest.post_send_messages_to_patient' );

        $response = $this->requestHelper->makeRequest( 'POST', $url, $data);

        if ( $response->statusCode === 201 ) {
            // Success !
            return true;
        } else {
            info('Response Body: ', (array) $response->body);
            return (object) [ 'errorDetail' => trans( 'common.internal_server_error' ) ];
        }
    }

}