<?php
/**
 * User: spatel
 * Date: 4/1/16
 */

namespace HealthSlateAdmin\Models;

use HealthSlateAdmin\Helpers\RequestHelper;
use Cookie;
use Log;


class AuthServiceProvider {

	/**
	 * @var RequestHelper
	 */
	protected $requestHelper;

	/**
	 * @param RequestHelper $requestHelper
	 */
	public function __construct( RequestHelper $requestHelper ) {
		$this->requestHelper = $requestHelper;
	}

	/**
	 * @param $username
	 * @param $password
	 *
	 * @return object
	 */
	public function getAccessToken( $username, $password ) {
		$url = config( 'rest.POST_get_auth_token' );

		$response = $this->requestHelper->makeRequest( 'POST', $url, [
			'username' => $username,
			'password' => $password
		] );

		if ( $response->statusCode === 401 ) {
			// Bad credentials
			return $response->body;
		} else if ( $response->statusCode === 200 && empty( $response->body->DATA ) ) {
			// Invalid username/password
			$response->body->errorDetail = !empty($response->body->REASON) ? $response->body->REASON : trans( 'auth.failed' );

			return $response->body;
		} else if ( $response->statusCode === 200 && ! empty( $response->body->DATA ) && ! empty( $response->body->DATA->TOKEN_INFO ) && ! empty( $response->body->DATA->USER_ROLES ) ) {
			if ( !$role_name = $this->hasUserRole($response->body->DATA->USER_ROLES) ) {
				return (object) [ 'errorDetail' => trans( 'auth.invalid_user_role' ) ];
			}

			$response->body->DATA->TOKEN_INFO->userRole = $role_name;
			$response->body->DATA->TOKEN_INFO->now = time();

			return $response->body->DATA->TOKEN_INFO;
		} else {
			//Log::error( $response->getBody() );
		}

		return (object) [ 'errorDetail' => trans( 'common.internal_server_error' ) ];

	}

	/**
	 * @param $user
	 *
	 * @return bool
	 */
	public function isInvalidToken( $user ) {
		if(empty($user->accessToken)){
			return true;
		}
		$url      = config( 'rest.GET_validate_token' );
		$response = $this->requestHelper->makeRequest( 'GET', $url, [ ], [
			'Authorization' => 'Bearer ' . $user->accessToken
		] );
                
		if ( $response->statusCode === 200 ) {
			return false;
		}

		return true;
	}

	/**
	 * @param $user
	 *
	 * @return bool
	 */
	public function refreshToken( $user ) {
		if(empty($user->refreshToken)){
			return false;
		}
		$url      = config( 'rest.GET_get_refresh_token' );
		$response = $this->requestHelper->makeRequest( 'GET', $url, [
			'refreshToken' => $user->refreshToken
		] );

		if ( $response->statusCode === 200 && ! empty( $response->body->DATA ) && ! empty( $response->body->DATA->TOKEN_INFO ) ) {

			$user->accessToken  = $response->body->DATA->TOKEN_INFO->accessToken;
			$user->refreshToken = $response->body->DATA->TOKEN_INFO->refreshToken;
			$user->expiresIn    = $response->body->DATA->TOKEN_INFO->expiresIn;
			$user->now          = time();
			session( [ 'user' => $user] );
                        Cookie::queue(Cookie::make('access_token', $user->accessToken, $user->expiresIn));
                        Cookie::queue(Cookie::make('expires_in', $user->expiresIn, $user->expiresIn));
			Log::info( "User token refreshed successfully" );

			return true;
		}

		// Token Not refreshed
		return false;
	}

	/**
	 * @param $accessToken
	 *
	 * @return bool
	 */
	public function getUserInfoByToken( $accessToken ) {
		$url = config( 'rest.GET_user_info_by_token' );

		$response = $this->requestHelper->makeRequest( 'GET', $url, [ ], [
			'Authorization' => 'Bearer ' . $accessToken
		] );

		if ( $response->statusCode === 200 && ! empty( $response->body ) ) {
			return ( $response->body );
		}

		return false;
	}

	/**
	 * get Admin user TokenInfo object by user token
	 * @param $accessToken
	 * @param null $refreshToken
	 * @param int $expiresIn
	 *
	 * @return bool|object
	 */
	public function getAdminUserByToken( $accessToken, $refreshToken = null, $expiresIn = -1 ) {

		$userInfo = $this->getUserInfoByToken( $accessToken );

		if ( $userInfo && ! empty( $userInfo->userRole ) && ! empty( $userInfo->user ) ) {
			// Allowing login to ROLE_PROVIDER too.
			// Here order of $allowed_roles should be Super admin to provider
			// so that highest possible permission is picked
			$role_name = $this->hasUserRole($userInfo->userRole, [ 'ROLE_SUPER_ADMIN', 'ROLE_PROVIDER', 'ROLE_ADMIN', 'ROLE_FACILITY_ADMIN' ]);
			if ( $role_name ) {
                            
				$token = [
					"accessToken"  => $accessToken,
					"refreshToken" => $refreshToken,
					"firstName"    => @$userInfo->user->firstName,
					"lastName"     => @$userInfo->user->lastName,
					"userName"     => @$userInfo->user->email,
					"userRole"     => $role_name,
					"expiresIn"    => intval($expiresIn), //    -1 will force Refresh token call just after this login.
					"now"          => time()
				];

				return (object) $token;
			} else {
				return (object) [ 'errorDetail' => trans( 'auth.invalid_user' ) ];
			}
		}

		return false;
	}

	/**
	 * hasUserRole
	 * @param $userRole
	 * @param array $allowed_roles
	 *
	 * @return bool|string
	 */
	private function hasUserRole( $userRole, $allowed_roles = [ 'ROLE_SUPER_ADMIN' ] ) {
		// Here order of $allowed_roles should be Super admin to provider
		// so that highest possible permission is picked
		$userRole = collect( $userRole );
		foreach($allowed_roles as $role){
			$filtered = $userRole->where( 'authority', $role );
			if ( $filtered->count() ) {
				return $role;
			}
		}
		return false;
	}
}