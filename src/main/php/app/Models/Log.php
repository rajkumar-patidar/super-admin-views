<?php

namespace HealthSlateAdmin\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Log extends Model {

	protected $table = 'log';
	protected $primaryKey = 'log_id';
	public $timestamps = false;

	protected $appends = [ 'readable_log_time','log_date_only','log_time_only'];

	public function getReadableLogTimeAttribute() {
		if ( empty( $this->log_time ) ) {
			return null;
		}
		return Carbon::createFromTimestamp( $this->log_time / 1000 , !empty($this->offset_key) ? $this->offset_key: null)->format('l, m/d');
	}

	public function getLogDateOnlyAttribute() {
		if ( empty( $this->log_time ) ) {
			return null;
		}
		return Carbon::createFromTimestamp( $this->log_time / 1000, !empty($this->offset_key) ? $this->offset_key: null )->format('d/m/Y');
	}

	public function getLogTimeOnlyAttribute() {
		if ( empty( $this->log_time ) ) {
			return null;
		}
		return Carbon::createFromTimestamp( $this->log_time / 1000, !empty($this->offset_key) ? $this->offset_key: null )->format('h:i A');
	}

	public function getIsProcessedAttribute($is_processed) {
		return  bit_to_int( $is_processed ) ;
	}

	/**
	 *  Scope a query to only include recent week logs
	 *
	 * @param $query
	 * @param int $week
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeRecentWeeks( $query , $week = 1) {
		return $query->where( 'log_time', '>', Carbon::now()->subWeek($week)->timestamp*1000 );
	}

	public function scopeLogsBetween( $query , $start, $end) {
		$start = Carbon::createFromFormat('m/d/Y',$start)->timestamp*1000;
		$end = Carbon::createFromFormat('m/d/Y',$end)->timestamp*1000;
		return $query->whereBetween( 'log_time', [$start, $end] );
	}

	public function logMoods() {
		return $this->hasMany( 'HealthSlateAdmin\Models\LogMood', 'log_id', 'log_id' );
	}

	public function glucoseLog() {
		return $this->hasOne( 'HealthSlateAdmin\Models\GlucoseLog', 'log_id', 'log_id' );
	}

	public function activityLog() {
		return $this->hasOne( 'HealthSlateAdmin\Models\ActivityLog', 'log_id', 'log_id' );
	}

	public function exerciseLogs() {
		return $this->hasMany( 'HealthSlateAdmin\Models\ExerciseLog', 'log_id', 'log_id' );
	}

	public function reportErrors() {
		return $this->hasMany( 'HealthSlateAdmin\Models\ReportError', 'log_id', 'log_id' );
	}

	public function medicationLog() {
		return $this->hasOne( 'HealthSlateAdmin\Models\MedicationLog', 'log_id', 'log_id' );
	}

	public function foodLogSummary() {
		return $this->hasOne( 'HealthSlateAdmin\Models\FoodLogSummary', 'log_id', 'log_id' );
	}

	public function miscLog() {
		return $this->hasOne( 'HealthSlateAdmin\Models\MiscLog', 'log_id', 'log_id' );
	}

	public function notes() {
		return $this->hasMany( 'HealthSlateAdmin\Models\Note', 'log_id', 'log_id' );
	}

	public function device() {
		return $this->hasOne( 'HealthSlateAdmin\Models\Device', 'device_mac_address', 'device_mac_address' )->whereNotNull( 'device_mac_address' );
	}
}
