<?php

namespace HealthSlateAdmin\Exceptions;

use Exception;
use Log;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;


class Handler extends ExceptionHandler {
	/**
	 * A list of the exception types that should not be reported.
	 *
	 * @var array
	 */
	protected $dontReport = [
		HttpException::class,
		ModelNotFoundException::class,
		TokenMismatchException::class,
	];

	/**
	 * Report or log an exception.
	 *
	 * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
	 *
	 * @param  \Exception $e
	 *
	 * @return void
	 */
	public function report( Exception $e ) {
		// As HTTP Exception are not reported, lets log simple message for them
		if ( $e instanceof HttpException ) {
			if ( $e instanceof MethodNotAllowedHttpException ) {
				Log::error( 'Requested method not allowed' );
			} else {
				Log::error( 'HttpException : ' . $e->getStatusCode() );
			}
		} else if ( $e instanceof TokenMismatchException ) {
			Log::error( 'CSRF TokenMismatchException' );
		}

		return parent::report( $e );
	}

	/**
	 * Render an exception into an HTTP response.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Exception $e
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function render( $request, Exception $e ) {
		if ( $e instanceof ModelNotFoundException ) {
			$e = new NotFoundHttpException( $e->getMessage(), $e );
		}

		// When user was ideal for long time on login page, don't show error page, instead redirect to login
		if($request->is('login') && $e instanceof TokenMismatchException){
			Log::error('CSRF TokenMismatchException from login page, path: '.$request->path());
			return redirect('/');
		}
		if ($this->isHttpException($e)) {
			return response()->view("errors.503", ['page_title' => $e->getStatusCode(), 'code' => $e->getStatusCode()], $e->getStatusCode());
		}
		return parent::render( $request, $e );
	}
}
