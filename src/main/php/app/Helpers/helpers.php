<?php
/**
 * User: spatel
 * Date: 24/2/16
 * Time: 12:35 PM
 */

if (! function_exists('bit_to_int')) {

	function bit_to_int($bit){
		if(is_null($bit)){
			return 0;
		}
		if(is_int($bit)){
			return $bit;
		}
		return ord($bit);
	}
}

if (! function_exists('format_weight')) {
	/**
	 * format_weight
	 *
	 * @param $weight
	 * @param int $decimal
	 *
	 * @return string
	 */
	function format_weight( $weight, $decimal = 2 ) {
		return number_format( (float) $weight, $decimal, '.', '' );
	}
}
if( ! function_exists('get_current_group_week')){

	/**
	 * @param \Carbon\Carbon $date_created
	 * @param \Carbon\Carbon $now
	 *
	 * @return object
	 */
	function get_group_current_week( \Carbon\Carbon $date_created, \Carbon\Carbon $now = null ) {
		if ( empty( $now ) ) {
			$now = \Carbon\Carbon::now();
		}
		$date = $date_created->copy()->startOfDay();
		// week start from next sunday of group creation date
		// so if timestamp is not sunday, jump to next sunday
		if ( ! $date->isSunday() ) {
			$date = $date->next( \Carbon\Carbon::SUNDAY );
		}
		info( 'Calculating group week number date_created: ' . $date_created . ' Sunday: ' . $date . ' now: ' . $now );
		$completed_week = $date->diffInDays( $now, false ) / \Carbon\Carbon::DAYS_PER_WEEK;
		if ( $completed_week < 0 ) {
			// Week not yet started
			$completed_week = 0;
		}
		info( 'Completed weeks are: ' . $completed_week );
		$current_week = (int) $completed_week + 1;

		return (object) [ 'current_week' => $current_week, 'completed_week' => (int) $completed_week, 'group_start_on' => $date ];
	}
}


if( ! function_exists('get_timezone_list')){

    /**
     * @param
     * @return timezone array
     */
    function get_timezone_list()
    {
        return $timezones = array(
            '(GMT-10:00) Hawaii' => 'Pacific/Honolulu',
            '(GMT-08:00) Pacific Time (US & Canada)' => 'America/Los_Angeles',
            '(GMT-07:00) Mountain Time (US & Canada)' => 'US/Mountain',
            '(GMT-07:00) Arizona' => 'US/Arizona',
            '(GMT-06:00) Central Time (US & Canada)' => 'US/Central',
            '(GMT-05:00) Eastern Time (US & Canada)' => 'US/Eastern',
            '(GMT+00:00) UTC' => 'UTC',
            '(GMT+05:30) New Delhi' => 'Asia/Calcutta',
        );
    }
}