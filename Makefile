PROJECT_NAME ?= 'hs-super-admin'
PROJECT_DESC ?= 'Health Slate Super Admin'
BUILD_NUMBER ?= 0
ITERATION ?= $(shell git rev-parse --short HEAD)
PROJECT_VERSION ?= 1.1.0
VERSION = ${PROJECT_VERSION}.${BUILD_NUMBER}
EPOCH=$(shell date +%s)

# By Default it should create rpm file.
all: deps version rpm

# rpm file generation
.PHONY: rpm
rpm: deps version
	fpm -f -s dir -t rpm -n ${PROJECT_NAME} -v ${VERSION} --iteration ${ITERATION} \
		--template-scripts --after-install dist/after-install.sh -a all \
		--description ${PROJECT_DESC} \
		--prefix /var/www/${PROJECT_NAME} \
		--depends 'httpd24 >= 2.4' \
		--depends 'php56 >= 5.6' \
		--depends 'php56-cli >= 5.6' \
		--depends 'php56-gd >= 5.6' \
		--depends 'php56-mbstring >= 5.6' \
		--depends 'php56-mcrypt >= 5.6' \
		--depends 'php56-mysqlnd >= 5.6' \
		--depends 'php56-pdo >= 5.6' \
		--depends 'php56-xml >= 5.6' \
		--exclude node_modules \
		--epoch ${EPOCH} \
		-C src/main/php/ -p ./dist/ ./

.PHONY: clean
clean:
	rm -f ./dist/${PROJECT_NAME}*.rpm

# execute dependency
.PHONY: deps
deps:
	composer --working-dir=src/main/php --no-dev --optimize-autoloader --no-interaction install
	npm install --prefix=src/main/php --loglevel warn
	bower --config.interaction=false --config.cwd=src/main/php --config.directory=resources/assets/vendor --allow-root install
	src/main/php/node_modules/.bin/gulp --cwd src/main/php --production

# create version file
.PHONY: version
version:
	echo ${VERSION}-${ITERATION} > src/main/php/public/VERSION
